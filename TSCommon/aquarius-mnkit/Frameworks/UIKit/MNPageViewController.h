//
//  MNPageViewController.h
//  MindNodeTouch
//
//  Created by Markus Müller on 24.03.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MNPageViewController : UIViewController 

@property (nonatomic, retain) NSArray *viewControllers;
@property (nonatomic, retain) UIPageControl *pageControl;

- (id)initWithViewControllers:(NSArray *)viewControllers;
- (IBAction)changePage:(id)sender;
- (void) changePageToIndex: (NSInteger) index;
@end
