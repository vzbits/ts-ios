//
//  RootViewController.m
//  MNKitCatalogue
//
//  Created by Markus Müller on 01.06.11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MNKOverviewTableViewController.h"

#import "MNTableViewCell.h"

// Stepper
#import "MNStepper.h"
#import "MNStepperTableViewCell.h"

// AttributedLabel
#import "MNAttributedLabel.h"
#import "MNAttributedLabelTableViewCell.h"
#import "NSAttributedString+Convenience.h"
#import "NSMutableAttributedString+Convenience.h"
#import "MNCoreText.h"
#import <CoreText/CoreText.h>


// Color
#import "MNColorSelectionViewController.h"
#import "MNPageViewController.h"
#import "MNColorTableViewCell.h"
#import "MNColorView.h"

enum {
    MNKOverviewStepperSection = 0,
    MNKOverviewColorSection,
    MNKOverviewAttributedTextSection,
};


@interface MNKOverviewTableViewController () <MNColorSelectionViewControllerDelegate>

- (UITableViewCell *)attributedCellForRowIndex:(NSUInteger)rowIndex;
- (UITableViewCell *)stepperCellForRowIndex:(NSUInteger)rowIndex;
- (UITableViewCell *)colorCellForRowIndex:(NSUInteger)rowIndex;

@end

@implementation MNKOverviewTableViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (!self) return nil;
    
    self.title = @"MNKit Catalogue";
    
    return self;
}


- (void)dealloc
{
    [super dealloc];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case MNKOverviewAttributedTextSection:
            return 1;
        case MNKOverviewStepperSection:
            return 1;
        case MNKOverviewColorSection: 
            return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case MNKOverviewAttributedTextSection:
            return [self attributedCellForRowIndex:indexPath.row];
        case MNKOverviewStepperSection:
            return [self stepperCellForRowIndex:indexPath.row];
        case MNKOverviewColorSection: 
            return [self colorCellForRowIndex:indexPath.row];
    }
    return nil;
}

- (UITableViewCell *)attributedCellForRowIndex:(NSUInteger)rowIndex 
{
    MNAttributedLabelTableViewCell *cell = [MNAttributedLabelTableViewCell cellForTableView:self.tableView];
    
    NSMutableAttributedString *attString;
    UIFont *uiFont = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    attString = [NSMutableAttributedString mn_mutableAttributedStringWithString:@"All along the watchtower."
                                                                           font:uiFont
                                                                          color:[UIColor darkGrayColor]
                                                                      alignment:kCTCenterTextAlignment];
    CTFontRef ctFont = MNCTFontCreateFromUIFont(uiFont);
    CTFontRef boldFont = MNCTFontCreateBoldFont(ctFont, YES);
    CFRelease(ctFont);
    [attString addAttribute:(NSString *)kCTFontAttributeName value:(id)boldFont range:NSMakeRange(4, 5)];
    CFRelease(boldFont);
    
    cell.attributedLabel.attributedText = attString;
    
    return cell;
}

- (UITableViewCell *)stepperCellForRowIndex:(NSUInteger)rowIndex 
{
    MNStepperTableViewCell *cell = [MNStepperTableViewCell cellForTableView:self.tableView];
    cell.stepper.minValue = 0;
    cell.stepper.maxValue = 1000;
    cell.stepper.value = 100;
    
    cell.textLabel.text = @"Stepper";
    
    return cell;
}

- (UITableViewCell *)colorCellForRowIndex:(NSUInteger)rowIndex
{
    MNColorTableViewCell *cell = [MNColorTableViewCell cellForTableView:self.tableView];
    cell.textLabel.text = NSLocalizedStringFromTable(@"Color", @"inspector", @"color cell text field");
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.colorView.color = [UIColor redColor];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case MNKOverviewAttributedTextSection:
            return @"MNAttributedLabel";
        case MNKOverviewStepperSection:
            return @"MNStepper";
        case MNKOverviewColorSection: 
            return @"MNColorSelectionViewController";
    }
    return nil;
}


#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MNKOverviewColorSection) {
        NSString *viewControllerTitle = @"Color";
        NSMutableArray *viewControllers = [NSMutableArray arrayWithCapacity:2];
        MNColorSelectionViewController *rainbowViewController = [MNColorSelectionViewController rainbowSelectionViewController];
        rainbowViewController.title = viewControllerTitle;
        if (rainbowViewController) [viewControllers addObject:rainbowViewController];
        rainbowViewController.delegate = self;
        
        MNColorSelectionViewController *monochromeViewController = [MNColorSelectionViewController monoChromeSelectionViewController];
        monochromeViewController.title = viewControllerTitle;
        if (monochromeViewController) [viewControllers addObject:monochromeViewController];
        monochromeViewController.delegate = self;
        
        // Navigation logic may go here. Create and push another view controller.
        MNPageViewController *pageViewController = [[MNPageViewController alloc] initWithViewControllers:viewControllers];
        
        [self.navigationController pushViewController:pageViewController animated:YES];
        [pageViewController release];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MNKOverviewStepperSection) return 90;
    return self.tableView.rowHeight;
}


#pragma mark - MNColorSelectionViewController Delegate

- (UIColor *)colorSelectionControllerSelectedColor:(MNColorSelectionViewController *)controller
{
    MNColorTableViewCell *cell = (MNColorTableViewCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:MNKOverviewColorSection]];
    return cell.colorView.color;
}
- (void)colorSelectionController:(MNColorSelectionViewController *)controller didSelectedColor:(UIColor *)color
{
    MNColorTableViewCell *cell = (MNColorTableViewCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:MNKOverviewColorSection]];
    cell.colorView.color = color;
}


@end
