(function () {
    readStyle = 'style-newspaper';
    readSize = 'size-medium';
    readMargin = 'margin-narrow';

    _readability_css = document.createElement('LINK');
    _readability_css.rel = 'stylesheet';
    _readability_css.href = 'https://dl.dropboxusercontent.com/u/10877172/readability.css';
    _readability_css.type = 'text/css';
    _readability_css.media = 'screen';
    document.getElementsByTagName('head')[0].appendChild(_readability_css);

    _readability_script = document.createElement('SCRIPT');
    _readability_script.type = 'text/javascript';
    _readability_script.src = 'https://arc90labs-readability.googlecode.com/svn/tags/qa-releases/1.5.0-1/js/readability.js?x=' + (Math.random());
    document.getElementsByTagName('head')[0].appendChild(_readability_script);
 
})();