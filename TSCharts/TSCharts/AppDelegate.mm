//
//  AppDelegate.m
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
//#import "DDLog.h"
//#import "DDTTYLogger.h"
//#import "JASidePanelController.h"
#import "LeftMenuViewController.h"
//static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#import "ECSlidingViewController.h"
#import "Orbiter.h"

static NSString * AFNormalizedDeviceTokenStringWithDeviceToken(id deviceToken) {
    if ([deviceToken isKindOfClass:[NSData class]]) {
        const unsigned *bytes = (unsigned *)[(NSData *)deviceToken bytes];
        return [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x", ntohl(bytes[0]), ntohl(bytes[1]), ntohl(bytes[2]), ntohl(bytes[3]), ntohl(bytes[4]), ntohl(bytes[5]), ntohl(bytes[6]), ntohl(bytes[7])];
    } else {
        return [[[[deviceToken description] uppercaseString] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
}

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (void) lookAndFeel
{
    // Create resizable images
//    UIImage *gradientImage44 = [[UIImage imageNamed:@"header"] 
//                                resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
//    
//    // Set the background image for *all* UINavigationBars
//    [[UINavigationBar appearance] setBackgroundImage:gradientImage44 
//                                       forBarMetrics:UIBarMetricsDefault];
    //[[UINavigationBar appearance] setTintColor: [UIColor colorWithRGBHex:0x404d6c] ];
    // Customize the title text for *all* UINavigationBars
//    [[UINavigationBar appearance] setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      [UIColor colorWithRGBHex:0x404d6c] /*#34363d*/, 
//      UITextAttributeTextColor, 
//      [UIColor colorWithRed:1 green:1 blue:1 alpha:0.8], 
//      UITextAttributeTextShadowColor, 
//      [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], 
//      UITextAttributeTextShadowOffset, 
//      [UIFont fontWithName:@"Helvetica-Neue" size:0.0], 
//      UITextAttributeFont, 
//      nil]];
    
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
	 (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
	
//	[DDLog addLogger:[DDTTYLogger sharedInstance]];
//	[[DDTTYLogger sharedInstance] setColorsEnabled:YES];
//	[[DDTTYLogger sharedInstance] setForegroundColor: [UIColor blueColor] backgroundColor:nil forFlag:LOG_FLAG_VERBOSE];
//	[[DDTTYLogger sharedInstance] setForegroundColor: [UIColor purpleColor] backgroundColor:nil forFlag:LOG_FLAG_INFO];
//
//	DDLogInfo(@"Application launched.");
	
    //[self lookAndFeel];

	[[UIApplication sharedApplication] setStatusBarHidden: NO withAnimation:UIStatusBarAnimationNone];
	[[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];

    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
//	self.window.backgroundColor = [UIColor colorWithRGBHex:0x4D6173];
//	self.window.tintColor = [UIColor colorWithRed:204.0/255.0 green:136.0/255.0 blue:153.0/255.0 alpha:1.0];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

		[[CoreDataStore instance] createDefaultList];

		ECSlidingViewController *viewController = [[ECSlidingViewController alloc] initWithNibName:nil bundle:nil];
		[viewController setAnchorRightRevealAmount:260.0];			//200.0f
		[viewController setUnderLeftWidthLayout: ECFixedRevealWidth];

		ViewController *centerPanel = [[[ViewController alloc] init] autorelease];
	    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:centerPanel];
		[navController setNavigationBarHidden:NO animated:NO];
		
		viewController.topViewController = centerPanel;
		LeftMenuViewController *leftPanel = [[[LeftMenuViewController alloc] init] autorelease];
		viewController.underLeftViewController = leftPanel;
		
		self.viewController = viewController;
		self.centerController = centerPanel;
		
		[leftPanel restoreState];
		
//		[navController setNeedsStatusBarAppearanceUpdate];
//		[centerPanel setNeedsStatusBarAppearanceUpdate];
		
    } else {
        self.viewController = [[[ViewController alloc] init] autorelease];
    }

    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// retry logic
	if([[Settings instance] tokenStatus] == -1)
	{
		NSURL *serverURL = [NSURL URLWithString: [NSString stringWithFormat: @"%@/api",[ServiceManager getServerURL]]];
		Orbiter *orbiter = [[Orbiter alloc] initWithBaseURL:serverURL credential:nil];
		NSString *deviceToken = [[Settings instance] deviceToken];

		[orbiter registerDeviceToken:deviceToken withAlias: nil success:^(id responseObject) {
			NSLog(@"Registration Success: %@", responseObject);
			[[Settings instance] setTokenStatus: 1];
		} failure:^(NSError *error) {
			NSLog(@"Registration Error: %@", error);
			[[Settings instance] setTokenStatus: -1];
		}];
	}
	
	NSLog(@"Entering foreground, reload prices.");
	ViewController *centerController = (ViewController*)CENTER_VIEWCONTROLLER();
	MainViewPhone *mainView = (MainViewPhone*)centerController.mainView;
	[mainView reloadData];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [[CoreDataManager instance] saveContext];
}

- (NSString *)convertDeviceTokenToString: (NSData*) deviceToken
{
	return [NSString stringWithFormat:@"%@",deviceToken];
}

//8CFF180B2228566E594B4CD8E18B2B736FF76AC40212CA69367336D306820CCE
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
	NSLog(@"TOKEN %@", [self convertDeviceTokenToString: deviceToken]);
	NSLog(@"TOKEN2 %@", [deviceToken description] );
	
	[[Settings instance] setDeviceToken: AFNormalizedDeviceTokenStringWithDeviceToken(deviceToken)];
    NSURL *serverURL = [NSURL URLWithString: [NSString stringWithFormat: @"%@/api",[ServiceManager getServerURL]]];
    Orbiter *orbiter = [[Orbiter alloc] initWithBaseURL:serverURL credential:nil];
    [orbiter registerDeviceToken:deviceToken withAlias:nil success:^(id responseObject) {
        NSLog(@"Registration Success: %@", responseObject);
		[[Settings instance] setTokenStatus: 1];
    } failure:^(NSError *error) {
        NSLog(@"Registration Error: %@", error);
		[[Settings instance] setTokenStatus: -1];
    }];
}
@end
