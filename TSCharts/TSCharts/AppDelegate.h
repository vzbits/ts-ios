//
//  AppDelegate.h
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIViewController *viewController;
@property (strong, nonatomic) UIViewController *centerController;

@end


#define ROOT_VIEWCONTROLLER() (UIViewController*)[(AppDelegate*)[[UIApplication sharedApplication] delegate] viewController];
#define CENTER_VIEWCONTROLLER() (UIViewController*)[(AppDelegate*)[[UIApplication sharedApplication] delegate] centerController];