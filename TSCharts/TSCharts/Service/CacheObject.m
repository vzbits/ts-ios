//
//  CacheObject.m
//  TSCharts
//
//  Created by Quoc Le on 7/3/13.
//
//

#import "CacheObject.h"

@implementation CacheObject
@dynamic symbol;
@dynamic data;
@dynamic createDate;
@dynamic category;
@dynamic priceType;
@end
