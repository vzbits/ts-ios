//
//  Alert.h
//  TSCharts
//
//  Created by Quoc Le on 8/17/13.
//
//

#import <CoreData/CoreData.h>

@interface Alert : NSManagedObject
@property (nonatomic, retain) NSString *symbol;
@property (nonatomic, retain) NSNumber *price;
@property (nonatomic, retain) NSNumber *lastPrice;
@property (nonatomic, assign) BOOL dirty;
@property (nonatomic, retain) NSDate *updatedDate;
@end
