//
//  UserListItemHistory.h
//  TSCharts
//
//  Created by Quoc Le on 8/4/13.
//
//

#import <CoreData/CoreData.h>

@class UserListItem;

@interface UserListItemHistory : NSManagedObject
@property (nonatomic, retain) UserListItem *listItem;
@property (nonatomic, retain) NSDecimalNumber *quantity;
@property (nonatomic, retain) NSDecimalNumber *purchasePrice;
@property (nonatomic, retain) NSDate *purchaseDate;

@end
