//
//  UserListItemHistory.m
//  TSCharts
//
//  Created by Quoc Le on 8/4/13.
//
//

#import "UserListItemHistory.h"

@implementation UserListItemHistory
@dynamic  listItem, purchaseDate, purchasePrice, quantity;
@end
