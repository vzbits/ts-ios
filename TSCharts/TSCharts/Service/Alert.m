//
//  Alert.m
//  TSCharts
//
//  Created by Quoc Le on 8/17/13.
//
//

#import "Alert.h"

@implementation Alert
@dynamic dirty, price, symbol, updatedDate, lastPrice;
@end
