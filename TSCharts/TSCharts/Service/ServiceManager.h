//
//  ServiceManager.h
//  TSCharts
//
//  Created by Quoc Le on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "service.h"
//using namespace vsi;
#import "TSocketClient.h"
#import "TBinaryProtocol.h"
#import "CoreDataStore.h"
#import "MWFeedParser.h"

typedef void (^ServiceCompleteBlock)(id data);
typedef void (^ServiceSearchBlock)(SymbolSearchResultData *data);
typedef void (^ServiceIndicatorListBlock)(IndicatorCodeListData *data);
typedef void (^ServiceImageURLBlock)(NSString *data);
typedef void (^ServiceErrorBlock)(NSString* error);

@interface ServiceManager : NSObject
{
}
@property(strong) StockPriceServiceClient *server;
@property(strong) NSOperationQueue *asyncQueue;
@property(strong) NSOperationQueue *mainQueue;
@property(strong) MWFeedParser *feedParser;

// ON THRIFT
- (void)getChartData: (NSString*) symbol
		 periodicity: (int) priceType
           withBlock: (ServiceCompleteBlock) completeBlock
      withErrorBlock:(ServiceErrorBlock) errorBlock;

- (void)getSymbolSearchData: (NSString*) text
           withBlock: (ServiceCompleteBlock) completeBlock
      withErrorBlock:(ServiceErrorBlock) errorBlock;

- (void)getYahooSymbolSearchData: (NSString*) text
				  withBlock: (ServiceCompleteBlock) completeBlock
			 withErrorBlock:(ServiceErrorBlock) errorBlock;

- (void)getInstrumentListDataWithBlock: (ServiceCompleteBlock) completeBlock
                        withErrorBlock:(ServiceErrorBlock) errorBlock;

- (void)getNewsData: (NSString*) symbol
           withBlock: (ServiceCompleteBlock) completeBlock
      withErrorBlock:(ServiceErrorBlock) errorBlock;

- (void)getNewsArticle: (NSString*) url
				 withBlock: (ServiceCompleteBlock) completeBlock
			withErrorBlock:(ServiceErrorBlock) errorBlock;

- (void)getPricesFromYahoo: (NSArray*) symbols
				 withBlock: (ServiceCompleteBlock) completeBlock
			withErrorBlock:(ServiceErrorBlock) errorBlock;

- (void) setAlerts: (NSArray*) alerts
		 withBlock: (ServiceCompleteBlock) completeBlock
	withErrorBlock:(ServiceErrorBlock) errorBlock;

// ON RAILS
+ (void)getImageURLForImage: (UIImage*) image withBlock: (ServiceImageURLBlock) completeBlock
                        withErrorBlock:(ServiceErrorBlock) errorBlock;

+ (NSString*) getServerURL;
+ (NSString*) getScriptPath;

declareSingleton(ServiceManager);

@end
