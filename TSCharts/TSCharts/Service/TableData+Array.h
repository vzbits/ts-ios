//
//  TableData+Array.h
//  TSCharts
//
//  Created by Quoc Le on 6/29/13.
//
//

#import "service.h"

@interface TableData (Array)
- (double) getLastForSymbol: (NSString*) symbol;
- (double) getDoubleForSymbol: (NSString*) symbol Key: (NSString*) key;
- (void) removeObjectAtIndex: (int) index;
- (NSUInteger) count;
- (void)addObject:(id)anObject;
- (void)insertObject:(id)anObject atIndex:(NSUInteger)index;
- (id)objectAtIndex:(NSUInteger)objectIndex;
- (void)replaceObjectAtIndex:(NSUInteger)objectIndex withObject:(id)anObject;
//- (BOOL) isEqual: (id) object;
@end
