//
//  UserList.h
//  TSCharts
//
//  Created by Quoc Le on 7/28/13.
//
//

#import <CoreData/CoreData.h>
#import "ObjectiveRecord.h"
#import "ServiceManager.h"

typedef enum
{
	ListPortfolioCalcType_DayGain,
	ListPortfolioCalcType_DayGainP,
	ListPortfolioCalcType_TotalGain,
	ListPortfolioCalcType_TotalGainP,
	ListPortfolioCalcType_TotalValue
} ListPortfolioCalcType;

typedef enum
{
	ListPriceToggle_Dollar,
	ListPriceToggle_Percentage,
	ListPriceToggle_MKCap
} ListPriceToggleType;

typedef enum
{
	ListViewType_List,
	ListViewType_Portfolio
} ListViewType;

@class UserListItem;

@interface UserList : NSManagedObject
@property (nonatomic, retain) NSString * listName;
@property (nonatomic, retain) NSString * listType;
@property (nonatomic, retain) NSNumber * indexSelected;
@property (nonatomic, retain) NSOrderedSet *listItems;
@property (nonatomic, assign) ListViewType listTypeEnum;

- (void) updateCalculation: (ServiceCompleteBlock) complete;
- (double) getCalculation: (int) type;
@end

@interface UserList (CoreDataGeneratedAccessors)

- (void)addListItemsObject:(UserListItem *)value;
- (void)removeListItemsObject:(UserListItem *)value;
- (void)addListItems:(NSOrderedSet *)values;
- (void)removeListItems:(NSOrderedSet *)values;

@end