//
//  TableData+Array.m
//  TSCharts
//
//  Created by Quoc Le on 6/29/13.
//
//

#import "TableData+Array.h"

@implementation TableData (Array)

- (double) getLastForSymbol: (NSString*) symbol
{
	return [self getDoubleForSymbol: symbol Key: @"Last"];
}

- (double) getDoubleForSymbol: (NSString*) symbol Key: (NSString*) key
{
	NSUInteger index = [self.symbols indexOfObject: symbol];
	if(index != NSNotFound)
	{
		NSDictionary *data = [self objectAtIndex: index];
		NSString *last = data[key];
		return [last doubleValue];
	}
	
	return NSNotFound;
}
- (void)replaceObjectAtIndex:(NSUInteger)objectIndex withObject:(id)anObject
{
	[self removeObjectAtIndex: objectIndex];
	[self insertObject: anObject atIndex: objectIndex];
}

- (void) removeObjectAtIndex: (int) index
{
	NSMutableArray *newSymbols = [NSMutableArray arrayWithArray: self.symbols];
	[newSymbols removeObjectAtIndex: index];

	NSMutableArray *newRows = [NSMutableArray arrayWithArray: self.rowList];
	[newRows removeObjectAtIndex: index];
	
	self.symbols = newSymbols;
	self.rowList = newRows;
}

- (NSUInteger) count
{
	return [self.symbols count];
}

- (id)objectAtIndex:(NSUInteger)objectIndex
{
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	NSArray *row = [self.rowList objectAtIndex: objectIndex];
	
	for (int i=0; i < self.columnNames.count; i++) {
		if(i < row.count)
		{
			NSString *value = [row objectAtIndex: i];
			[dict setObject: value forKey: [self.columnNames objectAtIndex:i]];
		}
	}
	
	[dict setObject: self.symbols[objectIndex] forKey: @"Symbol"];
	
	return dict;
}

- (void)addObject:(id)anObject
{
	[self insertObject: anObject atIndex: self.count];
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index
{
	if([anObject isKindOfClass:[NSDictionary class]])
	{
		NSDictionary *dict = (NSDictionary*) anObject;
		NSMutableArray *newSymbols = [NSMutableArray arrayWithArray: self.symbols];
		NSString *symbol = [dict objectForKey:@"Symbol"];
		if(symbol)
		{
			[newSymbols insertObject: symbol atIndex: index];
			
			NSMutableArray *newRows = [NSMutableArray arrayWithArray: self.rowList];
			NSMutableArray *row = [NSMutableArray array];
			
			for (NSString* key in self.columnNames) {
				NSString* value = [dict objectForKey:key];
				if(value)
				{
					[row addObject: value];
				}
				else
				{
					[row addObject: @""];
				}
			}
			
			[newRows insertObject: row atIndex: index];

			self.symbols = newSymbols;
			self.rowList = newRows;
		}
	}
	else
	{
		NSAssertFalse(@"Should always be dictionary");
	}
}

@end
