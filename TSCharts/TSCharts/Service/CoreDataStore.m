//
//  CoreDataStore.m
//  TSCharts
//
//  Created by Quoc Le on 7/3/13.
//
//

#import "CoreDataStore.h"
#import "CoreDataManager.h"
#import "NSManagedObject+ActiveRecord.h"
#import "UserList.h"
#import "UserListItem.h"
#import "NSArray+Extensions.h"
#import "JSONKit.h"
#import "Settings.h"

@implementation CoreDataStore
implementSingleton(CoreDataStore);

- (id) init
{
	self = [super init];
	if(self)
	{
	}
	
	return self;
}

- (CacheObject*) createCacheObject: (NSString*) symbol data: (NSDictionary*) data
{
	CacheObject *obj = [CacheObject create: @{ @"symbol": symbol, @"data": data }];
	return obj;
}

- (void) createDefaultList
{
	NSArray *list = [UserList where:@"listType == 'list'"];
	if(list == nil || list.count == 0)
	{
		NSDictionary *JSON = @{@"listType" : @"list",
							   @"listName" : @"My List",
							   @"listItems"  : @[ @{@"symbol" : @"AAPL" },
												  @{@"symbol" : @"ARCC" },
												  @{@"symbol" : @"BAC" },
												  @{@"symbol" : @"YELP" },
												  @{@"symbol" : @"CMG" },
												  @{@"symbol" : @"NFLX" }]
							   };
		UserList *newList = [UserList create: JSON];

		NSDictionary *JSON2 = @{@"listType" : @"portfolio",
							   @"listName" : @"Portfolio",
								@"listItems"  : @[ @{@"symbol" : @"AAPL" , @"historyList" : @[@{@"quantity": @100, @"purchasePrice": @120.0f}] },
												  @{@"symbol" : @"BAC" , @"historyList" : @[@{@"quantity": @100, @"purchasePrice": @3.0}]},
												  @{@"symbol" : @"YELP" , @"historyList" : @[@{@"quantity": @100, @"purchasePrice": @60.0}]},
												  @{@"symbol" : @"CMG", @"historyList" : @[@{@"quantity": @100, @"purchasePrice": @50.0}]},
												  @{@"symbol" : @"NFLX", @"historyList" : @[@{@"quantity": @100, @"purchasePrice": @60.0}]}]
							   };
		UserList *newList2 = [UserList create: JSON2];
		
		if(newList && newList2)
		{
			NSLog(@"Created default list successful");
			[newList save];
			[newList2 save];			
		}
		
		[[Settings instance] setListSelected: [NSIndexPath indexPathForRow:0 inSection:1 ]];
	}
}

// alert manager
/////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSArray*) allAlerts
{
	return [Alert all];
}

- (void) buildAlertMapping
{
	NSArray *alerts = [Alert all];
	self.alertsBySymbol = [alerts groupBy:^id(Alert *obj) {
		return obj.symbol;
	}];
	
	//NSLog(@"BuiltAlertMapping Keys: %d", self.alertsBySymbol.count);
}

- (NSArray*) alertsBySymbol: (NSString*) symbol
{
	//return [[self allAlerts] filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"self.symbol == %@", symbol]];
	if(self.alertsBySymbol == nil)
	{
		[self buildAlertMapping];
	}
	return [self.alertsBySymbol objectForKey: symbol];
}

- (void) setAlertForSymbol: (NSString*) symbol price: (double) price lastPrice: (double) lastPrice currentAlert: (Alert*)alert;
{
	if(alert == nil)
	{
		[Alert create:@{ @"symbol" : symbol, @"price" : [NSNumber numberWithDouble:price], @"dirty" : @NO ,
						 @"lastPrice" : [NSNumber numberWithDouble: lastPrice],
						 @"updatedDate" : [NSDate date]
						 }];
//		[[CoreDataManager instance] saveContext];
		[self buildAlertMapping];
		[[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_ALERT_CHANGED object: symbol];
	}
	else
	{
		NSLog(@"Update alert %@ %f", symbol, price);
//		alert.price = [NSNumber numberWithDouble:price];
//		alert.updatedDate = [NSDate date];
		[alert update:@{ @"price" : [NSNumber numberWithDouble:price],
						 @"lastPrice" : [NSNumber numberWithDouble: lastPrice],
						 @"updatedDate" : [NSDate date]}];
		[[CoreDataManager instance] saveContext];
		[self buildAlertMapping];
	}
	needToSync = YES;
	[self syncIfNecessary];
}

- (void) removeAlert: (Alert*) alert
{
	NSString *symbol = [[alert.symbol copy] autorelease];
	[alert delete];
//	[[CoreDataManager instance] saveContext];
	[self buildAlertMapping];
	[[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_ALERT_CHANGED object: symbol];
	needToSync = YES;
	[self syncIfNecessary];
}

- (void) syncIfNecessary
{
	NSArray *alerts = [Alert all];
	NSMutableArray *jsonObjects = [NSMutableArray array];
	for(Alert *alert in alerts)
	{
		NSString *gid = [[[alert objectID] URIRepresentation] absoluteString];
		[jsonObjects addObject:@{ @"price" : alert.price, @"symbol": alert.symbol, @"gid" : gid, @"last_price" : alert.lastPrice}];
	}
	NSString *deviceID = [[Settings instance] deviceToken]; //[[[UIDevice currentDevice] identifierForVendor] UUIDString];
	NSString *json = [jsonObjects JSONString];
	NSLog(@"JSON %@", json);

	NSDictionary *payload = @{ @"device_id" : deviceID , @"alerts" : jsonObjects };
	
	NSString *url = [NSString stringWithFormat:@"%@/api/alerts/update.json", [ServiceManager getServerURL]];
	__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL: [NSURL URLWithString: url]];
	[request addRequestHeader:@"Content-Type" value:@"application/json; encoding=utf-8"];
	[request addRequestHeader:@"Accept" value:@"application/json"];
	[request setRequestMethod:@"POST"];
	[request appendPostData: [payload JSONData]];

	[request setCompletionBlock:^{
		NSLog(@"Sync success!");
	}];

	[request startAsynchronous];
}

@end
