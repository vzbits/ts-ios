//
//  ServiceManager.m
//  TSCharts
//
//  Created by Quoc Le on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ServiceManager.h"
#import <ObjectiveRecord/ObjectiveRecord.h>
#import <ObjectiveSugar/ObjectiveSugar.h>
#import "NSString+Extras.h"
#import "TableData+Array.h"
#import "UIDateExtensions.h"
#import "NSDate+Escort.h"
#import "ASIDownloadCache.h"
#import "NSArray+Extensions.h"
#import "ChartUtils.h"
#import "NSDate+Escort.h"
#import "JSONKit.h"

#define USE_LOCAL_RSS YES
/*
 sqlite> select zsymbol from ZCACHEOBJECT;
 BAC
 AAPL
 BAC
 YELP
 C
 ZNGA
 ARCC
 sqlite> select zsymbol, zcreatedate, zcategory from ZCACHEOBJECT;
 BAC|394670015.332745|full_chart
 AAPL|394670292.431307|last_tick
 BAC|394670292.435285|last_tick
 YELP|394670292.438089|last_tick
 C|394670292.440769|last_tick
 ZNGA|394670292.443392|last_tick
 ARCC|394670292.446265|last_tick
 sqlite> 

*/


BOOL USE_YAHOO = YES;

//static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@implementation ServiceManager

implementSingleton(ServiceManager);

- (id) init
{
    if(self = [super init])
    {
        self.asyncQueue = [[[NSOperationQueue alloc] init] autorelease];
        [self.asyncQueue setMaxConcurrentOperationCount: 1];
        self.mainQueue  = [NSOperationQueue mainQueue];
		
		[ASIHTTPRequest setDefaultCache:[ASIDownloadCache sharedCache]];
		
        //[self connect];
    }
    return self;
}

- (void) connect
{
    NSLog(@"ServiceManager:: Connecting.. ");
    @try
    {
        TSocketClient *transport = [[[TSocketClient alloc] initWithHostname:@"topstock.us" port:8080] autorelease];
        TBinaryProtocol *protocol = [[[TBinaryProtocol alloc] initWithTransport:transport strictRead:YES strictWrite:YES] autorelease];
        self.server = [[StockPriceServiceClient alloc] initWithProtocol:protocol];
    }
    @catch (TException *e)
    {
        NSLog(@"Error1 %@ %@", e.description, e.reason);
    }
    @catch(NSException *e)
    {
        NSLog(@"Error2 %@ %@", e.description, e.reason);
    }
    
    [NSThread sleepForTimeInterval:0.200];    
}
+ (NSString*) getServerURL
{
    //return @"http://192.168.1.135:3000";
	return @"http://www.topstock.us";
}

+ (NSString*) getScriptPath
{
    return @"/assets/charting/vsi.indicator.js";
}

- (void)commonServiceCall: (SEL) selector
               withObject: (NSObject*) obj
               withObject: (NSObject*) obj2
                withBlock: (ServiceCompleteBlock) completeBlock
           withErrorBlock:(ServiceErrorBlock) errorBlock
                 attempts: (NSInteger) attempts
{
    
	NSMethodSignature *msig = [self.server methodSignatureForSelector:selector];

    NSLog(@"ServiceManager::Calling service %@ %x args=%d", [obj description], (unsigned int)self.server, [msig numberOfArguments]);
    
    [self.asyncQueue addOperationWithBlock:^(void) {
        @try
        {
			if([msig numberOfArguments] - 2  <= 1)
			{
				id data = [self.server performSelector: selector withObject: obj];
				[self.mainQueue addOperationWithBlock:^(void) {
					completeBlock(data);
				}];
			}
			else if([msig numberOfArguments] - 2 == 2)
			{
				id data = [self.server performSelector: selector withObject: obj withObject: obj2];
				[self.mainQueue addOperationWithBlock:^(void) {
					completeBlock(data);
				}];
			}
        } @catch (TException *e) {
            NSString *errorMsg = e.description;
            NSLog(@"Error %@ %@", errorMsg, e.reason);
            //Error writing to transport output stream
            
            // connection error
            if([errorMsg hasPrefix:@"TTransportException: Error writing to transport output stream"] && attempts > 0)
            {
                [self connect];
                [self commonServiceCall: selector
                             withObject: obj
							 withObject: nil
                              withBlock: completeBlock
                         withErrorBlock: errorBlock
                               attempts: attempts-1];
            }
            
            if(attempts == 0)
            {
                [self.mainQueue addOperationWithBlock:^(void) {
                    errorBlock(@"Unable to connect to server.");
                }];
            }
        }
        @catch(NSException *e)
        {
            NSLog(@"Error2 %@ %@", e.description, e.reason);
            
            if(attempts == 0)
            {
                [self.mainQueue addOperationWithBlock:^(void) {
                    errorBlock(@"Unable to connect to server.");
                }];
            }
        }
    }];
}

- (void)getYahooSymbolSearchData: (NSString*) text
                  withBlock: (ServiceCompleteBlock) completeBlock
             withErrorBlock:(ServiceErrorBlock) errorBlock
{
	NSString *host = @"http://d.yimg.com/autoc.finance.yahoo.com/autoc?query=%@&callback=YAHOO.Finance.SymbolSuggest.ssCallback";
	NSString *url = [NSString stringWithFormat:host, text];

	__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL: [NSURL URLWithString: url]];
	[request setSecondsToCache:5];
	
	NSString *prefix = @"YAHOO.Finance.SymbolSuggest.ssCallback(";
	NSString *suffix = @")";
	
	[request setCompletionBlock:^{
		@try
		{
			NSString *rs = [request responseString];
			NSString *newStr = [rs substringWithRange: NSMakeRange(prefix.length, rs.length - prefix.length - suffix.length)];
			//NSLog(@"newStr %@", newStr);
			
			NSDictionary *json = [newStr objectFromJSONString];
			NSDictionary *resultsetJSON = json[@"ResultSet"];
			NSArray *resultsJSON = resultsetJSON[@"Result"];
			NSMutableArray *results = [NSMutableArray array];
			
			for(NSDictionary *dict in resultsJSON)
			{
				SymbolData *symbol = [[SymbolData alloc] initWithSymbol:dict[@"symbol"] name:dict[@"name"] exchange: dict[@"exchDisp"]];
				[results addObject: symbol];
			}
			
			SymbolSearchResultData *finalOutput = [[[SymbolSearchResultData alloc] initWithResults: results searchString: resultsetJSON[@"Query"]] autorelease];
			completeBlock(finalOutput);
		}
		@catch(NSException *e)
		{
			errorBlock(@"Exception");
		}
	}];
	
	[request startAsynchronous];
}

static NSString * AFPercentEscapedQueryStringPairMemberFromStringWithEncoding(NSString *string, NSStringEncoding encoding) {
    static NSString * const kAFCharactersToBeEscaped = @":/?&=;+!@#$()',*";
    static NSString * const kAFCharactersToLeaveUnescaped = @"[].";
	
	return (__bridge_transfer  NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, (__bridge CFStringRef)kAFCharactersToLeaveUnescaped, (__bridge CFStringRef)kAFCharactersToBeEscaped, CFStringConvertNSStringEncodingToEncoding(encoding));
}


- (void)getYahooChartData: (NSString*) symbol
			  periodicity: (PriceType) priceType
				withBlock: (ServiceCompleteBlock)completeBlock
		   withErrorBlock:(ServiceErrorBlock) errorBlock
{
	int pixelPerTick = 4;
	int ticks = [UIScreen mainScreen].bounds.size.width / pixelPerTick;
	NSDate *startDate = [ChartUtils getStartDateForTick: ticks* 1.30f periodicity: priceType];
	NSDate *endDate = [NSDate date];
	
	NSLog(@"Get nticks  %d startDate = %@", ticks, [startDate description]);
	
	char pChar = 'd';
	if(priceType == PriceType_PT_WEEKLY)
		pChar = 'w';
	if(priceType == PriceType_PT_MONTHLY)
		pChar = 'm';
	//http://ichart.finance.yahoo.com/table.csv?s=AAPL&d=7&e=21&f=2013&g=d&a=8&b=7&c=1984&ignore=.csv
	NSString *host = @"http://ichart.finance.yahoo.com/table.csv?s=";
	NSString *url = [NSString stringWithFormat:@"%@%@&d=%d&e=%d&f=%d&g=%c&a=%d&b=%d&c=%d&ignore=.csv",
					 host,
					 AFPercentEscapedQueryStringPairMemberFromStringWithEncoding(symbol, NSUTF8StringEncoding),
					 [endDate month]-1,
					 [endDate day],
					 [endDate year],
					 pChar,
					 [startDate month]-1,
					 [startDate day],
					 [startDate year]
					 ];
		
	NSLog(@"URL %@", url);
	__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL: [NSURL URLWithString: url]];
	[request setSecondsToCache:5];
		
	[request setCompletionBlock:^{
		NSString *responseString = [request responseString];
		if([request didUseCachedResponse])
		{
			NSLog(@"Cache hit.");
		}
		
		//NSLog(@"response %@", responseString);
		
		NSMutableArray *data = [NSMutableArray array];
		NSArray *csv = [responseString csvRows];
		int64_t date;
		double open, high, low, close, volume, r, adjClose;
		for(int i=0; i < csv.count; i++)
		{
			if(i==0) continue;
			
			NSArray *columnsForRow = csv[i];
			if(columnsForRow.count == 7)
			{
				NSDate *dateObj = [NSDate dateWithFormat: columnsForRow[0] format: @"yyyy-MM-dd"];
				date = [dateObj timeIntervalSince1970] * 1000;
				open = [columnsForRow[1] doubleValue];
				high = [columnsForRow[2] doubleValue];
				low = [columnsForRow[3] doubleValue];
				close = [columnsForRow[4] doubleValue];
				volume = [columnsForRow[5] doubleValue];
				adjClose = [columnsForRow[6] doubleValue];
				r = adjClose/close;
				PriceData *priceData = [[[PriceData alloc] initWithDate:date open: r*open high: r*high low: r*low close: adjClose volume:volume] autorelease];
				[data addObject: priceData];
			}
		}
		
		NSString *exchange = @"";
		StockPriceData *priceData = [[StockPriceData alloc] initWithSymbol: symbol name: @"" exchange: exchange prices: [data reverse] priceType: priceType];
		completeBlock(priceData);
	}];
	
	[request startAsynchronous];
}

- (void)getChartData: (NSString*) symbol
		 periodicity: (PriceType) priceType
		   withBlock: (ServiceCompleteBlock) completeBlock
	  withErrorBlock:(ServiceErrorBlock) errorBlock
{
	CacheObject *unique = [[CacheObject where:@"symbol == '%@' and category == 'last_tick'", symbol] first];
	PriceData *lastTick = nil;
	NSString *name = nil;
	if(unique)
	{
		NSDictionary *dict = unique.data;
		NSLog(@"ServiceManager:: last_tick cache hit! %@", unique.symbol);
		lastTick = [[[PriceData alloc] init] autorelease];
		lastTick.open = [dict[@"Open"] floatValue];
		lastTick.close = [dict[@"Last"] floatValue];
		lastTick.high = [dict[@"High"] floatValue];
		lastTick.low = [dict[@"Low"] floatValue];
		lastTick.volume = [dict[@"Volume"] floatValue];
		name = dict[@"Name"];
		NSDate *tradeDate = [NSDate dateWithFormat: dict[@"TradeDate"] format: @"MM/dd/yyyy"];
		lastTick.date = [tradeDate timeIntervalSince1970] * 1000;
		
		if(lastTick.open == 0 || lastTick.close == 0 || lastTick.high == 0 || lastTick.low == 0)
		{
			lastTick = nil;
		}
	}
	
	NSArray *objects = [CacheObject where:@"symbol == '%@' and category == 'full_chart' and priceType == %d", symbol, priceType];
	StockPriceData *priceData = nil;

	if(objects.count > 0)
	{
		NSLog(@"ServiceManager:: full_chart cache hit %@", symbol);
		CacheObject *unique = objects.first;
		if([unique.data isKindOfClass: [StockPriceData class]])
		{
			StockPriceData *priceDataTmp = unique.data;
			PriceData *last = [priceDataTmp.prices last];
			NSDate *lDate = [NSDate dateFromPB: last.date];
			
			if([lDate isToday])
			{
				priceData = priceDataTmp;
			}
			else
			{
				[unique delete];
			}
		}
	}
	
	ServiceCompleteBlock postProcess = ^(StockPriceData *priceData){
		if(name)
		{
			priceData.name = name;
		}
		
		if(lastTick)
		{
			NSDate *lDate = [NSDate dateFromPB: lastTick.date];
			NSString *lastStr = [lDate  stringWithFormat:  @"MM/dd/yyyy"];
			
			//NSLog(@"ServiceManager::List data before count %d for date %@", priceData.prices.count, [lDate description]);
			
			NSMutableArray *newPricesMutable = [NSMutableArray arrayWithArray:  priceData.prices];
			if(newPricesMutable.count > 0)
			{
				PriceData *last = [newPricesMutable last];
				NSString *cDate = [[NSDate dateFromPB: last.date] stringWithFormat:  @"MM/dd/yyyy"];
				//				NSLog(@"%@ %@", [[NSDate dateFromPB: last.date] description], [lDate description]);
				if([lastStr isEqualToString: cDate])
				{
					[newPricesMutable removeObject: last];
					//					NSLog(@"Price removed %f", last.close);
				}
				[newPricesMutable addObject: lastTick];
			}
			//			NSLog(@"List data after count %d", newPricesMutable.count);
			
			priceData.prices = newPricesMutable;
		}
		
		completeBlock(priceData);
	};
	
	ServiceCompleteBlock handlerBlock = ^(id data){
		StockPriceData *priceData = data;
		
		//// begin cache data
		NSArray *objects = [CacheObject where:@"symbol == '%@' and category == 'full_chart' and priceType == %d", symbol, priceType];
		[objects each:^(id object) {
			[object delete];
		}];
		NSLog(@"ServiceManager:: Storing %@ and deleting %d stales", symbol, objects.count);
		
		[CacheObject create: @{ @"symbol": symbol,
								@"data": priceData,
								@"category": @"full_chart",
								@"priceType" : [NSNumber numberWithInt:priceType],
								@"createDate" : [NSDate date]
								}];
		[[CoreDataManager instance] saveContext];
		//// end cache data
		
		postProcess(priceData);
	};
	
	if(priceData == nil)
	{
		if(!USE_YAHOO)
		{
			[self commonServiceCall: @selector(getPrices::)
						 withObject: symbol
						 withObject: [NSNumber numberWithInt: priceType]
						  withBlock: handlerBlock
					 withErrorBlock:errorBlock
						   attempts:1];
		}
		else
		{
			[self getYahooChartData: symbol
						periodicity: priceType
			  withBlock: handlerBlock
			withErrorBlock:errorBlock];
		}
	}
	else
	{
		postProcess(priceData);
	}
}

- (void)getSymbolSearchData: (NSString*) text
                  withBlock: (ServiceCompleteBlock) completeBlock
             withErrorBlock:(ServiceErrorBlock) errorBlock
{
    [self commonServiceCall: @selector(getSymbol:)
                 withObject: text
				 withObject: nil
                  withBlock:completeBlock
             withErrorBlock:errorBlock
                   attempts:2];
}

- (void)getInstrumentListDataWithBlock: (ServiceCompleteBlock) completeBlock
               withErrorBlock:(ServiceErrorBlock) errorBlock
{
    [self commonServiceCall: @selector(getIndicatorList)
                 withObject: nil
				 withObject: nil	 
                  withBlock:completeBlock
             withErrorBlock:errorBlock
                   attempts:2];
}

- (void)getNewsData: (NSString*) symbol withBlock: (ServiceCompleteBlock) completeBlock withErrorBlock:(ServiceErrorBlock) errorBlock
{
	if(USE_LOCAL_RSS)
	{
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
			NSURL *feedURL = [NSURL URLWithString: [NSString stringWithFormat:@"http://finance.yahoo.com/rss/headline?s=%@", symbol]];
			self.feedParser = [[MWFeedParser alloc] initWithFeedURL:feedURL];
			self.feedParser.delegate = self;
			self.feedParser.feedParseType = ParseTypeFull; // Parse feed info and all items
			self.feedParser.connectionType = ConnectionTypeSynchronously;
			self.feedParser.userInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys: [[completeBlock copy] autorelease], @"COMPLETE", [[errorBlock copy] autorelease], @"ERROR", [NSMutableArray array] , @"DATA" , nil];
			[self.feedParser parse];
		});
	}
	else
	{
		[self commonServiceCall: @selector(getNews:)
					 withObject: symbol
					 withObject: nil	 
					  withBlock:completeBlock
				 withErrorBlock:errorBlock
					   attempts:1];
	}
}

- (void)getNewsArticle: (NSString*) url
			 withBlock: (ServiceCompleteBlock) completeBlock
		withErrorBlock:(ServiceErrorBlock) errorBlock;
{
    [self commonServiceCall: @selector(getArticle:)
                 withObject: url
				 withObject: nil
                  withBlock:completeBlock
             withErrorBlock:errorBlock
                   attempts:1];
}
- (void)getPricesFromYahoo: (NSArray*) symbols
				 withBlock: (ServiceCompleteBlock) completeBlock
			withErrorBlock:(ServiceErrorBlock) errorBlock
{
	NSString *symbolStr = AFPercentEscapedQueryStringPairMemberFromStringWithEncoding([symbols componentsJoinedByString:@","], NSUTF8StringEncoding);
	NSString *data = @"snl1c6p2ohgvd1j1pkja2yr";
	NSString *url = [NSString stringWithFormat:@"http://finance.yahoo.com/d/quotes.csv?s=%@&f=%@",symbolStr,
									data];
	
	__block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL: [NSURL URLWithString: url]];
	[request setSecondsToCache:5];
	
	NSArray *columns = @[@"Symbol", @"Name", @"Last", @"Change", @"ChangeP", @"Open",@"High", @"Low", @"Volume", @"TradeDate",@"MarketCap",@"PrevClose", @"52WeekHigh", @"52WeekLow", @"AvgVol", @"Yield", @"P/E"];
	
	[request setCompletionBlock:^{
		NSString *responseString = [request responseString];
		if([request didUseCachedResponse])
		{
			NSLog(@"Cache hit.");
		}
		NSArray *lines = [responseString componentsSeparatedByString: @"\n"];
		TableData *tableData = [[[TableData alloc] init] autorelease];
		NSMutableArray *rowList = [NSMutableArray array];
		NSMutableArray *symbols = [NSMutableArray array];
		
		for(int i=0; i < lines.count; i++)
		{
			NSString *line = lines[i];
			//NSLog(@"line %@", line);
			
			NSArray *csv = [line csvRows];
			if(csv.count == 0)
				continue;
			
			NSArray *columnsForRow = csv[0];
			
			NSMutableArray *row = [NSMutableArray array];
			for(int j=0; j < columnsForRow.count; j++)
			{
				NSString *column = columnsForRow[j];
				//NSLog(@"column %d %@ %@", j , column, columns[j]);

				row[j] = column;
				if(j==0)
				{
					[symbols addObject: column];
				}				
			}
			[rowList addObject: row];
		}
		
		tableData.columnNames = columns;		
		tableData.rowList = rowList;
		tableData.symbols = symbols;
		
		for(int i=0; i < tableData.count; i++)
		{
			NSDictionary *dict = [tableData objectAtIndex: i];
			NSString *symbol = dict[@"Symbol"];
			NSArray *objects = [CacheObject where:@"symbol == '%@' and category == 'last_tick'", symbol];
			//NSLog(@"ServiceManager:: cache found %d", objects.count);
			[objects each:^(id object) {
				[object delete];
			}];
			[CacheObject create: @{ @"symbol": symbol,
									@"data": dict,
									@"category": @"last_tick",
									@"createDate" : [NSDate date]
								  }];
		}
		
        completeBlock(tableData);
	}];

	[request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"Error %@", [error localizedDescription]);
        //errorBlock();
    }];
	
	[request startAsynchronous];
}

+ (void)getImageURLForImage: (UIImage*) image withBlock: (ServiceImageURLBlock) completeBlock
              withErrorBlock:(ServiceErrorBlock) errorBlock
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat: @"%@/api/chart_image/upload.txt",[ServiceManager getServerURL]]];
    __block ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    //[request appendPostData: UIImagePNGRepresentation(image)];
    NSData *data = UIImagePNGRepresentation(image);
    //[request setPostValue: UIImagePNGRepresentation(image) forKey:@"image"];

    NSLog(@"data size %d", data.length);
    
    [request setData: data
        withFileName:@"chart.png"
      andContentType: @"image/png"
              forKey:@"image"];
    
//    [request setRequestMethod:@"PUT"];
    
    [request setCompletionBlock:^{
        NSString *responseString = [request responseString];
        completeBlock(responseString);
    }];
    
    [request setFailedBlock:^{
        NSError *error = [request error];
        NSLog(@"Error %@", [error localizedDescription]);
        //errorBlock();
    }];

    [request startAsynchronous];
}

- (void) setAlerts: (NSArray*) alerts
		 withBlock: (ServiceCompleteBlock) completeBlock
	withErrorBlock:(ServiceErrorBlock) errorBlock
{
	
}

#pragma mark Parser

- (void)feedParserDidStart:(MWFeedParser *)parser {
	NSLog(@"Started Parsing: %@", parser.url);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
	NSLog(@"Parsed Feed Info: “%@”", info.title);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
	//NSLog(@"Parsed Feed Item: “%@”", item.title);
	if (item)
	{
		NSMutableArray *itemList = parser.userInfo[@"DATA"];
		NSArray *result = [itemList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"title == %@", item.title]];
		if(result.count > 0)
			return;
		
		NewsData *newsItem = [[[NewsData alloc] init] autorelease];
		newsItem.title = item.title;
		newsItem.publish_date = [item.date timeIntervalSince1970]*  1000;
		newsItem.summary = item.summary;
		newsItem.url = item.link;
		NSArray *splitStar = [item.link componentsSeparatedByString:@"*"];
		NSString *url;
		if(splitStar.count >= 2)
			url = splitStar[1];
		else
			url = splitStar[0];
		
		NSURL *urlType = [NSURL URLWithString: url];
		NSString *host = [urlType host];
		NSArray *hostSplit = [host componentsSeparatedByString:@"."];
		NSString *finalHost;
		if(hostSplit.count == 3)
		{
			finalHost = [NSString stringWithFormat:@"%@.%@", hostSplit[1], hostSplit[2]];
		}
		else
		{
			finalHost = host;
		}
		newsItem.source = finalHost;
		//NSLog(@"url %@ %@", newsItem.url, host);
		[itemList addObject: newsItem];
	}
}

- (void)feedParserDidFinish:(MWFeedParser *)parser {
	NSLog(@"Finished Parsing%@", (parser.stopped ? @" (Stopped)" : @""));
	
	dispatch_async(dispatch_get_main_queue(), ^{
		ServiceCompleteBlock completeBlock = parser.userInfo[@"COMPLETE"];
		completeBlock(parser.userInfo[@"DATA"]);
	});
}

- (void)feedParser:(MWFeedParser *)parser didFailWithError:(NSError *)error {
	NSLog(@"Finished Parsing With Error: %@", error);
	NSMutableArray *parsedItems = parser.userInfo[@"DATA"];
    if (parsedItems.count == 0) {
		ServiceErrorBlock errorBlock = parser.userInfo[@"ERROR"];
		errorBlock(@"Parse error");
    } else {
		ServiceCompleteBlock completeBlock = parser.userInfo[@"COMPLETE"];
		completeBlock(parsedItems);
    }
}

@end
