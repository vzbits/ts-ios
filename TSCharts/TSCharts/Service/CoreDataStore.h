//
//  CoreDataStore.h
//  TSCharts
//
//  Created by Quoc Le on 7/3/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CacheObject.h"
#import <ObjectiveSugar/ObjectiveSugar.h>
#import "ObjectiveRecord.h"
#import "Alert.h"

static NSString *NOTIFICATION_ALERT_CHANGED = @"AlertChanged";

@interface CoreDataStore : NSObject
{
	BOOL needToSync;
}

declareSingleton(CoreDataStore);

- (CacheObject*) createCacheObject: (NSString*) symbol data: (NSDictionary*) data;
- (void) createDefaultList;

// alert manager
@property (strong) NSDictionary *alertsBySymbol;

- (NSArray*) allAlerts;
- (void) buildAlertMapping;
- (NSArray*) alertsBySymbol: (NSString*) symbol;
- (void) setAlertForSymbol: (NSString*) symbol price: (double) price  lastPrice: (double) lastPrice currentAlert: (Alert*)alert;
- (void) removeAlert: (Alert*) alert;
- (void) syncIfNecessary;
@end
