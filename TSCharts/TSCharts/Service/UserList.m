//
//  UserList.m
//  TSCharts
//
//  Created by Quoc Le on 7/28/13.
//
//

#import "UserList.h"
#import "UserListItem.h"
#import "ServiceManager.h"
#import "TableData+Array.h"

@interface UserList()
{
	double calculation[5];
}
@end

@implementation UserList
@dynamic listName, listType, listItems;
@dynamic listTypeEnum,indexSelected;

- (NSDictionary *)mappings {
    return @{
             @"listItems"     : @{ @"class": [UserListItem class] },
			 };
}

- (ListViewType) listTypeEnum
{
	return [self.listType isEqualToString:@"list"] ? ListViewType_List : ListViewType_Portfolio;
}

- (NSArray*) symbols
{
	NSMutableArray *data = [NSMutableArray array];
	[self.listItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		[data addObject: [obj symbol]];
	}];
	
	return data;
}
- (void) updateCalculation: (ServiceCompleteBlock) complete
{
	NSArray *symbols = [self symbols];
	[[ServiceManager instance] getPricesFromYahoo: symbols withBlock:^(id data) {
		if([data isKindOfClass: [TableData class]])
		{
			TableData *tData = (TableData*) data;
			
			double totalGain = 0;
			double originalValue = 0;
			double dayValue = 0;
			double prevValue = 0;
			for(UserListItem *item in self.listItems)
			{
				double currentPrice = [tData getLastForSymbol: item.symbol];
				double prevClose = [tData getDoubleForSymbol: item.symbol Key: @"PrevClose"];

				NSLog(@"###CurrentPrice %f prevClose = %f", currentPrice, prevClose);
				if(currentPrice != NSNotFound)
				{
					totalGain += [item totalGainForCurrentPrice: currentPrice];
					originalValue += [item originalValue];
					dayValue += [item valueForPrice: currentPrice];
				}
				
				if(prevClose != NSNotFound)
				{
					prevValue += [item valueForPrice: prevClose];
				}
			}
			
			NSLog(@"###TotalGain %f dayvalue = %f prevValue = %f", totalGain, dayValue, prevValue);
			calculation[ListPortfolioCalcType_TotalGain] = totalGain;
			calculation[ListPortfolioCalcType_TotalGainP] = (totalGain/originalValue);
			double dayGain = (dayValue - prevValue);
			calculation[ListPortfolioCalcType_DayGain] = dayGain;
			calculation[ListPortfolioCalcType_DayGainP] = dayGain / prevValue;
			calculation[ListPortfolioCalcType_TotalValue] = dayValue;
			complete(nil);
		}
	 } withErrorBlock:^(NSString *error) {
		 
	 }];
}
- (double) getCalculation: (int) type
{
	return calculation[type];
}
@end
