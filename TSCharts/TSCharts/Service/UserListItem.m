//
//  ListItem.m
//  TSCharts
//
//  Created by Quoc Le on 7/28/13.
//
//

#import "UserListItem.h"
#import "UserListItemHistory.h"

@implementation UserListItem
@dynamic userList, symbol, historyList;

- (NSDictionary *)mappings {
    return @{
             @"historyList"     : @{ @"class": [UserListItemHistory class] },
			 };
}

- (double) totalShares
{
	double total = 0;
	for(UserListItemHistory *item in self.historyList)
	{
		total += [item.quantity doubleValue];
	}
	
	return total;
}

- (double) averagePurchasePrice
{
	double totalShares = [self totalShares];
	double totalPrice = 0;
	for(UserListItemHistory *item in self.historyList)
	{
		totalPrice += ([item.quantity doubleValue] * [item.purchasePrice doubleValue]);
	}
	
	return totalPrice / totalShares;
}

- (double) totalGainForCurrentPrice: (double) currentPrice
{
	if(self.historyList && self.historyList.count > 0)
	{
		return (currentPrice - [self averagePurchasePrice]) * [self totalShares];
	}
	
	return 0;
}

- (double) originalValue
{
	double shares = [self totalShares];
	if(shares > 0)
		return [self totalShares] * [self averagePurchasePrice];
	else
		return 0;
}

- (double) valueForPrice: (double) currentPrice
{
	return [self totalShares] * currentPrice;
}

@end
