//
//  CacheObject.h
//  TSCharts
//
//  Created by Quoc Le on 7/3/13.
//
//

#import <CoreData/CoreData.h>

@interface CacheObject : NSManagedObject
@property (nonatomic, retain) NSString * symbol;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSNumber * priceType;
@property (nonatomic, retain) id data;
@property (nonatomic, retain) NSDate *createDate;
@end
