//
//  ListItem.h
//  TSCharts
//
//  Created by Quoc Le on 7/28/13.
//
//

#import <CoreData/CoreData.h>
@class  UserList;

@interface UserListItem : NSManagedObject
@property (nonatomic, retain) UserList *userList;
@property (nonatomic, retain) NSString *symbol;
@property (nonatomic, retain) NSOrderedSet *historyList;

- (double) totalShares;
- (double) averagePurchasePrice;
- (double) originalValue;
- (double) valueForPrice: (double) currentPrice;
- (double) totalGainForCurrentPrice: (double) currentPrice;

@end
