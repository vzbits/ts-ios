//
//  ViewController.m
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize mainView = _mainView;

- (void) loadView
{
    [super loadView];
//    self.wantsFullScreenLayout = NO;
	if(!IS_IPHONE)
	{
		self.mainView = [[[MainView alloc] init] autorelease];
		[(MainView*)self.mainView selectMasterSegment: 0 animated: NO];
	}
	else
	{
		self.mainView = [[[MainViewPhone alloc] init] autorelease];
	}
	
    CGRect appFrame = [UIScreen mainScreen].applicationFrame;
    NSLog(@"Main size w=%f h=%f", appFrame.size.width, appFrame.size.height);
    self.mainView.frame = appFrame; //CGRectMake(0,0,appFrame.size.width, appFrame.size.height);
	//self.view.backgroundColor = [UIColor colorWithRGBHex:0x4D6173];

	self.view.clipsToBounds = NO;
	self.view.layer.shadowColor = [UIColor blackColor].CGColor;
	self.view.layer.shadowOffset = CGSizeMake(-8,0);
	self.view.layer.shadowRadius = 5;
	self.view.layer.shadowOpacity = 1.0;
	
// OLD SHADOW
//	CGRect shadowFrame = appFrame;
//	shadowFrame.origin.x = -16;
//	shadowFrame.origin.y = 16;
//	shadowFrame.size.height += 20;
//	self.view.clipsToBounds = NO;
//
//	UIView *shadowView = [[UIView alloc] initWithFrame: shadowFrame];
//	shadowView.backgroundColor = [UIColor colorWithRGBHex:0x313131];
//	shadowView.alpha = 0.85f;
//	[self.view addSubview: shadowView];

	
	[self.view addSubview: self.mainView];

	//self.view = self.mainView;
    self.mainView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//	return UIStatusBarStyleLightContent;
//}

- (BOOL)prefersStatusBarHidden
{
	return NO;
}

- (void) viewDidAppear:(BOOL)animated
{
		
	[self.view addGestureRecognizer:self.slidingViewController.panGesture];
	//[self.view setNeedsLayout];


	if(IS_IPHONE)
	{
//		[((MainViewPhone*)self.mainView).stockList loadData];
//		[((MainViewPhone*)self.mainView) performSelector:@selector(getChartData:) withObject:  @"AAPL"];		
	}
	else
	{
		[((MainView*)self.mainView).chartContainer performSelector:@selector(getChartData:) withObject:  @"AAPL"];
	}
//    [self.mainView.listMiniView.tabBarController viewDidAppear: YES];
//    self.mainView.listMiniView.tabBarController.selectedIndex = 0;
}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//	return UIInterfaceOrientationPortrait;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPHONE) {
//		MainViewPhone *mainView = (MainViewPhone*) self.mainView;
//		return mainView.userList.listItems.count > 0;
        return YES;
    } else {
        //self.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, self.view.frame.size.width);
        return  UIInterfaceOrientationIsLandscape(interfaceOrientation);
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
{
	if([self.mainView isKindOfClass: [MainViewPhone class]])
	{
		MainViewPhone *mainView = (MainViewPhone*) self.mainView;
		[mainView willRotateToInterfaceOrientation: toInterfaceOrientation duration: duration];
	}
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	[[self slidingViewController] resetTopView];

	if([self.mainView isKindOfClass: [MainViewPhone class]])
	{
		MainViewPhone *mainView = (MainViewPhone*) self.mainView;
		[mainView willAnimateRotationToInterfaceOrientation: toInterfaceOrientation duration: duration];
	}	
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
	if([self.mainView isKindOfClass: [MainViewPhone class]])
	{
		MainViewPhone *mainView = (MainViewPhone*) self.mainView;
		[mainView didRotateFromInterfaceOrientation: fromInterfaceOrientation];
	}	
}

@end
