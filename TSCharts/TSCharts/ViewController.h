//
//  ViewController.h
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainView.h"
#import "MainViewPhone.h"
#import "ECSlidingViewController.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) UIView *mainView;

@end
