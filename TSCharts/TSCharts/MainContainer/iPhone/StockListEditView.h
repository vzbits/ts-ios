//
//  StockListEditView.h
//  TSCharts
//
//  Created by Quoc Le on 8/4/13.
//
//

#import <UIKit/UIKit.h>
#import "UserListItem.h"
#import "UserListItemHistory.h"
#import "ServiceManager.h"

@interface StockListEditView : UIView <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UserListItem *userData;

- (void) setData: (UserListItem*) data;
- (void) save;

@end
