//
//  StockListEditCell.m
//  TSCharts
//
//  Created by Quoc Le on 8/4/13.
//
//

#import "StockListEditCell.h"

@implementation StockListEditCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
		self.quantityField = [[[UITextField alloc] init] autorelease];
		self.purchasePriceField = [[[UITextField alloc] init] autorelease];
		
		self.quantityField.keyboardType = UIKeyboardTypeDecimalPad;
		self.purchasePriceField.keyboardType = UIKeyboardTypeDecimalPad;
		self.quantityField.borderStyle = UITextBorderStyleLine;
		self.purchasePriceField.borderStyle = UITextBorderStyleLine;
		self.quantityField.textAlignment = UITextAlignmentCenter;
		self.purchasePriceField.textAlignment = UITextAlignmentCenter;
		self.quantityField.placeholder = @"shares";
		self.purchasePriceField.placeholder = @"cost";
		self.quantityField.delegate = self;
		self.purchasePriceField.delegate = self;
		
		self.totalCost = [[[UILabel alloc] init] autorelease];
		self.totalCost.textAlignment = NSTextAlignmentRight;
		//self.totalCost.backgroundColor = [UIColor yellowColor];
		self.totalCost.adjustsFontSizeToFitWidth = YES;
		self.totalCost.adjustsLetterSpacingToFitWidth = YES;
		
		[self addSubview: self.quantityField];
		[self addSubview: self.purchasePriceField];
		[self addSubview: self.totalCost];
		
		self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	//int w = self.width / 3;
	int h = 36;
	int y = (self.height - h)/2;
	
	self.quantityField.frame = CGRectMake(10,y, 70, 36);
	self.purchasePriceField.frame = CGRectMake(self.quantityField.right + 10 , y, 100, 36);
	self.totalCost.frame = CGRectMake(self.purchasePriceField.right + 10 , y, 110, 36);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) updateTotalCost {
	float total = [self.userData.quantity floatValue] * [self.userData.purchasePrice floatValue];
	NSNumberFormatter* formatter = [[[NSNumberFormatter alloc] init] autorelease];
	[formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
	[formatter setCurrencySymbol:@""];
	
	NSNumber *n = @(total);
	if([n isEqualToNumber:[NSDecimalNumber notANumber]])
		self.totalCost.text = @"N/A";
	else
		self.totalCost.text = [formatter stringFromNumber: n];
}

- (void) setData: (UserListItemHistory*) history
{
	self.userData = history;
	
	if([self.userData.quantity floatValue] == 0.0)
		self.quantityField.text= @"";
	else
		self.quantityField.text = [self.userData.quantity stringValue];

	if([self.userData.purchasePrice floatValue] == 0.0)
		self.purchasePriceField.text= @"";
	else
		self.purchasePriceField.text = [self.userData.purchasePrice stringValue];
	[self updateTotalCost];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//	if([textField.text floatValue] == 0.0)
//	{
//		textField.text = @"";
//	}
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text
//{
//}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	if(textField == self.quantityField)
	{
		self.userData.quantity = [NSDecimalNumber decimalNumberWithString: self.quantityField.text];
	}
	else if(textField == self.purchasePriceField)
	{
		self.userData.purchasePrice = [NSDecimalNumber decimalNumberWithString: self.purchasePriceField.text];
	}
	
	[self updateTotalCost];
}
@end
