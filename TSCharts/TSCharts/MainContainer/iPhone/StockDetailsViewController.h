//
//  StockDetailsViewController.h
//  TSCharts
//
//  Created by Quoc Le on 9/11/13.
//
//

#import <UIKit/UIKit.h>

@interface StockDetailsViewController : UIViewController
@property (strong) IBOutlet UILabel *openValue;
@property (strong) IBOutlet UILabel *highValue;
@property (strong) IBOutlet UILabel *lowValue;
@property (strong) IBOutlet UILabel *volValue;
@property (strong) IBOutlet UILabel *peValue;
@property (strong) IBOutlet UILabel *mktcapValue;
@property (strong) IBOutlet UILabel *w52highValue;
@property (strong) IBOutlet UILabel *w52lowValue;
@property (strong) IBOutlet UILabel *avgVolValue;
@property (strong) IBOutlet UILabel *yieldValue;
@end
