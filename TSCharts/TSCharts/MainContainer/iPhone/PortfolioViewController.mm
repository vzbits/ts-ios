//
//  PortfolioViewController.m
//  TSCharts
//
//  Created by Quoc Le on 11/17/13.
//
//

#import "PortfolioViewController.h"
#import "LeftMenuListItemCell.h"
#import "UINavigationBar+Styling.h"
#import "LeftMenuViewController.h"
#import "ECSlidingViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"

@interface PortfolioViewController ()
@property (strong) ViewController *chartPanel;
@end

@implementation PortfolioViewController

- (void) setType: (ListViewType) listType;
{
	self.listType = listType;
	if(listType == ListViewType_Portfolio) {
		self.portfolios = [UserList where:@"listType == 'portfolio'"];
		self.title = @"Portfolio";
	}
	else {
		self.portfolios = [UserList where:@"listType == 'list'"];
		self.title = @"Lists";
	}
	
	[self.tableView setTableHeaderView: [self tableHeaderView]];
	[self.tableView reloadData];
}

+ (UIViewController*) buildWithNavController: (ListViewType) listType
{
	PortfolioViewController *view = [[PortfolioViewController alloc] initType: listType];
	UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController: view];
	[controller.navigationBar stylize];

	controller.view.clipsToBounds = NO;
	controller.view.layer.shadowColor = [UIColor blackColor].CGColor;
	controller.view.layer.shadowOffset = CGSizeMake(-8,0);
	controller.view.layer.shadowRadius = 5;
	controller.view.layer.shadowOpacity = 1.0;

	UIBarButtonItem *listButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list-layout"] style: UIBarButtonItemStylePlain target:view action:@selector(toggleListView)] autorelease];
	view.navigationItem.leftBarButtonItem = listButton;

	return controller;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.tableView = [[UITableView alloc] initWithFrame: self.view.bounds style: UITableViewStylePlain];
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	self.tableView.backgroundColor = LEFT_BGCOLOR;
//	[self.tableView setSeparatorInset:UIEdgeInsetsZero];
//	self.tableView.separatorColor = DIVIDER_COLOR;
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	self.tableView.rowHeight = 60;
	[self.view addSubview: self.tableView];
}

- (void) viewDidLayoutSubviews {
	self.tableView.frame = self.view.bounds;
}

- (void) backClicked: (id) sender {
	[self.delegate portfolioViewBackClicked: self];
}

- (UIView*) tableHeaderView {
	UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeCustom];
	[backBtn setTitle: self.listType == ListViewType_Portfolio ? @"Portfolios" : @"Lists" forState: UIControlStateNormal];
	[backBtn addTarget: self action:@selector(backClicked:) forControlEvents: UIControlEventTouchUpInside];
	backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
	[backBtn setImage: [UIImage imageNamed:@"arrow-3"] forState: UIControlStateNormal];
	backBtn.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
	UIView *view = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.view.width, self.listType == ListViewType_Portfolio ? 120 : 60)];
	backBtn.frame = CGRectMake(0,0,self.view.width, 60);
	backBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
	backBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
	
	[view addSubview: backBtn];

	UIView *separator = [[UIView alloc] initWithFrame: CGRectMake(0,60,self.view.width, 0.5f)];
	separator.backgroundColor = MENU_DIVIDER_COLOR;
	[view addSubview: separator];

	if(self.listType == ListViewType_Portfolio)
	{
		UISegmentedControl *segment = [[UISegmentedControl alloc] initWithItems:@[@"Day P/L", @"Total P/L", @"Value"]];
		[view addSubview: segment];
		segment.tintColor = [UIColor colorWithRGBHex:0x9B75F2];
		segment.frame = CGRectMake(15,60 + 15,219, 30);
		
		if(_portfolioToggle == ListPortfolioCalcType_TotalGain)
			segment.selectedSegmentIndex = 1;
		else if(_portfolioToggle == ListPortfolioCalcType_TotalValue)
			segment.selectedSegmentIndex = 2;
		else
			segment.selectedSegmentIndex = 0;
		
		[segment addTarget: self action:@selector(segmentChanged:) forControlEvents: UIControlEventValueChanged];
		
		UIView *separator = [[UIView alloc] initWithFrame: CGRectMake(0,119,self.view.width, 0.5f)];
		separator.backgroundColor = MENU_DIVIDER_COLOR;
		[view addSubview: separator];
	}
	
	return view;
}

- (void) segmentChanged: (UISegmentedControl*) sender {
	if(sender.selectedSegmentIndex == 0)
		_portfolioToggle = ListPortfolioCalcType_DayGain;
	if(sender.selectedSegmentIndex == 1)
		_portfolioToggle = ListPortfolioCalcType_TotalGain;
	if(sender.selectedSegmentIndex == 2)
		_portfolioToggle = ListPortfolioCalcType_TotalValue;
	
	[self reloadWithSelection];
}

- (void) reloadWithSelection
{
	NSIndexPath *selected = [self.tableView indexPathForSelectedRow];
	
	[self.tableView reloadData];
	
	if(selected)
	{
		[self.tableView selectRowAtIndexPath: selected animated: NO scrollPosition: UITableViewScrollPositionNone];
	}
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.portfolios.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForListPortfolioRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *ID = @"ListCellID";
	LeftMenuListItemCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
	if(!cell)
	{
		cell = [[[LeftMenuListItemCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: ID] autorelease];
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.textColor = [UIColor colorWithRGBHex:0xd0cfcf];
	}
	
	UserList *list = self.portfolios[indexPath.row];
	[cell setData: list toggleType: _portfolioToggle];
	
	return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self tableView: tableView cellForListPortfolioRowAtIndexPath: indexPath];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[(LeftMenuListItemCell*)[tableView cellForRowAtIndexPath: indexPath] updateCalculation:_portfolioToggle];
	UserList *list = self.portfolios[indexPath.row];
	[self.delegate portfolioViewItemClicked: list row: indexPath.row];
}

- (void) toggleListView
{
	ECSlidingViewController *controller = (ECSlidingViewController*)ROOT_VIEWCONTROLLER();
	LeftMenuViewController *leftMenu = (LeftMenuViewController*) controller.underLeftViewController;

	[controller anchorTopViewTo:ECRight animations:^{
	} onComplete:^{
	}];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//	if(editingStyle == UITableViewCellEditingStyleDelete)
//	{
//		NSLog(@"Deleting at row %d ", indexPath.row);
//		
//		if(indexPath.section == SEC_PORTFOLIO)
//		{
//			UserList *list = self.portfolios[indexPath.row];
//			[list delete];
//			self.portfolios = [UserList where:@"listType == 'portfolio'"];
//		}
//		else if(indexPath.section == SEC_LIST)
//		{
//			UserList *list = self.lists[indexPath.row];
//			[list delete];
//			self.lists = [UserList whereFormat:@"listType == 'list'"];
//		}
//		
//		[self.tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath] withRowAnimation: UITableViewRowAnimationAutomatic];
//		
//		//		int topMargin = (self.view.height - self.tableView.contentSize.height)/2;
//		//		UIView *view = [[[UIView alloc] initWithFrame:CGRectMake(0,0, 320, topMargin)] autorelease];
//		//		self.tableView.tableHeaderView = view;
//		
//	}
//}

- (void) editButtonClicked:(UIButton*) sender
{
	if(!self.tableView.editing)
	{
		[sender setTitle:@"Done" forState: UIControlStateNormal];
	}
	else
	{
		[sender setTitle:@"Edit" forState: UIControlStateNormal];
	}
	
	[self.tableView setEditing: !self.tableView.editing animated: YES];
}

- (void) addButtonClicked
{
	if(self.addView == nil)
	{
		self.filterView = [[[UIView alloc] init] autorelease];
		self.filterView.frame = self.view.superview.bounds;
		[self.view.superview addSubview: self.filterView];
		self.filterView.backgroundColor = [UIColor blackColor];
		self.filterView.alpha = 0.6;
		
		self.addView = [[[AddListViewController alloc] initWithNibName:nil bundle:nil] autorelease];
		self.addView.delegate = self;
		[self.view.superview addSubview: self.addView.view];
	}
	self.addView.view.frame = CGRectMake((self.view.superview.width - self.addView.view.width)/2, -200, self.addView.view.width, self.addView.view.height);
	[UIView animateWithDuration: 0.25f delay: 0 options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 self.addView.view.frame = CGRectSetY(self.addView.view.frame, 50);
					 } completion:^(BOOL finished) {
						 [self.addView.listName becomeFirstResponder];
					 }];
}

- (void) addListCancelClicked: (id) sender
{
	[UIView animateWithDuration: 0.25f delay: 0 options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 self.addView.view.frame = CGRectSetY(self.addView.view.frame, -200);
					 } completion:^(BOOL finished) {
						 [self.filterView removeFromSuperview];
						 [self.addView.view removeFromSuperview];
						 self.addView = nil;
					 }];
}
- (void) addListCreateClicked: (id) sender
{
	NSString *listType = self.addView.listType.selectedSegmentIndex == 0 ? @"list" : @"portfolio";
	
	UserList *newList = [UserList create: @{@"listType" : listType, @"listName": self.addView.listName.text} ];
	[self loadData];
	
	[self addListCancelClicked: nil];
	
//	ViewController *centerController = (ViewController*)CENTER_VIEWCONTROLLER();
//	MainViewPhone *mainView = (MainViewPhone*)centerController.mainView;
//	[mainView setData: newList];
	//	[[Settings instance] setListSelected: indexPath];
//	[[centerController slidingViewController] resetTopView];
}
@end
