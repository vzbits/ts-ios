//
//  StockListEditView.m
//  TSCharts
//
//  Created by Quoc Le on 8/4/13.
//
//

#import "StockListEditView.h"
#import "StockListEditCell.h"

@implementation StockListEditView

- (id)init
{
    self = [super init];
    if (self) {
		self.tableView = [[[UITableView alloc] init] autorelease];
		[self addSubview: self.tableView];
		self.tableView.dataSource = self;
		self.tableView.delegate = self;
		self.tableView.backgroundColor = [UIColor lightGrayColor];
		
		UITapGestureRecognizer *tapGesture = [[[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(tapGesture:)] autorelease];
		[self.tableView addGestureRecognizer: tapGesture];
		
		UIButton *btn = [UIButton buttonWithType: UIButtonTypeCustom];
		btn.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width, 30);
		[btn setTitle:@"Add More" forState: UIControlStateNormal];
		[btn setTitleColor: [UIColor blackColor] forState: UIControlStateNormal];
		[btn addTarget:self action: @selector(addClicked:) forControlEvents: UIControlEventTouchUpInside];
		self.tableView.tableFooterView = btn;
    }
    return self;
}

- (void) dealloc
{
	
	[super dealloc];
}
- (void) layoutSubviews
{
	[super layoutSubviews];
	self.tableView.frame = self.bounds;
}

- (void) addClicked: (id) sender
{
	UserListItemHistory *item = [UserListItemHistory create: @{@"quantity" : @0 , @"purchasePrice": @0 }];
	NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.userData.historyList];
	[tempSet addObject:item];
	self.userData.historyList = tempSet;

	NSIndexPath *indexPath = [NSIndexPath indexPathForRow: self.userData.historyList.count - 1 inSection:0];
	[self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation: UITableViewRowAnimationAutomatic];
}

- (void) setData: (UserListItem*) data
{
	self.userData = data;
	
	if(self.userData.historyList.count == 0)
	{
		UserListItemHistory *item = [UserListItemHistory create: @{@"quantity" : @0 , @"purchasePrice": @0 }];
		NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.userData.historyList];
		[tempSet addObject:item];
		self.userData.historyList = tempSet;
	}
	[self.tableView reloadData];
}

- (void) save
{
	[self findAndResignFirstResponder];

	NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.userData.historyList];
	NSMutableOrderedSet* newSet = [NSMutableOrderedSet orderedSet];
	
	for(UserListItemHistory *item in tempSet)
	{
		if([item.quantity floatValue] == 0.0 ||
		   [item.purchasePrice floatValue] == 0.0 ||
		   [item.quantity isEqualToNumber: [NSDecimalNumber notANumber]] ||
		   [item.purchasePrice isEqualToNumber: [NSDecimalNumber notANumber]])
		{
			[item delete];
		}
		else
		{
			[newSet addObject: item];
		}
	}
	NSLog(@"Old data = %d new Data %d", tempSet.count, newSet.count);

	self.userData.historyList = newSet;
	[[CoreDataManager instance] saveContext];
}

- (void) tapGesture: (UITapGestureRecognizer*) gesture
{
	[self findAndResignFirstResponder];
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	[self findAndResignFirstResponder];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//	if(self.userData.historyList.count == 0)
//	{
//		return 2;
//	}

	return self.userData.historyList.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellID = @"EDIT_CELL_ID";
	StockListEditCell *cell = [tableView dequeueReusableCellWithIdentifier: cellID];
	if(cell == nil)
	{
		cell = [[[StockListEditCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: cellID] autorelease];
	}
	
	[cell setData: self.userData.historyList[indexPath.row]];

	return cell;
}
@end
