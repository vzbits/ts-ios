//
//  ButtonRight.m
//  TSCharts
//
//  Created by Quoc Le on 9/20/13.
//
//

#import "ButtonRight.h"

@implementation ButtonRight

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)layoutSubviews
{
    // Allow default layout, then adjust image and label positions
    [super layoutSubviews];
	
    UIImageView *imageView = [self imageView];
    UILabel *label = [self titleLabel];
	
    CGRect imageFrame = imageView.frame;
    CGRect labelFrame = label.frame;
	
    labelFrame.origin.x = self.contentEdgeInsets.left;
    imageFrame.origin.x = labelFrame.origin.x + 3 + CGRectGetWidth(labelFrame);
	
    imageView.frame = imageFrame;
    label.frame = labelFrame;
	
	NSLog(@"buttonright %@ %@", NSStringFromCGRect( imageFrame ) , NSStringFromCGRect( labelFrame));
}
@end
