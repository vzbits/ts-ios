//
//  LeftMenuListItemCell.h
//  TSCharts
//
//  Created by Quoc Le on 8/11/13.
//
//

#import <UIKit/UIKit.h>

#import "ColorScheme.h"
#import "UserList.h"

@interface LeftMenuListItemCell : UITableViewCell
//@property (strong,nonatomic) UIButton *trashCan;
//@property (strong,nonatomic) UIButton *editBtn;
@property (strong,nonatomic) UIButton *rightBox;
//@property (strong,nonatomic) UILabel *priceLabel;
//@property (assign,nonatomic) NSInteger slideStopPosition;
@property (strong,nonatomic) UserList *data;
@property (strong,nonatomic) UIView *separator;

- (void) updateCalculation: (ListPortfolioCalcType) toggleType;
- (void) setData: (UserList*) object
	  toggleType: (ListPortfolioCalcType) toggleType;

@end
