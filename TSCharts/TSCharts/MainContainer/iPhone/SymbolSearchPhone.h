//
//  SymbolSearchPhone.h
//  TSCharts
//
//  Created by Quoc Le on 6/30/13.
//
//

#import <UIKit/UIKit.h>
#import "SymbolSearchViewController.h"

@protocol SymbolSearchPhoneDelegate <NSObject>
- (void) symbolSearchCancelClicked: (id) sender;
- (void) symbolSearchSymbolClicked: (NSString*) symbol;
@end

@interface SymbolSearchPhone : UIView <SymbolSearchViewDelegate, UISearchBarDelegate>
@property (strong) SymbolSearchViewController *symbolSearch;
@property (strong) UISearchBar *inputBox;
@property (strong) UIButton *cancelButton;
@property (strong) UIView *filterView;
@property (strong) UIView *whiteView;
@property (assign) id<SymbolSearchPhoneDelegate> delegate;
@end
