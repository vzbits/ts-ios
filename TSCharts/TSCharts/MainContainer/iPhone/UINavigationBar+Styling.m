//
//  UINavigationBar+Styling.m
//  TSCharts
//
//  Created by Quoc Le on 9/11/13.
//
//

#import "UINavigationBar+Styling.h"
#import "ColorScheme.h"

@implementation UINavigationBar (Styling)
- (void) stylize
{
	self.barStyle = UIBarStyleDefault;
	self.translucent = NO;
	self.backgroundColor = NAVBAR_COLOR;
	self.barTintColor = NAVBAR_COLOR;
	self.tintColor = CELL_TEXTCOLOR;
	self.titleTextAttributes = @{UITextAttributeFont :
											LIST_PRICE_FONT,
										UITextAttributeTextColor:
											CELL_TEXTCOLOR
										};
}
@end


@implementation UIToolbar (Styling)
- (void) stylize
{
	self.barStyle = UIBarStyleDefault;
	self.translucent = NO;
	self.backgroundColor = NAVBAR_COLOR;
	self.barTintColor = NAVBAR_COLOR;
	self.tintColor = CELL_TEXTCOLOR;
//	self.titleTextAttributes = @{UITextAttributeFont :
//									 LIST_PRICE_FONT,
//								 UITextAttributeTextColor:
//									 CELL_TEXTCOLOR
//								 };
}
@end
