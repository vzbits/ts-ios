//
//  UINavigationBar+Styling.h
//  TSCharts
//
//  Created by Quoc Le on 9/11/13.
//
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Styling)
- (void) stylize;
@end


@interface UIToolbar (Styling)
- (void) stylize;
@end
