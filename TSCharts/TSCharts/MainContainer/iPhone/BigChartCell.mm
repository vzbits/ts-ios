//
//  BigChartCell.m
//  TSCharts
//
//  Created by Quoc Le on 6/30/13.
//
//

#import "BigChartCell.h"

PriceType periodicity;

@implementation BigChartCell 
- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
	if(self)
	{
		self.chartPanel = [[[ChartPanel alloc] init] autorelease];
        self.chartPanel.delegate = self;
		self.chartPanel.newsToggleBtn.hidden = YES;
		self.backgroundColor = [UIColor clearColor];
		NSLog(@"BigChartCell::Init frame %f %f", self.width, self.bounds.size.width);
		self.chartPanel.frame = self.bounds;
		//[self.chartPanel setNeedsLayout];
        [self addSubview: self.chartPanel];
	}
	
	return self;
}

- (void)prepareForReuse
{
	[super prepareForReuse];
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	self.chartPanel.frame = self.bounds;
}

- (void) setData: (NSString*) symbol
{
    [[ServiceManager instance] getChartData: symbol
								periodicity: PriceType_PT_DAILY
								  withBlock: ^(StockPriceData *data)
	 {
         [self.chartPanel performSelector:@selector(setData:) withObject:data afterDelay:0.0];
		 //self.chartPanel.hidden = NO;
     }
							 withErrorBlock: ^(NSString *error)
     {
         
     }];
}

- (void) chartPanelPeriodictyChanged: (PriceType) periodicity_ symbol: (NSString*) symbol;
{
	periodicity = periodicity_;
	[self getPrice: symbol periodicity: periodicity_];
}

- (void) getPrice: (NSString*) symbol periodicity: (PriceType) priceType
{
	[[ServiceManager instance].asyncQueue cancelAllOperations];
    [[ServiceManager instance] getChartData: symbol
								periodicity: priceType
								  withBlock: ^(StockPriceData *data)
	 {
         [self.chartPanel setData: data];
		 //PriceData *priceData = [self getPriceDataForSymbol: symbol];
		 //[self.chartPanel setLastTick: priceData];
		 
     }
							 withErrorBlock: ^(NSString *error)
     {
         
     }];
}


- (void) chartPanelAnnotationSelected: (AnnotationGraphics*) selected;
{
}

- (void) dealloc
{
	self.chartPanel = nil;
	[super dealloc];
}
@end
