//
//  BigChartScroller.m
//  TSCharts
//
//  Created by Quoc Le on 6/30/13.
//
//

#import "BigChartScroller.h"
#import "ChartPanel.h"
#import "TableData+Array.h"

@implementation BigChartScroller

- (id)init
{
    self = [super init];
    if (self) {
		self.collectionView = [[[InfiniteScroller alloc] init] autorelease];
		self.collectionView.dataSource = self;
		[self addSubview: self.collectionView];
//		[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(panCrossChairBegan:) name:@"START_SCROLL" object: nil];
//		[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(panCrossChairEnded:) name:@"END_SCROLL" object: nil];

    }
    return self;
}

- (void) panCrossChairBegan: (NSNotification*) notif
{
	self.collectionView.scrollView.panGestureRecognizer.enabled = NO;
}

- (void) panCrossChairEnded: (NSNotification*) notif
{
	self.collectionView.scrollView.panGestureRecognizer.enabled = YES;
}


- (void) layoutSubviews
{
	NSLog(@"%s", __FUNCTION__);
	
	[super layoutSubviews];
	self.collectionView.frame = self.bounds;
	[self.collectionView reloadData];
}

- (void) setSymbolData: (TableData*) symbols
{
	NSLog(@"%s count=%d ######", __FUNCTION__, symbols.count);

	self.data = symbols;
	[self.collectionView reloadData];
}

- (NSInteger) infiniteScrollerNumberOfRows: (InfiniteScroller*) scroller;
{
    return self.data.count;
}

- (UIView *) infiniteScroller: (InfiniteScroller*) scroller  cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    // Dequeue a prototype cell and set the label to indicate the page
    BigChartCell *cell = [scroller dequeueReusableCell];
	if(!cell)
	{
		cell = [[[BigChartCell alloc] initWithFrame: self.bounds] autorelease];
	}
	
	NSLog(@"CLASS TYPE : %@", NSStringFromClass([cell class]));
	
	NSDictionary *sp = [self.data objectAtIndex: indexPath.row];
	[cell.chartPanel setEmptyChart: sp[@"Name"] symbol: sp[@"Symbol"]];

//	cell.backgroundColor = [UIColor whiteColor];
//	cell.textAlignment = UITextAlignmentCenter;
//	cell.text = [NSString stringWithFormat:@"%d", indexPath.row];
//	
//	NSLog(@"Row %d count %d", indexPath.row, self.data.count);
	[cell setData: sp[@"Symbol"]];
    return cell;
}

- (BigChartCell*) getCurrentCell
{
	return (BigChartCell*)[self.collectionView cellForIndexPath: [NSIndexPath indexPathForRow: self.collectionView.pageNumber inSection:0]];
}

@end
