//
//  LeftMenuViewController.m
//  TSCharts
//
//  Created by Quoc Le on 7/8/13.
//
//

#import "LeftMenuViewController.h"
#import "UserList.h"
#import "ECSlidingViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "LeftMenuListItemCell.h"
#import "UINavigationBar+Styling.h"
#import "UIImageExtensions.h"

#define SEC_CREATE			-1
#define SEC_PORTFOLIO		0
#define SEC_LIST			1
#define SEC_OPTIONS			2
#define SECTION_HEIGHT 24

@interface LeftMenuViewController ()

@end

@implementation LeftMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar {
    
    return UIBarPositionTopAttached;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	
	self.topBar = [[[UIView alloc] init] autorelease];
	[self.view addSubview: self.topBar];
	self.topBar.backgroundColor = [UIColor colorWithRGBHex:0x51378D];
	
	self.dividerLine = [[[UIView alloc] init] autorelease];
	[self.view addSubview: self.dividerLine];
	self.dividerLine.backgroundColor = [UIColor colorWithRGBHex:0x452D7E];
	 
	self.leftButton = [UIButton buttonWithType: UIButtonTypeCustom];
	[self.leftButton setImage:[[UIImage imageNamed:@"edit"] getImageWithTint: [UIColor whiteColor] withIntensity: 1.0] forState: UIControlStateNormal];
	[self.leftButton setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
	[self.topBar addSubview: self.leftButton];
	[self.leftButton addTarget: self action:@selector(addButtonClicked) forControlEvents: UIControlEventTouchUpInside];

	self.rightButton = [UIButton buttonWithType: UIButtonTypeCustom];
	[self.rightButton setImage:[[UIImage imageNamed:@"pencil_icon"] getImageWithTint: [UIColor whiteColor] withIntensity: 1.0] forState: UIControlStateNormal];
	[self.rightButton setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
	[self.topBar addSubview: self.rightButton];
	[self.rightButton addTarget: self action:@selector(editButtonClicked:) forControlEvents: UIControlEventTouchUpInside];

	self.settingButton = [UIButton buttonWithType: UIButtonTypeCustom];
	[self.settingButton setImage:[[UIImage imageNamed:@"settings-1"] getImageWithTint: [UIColor whiteColor] withIntensity: 1.0] forState: UIControlStateNormal];
	[self.settingButton setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
	[self.topBar addSubview: self.settingButton];
	[self.settingButton addTarget: self action:@selector(settingsClicked:) forControlEvents: UIControlEventTouchUpInside];

	self.rightButton.enabled = NO;

	self.view.backgroundColor = LEFT_BGCOLOR;
	
	self.menuItems = @[ @"Portfolio", @"List", @"Help"];
	self.tableView = [[[UITableView alloc] initWithFrame: CGRectMake(0,20, 245, self.view.height-20)] autorelease];
	[self.view addSubview: self.tableView];
	self.tableView.dataSource = self;
	self.tableView.delegate = self;
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	self.tableView.backgroundColor = [UIColor clearColor];
	self.tableView.rowHeight = 60;
	
	self.portfolioToggleNames = @[@"Day P/L", @"Day Gain %", @"Total P/L", @"Total Gain %", @"Value"];
	
	
	self.portfolioView = [[PortfolioViewController alloc] init];
	self.portfolioView.delegate = self;
	
	[self loadData];
}

- (void) restoreState
{
	NSIndexPath *indexPath = [[Settings instance] listSelected];
	NSLog(@"Restoring state to section=%d row=%d", indexPath.section, indexPath.row);
	
	// step 1 : go to the top level.
	// 0 - portfolio  1 - list
	if(indexPath)
	{
		NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow: indexPath.section inSection: 0];
		if(newIndexPath.section < [self numberOfSectionsInTableView: self.tableView] &&
		   newIndexPath.row < [self tableView: self.tableView numberOfRowsInSection: newIndexPath.section])
		{
			[self tableView: self.tableView didSelectRowAtIndexPath: newIndexPath];
		}


		// step 2 : go to second level
		NSIndexPath *newIndexPath2 = [NSIndexPath indexPathForRow: indexPath.row inSection: 0];
		if(newIndexPath2.row < [self.portfolioView tableView: self.portfolioView.tableView numberOfRowsInSection: newIndexPath2.section])
		{
			[self.portfolioView.tableView selectRowAtIndexPath:newIndexPath2 animated: NO scrollPosition:UITableViewScrollPositionNone];
			[self.portfolioView tableView: self.portfolioView.tableView didSelectRowAtIndexPath: newIndexPath2];
		}
	}
}

- (void) reloadWithSelection
{
	NSIndexPath *selected = [self.tableView indexPathForSelectedRow];
	
	[self.tableView reloadData];

	if(selected)
	{
		[self.tableView selectRowAtIndexPath: selected animated: NO scrollPosition: UITableViewScrollPositionNone];
	}
}

- (void) loadData
{
	NSLog(@"Portfolios = %d  lists = %d", self.portfolios.count, self.lists.count);
	[self reloadWithSelection];
}

- (void) viewDidLayoutSubviews
{
	self.view.frame = CGRectMake(0,0, self.view.width, self.view.height);
	self.navBar.frame = CGRectMake(0,0, 240, 44+20);
	self.topBar.frame = CGRectMake(0,0,self.view.width, 94);
	self.backgroundView.frame = self.view.bounds;
	self.backgroundView.contentMode = UIViewContentModeScaleAspectFill;
	self.tableView.frame = CGRectMake(0,self.topBar.bottom,self.view.width, self.view.height - self.topBar.height);
	self.portfolioView.view.frame = self.tableView.frame;
	
	self.leftButton.frame = CGRectMake(5,self.topBar.height - 44, 44,44);
	self.rightButton.frame = CGRectMake(self.leftButton.right + 5,self.topBar.height - 44, 44,44);
	self.dividerLine.frame = CGRectMake(self.rightButton.right + 5, self.topBar.height - 44, 1, 44);
	self.settingButton.frame = CGRectMake(self.view.width - 44 - 10,self.topBar.height - 44, 44,44);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 3;
}

- (void) portfolioViewBackClicked:(id)sender {
	
	[UIView transitionFromView: self.portfolioView.view
						toView: self.tableView
					  duration: 0.5
					   options:UIViewAnimationOptionTransitionCrossDissolve
					completion:^(BOOL finished) {
						[self.portfolioView.view removeFromSuperview];
                    }];

	self.rightButton.enabled = NO;
}

- (void) portfolioViewItemClicked: (UserList *)list row: (int) row {

	ViewController *centerController = (ViewController*)CENTER_VIEWCONTROLLER();
	MainViewPhone *mainView = (MainViewPhone*)centerController.mainView;
	[mainView setData: list];

	NSIndexPath *indexPath;
	if(list.listTypeEnum == ListViewType_List) {
		indexPath = [NSIndexPath indexPathForRow: row inSection: 1];
	} else {
		indexPath = [NSIndexPath indexPathForRow: row inSection: 0];
	}
	
	[[Settings instance] setListSelected: indexPath];

	if([self slidingViewController].topViewController != centerController)
	{
		[self slidingViewController].topViewController = centerController;
	}
	
	[[self slidingViewController] resetTopView];

}
- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *ID = @"Setting_ID";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
	if(!cell)
	{
		cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: ID] autorelease];
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.textColor = [UIColor colorWithRGBHex:0xd0cfcf];
		UIView *selected = [[[UIView alloc] init] autorelease];
		selected.backgroundColor = CELL_SELCOLOR;
		cell.selectedBackgroundView = selected;
		
		UIView *separator = [[UIView alloc] initWithFrame: CGRectMake(0,59,self.tableView.width, 0.5f)];
		separator.backgroundColor = MENU_DIVIDER_COLOR;
		[cell addSubview: separator];

	}
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.text = self.menuItems[indexPath.row];
	
	return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath: indexPath animated: YES];
	ViewController *centerController = (ViewController*)CENTER_VIEWCONTROLLER();
	MainViewPhone *mainView = (MainViewPhone*)centerController.mainView;
	
	if(indexPath.row == SEC_PORTFOLIO || indexPath.row == SEC_LIST)
	{
		self.rightButton.enabled = YES;
		self.portfolioView.view.frame = self.tableView.frame;
		[self.portfolioView setType: indexPath.row == SEC_PORTFOLIO ? ListViewType_Portfolio : ListViewType_List];

		[self.view addSubview: self.portfolioView.view];
		[UIView transitionFromView: self.tableView
							toView: self.portfolioView.view
						  duration: 0.5
						   options:UIViewAnimationOptionTransitionCrossDissolve
						completion:^(BOOL finished) {
						}];

	}
	else if(indexPath.section==SEC_OPTIONS)
	{

	}
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(indexPath.section == SEC_OPTIONS || indexPath.section == SEC_CREATE)
		return NO;
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if(editingStyle == UITableViewCellEditingStyleDelete)
	{
		NSLog(@"Deleting at row %d ", indexPath.row);
		
		if(indexPath.section == SEC_PORTFOLIO)
		{
			UserList *list = self.portfolios[indexPath.row];
			[list delete];
			self.portfolios = [UserList where:@"listType == 'portfolio'"];
		}
		else if(indexPath.section == SEC_LIST)
		{
			UserList *list = self.lists[indexPath.row];
			[list delete];
			self.lists = [UserList where:@"listType == 'list'"];
		}
		
		[self.tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath] withRowAnimation: UITableViewRowAnimationAutomatic];
	}
}

- (void) settingsClicked:(UIButton*) sender
{
	ViewController *centerController = (ViewController*)CENTER_VIEWCONTROLLER();
	MainViewPhone *mainView = (MainViewPhone*)centerController.mainView;

	BAMSettings *settings = [[BAMSettings alloc] initWithTitle:@"Chart Settings" propertyListNamed:@"ChartSettings"];
	UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController: settings];
	[controller.navigationBar stylize];
	//		settings.view.backgroundColor
	//		settings.view.transform = CGAffineTransformIdentity;
	//		controller.view.transform = CGAffineTransformMakeScale(0.60, 0.60);
	[[self slidingViewController] setTopViewController: controller];
	UIBarButtonItem *listButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list-layout"] style: UIBarButtonItemStylePlain target:mainView action:@selector(toggleListView)] autorelease];
	settings.navigationItem.leftBarButtonItem = listButton;
	
	[[self slidingViewController] resetTopView];
}

- (void) editButtonClicked:(UIButton*) sender
{
	[self.portfolioView.tableView setEditing: !self.portfolioView.tableView.editing animated: YES];
}

- (void) addButtonClicked
{
	if(self.addView == nil)
	{
		self.filterView = [[[UIView alloc] init] autorelease];
		self.filterView.frame = self.view.superview.bounds;
		[self.view.superview addSubview: self.filterView];
		self.filterView.backgroundColor = [UIColor blackColor];
		self.filterView.alpha = 0.6;
		
		self.addView = [[[AddListViewController alloc] initWithNibName:nil bundle:nil] autorelease];
		self.addView.delegate = self;
		[self.view.superview addSubview: self.addView.view];
	}
	self.addView.view.frame = CGRectMake((self.view.superview.width - self.addView.view.width)/2, -200, self.addView.view.width, self.addView.view.height);
	[UIView animateWithDuration: 0.25f delay: 0 options:UIViewAnimationOptionCurveEaseIn
	 animations:^{
		 self.addView.view.frame = CGRectSetY(self.addView.view.frame, 50);
	 } completion:^(BOOL finished) {
		 [self.addView.listName becomeFirstResponder];
	 }];
}

- (void) addListCancelClicked: (id) sender
{
	[UIView animateWithDuration: 0.25f delay: 0 options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 self.addView.view.frame = CGRectSetY(self.addView.view.frame, -200);
					 } completion:^(BOOL finished) {
						 [self.filterView removeFromSuperview];
						 [self.addView.view removeFromSuperview];
						 self.addView = nil;
					 }];
}
- (void) addListCreateClicked: (id) sender
{
	NSString *listType = self.addView.listType.selectedSegmentIndex == 0 ? @"list" : @"portfolio";
	
	UserList *newList = [UserList create: @{@"listType" : listType, @"listName": self.addView.listName.text} ];
	[self loadData];
	
	[self addListCancelClicked: nil];
	
	ViewController *centerController = (ViewController*)CENTER_VIEWCONTROLLER();
	MainViewPhone *mainView = (MainViewPhone*)centerController.mainView;
	[mainView setData: newList];
//	[[Settings instance] setListSelected: indexPath];
	[[centerController slidingViewController] resetTopView];
}

@end
