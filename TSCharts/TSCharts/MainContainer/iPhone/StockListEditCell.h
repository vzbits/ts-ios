//
//  StockListEditCell.h
//  TSCharts
//
//  Created by Quoc Le on 8/4/13.
//
//

#import <UIKit/UIKit.h>
#import "UserListItemHistory.h"

@interface StockListEditCell : UITableViewCell <UITextFieldDelegate>
@property (strong, nonatomic) UITextField *quantityField;
@property (strong, nonatomic) UITextField *purchasePriceField;
@property (strong, nonatomic) UILabel *totalCost;
@property (strong, nonatomic) UserListItemHistory *userData;

- (void) setData: (UserListItemHistory*) history;

@end
