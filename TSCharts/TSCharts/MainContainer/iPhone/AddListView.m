//
//  AddListView.m
//  TSCharts
//
//  Created by Quoc Le on 7/28/13.
//
//

#import "AddListView.h"

@implementation AddListView

- (id)init
{
    self = [super init];
    if (self) {
		self.listName = [[[UITextField alloc] init] autorelease];
		self.listType = [[[UISegmentedControl alloc] initWithItems:@[@"List", @"Portfolio"]] autorelease];
		self.listType.segmentedControlStyle = UISegmentedControlStyleBar;
		self.listType.selectedSegmentIndex = 0;
		[self addSubview: self.listType];
		[self addSubview: self.listName];
		self.backgroundColor = [UIColor lightGrayColor];
		self.layer.borderColor = [UIColor grayColor].CGColor;
		self.layer.borderWidth = 1;
    }
    return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	self.listName.frame = CGRectMake(0, 0, 150, 44);
	self.listType.frame = CGRectMake(0, 50, 150, self.listType.height);
}

- (void) dealloc
{
	self.listName = nil;
	self.listType = nil;
	[super dealloc];
}
@end
