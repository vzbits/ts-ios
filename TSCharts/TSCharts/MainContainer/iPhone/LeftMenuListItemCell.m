//
//  LeftMenuListItemCell.m
//  TSCharts
//
//  Created by Quoc Le on 8/11/13.
//
//

#import "LeftMenuListItemCell.h"

#define RIGHTBOX_WIDTH 120

@implementation LeftMenuListItemCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
	{
		self.backgroundColor = [UIColor colorWithRGBHex: 0x06708E];
		self.textLabel.adjustsFontSizeToFitWidth = YES;
		self.textLabel.backgroundColor = [UIColor clearColor];
		self.selectionStyle = UITableViewCellSelectionStyleGray;
		//self.selectedBackgroundView = [[[UIView alloc] init] autorelease];
		//self.selectedBackgroundView.backgroundColor = [self.backgroundColor colorWithBrightness: 0.90];
		self.textLabel.shadowOffset = CGSizeZero;
		self.textLabel.textColor = CELL_TEXTCOLOR;
		
		self.rightBox = [[[UIButton alloc] init] autorelease];
		self.rightBox.tag = 100;
		self.rightBox.frame = CGRectMake(250, 2, RIGHTBOX_WIDTH, 40);
		self.rightBox.titleLabel.shadowOffset = CGSizeZero;
		[self.rightBox setTitleColor:CELL_TEXTCOLOR forState: UIControlStateNormal];
		[self.rightBox setTitle:@"" forState: UIControlStateNormal];
//		self.rightBox.layer.cornerRadius = 3;
		self.rightBox.titleLabel.textAlignment = UITextAlignmentCenter;
		self.rightBox.titleLabel.adjustsFontSizeToFitWidth = YES;
		self.rightBox.titleLabel.numberOfLines = 0;
//		self.rightBox.backgroundColor = BOX_COLOR;
		self.rightBox.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
		self.rightBox.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size: 14.0f];
		//[self.rightBox addTarget: nil action:@selector(togglePriceBox) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview: self.rightBox];
		
		self.rightBox.userInteractionEnabled = NO;
		self.detailTextLabel.backgroundColor = [UIColor clearColor];
		
		self.separator = [[UIView alloc] initWithFrame: CGRectMake(0,0,self.width, 0.5f)];
		self.separator.backgroundColor = MENU_DIVIDER_COLOR;
		[self addSubview: self.separator];
		
        self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size: 19.0f];
    }
    return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	//int w = self.data.listTypeEnum == ListViewType_List ? 30 : 60;
		
	if(self.editing)
		self.rightBox.frame = CGRectMake(self.width + 100, (self.height - 40)/2, RIGHTBOX_WIDTH, 40);
	else
		self.rightBox.frame = CGRectMake(self.width - RIGHTBOX_WIDTH - 14, (self.height - 40)/2, RIGHTBOX_WIDTH, 40);
	
	self.separator.frame = CGRectSetY(self.separator.frame, 59);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];
	
	if(!selected)
		self.contentView.backgroundColor = self.backgroundColor;
	else
		self.contentView.backgroundColor = CELL_SELCOLOR;
}

- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
	
}

- (void) setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing: editing animated: animated];
}

- (void) updateCalculation: (ListPortfolioCalcType) toggleType
{
	BOOL isNormalList = [self.data.listType isEqualToString:@"list"];
	if(!isNormalList)
	{
		NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
		[numberFormatter setCurrencySymbol: @""];
		[numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
		[numberFormatter setNegativePrefix:@""];
		[numberFormatter setNegativeSuffix:@""];		
		[self.data updateCalculation: ^(id data)
		 {
			 NSString *text = @"";
			 UIColor *titleColor = nil;
			 
			 if(toggleType == ListPortfolioCalcType_DayGain)
			 {
				 double totalGainP = [self.data getCalculation:ListPortfolioCalcType_DayGainP];
				 double totalGain = [self.data getCalculation:ListPortfolioCalcType_DayGain];
				 NSString *totalGainStr = [numberFormatter stringFromNumber: @(totalGain)];

				 text = [NSString stringWithFormat:@"%c%@\n(%c%.0f%%)", (totalGain > 0 ? '+' : '-') , totalGainStr,
									(totalGainP > 0 ? '+' : '-') , fabsf(totalGainP)* 100];
				 titleColor = totalGainP > 0 ? CELL_UPCOLOR : CELL_DOWNCOLOR;
			 }
			 else if(toggleType == ListPortfolioCalcType_TotalGain)
			 {
				 double totalGainP = [self.data getCalculation:ListPortfolioCalcType_TotalGainP];
				 double totalGain = [self.data getCalculation:ListPortfolioCalcType_TotalGain];
				 NSString *totalGainStr = [numberFormatter stringFromNumber: @(totalGain)];
				 
				 text = [NSString stringWithFormat:@"%c%@\n(%c%.0f%%)", (totalGain > 0 ? '+' : '-') , totalGainStr,
						 (totalGainP > 0 ? '+' : '-') , fabsf(totalGainP)* 100];
				 titleColor = totalGainP > 0 ? CELL_UPCOLOR : CELL_DOWNCOLOR;
			 }
			 else
			 {
				 double totalValue = [self.data getCalculation:ListPortfolioCalcType_TotalValue];
				 NSString *formattedNumberString = [numberFormatter stringFromNumber: @(totalValue)];
				 text = formattedNumberString;

				 titleColor = [UIColor whiteColor];
			 }
			 
			 [self.rightBox setTitleColor: titleColor forState:UIControlStateNormal];
			 [self.rightBox setTitle: text forState:UIControlStateNormal];
		 }];
	}
}

- (void) setData: (UserList*) object
	  toggleType: (ListPortfolioCalcType) toggleType
{
	self.data = object;
	
	self.textLabel.text = [NSString stringWithFormat:@"%@", [object.listName capitalizedString]];
	BOOL isNormalList = [object.listType isEqualToString:@"list"];

	if(isNormalList)
	{
		NSString *text = nil; //[NSString stringWithFormat:@"%d", object.listItems.count];
		[self.rightBox setTitle: text forState:UIControlStateNormal];
		self.rightBox.layer.cornerRadius = 0;
		self.rightBox.backgroundColor = [UIColor clearColor];
	}
	else
	{
//		self.rightBox.backgroundColor = LEFT_MENU_BOX;
//		self.rightBox.layer.cornerRadius = 10;
		[self updateCalculation: toggleType];
	}
}
@end
