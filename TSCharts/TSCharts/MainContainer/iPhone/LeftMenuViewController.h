//
//  LeftMenuViewController.h
//  TSCharts
//
//  Created by Quoc Le on 7/8/13.
//
//

#import <UIKit/UIKit.h>
#import "AddListViewController.h"
#import "UserList.h"
#import "PortfolioViewController.h"

@interface LeftMenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, AddListDelegate, PortfolioViewControllerDelegate>
{
	ListPortfolioCalcType portfolioToggle;
}
@property (strong) UINavigationBar *navBar;
@property (strong) UIButton *leftButton;
@property (strong) UIButton *rightButton;
@property (strong) UIButton *settingButton;
@property (strong) UITableView *tableView;
@property (strong) UIImageView *backgroundView;
@property (strong) NSArray *menuItems;
@property (strong) NSArray *portfolios;
@property (strong) NSArray *lists;
@property (strong) NSArray *portfolioToggleNames;
@property (strong) UIView *filterView;
@property (strong) UIView *topBar;
@property (strong) UIView *dividerLine;
@property (strong) AddListViewController *addView;
@property (strong) PortfolioViewController *portfolioView;

- (void) restoreState;
- (void) reloadWithSelection;

@end
