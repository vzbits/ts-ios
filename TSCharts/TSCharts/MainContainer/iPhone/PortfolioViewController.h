//
//  PortfolioViewController.h
//  TSCharts
//
//  Created by Quoc Le on 11/17/13.
//
//

#import <UIKit/UIKit.h>
#import "UserList.h"
#import "AddListViewController.h"

@protocol PortfolioViewControllerDelegate<NSObject>
- (void) portfolioViewBackClicked: (id) sender;
- (void) portfolioViewItemClicked: (UserList *)list row: (int) row;
@end

@interface PortfolioViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,AddListDelegate>

@property (strong) UITableView *tableView;
@property (strong) UIView *filterView;
@property (strong) AddListViewController *addView;
@property (strong) NSArray *portfolios;
@property (assign) ListViewType listType;
@property (assign) ListPortfolioCalcType portfolioToggle;
@property (assign) id<PortfolioViewControllerDelegate> delegate;

- (void) setType: (ListViewType) listType;

+ (UIViewController*) buildWithNavController: (ListViewType) listType;
@end
