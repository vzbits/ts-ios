//
//  AddListView.h
//  TSCharts
//
//  Created by Quoc Le on 7/28/13.
//
//

#import <UIKit/UIKit.h>

@interface AddListView : UIView
@property (strong) UISegmentedControl *listType;
@property (strong) UITextField *listName;
@end
