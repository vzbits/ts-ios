//
//  MainViewPhone.h
//  TSCharts
//
//  Created by Quoc Le on 6/29/13.
//
//

#import <UIKit/UIKit.h>
#import "ListMiniView.h"
#import "ChartContainer.h"
#import "NewsReaderView.h"
#import "FullListView.h"
#import "StockListPhone.h"
#import "NewsViewController.h"
#import "NewsReaderView.h"
#import "BigChartScroller.h"
#import "UserList.h"
#import "StockDetailsViewController.h"

@interface MainViewPhone : UIView <ChartPanelDelegate, StockListPhoneDelegate, NewsViewDelegate, NewsReaderViewDelegate, UIBarPositioningDelegate, UIScrollViewDelegate, UINavigationBarDelegate>
{
	CGRect bottomRect;
	CGRect fullRect;
	BOOL rotating;
	BOOL opened;
	PriceType periodicity;
}
@property (strong, nonatomic) UINavigationBar *navBar;
@property (strong, nonatomic) ChartPanel *chartPanel;
@property (strong, nonatomic) UIScrollView *bottomScroller;
@property (strong, nonatomic) StockListPhone *stockList;
@property (strong, nonatomic) NewsViewController *newsController;
@property (strong, nonatomic) StockDetailsViewController *detailsController;
@property (strong, nonatomic) NewsReaderView *newsReader;
@property (strong, nonatomic) BigChartScroller *bigScroller;
@property (strong, nonatomic) UIImageView *testImage;
@property (strong, nonatomic) UserList *userList;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) UILabel *newsLabel;
@property (strong, nonatomic) UILabel *emptyListLabel;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) NSString *currentSymbol;

- (void) setData: (UserList*) userList;
- (void) getChartData:(NSString *)symbol;
- (void) reloadData;

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;

@end
