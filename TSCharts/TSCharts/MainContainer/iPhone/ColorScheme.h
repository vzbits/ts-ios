//
//  ColorScheme.h
//  TSCharts
//
//  Created by Quoc Le on 8/4/13.
//
//

#import <Foundation/Foundation.h>

#define DARK_SCHEME 0

#if DARK_SCHEME

#define COMMITING_CREATE_CELL_HEIGHT 48
#define NORMAL_CELL_FINISHING_HEIGHT 48

#define NAVBAR_COLOR [UIColor colorWithRGBHex: 0x640BBA]
#define HEADER_COLOR [UIColor colorWithRGBHex: 0x850AFF]
#define HEADER_TEXTCOLOR [UIColor colorWithRGBHex: 0xFFFFFF]
#define LEFT_BGCOLOR [UIColor colorWithRGBHex: 0x222222]
#define TIMESTAMP_COLOR [UIColor colorWithRGBHex: 0x425066]

#define DIVIDER_COLOR [UIColor colorWithRGBHex: 0x2E3849]
#define TABLE_BGCOLOR [UIColor colorWithRGBHex:0x101419]

#define BOX_COLOR [UIColor clearColor]
#define CELL_BGCOLOR [UIColor colorWithRGBHex: 0x191919]
#define CELL_UPCOLOR [UIColor colorWithRGBHex: 0x25C770]
#define CELL_DOWNCOLOR [UIColor colorWithRGBHex: 0xFC4C4D]
#define CELL_TEXTCOLOR [UIColor colorWithRGBHex: 0xE8E8E8]
#define CELL_COLOR [UIColor colorWithRGBHex: 0x101419]
#define CELL_SELCOLOR [UIColor colorWithRGBHex: 0x1B2028]
#define LIST_SYMBOL_FONT [UIFont fontWithName:@"DINCondensed-Bold" size: 19]
#define LIST_PRICE_FONT [UIFont fontWithName:@"DINCondensed-Bold" size: 19]
#define LIST_RIGHTBOX_FONT [UIFont fontWithName:@"DINCondensed-Bold" size: 19]
#define CHART_PERIODICITY_FONT [UIFont fontWithName:@"HelveticaNeue" size: 12.0f]
#define CHART_PERIODICITY_COLOR_ON [UIColor colorWithRGBHex: 0x850AFF]
#define CHART_PERIODICITY_COLOR_OFF [UIColor colorWithRGBHex: 0x323D51]

#define CELL_NEWS_TEXTCOLOR [UIColor whiteColor]
#define CELL_NEWS_SUBTEXTCOLOR [UIColor colorWithRGBHex: 0x450780]

// search
#define CELL_SEARCH_BGCOLOR [UIColor colorWithRGBHex: 0x1F2020]
#define CELL_SEARCH_ALTBGCOLOR [UIColor colorWithRGBHex: 0x191919]
#define CELL_SEARCH_TEXTHIGHLIGHTCOLOR [UIColor colorWithRGBHex: 0x790BE7]
#define CELL_SEARCH_SEPCOLOR [UIColor colorWithRGBHex: 0x0]
#define CELL_SEARCH_TEXTCOLOR [UIColor colorWithRGBHex: 0xffffff]
#define CELL_SEARCH_FONT [UIFont fontWithName:@"DINCondensed-Bold" size: 18]

#else

#define COMMITING_CREATE_CELL_HEIGHT 48
#define NORMAL_CELL_FINISHING_HEIGHT 48

#define NAVBAR_COLOR [UIColor colorWithRGBHex: 0x5e3fad]
#define HEADER_COLOR [UIColor colorWithRGBHex: 0x9a74fa]
#define HEADER_TEXTCOLOR [UIColor colorWithRGBHex: 0xFFFFFF]
#define LEFT_BGCOLOR [UIColor colorWithRGBHex: 0x222222]
#define TIMESTAMP_COLOR [UIColor colorWithRGBHex: 0x8D9FA4]

#define DIVIDER_COLOR [UIColor colorWithRGBHex: 0x1d2022]
#define DIVIDER_COLOR2 [UIColor colorWithRGBHex: 0x5f6365]
#define MENU_DIVIDER_COLOR [UIColor colorWithRGBHex: 0x323131]
#define TABLE_BGCOLOR [UIColor colorWithRGBHex:0x272c2e]

#define BOX_COLOR [UIColor clearColor]
#define CELL_BGCOLOR [UIColor colorWithRGBHex: 0x272c2e]
#define CELL_BGCOLOR2 [UIColor colorWithRGBHex: 0x414140]
#define CELL_SLIDE_BGCOLOR [UIColor colorWithRGBHex: 0x6041A8]
#define CELL_UPCOLOR [UIColor colorWithRGBHex: 0x29ce7b]
#define CELL_DOWNCOLOR [UIColor colorWithRGBHex: 0xfc5757]
#define CELL_TEXTCOLOR [UIColor colorWithRGBHex: 0xffffff]
#define CELL_COLOR [UIColor colorWithRGBHex: 0x272c2e]
#define CELL_SELCOLOR [UIColor colorWithRGBHex: 0x424749]
#define LIST_SYMBOL_FONT [UIFont fontWithName:@"DINCondensed-Bold" size: 20]
#define LIST_PRICE_FONT [UIFont fontWithName:@"DINCondensed-Bold" size: 20]
#define LIST_RIGHTBOX_FONT [UIFont fontWithName:@"DINCondensed-Bold" size: 21]
#define CHART_PERIODICITY_FONT [UIFont fontWithName:@"HelveticaNeue-Light" size: 12.0f]
#define CHART_PERIODICITY_FONT_ON [UIFont fontWithName:@"HelveticaNeue-Bold" size: 12.0f]
#define CHART_PERIODICITY_COLOR_ON [UIColor colorWithRGBHex: 0x936BF9]
#define CHART_PERIODICITY_COLOR_OFF [UIColor colorWithRGBHex: 0x8D9FA5]

#define CELL_NEWS_TEXTCOLOR [UIColor whiteColor]
#define CELL_NEWS_SUBTEXTCOLOR [UIColor colorWithRGBHex: 0x450780]

// search
#define CELL_SEARCH_BGCOLOR [UIColor colorWithRGBHex: 0x1F2020]
#define CELL_SEARCH_ALTBGCOLOR [UIColor colorWithRGBHex: 0x191919]
#define CELL_SEARCH_TEXTHIGHLIGHTCOLOR [UIColor colorWithRGBHex: 0x790BE7]
#define CELL_SEARCH_SEPCOLOR [UIColor colorWithRGBHex: 0x0]
#define CELL_SEARCH_TEXTCOLOR [UIColor colorWithRGBHex: 0xffffff]
#define CELL_SEARCH_FONT [UIFont fontWithName:@"DINCondensed-Bold" size: 18]

#define LEFT_MENU_BOX [UIColor colorWithRGBHex: 0x262626]
#define NEWS_TOGGLE_OFF_TEXT [UIColor colorWithRGBHex: 0x8D9FA5]
#define NEWS_TOGGLE_ON_TEXT [UIColor colorWithRGBHex: 0x272C2E]
#define NEWS_TOGGLE_BG_COLOR [UIColor colorWithRGBHex: 0x8D9FA5]

#endif