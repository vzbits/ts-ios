//
//  BigChartCell.h
//  TSCharts
//
//  Created by Quoc Le on 6/30/13.
//
//

#import <UIKit/UIKit.h>
#import "ChartPanel.h"

@interface BigChartCell : UICollectionViewCell <ChartPanelDelegate>
@property (strong) ChartPanel *chartPanel;
- (void) setData: (NSString*) symbol;
@end
