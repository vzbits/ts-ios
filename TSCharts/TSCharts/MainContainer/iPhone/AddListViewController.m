//
//  AddListViewController.m
//  TSCharts
//
//  Created by Quoc Le on 7/28/13.
//
//

#import "AddListViewController.h"

@interface AddListViewController ()

@end

@implementation AddListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) cancelClicked:(id)sender
{
	[self.delegate addListCancelClicked: nil];
}

- (IBAction) createClicked:(id)sender
{
	[self.delegate addListCreateClicked: nil];
}

@end
