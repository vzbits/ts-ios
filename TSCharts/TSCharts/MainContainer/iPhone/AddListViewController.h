//
//  AddListViewController.h
//  TSCharts
//
//  Created by Quoc Le on 7/28/13.
//
//

#import <UIKit/UIKit.h>

@protocol AddListDelegate <NSObject>
- (void) addListCancelClicked: (id) sender;
- (void) addListCreateClicked: (id) sender;
@end

@interface AddListViewController : UIViewController
@property (strong) IBOutlet UISegmentedControl *listType;
@property (strong) IBOutlet UITextField *listName;
@property (assign) id<AddListDelegate> delegate;

- (IBAction) cancelClicked:(id)sender;
- (IBAction) createClicked:(id)sender;
@end
