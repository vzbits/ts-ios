//
//  MainViewPhone.m
//  TSCharts
//
//  Created by Quoc Le on 6/29/13.
//
//

#import "MainViewPhone.h"
#import "UIColorExtensions.h"
#import "AppDelegate.h"
#import "ECSlidingViewController.h"
#import "ColorScheme.h"
#import "UINavigationBar+Styling.h"
#import "LeftMenuViewController.h"

#define NUM_OF_ITEMS (IS_IPHONE5 ? 6  : 5)

@implementation MainViewPhone

- (id)init
{
    self = [super init];
    if (self)
    {
//		for(NSString *familyName in [UIFont familyNames])
//		{
//			NSLog(@"tt0001m_: %@", [UIFont fontNamesForFamilyName:familyName] );
//		}
		
		periodicity = PriceType_PT_DAILY;
		
		self.backgroundColor = CELL_COLOR;
		
		self.navBar = [[[UINavigationBar alloc] init] autorelease];
		[self.navBar stylize];
		self.navBar.delegate = self;

		self.containerView = [[UIView alloc] init];
		[self addSubview: self.containerView];
		self.containerView.backgroundColor = NAVBAR_COLOR;
		
		self.bottomScroller = [[UIScrollView alloc] init];
		self.bottomScroller.pagingEnabled = YES;
		self.bottomScroller.backgroundColor = NAVBAR_COLOR;
		self.bottomScroller.showsHorizontalScrollIndicator = NO;
		//self.bottomScroller.layer.cornerRadius = 8;
		self.bottomScroller.delegate = self;
		//self.bottomScroller.layer.masksToBounds = YES;
		[self addSubview: self.bottomScroller];

		self.newsLabel = [[UILabel alloc] init];
		self.newsLabel.text = @"IN THE NEWS";
		self.newsLabel.textColor = CELL_TEXTCOLOR;
		self.newsLabel.font = LIST_PRICE_FONT;
		self.newsLabel.textAlignment = UITextAlignmentCenter;
		[self.bottomScroller addSubview: self.newsLabel];
		
		UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:@"Watch List"];
		UIBarButtonItem *listButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slider_icon"] style: UIBarButtonItemStylePlain target:self action:@selector(toggleListView)] autorelease];
		UIBarButtonItem *addButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add_icon"] style: UIBarButtonItemStylePlain target:self action:@selector(addButtonClicked)] autorelease];
		item.leftBarButtonItem = listButton;
		item.rightBarButtonItem = addButton;
		self.navBar.items = @[item];
		
		[self addSubview: self.navBar];
		
		self.stockList = [[[StockListPhone alloc] init] autorelease];
		self.stockList.delegate = self;
		//self.stockList.layer.cornerRadius = 8;
		//self.stockList.layer.masksToBounds = YES;
		[self addSubview: self.stockList];
				
        self.chartPanel = [ChartPanel instance];
        self.chartPanel.delegate = self;
        [self addSubview: self.chartPanel];
		
		self.newsController = [[NewsViewController alloc] init];
		self.newsController.delegate = self;
		[self.bottomScroller addSubview: self.newsController.view];
		self.newsController.view.backgroundColor = NAVBAR_COLOR;
		self.newsController.tableView.backgroundColor = NAVBAR_COLOR;
//		self.newsController.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, -20);
//		self.newsController.tableView.clipsToBounds = NO;
//		self.newsController.view.clipsToBounds = NO;
		
		self.detailsController = [[StockDetailsViewController alloc] initWithNibName:nil bundle:nil];
		[self.bottomScroller addSubview: self.detailsController.view];
		self.detailsController.view.backgroundColor = NAVBAR_COLOR;

		self.bigScroller = [[BigChartScroller alloc] init];
		self.bigScroller.alpha = 0.0;
		self.bigScroller.tag = 100;
		self.bigScroller.backgroundColor = [UIColor blackColor];
		[self addSubview: self.bigScroller];
		self.bigScroller.frame = CGRectMake(0,-20,[UIScreen mainScreen].bounds.size.height , [UIScreen mainScreen].bounds.size.width);
		
		NSLog(@"BIG SCROLLER width = %f height = %f", [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
		
		[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(panCrossChairBegan:) name:@"START_SCROLL" object: nil];
		[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(panCrossChairEnded:) name:@"END_SCROLL" object: nil];
//
//		self.testImage = [[UIImageView alloc] init];
//		[self addSubview: self.testImage];
		
		UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(headerTapped)];
		UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(headerTapped)];
		[self.chartPanel.chartHeader addGestureRecognizer: tapGesture];
		[self.chartPanel.newsToggleBtn addGestureRecognizer: tapGesture2];

		UIPanGestureRecognizer *tapGesture3 = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(headerDragged:)];
		[self.chartPanel.chartHeader addGestureRecognizer: tapGesture3];

		
		self.emptyListLabel = [[UILabel alloc] initWithFrame: CGRectMake(0,0,200,60)];
		[self addSubview: self.emptyListLabel];
		self.emptyListLabel.backgroundColor = [UIColor clearColor];
		self.emptyListLabel.font = LIST_SYMBOL_FONT;
		self.emptyListLabel.textColor = CELL_TEXTCOLOR;
		self.emptyListLabel.text = @"Tap (+) to add stocks.";
		self.emptyListLabel.hidden = YES;
		self.emptyListLabel.textAlignment = NSTextAlignmentCenter;
		
		self.pageControl = [[UIPageControl alloc] initWithFrame: CGRectMake(0,0,50,20)];
		self.pageControl.numberOfPages = 2;
//		self.pageControl.backgroundColor = [UIColor yellowColor];
		[self insertSubview: self.pageControl aboveSubview:self.bottomScroller];
		self.pageControl.pageIndicatorTintColor = [UIColor colorWithRGBHex:0x450780];
	}
	return self;
}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (void) panCrossChairBegan: (NSNotification*) notif
{
	UIInterfaceOrientation toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];

	if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
	{
		ECSlidingViewController *controller = (ECSlidingViewController*)ROOT_VIEWCONTROLLER();
		controller.panGesture.enabled = NO;
	}
	
}

- (void) panCrossChairEnded: (NSNotification*) notif
{
	UIInterfaceOrientation toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
	
	if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
	{
		ECSlidingViewController *controller = (ECSlidingViewController*)ROOT_VIEWCONTROLLER();
		controller.panGesture.enabled = YES;
	}
	
}

- (void) manualLayouts: (UIInterfaceOrientation) interfaceOrientation
{
	if(UIInterfaceOrientationIsPortrait(interfaceOrientation))
	{
		//int gap = 1;
		//self.stockList.alpha = 1.0;
		//int h = 60*NUM_OF_ITEMS;
		int chartHeight = 237;
		int bottomHeight = self.height - chartHeight - 44;
		float targetY = self.height - chartHeight;

		self.navBar.frame = CGRectMake(0, -20, self.width, 44+20);
		self.containerView.frame = CGRectMake(0, -20, self.width, targetY);

		//		CGRect bottomFrame = CGRectMake(0, self.stockList.bottom+gap, self.width, self.height - h);
//		self.bottomScroller.frame = bottomFrame;
		
		if(!opened)
		{
			self.chartPanel.frame = CGRectMake(0, self.navBar.bottom, self.width, chartHeight);
			self.stockList.frame = CGRectMake(0, self.chartPanel.bottom, self.width, bottomHeight);
		}
		
		self.bottomScroller.frame = self.containerView.bounds;
		self.bottomScroller.contentSize = CGSizeMake(self.width * 2, self.containerView.height);
		self.newsController.view.frame = CGRectMake(self.bottomScroller.width,60, self.bottomScroller.width, targetY-80);
		self.detailsController.view.frame = CGRectMake(0,0, self.bottomScroller.width, targetY-20);
		self.newsLabel.frame = CGRectMake(self.bottomScroller.width,0, self.width, 44);
		self.pageControl.frame = CGRectMake((self.width - 50)/2, targetY - 20, 50, 20);
		[self centerSubview: self.emptyListLabel];
		
		//		self.bigScroller.alpha = 0.0;
		
//		bottomRect = bottomFrame;
	}
	else
	{
		self.bigScroller.frame = CGRectMake(0,0,[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width);
		self.bigScroller.alpha = 1.0;
		//self.stockList.alpha = 0.0;
	}
}
- (void) layoutSubviews
{
	if(!rotating)
	{
		[super layoutSubviews];

		UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
		[self manualLayouts: interfaceOrientation];
		[self.chartPanel reloadChart];

		NSLog(@"MainViewIPhone::layout %d", rotating);
	}
}

#pragma mark - ChartPanel Delegates

- (void) chartPanelPeriodictyChanged: (PriceType) periodicity_  symbol: (NSString*) symbol
{
	periodicity = periodicity_;
	[self getPrice: symbol periodicity: periodicity_];
}

- (void) chartPanelAnnotationSelected: (AnnotationGraphics*) selected
{
	
}

#pragma mark - StockList Delegates

- (void) stockListSymbolClicked: (NSString*) symbol
{
	[self getChartData: symbol];

	NSDictionary *dict = [self.stockList dataForSymbol: symbol];

	self.detailsController.openValue.text = dict[@"Open"];
	self.detailsController.highValue.text = dict[@"High"];
	self.detailsController.lowValue.text = dict[@"Low"];
	self.detailsController.volValue.text = dict[@"Volume"];
	self.detailsController.peValue.text = dict[@"P/E"];
	self.detailsController.mktcapValue.text = dict[@"MarketCap"];
	self.detailsController.w52highValue.text = dict[@"52WeekHigh"];
	self.detailsController.w52lowValue.text = dict[@"52WeekLow"];
	self.detailsController.avgVolValue.text = dict[@"AvgVol"];
	self.detailsController.yieldValue.text = dict[@"Yield"];
}

- (void) stockListUpdateList: (TableData*) symbols
{
	//[self.bigScroller setSymbolData: symbols];
	self.chartPanel.hidden = (symbols.rowList.count == 0);
	self.bottomScroller.hidden = self.chartPanel.hidden;
	self.containerView.hidden = self.chartPanel.hidden;
	self.emptyListLabel.hidden = !self.chartPanel.hidden;
	
	// update numbers on the left
	ECSlidingViewController *controller = (ECSlidingViewController*)ROOT_VIEWCONTROLLER();
	LeftMenuViewController *leftMenu = (LeftMenuViewController*) controller.underLeftViewController;
	[leftMenu reloadWithSelection];
}

#pragma mark - News Delegates

- (void) newsViewDidSelect:(NewsData *)news
{
    if(self.newsReader == nil)
    {
        self.newsReader = [[[NewsReaderView alloc] init] autorelease];
        self.newsReader.delegate = self;
        
        CGRect endRect = CGRectMake(0,0, self.width, self.height);
        CGRect startRect = CGRectSetY(endRect, self.height);
        self.newsReader.frame = startRect;
        [self addSubview: self.newsReader];
        
        [UIView beginAnimations:nil context:@"MINE"];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDuration: .25];
        self.newsReader.frame = endRect;
        [UIView commitAnimations];
    }
    
    [self.newsReader loadURL: news.url];
}

- (void) newsReaderViewCloseClicked:(id)sender
{
    CGRect endRect = CGRectMake(0,0, self.width, self.height);
    CGRect startRect = CGRectSetY(endRect, self.height);
	
    [UIView animateWithDuration: 0.25
                     animations:^{
                         self.newsReader.frame = startRect;
                     }
                     completion:^(BOOL finished){
                         [self.newsReader removeFromSuperview];
                         self.newsReader = nil;
                     }];
}

#pragma mark - Rotation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration;
{
	NSLog(@"%s", __FUNCTION__);
	rotating = YES;
	
	if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
	{
		[[UIApplication sharedApplication] setStatusBarHidden: NO withAnimation: UIStatusBarAnimationNone];
		
		self.testImage.image = [self.chartPanel generateImage];
		self.testImage.alpha = 1.0;
		self.bigScroller.alpha = 0.0;
		self.testImage.frame = self.bounds;
		self.bigScroller.alpha = 0.0;
	}
	else
	{
		[[UIApplication sharedApplication] setStatusBarHidden: YES withAnimation: UIStatusBarAnimationNone];

		[self.bigScroller setSymbolData: self.stockList.rows];
		
		self.stockList.alpha = 0.0;
		self.bottomScroller.alpha = 0.0;
		self.bigScroller.alpha = 1.0;

//		self.testImage.image = [[self.bigScroller getCurrentCell].chartPanel generateImage];
//		self.testImage.alpha = 1.0;
//		self.testImage.frame = self.bottomScroller.frame;
	}

	[self bringSubviewToFront: self.testImage];
	
//	self.bigScroller.layer.anchorPoint = CGPointMake(0,0);
//	self.bigScroller.alpha = 0.0;
//	self.stockList.alpha = 0.0;
//	self.bottomScroller.alpha = 0.0;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{

	UIInterfaceOrientation toInterfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
	NSLog(@"%s portrait %d", __FUNCTION__, UIInterfaceOrientationIsPortrait(toInterfaceOrientation));

	ECSlidingViewController *controller = (ECSlidingViewController*)ROOT_VIEWCONTROLLER();
	controller.panGesture.enabled = UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
	controller.topViewSnapshotPanGesture.enabled = controller.panGesture.enabled;
	
//	[UIView animateWithDuration: 0.0 animations:^{
		if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
		{
			[UIView animateWithDuration:0.40 animations:^{
				self.stockList.alpha = 1.0;
				self.bottomScroller.alpha = 1.0;
			} completion:^(BOOL finished) {
				self.testImage.alpha = 0.0;
				rotating = NO;
			}];				
		}
		else
		{
			self.bigScroller.alpha = 1.0;
			self.testImage.alpha = 0.0;

//			[UIView animateWithDuration:0.40 animations:^{
//			}
//			 completion:^(BOOL finished) {
//				 rotating = NO;
//			 }];
		}
//	}];
	
	//[self manualLayouts: toInterfaceOrientation];
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	NSLog(@"%s", __FUNCTION__);

	if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
	{
//		[UIView animateWithDuration: duration animations:^{
//		}];
//		
//		newRect.size.width = bottomRect.size.height;
//		newRect.size.height = bottomRect.size.width;
//		self.bigScroller.alpha = 0.0;
		//self.stockList.alpha = 1.0;
		CGRect newRect = bottomRect;
//		self.bigScroller.frame = newRect;
		self.testImage.frame = newRect;
	}
	else
	{
//		self.bigScroller.alpha = 1.0;
//		self.bigScroller.frame = self.bounds;
		//self.stockList.alpha = 0.0;
		self.testImage.frame = self.bounds;
	}
}

#pragma mark - ScrollView Delegates

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	int page = (scrollView.contentOffset.x / scrollView.width);
	self.pageControl.currentPage = page;
}

#pragma mark - Button Delegates

- (void) headerTapped
{
	[self setNewsState: !opened];
	opened = !opened;
}

- (void) headerDragged: (UIPanGestureRecognizer*) gesture
{
	CGPoint location = [gesture locationInView: self];

	if(gesture.state == UIGestureRecognizerStateBegan)
	{
	}
	else if(gesture.state == UIGestureRecognizerStateChanged)
	{
		float maxY = self.height - self.chartPanel.height;
		float targetY = location.y;
		self.chartPanel.frame = CGRectSetY(self.chartPanel.frame, MAX(self.navBar.bottom, MIN(targetY, maxY)));
		self.stockList.frame = CGRectSetY(self.stockList.frame, self.chartPanel.bottom);
		
		float p = (targetY-self.navBar.bottom)/(maxY-self.navBar.bottom);
		self.navBar.alpha = (1 - p);
	}
	else if(gesture.state == UIGestureRecognizerStateEnded)
	{
		CGPoint dir = [gesture velocityInView: self];
		if(dir.y > 0)
		{
			[self setNewsState: YES];
			opened = YES;
		}
		else
		{
			[self setNewsState: NO];
			opened = NO;
		}
	}
}

- (void) toggleListView
{
	ECSlidingViewController *controller = (ECSlidingViewController*)ROOT_VIEWCONTROLLER();
	LeftMenuViewController *leftMenu = (LeftMenuViewController*) controller.underLeftViewController;

//	leftMenu.view.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(-controller.anchorRightRevealAmount*1.3, 0), 1.3, 1.3);
//	leftMenu.view.frame = [UIScreen mainScreen].applicationFrame;
//	leftMenu.view.backgroundColor = LEFT_BGCOLOR;
//	[UIApplication sharedApplication].keyWindow.backgroundColor = LEFT_BGCOLOR;
	
	[controller anchorTopViewTo:ECRight animations:^{
//		NSArray *cells = [leftMenu.tableView visibleCells];
//		CGFloat startTime = 0;
//		for(UITableViewCell *cell in cells)
//		{
//			UILabel *textLabel  = cell.textLabel;
//			textLabel.transform = CGAffineTransformScale(CGAffineTransformMakeTranslation(50, 0), 0.2, 0.2);
//			textLabel.alpha = 0.0;
//			startTime += 0.040;
//			[UIView animateWithDuration: 0.40 delay:startTime options:UIViewAnimationCurveLinear
//							 animations:^{
//								 textLabel.transform = CGAffineTransformIdentity;
//								 textLabel.alpha = 1.0;
//							 } completion:^(BOOL finished) {
//							 }];
//		}
//
//		leftMenu.view.transform = CGAffineTransformIdentity; //CGAffineTransformMakeScale(1.0, 1.0);
	} onComplete:^{
	}];
}

- (void) addButtonClicked
{
	[self.stockList displaySymbolSearch];
}

#pragma mark - Private Functions

- (PriceData*) getPriceDataForSymbol: (NSString*) symbol
{
	NSDictionary *dict = [self.stockList dataForSymbol: symbol];
	
	if(dict)
	{
		PriceData *priceData = [[[PriceData alloc] init] autorelease];
		priceData.close = [[dict objectForKey:@"Last"] floatValue];
		
		return priceData;
	}
	
	return nil;
}

- (void) setNewsState: (BOOL) open
{
	void (^completeBlock)(BOOL) = ^(BOOL completed){
		if(opened)
		{
			self.chartPanel.newsToggleBtn.backgroundColor = NEWS_TOGGLE_BG_COLOR;
		}
		else
		{
			self.chartPanel.newsToggleBtn.backgroundColor = [UIColor clearColor];
		}
		self.chartPanel.newsToggleBtn.selected = opened;
	};
	
	if(open)
	{
		float targetY = self.height - self.chartPanel.height;
		[UIView animateWithDuration:0.25 delay: 0 options:UIViewAnimationOptionCurveLinear
						 animations:^{
							 self.chartPanel.frame = CGRectSetY(self.chartPanel.frame, targetY);
							 self.stockList.frame = CGRectSetY(self.stockList.frame, self.chartPanel.bottom);
							 self.navBar.alpha = 0.0;
						 } completion: completeBlock];
		[self.newsController loadData: self.currentSymbol];
	}
	else
	{
		[UIView animateWithDuration:0.25 delay: 0 options:UIViewAnimationOptionCurveLinear
						 animations:^{
							 self.chartPanel.frame = CGRectSetY(self.chartPanel.frame, self.navBar.bottom);
							 self.stockList.frame = CGRectSetY(self.stockList.frame, self.chartPanel.bottom);
							 self.navBar.alpha = 1.0;
						 } completion: completeBlock];
	}
}

#pragma mark - Public Functions

- (void) setData: (UserList*) userList
{
	self.navBar.topItem.title = userList.listName;
	self.userList = userList;
	[self.stockList setData: self.userList];
	
	self.chartPanel.hidden = (userList.listItems.count == 0);
	self.bottomScroller.hidden = self.chartPanel.hidden;
	self.containerView.hidden = self.chartPanel.hidden;
	self.emptyListLabel.hidden = !self.chartPanel.hidden;
}

- (void) getPrice: (NSString*) symbol periodicity: (PriceType) priceType
{
	self.currentSymbol = symbol;
	
	[[ServiceManager instance].asyncQueue cancelAllOperations];
    [[ServiceManager instance] getChartData: symbol
								periodicity: priceType
								  withBlock: ^(StockPriceData *data)
	 {
         [self.chartPanel setData: data];
		 PriceData *priceData = [self getPriceDataForSymbol: symbol];
		 [self.chartPanel setLastTick: priceData];
		 
     }
							 withErrorBlock: ^(NSString *error)
     {
         
     }];
}

- (void) getChartData:(NSString *)symbol
{
	[self getPrice: symbol periodicity: periodicity];
}

- (void) reloadData
{
	[self setData: self.userList];
}
@end
