//
//  SymbolSearchPhone.m
//  TSCharts
//
//  Created by Quoc Le on 6/30/13.
//
//

#import "SymbolSearchPhone.h"
#import "ServiceManager.h"
#define SEARCH_TIMER YES
#import "ColorScheme.h"

@interface SymbolSearchPhone()
@property (strong, nonatomic) NSTimer *lastSymbolQueried;
@property (strong, nonatomic) NSString *lastSymbolInSearch;

@end

@implementation SymbolSearchPhone

- (id)init
{
    self = [super init];
    if (self) {
		self.backgroundColor = [UIColor clearColor];
		self.exclusiveTouch = YES;
		self.clipsToBounds = YES;
//		self.filterView = [[[UIView alloc] init] autorelease];
//		self.filterView.backgroundColor = [UIColor blackColor];
//		self.filterView.alpha = 0.7f;
//		[self addSubview: self.filterView];
		
		self.whiteView = [[[UIView alloc] init] autorelease];
		self.whiteView.backgroundColor = [UIColor clearColor];
		[self addSubview: self.whiteView];
		
		self.symbolSearch = [[[SymbolSearchViewController alloc] init] autorelease];
		self.symbolSearch.delegate = self;
		[self addSubview: self.symbolSearch.view];
		self.symbolSearch.tableView.exclusiveTouch = YES;
		
		self.inputBox = [[[UISearchBar alloc] init] autorelease];
		self.inputBox.delegate = self;
		self.inputBox.showsCancelButton = YES;
		self.inputBox.prompt = @"Type a company name or stock symbol.";
		self.inputBox.barTintColor = [UIColor whiteColor];
		self.inputBox.tintColor = [UIColor purpleColor];
		self.inputBox.translucent = YES;
		self.inputBox.barStyle = UISearchBarStyleProminent;
		self.inputBox.autocorrectionType = UITextAutocorrectionTypeNo;
		self.inputBox.autocapitalizationType = UITextAutocapitalizationTypeNone;
		self.inputBox.searchTextPositionAdjustment = UIOffsetMake(5, 0);
		
		[self.inputBox setScopeBarButtonTitleTextAttributes: @{UITextAttributeFont :
																   [UIFont systemFontOfSize:12],
															   UITextAttributeTextColor:
																   CELL_TEXTCOLOR
															   } forState: UIControlStateNormal];

		[[UILabel appearanceWhenContainedIn:[UISearchBar class], nil] setFont: [UIFont systemFontOfSize:12]];

		[self.inputBox setSearchFieldBackgroundImage:[UIImage imageNamed:@"inputbox"] forState: UIControlStateNormal];
		[[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
																									  [UIColor colorWithRGBHex:0x2A2A2A],
																									  UITextAttributeTextColor,
																									  [UIFont systemFontOfSize:14],
																									  UITextAttributeFont,
																									  [UIColor clearColor],
																									  UITextAttributeTextShadowColor,
																									  [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
																									  UITextAttributeTextShadowOffset,
																									  nil]
																							forState:UIControlStateNormal];
		//[self.inputBox setBackgroundColor: [UIColor whiteColor]];
		[self addSubview: self.inputBox];
		
//		self.cancelButton = [UIButton buttonWithType: UIButtonTypeCustom];
//		[self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
//		[self.cancelButton addTarget: self action:@selector(cancelClicked:) forControlEvents: UIControlEventTouchUpInside];
//		[self addSubview: self.cancelButton];
    }
    return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	float margin = 14;
	self.filterView.frame = self.bounds;
	self.whiteView.frame = CGRectMake(margin, 64, self.width - (margin*2), 64);
	self.inputBox.frame = CGRectMake(margin, 64, self.width - (margin*2), 64);
//	self.cancelButton.frame = CGRectMake(self.inputBox.right + 10, 64, 65, 44);
	self.symbolSearch.view.frame = CGRectMake(margin, self.inputBox.bottom, self.width - (margin*2), self.height);
}

- (void) dealloc
{
	self.symbolSearch = nil;
	self.inputBox = nil;
	self.cancelButton = nil;
	[super dealloc];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
	SymbolSearchResultData *data = (self.symbolSearch).searchData;
    if(data.results.count == 1)
    {
        [self symbolSearchSelected: [data.results objectAtIndex:0]];
    }
	return YES;
}

- (void) getSearchDataWithTimer: (NSTimer*) timer
{
    NSString *text = [[timer userInfo] objectForKey:@"text"];
    
    [[ServiceManager instance] getYahooSymbolSearchData: text
										 withBlock:^(SymbolSearchResultData *data) {
											 [self.symbolSearch setData: data];
										 }
									withErrorBlock:^(NSString *error) {
										
									}];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	NSLog(@"Search %@", searchText);
	
	if(self.lastSymbolQueried != nil)
    {
        [self.lastSymbolQueried invalidate];
        self.lastSymbolQueried = nil;
    }
    
    if(searchText == nil || [searchText isEqualToString:@""])
    {
        [self.symbolSearch showRecentResults];
		return;
    }
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:searchText forKey: @"text"];
    
    if(SEARCH_TIMER)
    {
        self.lastSymbolQueried = [NSTimer scheduledTimerWithTimeInterval: 0.20f
                                                                  target: self
                                                                selector:@selector(getSearchDataWithTimer:)
                                                                userInfo: userInfo
                                                                 repeats: NO];
    }
    else
    {
        [[ServiceManager instance] getYahooSymbolSearchData: searchText
                                             withBlock:^(SymbolSearchResultData *data) {
                                                 [(SymbolSearchViewController*)self.symbolSearch setData: data];
                                             }
                                        withErrorBlock:^(NSString *error) {
                                            
                                        }];
    }

}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	[self.delegate symbolSearchCancelClicked: self];
}

- (void) symbolSearchSelected: (SymbolData*) symbol;
{
	[self.delegate symbolSearchSymbolClicked: symbol.symbol];
}
@end
