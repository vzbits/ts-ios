//
//  StockListCell.h
//  TSCharts
//
//  Created by Quoc Le on 7/31/13.
//
//

#import <UIKit/UIKit.h>
#import "ColorScheme.h"
#import "UserList.h"

@protocol StockListItemCellDelegate < NSObject>
- (void) stocklistItemCellEditClicked: (id) sender;
- (void) stocklistItemCellTrashClicked: (id) sender;
@end

@interface StockListItemCell : UITableViewCell
@property (strong,nonatomic) UIView *separator;
@property (strong,nonatomic) UIView *separatorTop;
@property (strong,nonatomic) UIButton *trashCan;
@property (strong,nonatomic) UIButton *editBtn;
@property (strong,nonatomic) UIButton *rightBox;
@property (strong,nonatomic) UILabel *priceLabel;
@property (assign,nonatomic) NSInteger slideStopPosition;
@property (strong,nonatomic) UIImageView *bellImage;

@property (assign,nonatomic) id<StockListItemCellDelegate> delegate;

- (void) setData: (NSDictionary*) object
	  toggleType: (ListPriceToggleType) toggleType
		viewType: (ListViewType) viewType;

@end
