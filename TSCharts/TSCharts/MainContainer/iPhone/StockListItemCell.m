//
//  StockListCell.m
//  TSCharts
//
//  Created by Quoc Le on 7/31/13.
//
//

#import "StockListItemCell.h"
#import "ColorScheme.h"
#import "UIImageExtensions.h"


@implementation StockListItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
	{
		self.backgroundColor = CELL_SLIDE_BGCOLOR;
		self.textLabel.adjustsFontSizeToFitWidth = YES;
		self.textLabel.backgroundColor = [UIColor clearColor];
		self.selectionStyle = UITableViewCellSelectionStyleGray;
		//self.selectedBackgroundView = [[[UIView alloc] init] autorelease];
		//self.selectedBackgroundView.backgroundColor = [self.backgroundColor colorWithBrightness: 0.90];
		self.textLabel.shadowOffset = CGSizeZero;
		self.textLabel.textColor = CELL_TEXTCOLOR;
		
		self.trashCan = [[[UIButton alloc] init] autorelease];
		[self.trashCan addTarget: self action:@selector(trashCanClicked:) forControlEvents:UIControlEventTouchUpInside];
		[self.trashCan setImage:[[UIImage imageNamed:@"trash_icon"] getImageWithTint: [UIColor whiteColor] withIntensity: 1.0] forState: UIControlStateNormal];
		self.trashCan.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 55, (NORMAL_CELL_FINISHING_HEIGHT-44)/2, 44, 44);
		//self.trashCan.tintAdjustmentMode = UIViewTintAdjustmentModeNormal;
		self.trashCan.tintColor = [UIColor whiteColor];
		//[cell insertSubview: trashCan belowSubview: cell.selectedBackgroundView];
		[self insertSubview: self.trashCan belowSubview: self.backgroundView];

		self.editBtn = [[[UIButton alloc] init] autorelease];
		[self.editBtn addTarget: self action:@selector(editClicked:) forControlEvents:UIControlEventTouchUpInside];
		[self.editBtn setImage:[[UIImage imageNamed:@"pencil_icon"] getImageWithTint: [UIColor whiteColor] withIntensity: 1.0]  forState: UIControlStateNormal];
		self.editBtn.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 105, (NORMAL_CELL_FINISHING_HEIGHT-44)/2, 44, 44);
		self.editBtn.tintColor = [UIColor whiteColor];
		//[self insertSubview: self.editBtn belowSubview: cell.selectedBackgroundView];
		[self insertSubview: self.editBtn belowSubview: self.backgroundView];
		
		self.rightBox = [[[UIButton alloc] init] autorelease];
		self.rightBox.tag = 100;
		self.rightBox.frame = CGRectMake(320 - 60 - 16, 10, 60, 40);
		self.rightBox.titleLabel.shadowOffset = CGSizeZero;
		[self.rightBox setTitleColor:CELL_TEXTCOLOR forState: UIControlStateNormal];
		[self.rightBox setTitle:@"" forState: UIControlStateNormal];
		self.rightBox.layer.cornerRadius = 3;
		self.rightBox.titleLabel.textAlignment = UITextAlignmentRight;
		self.rightBox.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
		self.rightBox.titleLabel.adjustsFontSizeToFitWidth = YES;
		self.rightBox.backgroundColor = BOX_COLOR;
		self.rightBox.titleLabel.font = LIST_RIGHTBOX_FONT;
		[self.rightBox addTarget: nil action:@selector(togglePriceBox) forControlEvents:UIControlEventTouchUpInside];
		[self.contentView addSubview: self.rightBox];
		
		self.priceLabel = [[[UILabel alloc] init] autorelease];
		self.priceLabel.tag = 101;
		self.priceLabel.shadowOffset = CGSizeZero;
		self.priceLabel.frame = CGRectMake(160, 10, 60, 40);
		self.priceLabel.backgroundColor = [UIColor clearColor];
		self.priceLabel.textAlignment = UITextAlignmentRight;
		self.priceLabel.textColor = CELL_TEXTCOLOR;
		self.priceLabel.font = LIST_PRICE_FONT;
		
		self.bellImage = [[[UIImageView alloc] initWithImage: [[UIImage imageNamed:@"bell.png"] getImageWithTint: CELL_TEXTCOLOR withIntensity: 0.7]] autorelease];
		[self.contentView addSubview: self.bellImage];
		
		[self.contentView addSubview: self.priceLabel];
		self.detailTextLabel.backgroundColor = [UIColor clearColor];
		
		self.separator = [[UIView alloc] initWithFrame: CGRectMake(0,0,self.width, 0.5f)];
		self.separator.backgroundColor = DIVIDER_COLOR;
		[self addSubview: self.separator];
		
		self.separatorTop = [[UIView alloc] initWithFrame: CGRectMake(0,0,self.width, 0.5f)];
		self.separatorTop.backgroundColor = DIVIDER_COLOR;
		[self addSubview: self.separatorTop];

        self.textLabel.font = LIST_SYMBOL_FONT;
    }
    return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	self.separator.frame = CGRectSetY(self.separator.frame,  self.height - 0.5f);
	self.rightBox.frame = CGRectMake(self.width - 75 - 16, (self.height - 40)/2, 75, 40);
//	[self.priceLabel centerVertical];
//	[self.textLabel centerVertical];
//	[self.rightBox centerVertical];

	self.priceLabel.frame = CGRectSetY(self.priceLabel.frame,  ((self.height - self.priceLabel.height) /2) + 3);
	self.textLabel.frame = CGRectSetY(self.textLabel.frame,  ((self.height - self.textLabel.height) /2) + 3);
	self.rightBox .frame = CGRectSetY(self.rightBox.frame,  ((self.height - self.rightBox .height) /2) + 3);
	
	CGSize sizeOfSymbol = [self.textLabel sizeThatFits: CGSizeMake(0, self.priceLabel.height)];
	self.bellImage.frame = CGRectMake(sizeOfSymbol.width + self.textLabel.x + 6, -2 + (self.height-15)/2, 15,15);

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];

	if(!selected)
		self.contentView.backgroundColor = self.backgroundColor;
	else
		self.contentView.backgroundColor = CELL_SELCOLOR;
	
	self.separator.backgroundColor = selected ? DIVIDER_COLOR2 : DIVIDER_COLOR;
	self.separatorTop.backgroundColor = selected ? DIVIDER_COLOR2 : DIVIDER_COLOR;

}

- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
	
}

- (void) setData: (NSDictionary*) object
	  toggleType: (ListPriceToggleType) toggleType
		viewType: (ListViewType) viewType;
{
	if([object[@"Symbol"] isEqualToString: @"Dummy"])
	{
		self.backgroundColor = CELL_BGCOLOR2;
		self.textLabel.text = @"";
		self.priceLabel.text = @"";
		self.detailTextLabel.text = @"";
		[self.rightBox setTitle: @"" forState:UIControlStateNormal];
		
		return;
	}
	
	self.backgroundColor = CELL_BGCOLOR;

	if(viewType == ListViewType_List)
	{
		self.editBtn.enabled = NO;
	}
	else
	{
		self.editBtn.enabled = YES;
	}
	
	self.textLabel.text = [NSString stringWithFormat:@"%@", object[@"Symbol"]];
	
	if(toggleType == ListPriceToggle_Dollar)
	{
		NSString *text = [NSString stringWithFormat:@"%@", object[@"Change"] ? object[@"Change"] : @""];
		[self.rightBox setTitle: text forState:UIControlStateNormal];
		
		UIColor *color = [text doubleValue] > 0 ? CELL_UPCOLOR : CELL_DOWNCOLOR;
		[self.rightBox setTitleColor: color forState: UIControlStateNormal];
	}
	if(toggleType == ListPriceToggle_Percentage)
	{
		NSString *text = [NSString stringWithFormat:@"%@", object[@"ChangeP"] ? object[@"ChangeP"] : @""];
		[self.rightBox setTitle: text forState:UIControlStateNormal];
		
		UIColor *color = [text doubleValue] > 0 ? CELL_UPCOLOR : CELL_DOWNCOLOR;
		[self.rightBox setTitleColor: color forState: UIControlStateNormal];
	}
	if(toggleType == ListPriceToggle_MKCap)
	{
		NSString *text = [NSString stringWithFormat:@"%@", object[@"MarketCap"] ? object[@"MarketCap"] : @""];
		[self.rightBox setTitle: text forState:UIControlStateNormal];
	}

	self.priceLabel.text = [NSString stringWithFormat:@"%.2f", [object[@"Last"] floatValue]];
	self.detailTextLabel.text =  [NSString stringWithFormat:@"%@", object[@"Name"]];
	

/*
	if([object[@"Symbol"] isEqualToString: DONE_CELL[@"Symbol"]])
	{
		self.contentView.backgroundColor = [UIColor darkGrayColor];
	} else
	{
		if([object[@"Symbol"] isEqualToString: DUMMY_CELL[@"Symbol"]])
		{
			self.textLabel.text = @"";
			self.contentView.backgroundColor = [UIColor clearColor];
		} else {
			//            cell.textLabel.textColor = [UIColor whiteColor];
			self.contentView.backgroundColor = CELL_COLOR;
		}
	}
*/
}

- (void) trashCanClicked: (id) sender {
	[self.delegate stocklistItemCellTrashClicked: self];
}

- (void) editClicked :(id) sender {
	[self.delegate stocklistItemCellEditClicked: self];
}

@end
