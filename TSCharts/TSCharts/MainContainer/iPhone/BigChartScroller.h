//
//  BigChartScroller.h
//  TSCharts
//
//  Created by Quoc Le on 6/30/13.
//
//

#import <UIKit/UIKit.h>
#import "BigChartCell.h"
#import "InfiniteScroller.h"

@interface BigChartScroller : UIView <InfiniteScrollerDataSource>
@property (strong, nonatomic) TableData *data;
@property (strong, nonatomic) InfiniteScroller *collectionView;
- (void) setSymbolData: (TableData*) symbols;

- (BigChartCell*) getCurrentCell;
@end
