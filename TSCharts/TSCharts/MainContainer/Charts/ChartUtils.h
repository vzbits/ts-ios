//
//  ChartUtils.h
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "service.h"

#define PP(x) x+0.5

@interface ChartUtils : NSObject
+ (NSString*) formatYAxis: (float) val twoDecimal: (BOOL) two;
+ (NSString*) formatCurrentPrice: (float) val;
+ (UIColor*) convertColor: (NSString*) str;
+ (float) pp: (float) pixel;
+ (NSString*) arrayToString: (NSArray*) arr;
+ (NSDate*) getStartDateForTick: (int) ticks periodicity: (PriceType) priceType;
@end
