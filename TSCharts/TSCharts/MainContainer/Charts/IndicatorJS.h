//
//  IndicatorJS.h
//  TSCharts
//
//  Created by Quoc Le on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <jsapi.h>
#include <stdlib.h>  
#include <string.h>
#import <string>
#import "Indicator.h"
//#import "ServicePb.pb.h"
#import "service.h"
#import "SimpleValue.h"

//using namespace vsi;

class IndicatorJS
{
    JSContext *context;
    JSRuntime *runtime;
    JSObject *jsObj;
    JSObject *vsiObj;
    
    std::string documentsDir;

    void download_script(std::string script, bool engine=false);
    std::string build_remote_url(std::string script, bool engine);

    bool file_exists(std::string script);
    std::string get_absolute(std::string script);
    bool buildjs_runtime();
    
    NSMutableArray *indicators;
    
    JSObject *stockPriceToJSArray(StockPriceData *data);
    std::string lastIdentifier;
public:
    static IndicatorJS *instance()
    {
        static IndicatorJS instance;
        return &instance;
    }
    
    IndicatorJS();
    JSBool execute_file(JSContext *ctx, const char *file);
    void add_indicator(std::string identifier);
    void get_indicator();
    void run_indicator(StockPriceData *data);
    void load_settings();
    void set_inputs(int index);
    void set_inputs();
    void remove_indicator(int index);
    void remove_all();
    
    NSMutableArray *get_data()
    {
        return indicators;
    }
};
