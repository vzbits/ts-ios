//
//  ScalerLinear.m
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ScalerLinear.h"

@implementation ScalerLinear

+ (ScalerLinear*) scaleForDomain: (NSArray*) domain rangeFrom: (float) from rangeTo: (float) to
{
    ScalerLinear *ordinal = [[[ScalerLinear alloc] init] autorelease];
    ordinal.domain = domain;
    ordinal.rangeFrom = from;
    ordinal.rangeTo  = to;
    ordinal.domainRange = fabs([[domain objectAtIndex:0] floatValue] - [[domain objectAtIndex:1] floatValue]);
    
    ordinal.step = abs(from-to) / ordinal.domainRange;
    return ordinal;
}

- (float) toPixel: (float) value
{
    float d_from = [[self.domain objectAtIndex:0] floatValue];    
    //NSLog(@"Domain range = %f step=%f value=%f", self.domainRange, self.step, value);
    
    float diff = (self.step * (value-d_from));
    if(self.rangeFrom > self.rangeTo)
    {
        float r = self.rangeFrom - diff;
        //return MAX(self.rangeTo, MIN(self.rangeFrom, r));
        return r;
    }
    else
    {
        float r = self.rangeFrom + diff;
        //return MIN(self.rangeTo, MAX(self.rangeFrom, r));
        return r;
    }
}

- (float) toValue: (float) y
{
    float diff;
    if(self.rangeFrom > self.rangeTo)
        diff = self.rangeFrom - y;
    else
        diff = y - self.rangeFrom;
    
    float d_from = [[self.domain objectAtIndex:0] floatValue];
    float value = (diff/ self.step) + d_from;    
    return value;
}

- (float) nicenum:(float) x round: (bool)  round
{
    int exp;
    float f;
    float nf;
    
    exp = floor(log10(x));
    f   = x / pow(10,exp);
    
    if(round)
    {
        if (f < 1.5) nf = 1;
        else if (f < 3) nf = 2;
        else if (f < 7) nf = 5;
        else nf = 10;
    }
    else 
    {
        if (f <= 1) nf = 1;
        else if (f <= 2) nf = 2;
        else if (f <= 5) nf = 5;
        else nf = 10;
    }
    
    return nf*pow(10, exp);
}

- (NSArray*) getNiceGrids: (int) ntick
{
    NSMutableArray *niceGrids = [NSMutableArray array];
    float min = [[self.domain objectAtIndex:0] floatValue];
    float max = [[self.domain objectAtIndex:1] floatValue];    
    float graphMin, graphMax;
    float nfrac;
    float d;
    float range;
    
    range = [self nicenum: (max - min) round: false];
    d     = [self nicenum: range/(ntick -1) round: true];
    graphMin = floor(min/d)*d;
    graphMax = ceil(max/d)*d;
    nfrac = MAX(-floor(log10(d)), 0);
    
    for(float x = graphMin; x <= graphMax + 0.5*d; x += d)
    {
        //NSLog(@"tick at %f", x);
        [niceGrids addObject: [NSNumber numberWithFloat: x]];
    }
    //NSLog(@"Grids = %f", n2);
    
    return niceGrids;
}
@end
