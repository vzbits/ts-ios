//
//  ScalerOrdinal.h
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScalerOrdinal : NSObject

@property (strong, nonatomic) NSArray *domain;
@property (assign, nonatomic) int rangeFrom;
@property (assign, nonatomic) int rangeTo;
@property (assign, nonatomic) int step;

+ (ScalerOrdinal*) scaleForDomain: (NSArray*) domain rangeFrom: (int) from rangeTo: (int) to;
- (float) toPixelByObject: (id) value;
- (id) toObjectByPixel: (float) pixel;
@end
