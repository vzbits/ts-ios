//
//  ScalerOrdinal.m
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ScalerOrdinal.h"

@implementation ScalerOrdinal

@synthesize domain = _domain;
@synthesize rangeFrom = _rangeFrom;
@synthesize rangeTo   = _rangeTo;
@synthesize step      = _step;

+ (ScalerOrdinal*) scaleForDomain: (NSArray*) domain rangeFrom: (int) from rangeTo: (int) to
{
    ScalerOrdinal *ordinal = [[[ScalerOrdinal alloc] init] autorelease];
    ordinal.domain = domain;
    ordinal.rangeFrom = from;
    ordinal.rangeTo  = to;
    ordinal.step = (to-from)/ domain.count;
    return ordinal;
}

- (float) toPixelByObject: (id) value
{
	if([self.domain containsObject: value])
	{
		int index = [self.domain indexOfObject: value];
		return index * self.step;
	}
	else
	{
		return -100;
	}
}

- (id) toObjectByPixel: (float) pixel
{
    int index = floorf(pixel/ self.step);

    if(index >= self.domain.count)
        return nil;
    
    return [self.domain objectAtIndex: index];
}
@end
