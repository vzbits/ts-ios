//
//  ScalerLinear.m
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ScalerLog.h"

@implementation ScalerLog

+ (float) logp: (float) x
{
    if(x == 0) return 0;
    
    float r = log(x < 0 ? 0 : x) / M_LN10;
    return r;
}

+ (float) powp: (float) x
{
    return pow(x, 10);
}

+ (float) logn: (float) x
{
    float r = -log(x > 0 ? 0 : -x) / M_LN10;
    return r;
}
+ (ScalerLog*) scaleForDomain: (NSArray*) domain rangeFrom: (float) from rangeTo: (float) to
{
    NSMutableArray *newDomain = [NSMutableArray array];

    for(NSNumber *n in domain)
    {
        float newVal =  [n floatValue] < 0 ? [ScalerLog logn: [n floatValue]] : [ScalerLog logp: [n floatValue]];
        [newDomain addObject: [NSNumber numberWithFloat: newVal ]];
    }
    
    ScalerLog *ordinal = [[[ScalerLog alloc] init] autorelease];
    ordinal.originalDomain = domain;
    ordinal.domain = newDomain;
    ordinal.rangeFrom = from;
    ordinal.rangeTo  = to;
    ordinal.domainRange = fabs([[newDomain objectAtIndex:0] floatValue] - [[newDomain objectAtIndex:1] floatValue]);
    
    ordinal.step = abs(from-to) / ordinal.domainRange;
    return ordinal;
}

- (float) toPixel: (float) x
{
    float r = [ScalerLog logp: x];
    float r2 = [super toPixel: r];
    //NSLog(@"%f => %f", x , r2);
    return r2;
}

- (float) toValue: (float) y
{
    float r = [super toValue: y];
    return  [ScalerLog powp: r];
}

- (float) nicenum:(float) x round: (bool)  round
{
    int exp;
    float f;
    float nf;
    
    exp = floor(log10(x));
    f   = x / pow(10,exp);
    
    if(round)
    {
        if (f < 1.5) nf = 1;
        else if (f < 3) nf = 2;
        else if (f < 7) nf = 5;
        else nf = 10;
    }
    else
    {
        if (f <= 1) nf = 1;
        else if (f <= 2) nf = 2;
        else if (f <= 5) nf = 5;
        else nf = 10;
    }
    
    return nf*pow(10, exp);
}

- (NSArray*) getNiceGrids: (int) ntick
{
    NSMutableArray *niceGrids = [NSMutableArray array];
    float min = [[self.originalDomain objectAtIndex:0] floatValue];
    float max = [[self.originalDomain objectAtIndex:1] floatValue];
    float graphMin, graphMax;
    float nfrac;
    float d;
    float range;
    
    range = [self nicenum: (max - min) round: false];
    d     = [self nicenum: range/(ntick -1) round: true];
    graphMin = floor(min/d)*d;
    graphMax = ceil(max/d)*d;
    nfrac = MAX(-floor(log10(d)), 0);
    
    for(float x = graphMin; x <= graphMax + 0.5*d; x += d)
    {
        //NSLog(@"tick at %f", x);
        [niceGrids addObject: [NSNumber numberWithFloat: x]];
    }
    //NSLog(@"Grids = %f", n2);
    
    return niceGrids;
}
@end
