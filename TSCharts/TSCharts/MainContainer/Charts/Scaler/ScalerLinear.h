//
//  ScalerLinear.h
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Scaler.h"

@interface ScalerLinear : Scaler

+ (ScalerLinear*) scaleForDomain: (NSArray*) domain rangeFrom: (float) from rangeTo: (float) to;

@end
