//
//  Scaler.h
//  TSCharts
//
//  Created by Quoc Le on 8/7/12.
//
//

#import <Foundation/Foundation.h>

@interface Scaler : NSObject
@property (strong, nonatomic) NSArray *domain;
@property (assign, nonatomic) float rangeFrom;
@property (assign, nonatomic) float rangeTo;
@property (assign, nonatomic) float step;
@property (assign, nonatomic) float domainRange;

- (float) toPixel: (float) value;
- (float) toValue: (float) y;

- (NSArray*) getNiceGrids: (int) ntick;
@end
