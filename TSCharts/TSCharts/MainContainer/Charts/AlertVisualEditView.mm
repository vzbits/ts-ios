//
//  CrossChairView.m
//  TSCharts
//
//  Created by Quoc Le on 8/2/12.
//
//

#import "AlertVisualEditView.h"

@implementation AlertVisualEditView

@synthesize myChart   = _myChart;
@synthesize delegate = _delegate;

- (id)initWithChart: (Chart*) chart
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.myChart = chart;
        self.multipleTouchEnabled = YES;
        self.myTouches = [NSMutableSet set];
		self.alerts = [NSMutableArray array];
		handleIndexDown = -1;
    }
    return self;
}

- (void) loadData:(NSString*) symbol
{
	NSLog(@"Load alert data %@", symbol);
	self.symbol = symbol;
	self.alerts = [[CoreDataStore instance] alertsBySymbol: symbol];
	[self setNeedsDisplay];
}

- (void) addNewAlertForPrice: (double) price lastPrice: (double) lastPrice
{
	NSLog(@"Add alert at %f", price);
	//[self.alerts addObject: [NSMutableArray arrayWithObject:[NSNumber numberWithDouble: price]]];
	[[CoreDataStore instance] setAlertForSymbol: self.symbol price: price lastPrice: lastPrice currentAlert: nil];
	[self loadData: self.symbol];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	notified = NO;
	self.panStartDate = [NSDate date];
	[self.myTouches unionSet: touches];
	
	[NSTimer scheduledTimerWithTimeInterval:0.15f target: self selector:@selector(timerFired:) userInfo:nil repeats: NO];

	for(int i=0; i < self.alerts.count; i++)
	{
		Alert *object = self.alerts[i];
		
		NSNumber *n = object.price;
		CGFloat yPos = [self.myChart.scalerY toPixel: [n doubleValue]];
		CGRect holderRect = CGRectMake(0,PP(yPos-10), 30, 20);
		
		UITouch *touch = [self.myTouches anyObject];
		if(touch)
		{
			CGPoint p = [touch locationInView: self];
			if(CGRectContainsPoint(holderRect, p))
			{
				handleIndexDown = i;
			}
		}
	}
}

- (void) timerFired: (NSTimer*) timer
{
	[self setNeedsDisplay];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	if(!notified)
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:@"START_SCROLL" object: nil];
		notified = YES;
	}
	
	if(fabsf([self.panStartDate timeIntervalSinceNow]) > 0.15f)
	{
		[self.myTouches unionSet: touches];
		[self setNeedsDisplay];
	}
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.myTouches minusSet: touches];
    [self setNeedsDisplay];
	
	if(handleIndexDown >= 0 && handleIndexDown < self.alerts.count)
	{
		double lastPrice = (self.myChart.lastTick != nil) ? self.myChart.lastTick.close : ((PriceData*)[self.myChart.priceData lastObject]).close;
		Alert *object = self.alerts[handleIndexDown];
		[[CoreDataStore instance] setAlertForSymbol: self.symbol
											  price: object.price.doubleValue
										  lastPrice: lastPrice
									   currentAlert: object];
	}
	
	handleIndexDown = -1;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"END_SCROLL" object: nil];
	
    //NSLog(@"Cross chair ended");
	
	for(int i=0; i < self.alerts.count; i++)
	{
		Alert *object = self.alerts[i];
		NSNumber *n = object.price;
		CGFloat yPos = [self.myChart.scalerY toPixel: [n doubleValue]];
		NSLog(@"touchended at py = %f %f", yPos, [n doubleValue]);
		if(yPos < 0)
		{
			[[CoreDataStore instance] removeAlert: object];
			[self loadData: self.symbol];
		}
	}
}
- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded: touches withEvent: event];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	UITouch *touch = [self.myTouches anyObject];
	UIColor *lineColor = [UIColor colorWithWhite: 0.6 alpha: 0.3f];
	
	for(int i=0; i < self.alerts.count; i++)
	{
		Alert *object = self.alerts[i];
		NSNumber *n = object.price;
		NSLog(@"##### ALERT PRICE %f ####", [n doubleValue]);
		
		CGFloat yPos = [self.myChart.scalerY toPixel: [n doubleValue]];
		
		if(handleIndexDown >= 0 && i == handleIndexDown)
		{
			CGPoint p = [touch locationInView: self];
			yPos = p.y;
			CGFloat value = [self.myChart.scalerY toValue: yPos];
			NSNumber *newValue = [NSNumber numberWithDouble: value];
			object.price = newValue;
		}
		
		CGRect holderRect = CGRectMake(0,PP(yPos-10), 30, 20);
		CGContextSetLineWidth(context, 1.0);
        CGContextSetStrokeColorWithColor(context, lineColor.CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, 0.5, PP(yPos));
        CGContextAddLineToPoint(context, self.width - self.myChart.currentPriceView.width, PP(yPos));
        CGContextStrokePath(context);
		
		[lineColor setStroke];
		UIRectFrame(holderRect);
		
		PriceData *p = [self.myChart.priceData lastObject];
		double newValue = [object.price doubleValue];
		double gain = (newValue - p.close)/ p.close;
		[[UIColor whiteColor] set];
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size: 9];
        NSString *indicatorName = [NSString stringWithFormat:@"%.2f (%.0f%%)", newValue, gain*100];
		CGRect valueRect = CGRectMake(self.width/2,PP(yPos-5), 70, 10);
        [indicatorName drawInRect:valueRect withFont: font lineBreakMode: UILineBreakModeClip alignment: UITextAlignmentCenter];
	}
    
//    if(numberOfTouches == 0)
//    {
//        [self.delegate alertVisualEditViewStateChanged: UIGestureRecognizerStateEnded object: firstObject point: firstPoint];
//    }
//    else
//    {
//        [self.delegate alertVisualEditViewStateChanged: UIGestureRecognizerStateChanged object: firstObject point: firstPoint];
//    }
}

@end
