//
//  Chart.mm
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Chart.h"
#import "NSArray+Extensions.h"
#import "NSDate+Escort.h"

//#define NSLog //

static float indicator_default_height[6][6]  = { {1.0},{0.75, 0.25}, {0.6, 0.2, 0.2}, {0.55, 0.15, 0.15, 0.15 }, {0.52, 0.12, 0.12, 0.12, 0.12 } ,  { 0.50, 0.10, 0.10, 0.10, 0.10, 0.10 }};
static BOOL CHART_TIME = NO;

NSComparisonResult (^simpleValueSortFN)(SimpleValue*,SimpleValue*) = ^NSComparisonResult(SimpleValue *obj1, SimpleValue *obj2) {
    
    if([obj1 value] < [obj2 value])
        return NSOrderedAscending;
    else if([obj1 value] > [obj2 value])
        return NSOrderedDescending;
    else
        return NSOrderedSame;
};

@interface Chart()
- (void) addPlotForIndex: (int) index visualIndex: (int) visualIndex;
@end

@implementation Chart

@synthesize padding = _padding;
@synthesize scalerX = _scalerX;
@synthesize scalerY = _scalerY;
@synthesize xGrids  = _xGrids;
//@synthesize priceData  = _priceData;
@synthesize total_plots = _total_plots;
@synthesize scalerVolume = _scalerVolume;
@synthesize indicators = _indicators;
@synthesize scalers = _scalers;

- (id)init
{
    self = [super init];
    if (self) {
        _padding = 20;
        axis_width = 30;
        
		self.style = [ChartStyle defaultStyle];
		//self.backgroundColor = self.style.backgroundColor;
		self.clipsToBounds = NO;
		
        self.currentPriceView = [[[CurrentPriceView alloc] init] autorelease];
		self.lastTickView = [[[TickView alloc] init] autorelease];
        self.mountainStartFillColor = [UIColor colorWithRGBHex: 0x5E86B8];
        self.mountainEndFillColor = [[UIColor colorWithRGBHex: 0x5E86B8] colorWithAlphaComponent: 0.4];
        self.ohlcThick = 1.0;
        self.ohlcWidth = 2;
        
        [self addSubview: self.currentPriceView];
		[self addSubview: self.lastTickView];
		
        self.scalers = [NSMutableArray array];
        self.priceData = [NSMutableArray array];
        self.valueLabels = [NSMutableArray array];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
}

+ (void) generateTicks: (int)ticks container: (NSMutableArray*) store startDate: (NSDate*) date
{
    NSTimeInterval DAY_S = 24 * 60 * 60;
    int count = 0;
    NSMutableArray *tmp = [NSMutableArray array];
    for(int i=0; i < ticks; i++)
    {
        NSDate *newDate = [NSDate dateWithTimeIntervalSince1970: date.timeIntervalSince1970 - (++count*DAY_S)];
        while([newDate weekday] == 1 || [newDate weekday] == 7)
        {
            newDate = [NSDate dateWithTimeIntervalSince1970: date.timeIntervalSince1970 - (++count *DAY_S)];
        }
        [tmp addObject: newDate];
    }
    
    NSEnumerator *enumerator = [tmp reverseObjectEnumerator];
    for (id element in enumerator) {
        [store addObject:element];
    }
}

- (void) setLastTick: (PriceData*) priceData
{
	[_lastTick release];
	_lastTick = [priceData retain];
	
	[self drawCurrentPrice];
}

- (void) loadChart:  (StockPriceData*) stockPrice Indicators: (NSArray*) indicators_ 
{	
    volume = true;
    NSDate *start = [NSDate date];
	self.lastTick = nil;
	self.priceType = (PriceType)stockPrice.priceType;
	
    NSLog(@"Chart::Load chart data=%d",stockPrice.prices.count);
    int tw = self.width  - 10;
    int th = self.height - self.padding;
    int tick_width = 3;
    bool volume_ = true;
    
    int chart_width = tw - axis_width;
    int num_ticks = chart_width / tick_width;
    int new_chart_width = num_ticks * tick_width;
 
    NSArray *non_overlays = [indicators_ filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"type != %d", INDICATOR_OVERLAY]];
    self.total_plots = volume_ ? 2 + non_overlays.count : 1 + non_overlays.count;
    int pos_x = 0;
    
    //slice data
    int start_r = stockPrice.prices.count - num_ticks;
    //int available_ticks = MIN(num_ticks, stockPrice.prices.count);
    NSMutableArray *x_axis  = [NSMutableArray array];

    //NSLog(@"Chart::Missing %d ticks",start_r);
    if(start_r < 0)
    {
		if(stockPrice.prices.count > 0)
		{
			PriceData *first = [stockPrice.prices objectAtIndex:0];
			[Chart generateTicks: -1*start_r container: x_axis startDate: [NSDate dateFromPB: first.date]];
		}
		else
		{
			[Chart generateTicks: -1*start_r container: x_axis startDate: [NSDate date]];
		}
    }
    //self.priceData = [stockPrice.prices subarrayWithRange: NSMakeRange(MAX(start_r-1,0), num_ticks)];
    [self.priceData removeAllObjects];
    
    for(int i= MAX(start_r,0); i < stockPrice.prices.count; i++)
    {
        PriceData *data = [stockPrice.prices objectAtIndex:i];
        [self.priceData addObject: data ];
        [x_axis addObject: [NSDate dateFromPB: data.date]];
    }
	
//	if([x_axis containsObject: [NSDate da]])
	
	if(self.priceData.count == 0)
	{
		self.currentPriceView.alpha = 0.0;
		[self setNeedsDisplay];
		NSLog(@"Chart::Weird error framesize %f" , self.width);
		return;
	}
	self.currentPriceView.alpha = 1.0;
    
    //for(Price *p in self.priceData)
    //    [x_axis addObject: p.date];
    
    for(int i=0; i < self.total_plots; i++)
    {
        int h = (th * indicator_default_height[self.total_plots-1][i]);
        hs_h[i] = h;
        
        if(i==0)
            hs_x[i] = 0;
        else
            hs_x[i] = pos_x;

        pos_x += h;
    }
    //NSLog(@"Chart::width = %d h=%d ph=%d nticks=%d prices=%d", tw, th, hs_h[0], num_ticks, stockPrice.prices.count);
    //NSLog(@"Chart::chart_width = %d newWidth = %d", chart_width, new_chart_width);

    double min = [(PriceData*)[self.priceData objectAtIndex: 0] low];
    double max = [(PriceData*)[self.priceData objectAtIndex: 0] high];

    for(int i=1; i< self.priceData.count; i++)
    {
        min = MIN(min, [(PriceData*)[self.priceData objectAtIndex: i] low]);
        max = MAX(max, [(PriceData*)[self.priceData objectAtIndex: i] close]);
    }

    double maxVol = [(PriceData*)[self.priceData objectAtIndex: 0] volume];
    for(int i=1; i < self.priceData.count; i++)
    {
        maxVol = MAX(maxVol, [(PriceData*)[self.priceData objectAtIndex: i] volume]);
    }
    
    double buffer = (max-min) * 0.15; // 10 %

    //NSLog(@"Chart::Min price %f max=%f buffer=%f", min,max, buffer);
    NSArray *y_domain = [NSArray arrayWithObjects: [NSNumber numberWithFloat:min-buffer], [NSNumber numberWithFloat:max+buffer], nil];     

    NSString *priceScale = [[NSUserDefaults standardUserDefaults] objectForKey:@"PriceScale"];
    
    if([priceScale isEqualToString:@"Linear"] || priceScale == nil)
        self.scalerY = [ScalerLinear scaleForDomain: y_domain rangeFrom: hs_h[0] rangeTo: 0];
    else
        self.scalerY = [ScalerLog scaleForDomain: y_domain rangeFrom: hs_h[0] rangeTo: 0];
    
    self.scalerX = [ScalerOrdinal scaleForDomain: x_axis rangeFrom:0 rangeTo: new_chart_width];
    
    NSString *volumeScale = [[NSUserDefaults standardUserDefaults] objectForKey:@"VolumeScale"];
    
    if([volumeScale isEqualToString:@"Linear"] || volumeScale == nil)
    {
        NSArray *vol_domain = [NSArray arrayWithObjects: [NSNumber numberWithFloat: 0], [NSNumber numberWithFloat:maxVol+1.10], nil];
        self.scalerVolume = [ScalerLinear scaleForDomain: vol_domain rangeFrom: hs_x[1]+ hs_h[1] rangeTo: hs_x[1] ];
    } else
    {
        NSArray *vol_domain = [NSArray arrayWithObjects: [NSNumber numberWithFloat: maxVol*0.10], [NSNumber numberWithFloat:maxVol+1.10], nil];
        self.scalerVolume = [ScalerLog scaleForDomain: vol_domain rangeFrom: hs_x[1]+ hs_h[1] rangeTo: hs_x[1] ];
    }
    [self.scalers addObject: self.scalerY];
    [self.scalers addObject: self.scalerVolume];
    
	[self generateGrids];
    
    self.indicators = indicators_;

    timeToCalculate = fabs([start timeIntervalSinceNow]);
    [self setNeedsDisplay];
}

- (void) generateGrids
{
	self.xGrids = [NSMutableArray array];
	//int c = 0;
	
	if(self.priceType == PriceType_PT_DAILY)
	{
		NSMutableArray *lastMonth = [NSMutableArray array];
		NSInteger lastMonthIndex = -1;
		for(NSDate *d in self.scalerX.domain)
		{
			NSInteger thisMonth = [d month];
			//NSLog(@"month = %d %@",thisMonth, d.description);
			if(thisMonth != lastMonthIndex)
			{
				if(lastMonth.count > 2)
				{
					[self.xGrids addObject: [lastMonth objectAtIndex:0]];
					if(lastMonth.count >= 10)
						[self.xGrids addObject: [lastMonth objectAtIndex: MAX(9,lastMonth.count/2)]];
				}
				lastMonthIndex = thisMonth;
				[lastMonth removeAllObjects];
			}
			[lastMonth addObject: d];
		}
		
		if(lastMonth.count > 2)
		{
			[self.xGrids addObject: [lastMonth objectAtIndex:0]];
			if(lastMonth.count >= 10)
				[self.xGrids addObject: [lastMonth objectAtIndex: MAX(9,lastMonth.count/2)]];
		}
	}

	if(self.priceType == PriceType_PT_WEEKLY)
	{
		NSInteger prevMonth;
		for(NSDate *d in self.scalerX.domain)
		{
			NSInteger currMonth = [d month];
			NSInteger weekOfMonth = [d weekOfMonth];
			
			if((currMonth == 1|| currMonth == 4 || currMonth == 7 || currMonth == 10) && currMonth != prevMonth) //
			{
				[self.xGrids addObject: d];
				NSLog(@"month = %d %@ *** ",currMonth, d.description);
			}
			else
			{
				NSLog(@"month = %d week %d %@",currMonth, weekOfMonth, d.description);
			}
			prevMonth =  currMonth;
		}
	}
	
	if(self.priceType == PriceType_PT_MONTHLY)
	{
		NSInteger prevYear = 0;
		for(NSDate *d in self.scalerX.domain)
		{
			NSInteger currYear = [d year];
			NSInteger currMonth = [d month];
			
			if(prevYear != currYear && currMonth == 1) //currMonth == 4 || currMonth == 7 ||
			{
				[self.xGrids addObject: d];
				NSLog(@"month = %d %@ *** ",currYear, d.description);
			}
			else
			{
				NSLog(@"month = %d %@",currYear, d.description);
			}
			prevYear =  currYear;
		}
	}
}
- (void) drawXAxis: (NSArray*) grids
{
	if(self.priceType == PriceType_PT_DAILY)
		[self drawXAxisDaily: grids];
	if(self.priceType == PriceType_PT_WEEKLY)
		[self drawXAxisWeekly: grids];
	if(self.priceType == PriceType_PT_MONTHLY)
		[self drawXAxisMonthly: grids];
}

- (void) drawXAxisDaily: (NSArray*) grids
{
	//CGContextRef context = UIGraphicsGetCurrentContext();
    //CGContextSetLineWidth(context, 1.0);
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size: 10];
    UIBezierPath *gridPath = [UIBezierPath bezierPath];
	
    BOOL firstMonth = NO;
    for(int i=0; i< grids.count; i++)
    {
        NSDate *d = [grids objectAtIndex:i];
        int xVal = [self.scalerX toPixelByObject: d];
        //CGContextMoveToPoint(context, xVal +0.5 , 0);
        //CGContextAddLineToPoint(context, xVal +0.5, self.height - self.padding);
        [gridPath moveToPoint: CGPointMake(xVal +0.5 , 0)];
        [gridPath addLineToPoint:CGPointMake(xVal +0.5, self.height - self.padding-2)];
        // look ahead
        if(i == grids.count -1)
        {
            NSDate *d2 = [grids objectAtIndex:i - 1];
            if([d month] == [d2 month] )
                firstMonth = NO;
            else
                firstMonth = YES;
        }
        else if(i < grids.count - 1)
        {
            NSDate *d2 = [grids objectAtIndex:i + 1];
            if([d month] == [d2 month] )
                firstMonth = YES;
            else
                firstMonth = NO;
        }
        
        UIColor *darkGray = [UIColor colorWithRGBHex:0xA9A9A9];
        if(firstMonth)
        {
			//            UIBezierPath *path = [UIBezierPath bezierPath];
			//            [path setLineWidth: 1.0];
			//            [path moveToPoint:CGPointMake(PP(xVal), self.height - self.padding)];
			//            [path addLineToPoint:CGPointMake(PP(xVal), self.height - self.padding + 8)];
			//            [darkGray setStroke];
			//            [path stroke];
        }
        else
        {
			//            UIBezierPath *path = [UIBezierPath bezierPath];
			//            [path setLineWidth: 1.0];
			//            [path moveToPoint:CGPointMake(PP(xVal), self.height)];
			//            [path addLineToPoint:CGPointMake(PP(xVal), self.height - 5)];
			//            [darkGray setStroke];
			//            [path stroke];
			
            [_style.fontColor set];
            [[[d stringWithFormat:@"MMM"] uppercaseString] drawInRect: CGRectMake(xVal-10,self.height-20,40,15) withFont: font];
        }
    }
    
    [_style.gridColor setStroke];
    [gridPath stroke];
    
    //CGContextSetStrokeColorWithColor(context, [UIColor colorWithRGBHex:0xeeeeee].CGColor);
    //CGContextStrokePath(context);
}

- (void) drawXAxisWeekly: (NSArray*) grids
{
	UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size: 10];

	for(int i=0; i< grids.count; i++)
    {
		NSDate *d = [grids objectAtIndex:i];
        int xVal = [self.scalerX toPixelByObject: d];
		[_style.fontColor set];
		
		//NSLog(@"*** %d %@", xVal, [d description]);
		
		if([d month] == 1)
			[[[d stringWithFormat:@"MMM"] uppercaseString] drawInRect: CGRectMake(xVal-10,self.height-20,40,15) withFont: font];
		else
			[[[d stringWithFormat:@"MMM"] uppercaseString] drawInRect: CGRectMake(xVal-10,self.height-20,40,15) withFont: font];
	}
}

- (void) drawXAxisMonthly: (NSArray*) grids
{
	UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size: 10];
	
	for(int i=0; i< grids.count; i++)
    {
		NSDate *d = [grids objectAtIndex:i];
        int xVal = [self.scalerX toPixelByObject: d];
		[_style.fontColor set];
		
		//NSLog(@"*** %d %@", xVal, [d description]);

		[[d stringWithFormat:@"YYYY"] drawInRect: CGRectMake(xVal-10,self.height-20,40,15) withFont: font];
	}
}

- (void) drawYAxis: (int) i Scaler: (id) scaler NumGrids: (int) numGrids  
{
    NSArray *grids = [scaler getNiceGrids: numGrids];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0);
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size: 10];
	
	BOOL twoDecimal = NO;
    for(NSNumber *ns_y in grids) {
		float fractpart, intpart;
        float yVal = [ns_y floatValue];
		fractpart = modff (yVal , &intpart);
		
		if(fractpart != 0.0) {
			twoDecimal = YES;
			break;
		}
	}
	
    for(NSNumber *ns_y in grids)
    {
        float yVal = [ns_y floatValue];
        float yPixel = [scaler toPixel: yVal];
        
        //NSLog(@" axis val = %f pixel = %f", yVal, yPixel);
        
        if(yPixel >= hs_x[i] + hs_h[i])
            continue;

        if(yPixel <= hs_x[i])
            continue;

        CGContextMoveToPoint(context, 0, yPixel+0.5);
        CGContextAddLineToPoint(context, self.width - axis_width, yPixel+0.5);
        
		[_style.fontColor set];
        NSString *axisLabel = [ChartUtils formatYAxis: yVal twoDecimal: twoDecimal];
        CGRect rect = CGRectMake(self.width-axis_width, yPixel-7, 50, 14);
        [axisLabel drawInRect:rect withFont: font lineBreakMode: UILineBreakModeClip alignment: UITextAlignmentLeft];
    }
    CGContextSetStrokeColorWithColor(context, _style.gridColor.CGColor);
    CGContextStrokePath(context);
}

- (void) drawLine
{
    UIBezierPath *strokePath = [UIBezierPath bezierPath];
    
    for(int i=0; i< self.priceData.count; i++)
    {
        PriceData *p = [self.priceData objectAtIndex:i];
        int x = [self.scalerX toPixelByObject: [NSDate dateFromPB: p.date]];
        //int y_open = [self.scalerY toPixel: p.open()];
        int y_close = [self.scalerY toPixel: p.close];
        //int y_high = [self.scalerY toPixel: p.high()];
        //int y_low = [self.scalerY toPixel: p.low()];
        
        //NSLog(@"x=%d open=%d close=%d low=%d high=%d",x,y_open, y_close, y_low, y_high);
        if(i==0)
        {
            [strokePath moveToPoint: CGPointMake(x, y_close)];
        }
        else
        {
            [strokePath addLineToPoint: CGPointMake(x, y_close)];
        }
    }
    [strokePath setLineWidth: _style.lineWidth];
    [_style.lineColor setStroke];
    [strokePath stroke];
}

- (void) drawMountain
{
    BOOL gradient = YES;
    
    if(self.priceData.count <= 0)
        return;
    
    UIBezierPath *fillPath = [UIBezierPath bezierPath];
    UIBezierPath *strokePath = [UIBezierPath bezierPath];
    
    for(int i=0; i<self.priceData.count; i++)
    {
        PriceData *p = [self.priceData objectAtIndex:i];
        int x = [self.scalerX toPixelByObject: [NSDate dateFromPB: p.date]];
        //int y_open = [self.scalerY toPixel: p.open()];
        int y_close = [self.scalerY toPixel: p.close];
        //int y_high = [self.scalerY toPixel: p.high()];
        //int y_low = [self.scalerY toPixel: p.low()];
        
        //NSLog(@"x=%d open=%d close=%d low=%d high=%d",x,y_open, y_close, y_low, y_high);
        if(i==0)
        {
            [fillPath moveToPoint: CGPointMake(0, hs_x[1])];
            [fillPath addLineToPoint: CGPointMake(0, y_close)];
            
            [strokePath moveToPoint: CGPointMake(x, y_close)];
        }
        else if(i == self.priceData.count-1) // last node
        {
            [fillPath addLineToPoint: CGPointMake(x, hs_x[1])];
            [fillPath addLineToPoint: CGPointMake(0, hs_x[1])];
        }
        else
        {
            
            [fillPath addLineToPoint: CGPointMake(x, y_close)];
            [strokePath addLineToPoint: CGPointMake(x, y_close)];
        }
    }
        
    [_style.lineColor setStroke];
    //

    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextSaveGState(currentContext);
    
    if(gradient)
    {
        [fillPath addClip];
        
        CGGradientRef glossGradient;
        CGColorSpaceRef rgbColorspace;
        size_t num_locations = 2;
        CGFloat locations[2] = { 0.0, 1.0 };
        CGFloat components[8] = { self.mountainStartFillColor.red, self.mountainStartFillColor.green, self.mountainStartFillColor.blue, self.mountainStartFillColor.alpha,  // Start color
            self.mountainEndFillColor.red, self.mountainEndFillColor.green, self.mountainEndFillColor.blue, self.mountainEndFillColor.alpha }; // End color
        
        rgbColorspace = CGColorSpaceCreateDeviceRGB();
        glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
        
        CGRect currentBounds = self.bounds;
        CGPoint topCenter = CGPointMake(CGRectGetMidX(currentBounds), hs_x[0]);
        CGPoint midCenter = CGPointMake(CGRectGetMidX(currentBounds), hs_x[0] + hs_h[0]);
        CGContextDrawLinearGradient(currentContext, glossGradient, topCenter, midCenter, 0);
        
        CGGradientRelease(glossGradient);
        CGColorSpaceRelease(rgbColorspace);

    } else
    {
        [self.mountainStartFillColor setFill];
        [fillPath fill];
    }
    [strokePath setLineWidth: _style.lineWidth];
    [strokePath stroke];
    CGContextRestoreGState(currentContext);

}

- (void) drawOHLC
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRGBHex: _style.wickColor].CGColor);
    CGContextSetLineWidth(context, self.ohlcThick);

    
    for(int i=0; i<self.priceData.count; i++)
    {
        PriceData *p = [self.priceData objectAtIndex:i];
        int x = [self.scalerX toPixelByObject: [NSDate dateFromPB: p.date]];
        int y_open = [self.scalerY toPixel: p.open];
        int y_close = [self.scalerY toPixel: p.close];
        int y_high = [self.scalerY toPixel: p.high];
        int y_low = [self.scalerY toPixel: p.low];
        
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x - self.ohlcWidth, PP(y_open));
        CGContextAddLineToPoint(context, x, PP(y_open));
        CGContextMoveToPoint(context, PP(x), PP(y_high));
        CGContextAddLineToPoint(context, PP(x), PP(y_low));
        CGContextMoveToPoint(context, x, PP(y_close));
        CGContextAddLineToPoint(context, x + self.ohlcWidth, PP(y_close));
        CGContextStrokePath(context);
        
    }
}

- (void) drawCandleStick
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for(int i=0; i<self.priceData.count; i++)
    {
        PriceData *p = [self.priceData objectAtIndex:i];
        int x = [self.scalerX toPixelByObject: [NSDate dateFromPB: p.date]];
        int y_open = [self.scalerY toPixel: p.open];
        int y_close = [self.scalerY toPixel: p.close];
        int y_high = [self.scalerY toPixel: p.high];
        int y_low = [self.scalerY toPixel: p.low];
        
        //NSLog(@"x=%d open=%d close=%d low=%d high=%d",x,y_open, y_close, y_low, y_high);
        CGContextSetLineWidth(context, _style.wickPixelSize);
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRGBHex: _style.wickColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x+0.5, y_low);
        CGContextAddLineToPoint(context, x+0.5, y_high);
        CGContextStrokePath(context);
        
        CGContextSetLineWidth(context, _style.bodyPixelSize);
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRGBHex: (p.close > p.open ? _style.upColor : _style.downColor)].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x+0.5, y_close);
        CGContextAddLineToPoint(context, x+0.5, y_open == y_close ? y_close + 1 : y_open);
        CGContextStrokePath(context);
        
    }
}

- (void) drawCandleStickHollow
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    for(int i=0; i<self.priceData.count; i++)
    {
        PriceData *p = [self.priceData objectAtIndex:i];
        int x = [self.scalerX toPixelByObject: [NSDate dateFromPB: p.date]];
        int y_open = [self.scalerY toPixel: p.open];
        int y_close = [self.scalerY toPixel: p.close];
        int y_high = [self.scalerY toPixel: p.high];
        int y_low = [self.scalerY toPixel: p.low];
        
        //NSLog(@"x=%d open=%d close=%d low=%d high=%d",x,y_open, y_close, y_low, y_high);
        CGContextSetLineWidth(context, _style.wickPixelSize);
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRGBHex: _style.wickColor].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x+0.5, y_low);
        CGContextAddLineToPoint(context, x+0.5, y_high);
        CGContextStrokePath(context);
        
        if(p.close < p.open)
        {
            CGContextSetLineWidth(context, _style.bodyPixelSize);
            CGContextSetStrokeColorWithColor(context, [UIColor colorWithRGBHex: (p.close > p.open ? _style.upColor : _style.downColor)].CGColor);
            CGContextBeginPath(context);
            CGContextMoveToPoint(context, x+0.5, y_close);
            CGContextAddLineToPoint(context, x+0.5, y_open);
            CGContextStrokePath(context);
        }
        else
        {
            CGContextSetStrokeColorWithColor(context, [UIColor colorWithRGBHex: _style.upColor].CGColor);
            CGContextSetLineWidth(context, 1);
            float halfWidth = _style.bodyPixelSize/2;
            CGRect bodyRect =  CGRectMake( PP(x-halfWidth), MIN(y_open, y_close), _style.bodyPixelSize, fabs(y_close-y_open));
            CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
            CGContextFillRect(context,bodyRect);
            CGContextStrokeRect(context, bodyRect);
        }
    }
}

- (void) drawPrices
{
    self.drawStyle = [[NSUserDefaults standardUserDefaults] objectForKey: @"ChartStyle"];

    if([_drawStyle isEqualToString:@"Line"] || _drawStyle == nil)
    {
        [self drawLine];
    }
    if([_drawStyle isEqualToString:@"CandleStick"])
    {
        [self drawCandleStick];
    }
    if([_drawStyle isEqualToString:@"CandleStick (Hollow)"])
    {
        [self drawCandleStickHollow];
    }
    if([_drawStyle isEqualToString:@"Mountain"])
    {
        [self drawMountain];
    }
    if([_drawStyle isEqualToString:@"OHLC"])
    {
        [self drawOHLC];
    }
}

- (void) drawVolume
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextClipToRect(context, CGRectMake(0,hs_x[1], self.width, hs_h[1]));
    
    CGContextSetLineWidth(context, 2);
    
    for(int i=0; i< self.priceData.count; i++)
    {
        PriceData *p = [self.priceData objectAtIndex:i];
        int x = [self.scalerX toPixelByObject: [NSDate dateFromPB: p.date]];
        float y_volume = [self.scalerVolume toPixel: p.volume];
        //NSLog(@"Volume %f y=%f", p.volume(), y_volume);
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRGBHex: (p.close > p.open ? _style.upColor : _style.downColor)].CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, x  , hs_x[1] + hs_h[1]);
        CGContextAddLineToPoint(context, x , y_volume);
        CGContextStrokePath(context);
        
    }
    
//    [_style.fontColor set];
//    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size: 12];
//    NSString *indicatorName = [NSString stringWithFormat:@"Volume"];
//    CGRect rect = CGRectMake(7, hs_x[1] + 5, 200, 20);
//    [indicatorName drawInRect:rect withFont: font lineBreakMode: UILineBreakModeClip alignment: UITextAlignmentLeft];
    
    CGContextRestoreGState(context);
}

- (NSString*) getNextColor
{
    return @"yellow";
}

- (void) addPlotForIndex: (int) index visualIndex: (int) visualIndex
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextClipToRect(context, CGRectMake(0,hs_x[visualIndex], self.width, hs_h[visualIndex]));
    
    Indicator *ind = [self.indicators objectAtIndex: index];
    int h2, y2;
    
    if( ind.type == INDICATOR_OVERLAY)
    {
        h2 = 0;
        y2 = 0;
        
        for(int d = 0; d < ind.plotData.count; d++)
        {
            NSArray *plot = [ind.plotData objectAtIndex: d];
            NSString *plotColor =  d < ind.plotColor.count ? [ind.plotColor objectAtIndex:d] : [self getNextColor];
            CGContextSetLineWidth(context, 1);
            CGContextSetStrokeColorWithColor(context, [ChartUtils convertColor: plotColor].CGColor);
            CGContextBeginPath(context);

            NSArray *plotDataF = [plot filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"date >= %@", [self.scalerX.domain objectAtIndex: 0]]];

            for(int i=0; i< plotDataF.count; i++)
            {
                SimpleValue *val = [plotDataF objectAtIndex:i];
                int x = [self.scalerX toPixelByObject: val.date];
                int y_value = [self.scalerY toPixel: val.value];

                //NSLog(@"index = %d d=%@ y=%f", index, [val.date description], val.value);

                if(i==0)
                {
                    CGContextMoveToPoint(context, x  , y_value);
                }
                else
                {
                    CGContextAddLineToPoint(context, x  , y_value);
                }
            }
            CGContextStrokePath(context);
        }
    }
    else
    {
        float minValue;
        float maxValue;
        
        for(int d = 0; d < ind.plotData.count; d++)
        {
            NSArray *currentPlot = [ind.plotData objectAtIndex: d];
            NSArray *plotDataF = [currentPlot filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"date >= %@", [self.scalerX.domain objectAtIndex: 0]]];

            if(d==0)
            {
                minValue = [(SimpleValue*)[plotDataF min: simpleValueSortFN] value];
                maxValue = [(SimpleValue*)[plotDataF max: simpleValueSortFN] value];
            }
            else
            {
                minValue = MIN(minValue, [(SimpleValue*)[plotDataF min: simpleValueSortFN] value]);
                maxValue = MAX(maxValue, [(SimpleValue*)[plotDataF max: simpleValueSortFN] value]);
            }
        }
        
        float range = ABS(maxValue - minValue);
        NSLog(@"Range %f min=%f max=%f", range,minValue, maxValue);
        
        NSArray *domain = [NSArray arrayWithObjects: [NSNumber numberWithFloat: minValue - (0.10 * range)], [NSNumber numberWithFloat:maxValue + (0.10 *range)], nil];
        ScalerLinear *scalerY = [ScalerLinear scaleForDomain: domain rangeFrom: hs_x[visualIndex]+ hs_h[visualIndex] rangeTo: hs_x[visualIndex] ];
        [self drawYAxis: visualIndex Scaler: scalerY NumGrids: 5];
        [self.scalers addObject: scalerY];
        
        for(int d = 0; d < ind.plotData.count; d++)
        {
            NSString* plotType = d < ind.plotType.count ? [ind.plotType objectAtIndex: d] : @"line";
            NSArray *plotData = [ind.plotData objectAtIndex: d];
            NSArray *plotDataF = [plotData filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"date >= %@", [self.scalerX.domain objectAtIndex: 0]]];
            NSString *plotColor = d < ind.plotColor.count ? [ind.plotColor objectAtIndex:d] : [self getNextColor];
            
            if([plotType isEqualToString:@"line"])
            {
                CGContextRef context = UIGraphicsGetCurrentContext();
                CGContextSetLineWidth(context, 1);
                CGContextSetStrokeColorWithColor(context, [ChartUtils convertColor: plotColor].CGColor);
                CGContextBeginPath(context);
                
                for(int i=0; i< plotDataF.count; i++)
                {
                    SimpleValue *val = [plotDataF objectAtIndex:i];
                    int x = [self.scalerX toPixelByObject: val.date];
                    int y_value = [scalerY toPixel: val.value];
                    
                    if(i==0)
                    {
                        CGContextMoveToPoint(context, x  , y_value);
                    }
                    else
                    {
                        CGContextAddLineToPoint(context, x  , y_value);
                    }
                }
                CGContextStrokePath(context);
            }
            if([plotType isEqualToString:@"bar"])
            {
                CGContextRef context = UIGraphicsGetCurrentContext();
                CGContextSetLineWidth(context, 2);
                CGContextSetStrokeColorWithColor(context, [ChartUtils convertColor: plotColor].CGColor);
                
                int pixelAtZero = [scalerY toPixel: 0];
                
                for(int i=0; i< plotDataF.count; i++)
                {
                    SimpleValue *val = [plotDataF objectAtIndex:i];
                    int x = [self.scalerX toPixelByObject:  val.date];
                    int y_volume = [scalerY toPixel: val.value];
                    
                    CGContextBeginPath(context);
                    CGContextMoveToPoint(context, x  , pixelAtZero);
                    CGContextAddLineToPoint(context, x , y_volume);
                    CGContextStrokePath(context);
                    
                }
            }
        }
        
    }
    
    CGContextRestoreGState(context);
}

- (void) drawIndicators
{
    //NSLog(@"%@", [self.indicators description]);
    //NSLog(@"Total indicators: %d", [self.indicators count]);
    int startIndex = volume ? 2 : 1;
    int overlayCount = 0;

    for(UILabel *label in self.valueLabels)
    {
        [label removeFromSuperview];
    }
    [self.valueLabels removeAllObjects];
    
    float y_pos, x_pos;
    for(int y=0; y < [self.indicators count]; y++)
    {
        Indicator *ind = [self.indicators objectAtIndex: y];
        
        if( ind.type == INDICATOR_OVERLAY)
        {
            [self addPlotForIndex: y visualIndex: 0];
            y_pos =  5 + hs_x[0] + (23*overlayCount);
            x_pos =  7;
            overlayCount++;
        }
        else
        {
            y_pos = 5 + hs_x[startIndex];
            x_pos = 7;
            [self addPlotForIndex: y visualIndex: startIndex++];
        }
        
        // draw the name of the indicators
        [[UIColor blackColor] set];
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size: 12];
        NSString *indicatorName = [NSString stringWithFormat:@"%@ (%@)", ind.name, [ChartUtils arrayToString: ind.inputValues]];
        CGRect rect = CGRectMake(x_pos, y_pos, 200, 20);
        [indicatorName drawInRect:rect withFont: font lineBreakMode: UILineBreakModeClip alignment: UITextAlignmentLeft];

        CGSize size = [indicatorName sizeWithFont: font];
        float spacing = 10;
        float x_start = x_pos + size.width + spacing;

        //NSLog(@"Plot index=%d plots=%d",y, ind.plotData.count);
        for(int d=0; d < ind.plotData.count; d++)
        {
            NSArray *plotData = [ind.plotData objectAtIndex: d];
            NSString *plotColor = d < ind.plotColor.count ? [ind.plotColor objectAtIndex:d] : [self getNextColor];
            UIColor *color = [ChartUtils convertColor: plotColor];
            
            SimpleValue *val = [plotData lastObject];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x_start, y_pos, 200,20)];
            label.font = font;
            label.textColor = color;
            label.text = [ChartUtils formatCurrentPrice: val.value];
            [label sizeToFit];
            [self addSubview: label];
            [self.valueLabels addObject: label];
            [self bringSubviewToFront: label];
            //label.backgroundColor = [UIColor yellowColor];
            //NSLog(@"type=%d x=%f y=%f w=%f h=%f text=%@", ind.type, label.x, label.y, label.width, label.height,label.text);
            
            CGSize size = [label.text sizeWithFont: font];
            x_start += size.width + spacing;
        }
//
//        UIButton *buttonImage = [UIButton buttonWithType: UIButtonTypeCustom];
//        buttonImage.frame = CGRectMake(7, y_pos, 20, 20);
//        [buttonImage setImage: [UIImage imageNamed:@"indicator_config.png"] forState: UIControlStateNormal];
//        [buttonImage addTarget: self  action: @selector(testCall:) forControlEvents: UIControlEventTouchUpInside];
//        [self addSubview: buttonImage];
//        [self.configButtons addObject: buttonImage];
    }
}

- (void) testCall: (id) sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Test"
                                                    message: @"Test"
                                                   delegate: nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
    [alert release];
}

- (void) drawPriceChart
{
    [self drawXAxis: self.xGrids];
    [self drawYAxis: 0 Scaler: self.scalerY NumGrids: 10];
    [self drawPrices];
    [self drawYAxis: 1 Scaler: self.scalerVolume NumGrids: 5];
    [self drawVolume];
    [self drawIndicators];
}

- (void) drawCurrentPriceAtY: (int) y price: (float) price
{
    int currentPriceHeight = 18;
    self.currentPriceView.frame = CGRectMake(self.width - axis_width - 15 , y - (currentPriceHeight/2), axis_width + 15, currentPriceHeight);
    self.currentPriceView.price = price;
	self.currentPriceView.hidden = YES;
	//TODO: FIX THIS
}

- (void) drawCurrentPriceAtY: (int) y
{
    int visualIndex = 0;
    for(int i=0; i < 10; i++)
    {
        if( y >= hs_x[i]  &&  y <= hs_x[i] + hs_h[i])
        {
            visualIndex = i;
            break;
        }
    }
    
    ScalerLinear *scaler = [self.scalers objectAtIndex: visualIndex];
    float price = [scaler toValue: y];
    
    [self drawCurrentPriceAtY: y price: price];
}
- (void) drawCurrentPrice
{
	if(self.lastTick)
	{
        float y = [self.scalerY toPixel: self.lastTick.close];
        [self drawCurrentPriceAtY: y price: self.lastTick.close];
		return;
	}

    if(self.priceData.count > 0)
    {
        PriceData *p = [self.priceData lastObject];
        float y = [self.scalerY toPixel: p.close];
        [self drawCurrentPriceAtY: y price: p.close];
    }
}

- (void) drawRect:(CGRect)rect
{
    NSDate *start = [NSDate date];
    
    //CGContextRef context = UIGraphicsGetCurrentContext();
    [_style.backgroundColor set];
    UIRectFill(rect);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, _style.dividerColor.CGColor);
    CGContextBeginPath(context);
    for(int i=0; i< self.total_plots; i++)
    {
        CGContextMoveToPoint(context, 0, hs_x[i]);
        CGContextAddLineToPoint(context, self.width, hs_x[i]);
    }
    CGContextMoveToPoint(context, 0, hs_x[self.total_plots-1] + hs_h[self.total_plots-1]);
    CGContextAddLineToPoint(context, self.width, hs_x[self.total_plots-1] + hs_h[self.total_plots-1]);
    CGContextStrokePath(context);
    
    [self drawPriceChart];
    [self drawCurrentPrice];
    
    if(CHART_TIME)
    {
        NSTimeInterval time = [start timeIntervalSinceNow];
        NSString *timeStr = [NSString stringWithFormat: @"Calculate Time %.3f s | Draw time %.3f s", timeToCalculate, fabs(time)];
        [timeStr drawInRect: CGRectMake(5,hs_x[0] + hs_h[0] - 15,300,12) withFont: [UIFont boldSystemFontOfSize: 10]];
    }
}
@end
