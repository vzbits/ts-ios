//
//  AlertVisualEditView.h
//  TSCharts
//
//  Created by Quoc Le on 8/16/13.
//
//

#import <UIKit/UIKit.h>

#import "Chart.h"

@protocol AlertVisualEditViewDelegate <NSObject>
- (void) alertVisualEditViewStateChanged: (UIGestureRecognizerState) state object: (id) obj  point: (CGPoint) point;
@end

@interface AlertVisualEditView : UIView
{
    CGPoint touchPoints[3];
    NSInteger numberOfTouches;
	BOOL notified;
	NSInteger handleIndexDown;
}
@property (strong) NSString *symbol;
@property (strong) NSArray *alerts;
@property (strong) NSDate *panStartDate;
@property (strong) NSMutableSet *myTouches;
@property (assign) Chart *myChart;
@property (assign) id<AlertVisualEditViewDelegate> delegate;

- (id)initWithChart: (Chart*) chart;
- (void) addNewAlertForPrice: (double) price  lastPrice: (double) lastPrice;
- (void) loadData:(NSString*) symbol;
@end
