//
//  CrossChairView.h
//  TSCharts
//
//  Created by Quoc Le on 8/2/12.
//
//

#import <UIKit/UIKit.h>
#import "Chart.h"

@protocol CrossChairDelegate <NSObject>
- (void) crossChairStateChanged: (UIGestureRecognizerState) state object: (id) obj  point: (CGPoint) point;
@end

@interface CrossChairView : UIView
{
    CGPoint touchPoints[3];
    NSInteger numberOfTouches;
	BOOL notified;
}
@property (strong) NSDate *panStartDate;
@property (strong) NSMutableSet *myTouches;
@property (assign) Chart *myChart;
@property (assign) id<CrossChairDelegate> delegate;

- (id)initWithChart: (Chart*) chart;

@end
