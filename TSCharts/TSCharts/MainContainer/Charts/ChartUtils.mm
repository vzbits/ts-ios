//
//  ChartUtils.m
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChartUtils.h"
#import "NSDate+Escort.h"

@implementation ChartUtils

+ (NSString*) formatYAxis: (float) val twoDecimal: (BOOL) twoDecimal
{
    NSString *sign = val < 0 ? @"-" : @"";
    val = fabs(val);
    
	if(val > 1e6) 		
        return [NSString stringWithFormat:@"%@%dM",sign, (int)round(val/1e6)];
    else if(val > 1e3)
        return [NSString stringWithFormat:@"%@%dK",sign, (int)round(val/1e3)];
    else if( val < 10  && val >= 0)
        return [NSString stringWithFormat:@"%@%.2f",sign, val];
    else {
		if (twoDecimal)
			return [NSString stringWithFormat:@"%@%.1f",sign, val];
		else
			return [NSString stringWithFormat:@"%@%d",sign, (int)val];
	}
}

+ (NSString*) formatCurrentPrice: (float) val
{
	if(val > 1e6)
        return [NSString stringWithFormat:@"%dM",(int)round(val/1e6)];
    else if(val > 1e3)
        return [NSString stringWithFormat:@"%dK",(int)round(val/1e3)];
    else if( val < 10  && val >= 0)
        return [NSString stringWithFormat:@"%.2f",val];
    else
        return [NSString stringWithFormat:@"%.2f",val];
}
+ (UIColor*) convertColor: (NSString*) str
{
    if([str hasPrefix:@"0x"])
    {
        return [UIColor colorWithHexString: str];
    }
    else
    {
        return [UIColor colorWithName: str];
    }
}

+ (float) pp: (float) pixel
{
    return pixel + 0.5f;
}

+ (NSString*) arrayToString: (NSArray*) arr
{
    NSMutableString *str = [NSMutableString string];
    for(int i=0; i< arr.count; i++)
    {
        id obj = [arr objectAtIndex:i];
        
        if([obj isKindOfClass:[NSNumber class]])
        {
            [str appendString: [obj stringValue]];
        }
        else if([obj isKindOfClass:[NSString class]])
        {
            [str appendString: obj];
        }
        
        // if not last one
        if(i != arr.count - 1)
        {
            [str appendString:@", "];
        }
    }
    return str;
}

+ (NSDate*) getStartDateForTick: (int) ticks periodicity: (PriceType) priceType
{
	if(priceType == PriceType_PT_DAILY)
	{
		int weeks = ceilf(ticks/5.0f);
		return [NSDate dateWithDaysBeforeNow: weeks * 7];
	}
	if(priceType == PriceType_PT_WEEKLY)
	{
		return [NSDate dateWithDaysBeforeNow: ticks * 7];
	}
	if(priceType == PriceType_PT_MONTHLY)
	{
		return [NSDate dateWithDaysBeforeNow: ticks * 30];
	}
	
	return nil;
}
@end
