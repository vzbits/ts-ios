//
//  Chart.h
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Indicator.h"
#import "StockPrice.h"
#import "ScalerOrdinal.h"
#import "ScalerLinear.h"
#import "ScalerLog.h"
#import "UIDateExtensions.h"
#import "ChartUtils.h"
#import "ServiceManager.h"
#import "IndicatorJS.h"
#import "CurrentPriceView.h"
#import "TickView.h"
#import "ChartStyle.h"

@interface Chart : UIView
{
    int hs_x[10];
    int hs_h[10];
    int axis_width;
    
    NSTimeInterval timeToCalculate;
    bool volume;
}
@property (strong, nonatomic) NSString *drawStyle;
@property (strong, nonatomic) ChartStyle *style;
@property (assign, nonatomic) int padding;
@property (assign, nonatomic) int total_plots;
@property (strong, nonatomic) ScalerOrdinal *scalerX;
@property (strong, nonatomic) Scaler *scalerY;
@property (strong, nonatomic) Scaler *scalerVolume;
@property (strong, nonatomic) CurrentPriceView *currentPriceView;
@property (strong, nonatomic) PriceData* lastTick;
@property (strong, nonatomic) TickView *lastTickView;

@property (strong, nonatomic) NSMutableArray *xGrids;
//@property (strong, nonatomic) NSArray *priceData;
@property (strong, nonatomic) NSArray *indicators;
@property (strong, nonatomic) NSMutableArray *scalers;
@property (strong, nonatomic) NSMutableArray *valueLabels;
@property (assign, nonatomic) PriceType priceType;

@property (strong, nonatomic) UIColor *mountainStartFillColor;
@property (strong, nonatomic) UIColor *mountainEndFillColor;

@property (assign, nonatomic) float ohlcWidth;
@property (assign, nonatomic) float ohlcThick;
@property (strong) NSMutableArray *priceData;
- (void) loadChart: (StockPriceData*) stockPrice Indicators: (NSArray*) indicators;
- (void) drawCurrentPriceAtY: (int) y;
- (void) drawCurrentPrice;
@end
