//
//  Indicator.h
//  TSCharts
//
//  Created by Quoc Le on 7/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum
{
    INDICATOR_OVERLAY,
    INDICATOR_PLOT
} IndicatorType;

@interface Indicator : NSObject <NSCoding>
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) IndicatorType type;
@property (strong, nonatomic) NSMutableArray *plotData;
@property (strong, nonatomic) NSMutableArray *plotColor;
@property (strong, nonatomic) NSMutableArray *plotType;
@property (strong, nonatomic) NSMutableArray *plotNames;
@property (strong, nonatomic) NSMutableArray *inputNames;
@property (strong, nonatomic) NSMutableArray *inputValues;
@property (strong, nonatomic) NSMutableArray *inputTypes;
@end
