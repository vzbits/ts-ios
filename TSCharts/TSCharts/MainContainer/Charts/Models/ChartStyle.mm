//
//  ChartStyle.m
//  TSCharts
//
//  Created by Quoc Le on 7/27/13.
//
//

#import "ChartStyle.h"

@implementation ChartStyle

+ (id) defaultStyle
{
	return [ChartStyle blueStyle];
}

+ (id) whiteStyle
{
	ChartStyle *chartStyle = [[[ChartStyle alloc] init] autorelease];
	chartStyle.fontColor = [UIColor blackColor];
	chartStyle.backgroundColor = [UIColor whiteColor];
	chartStyle.upColor = 0xff;
	chartStyle.downColor = 0xff0000;
	chartStyle.lineWidth = 2;
	chartStyle.lineColor = [UIColor colorWithRGBHex:0x4572A7];
	chartStyle.wickPixelSize = 1;
	chartStyle.bodyPixelSize = 3;
	chartStyle.gridColor = [UIColor colorWithRGBHex:0xeeeeee];
	return chartStyle;
}

+ (id) blueStyle
{
	ChartStyle *chartStyle = [[[ChartStyle alloc] init] autorelease];
	chartStyle.gridColor = [UIColor clearColor];
	chartStyle.dividerColor = [UIColor clearColor];
	chartStyle.fontColor = [UIColor colorWithRGBHex: 0x8d9fa5];
	chartStyle.backgroundColor = [UIColor colorWithRGBHex:0x272c2e];
	chartStyle.upColor = 0x936bf9;
	chartStyle.downColor = 0x936bf9;
	chartStyle.wickColor = 0x08FF3A;
	chartStyle.lineWidth = 1.5;
	chartStyle.lineColor = [UIColor colorWithRGBHex:0x936bf9];
	chartStyle.wickPixelSize = 1;
	chartStyle.bodyPixelSize = 3;
	return chartStyle;
}
@end
