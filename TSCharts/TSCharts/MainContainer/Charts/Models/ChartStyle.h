//
//  ChartStyle.h
//  TSCharts
//
//  Created by Quoc Le on 7/27/13.
//
//

#import <Foundation/Foundation.h>

@interface ChartStyle : NSObject
@property (strong) UIColor *backgroundColor;
@property (strong) UIColor *fontColor;
@property (strong) UIColor *gridColor;
@property (strong) UIColor *dividerColor;
@property (assign) int wickColor;
@property (assign) int upColor;
@property (assign) int downColor;
@property (strong) UIFont *axisFontColor;
@property (assign) int lineWidth;
@property (strong) UIColor *lineColor;
@property (assign) int wickPixelSize;
@property (assign) int bodyPixelSize;

+ (id) whiteStyle;
+ (id) blueStyle;
+ (id) defaultStyle;
@end
