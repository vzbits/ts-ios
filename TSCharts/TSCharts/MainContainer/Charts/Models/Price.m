//
//  Price.m
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Price.h"

@implementation Price
@synthesize open = _open;
@synthesize close = _close;
@synthesize high = _high;
@synthesize low = _low;
@synthesize volume = _volume;
@synthesize date = _date;


- (void) dealloc
{
    self.date = nil;
    [super dealloc];
}

@end
