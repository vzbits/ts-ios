//
//  Indicator.m
//  TSCharts
//
//  Created by Quoc Le on 7/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Indicator.h"

@implementation Indicator
@synthesize type = _type;
@synthesize name = _name;
@synthesize plotData = _plotData;
@synthesize plotColor = _plotColor;
@synthesize plotType = _plotType;
@synthesize identifier = _identifier;

- (id) init
{
    if(self = [super init])
    {
        self.plotData = [NSMutableArray arrayWithCapacity:10];
        self.plotColor = [NSMutableArray arrayWithCapacity:10];
        self.plotType = [NSMutableArray arrayWithCapacity:10];
        self.inputNames = [NSMutableArray array];
        self.inputTypes = [NSMutableArray array];
        self.inputValues = [NSMutableArray array];
        self.plotNames = [NSMutableArray array];
        self.identifier = @"";
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)coder
{
    self = [[Indicator alloc] init];
    
    if(self != nil)
    {
        self.identifier = [coder decodeObjectForKey:@"identifier"];
        self.name = [coder decodeObjectForKey:@"name"];

        self.plotColor = [coder decodeObjectForKey:@"plotColor"];
        self.plotType = [coder decodeObjectForKey:@"plotType"];
        self.inputNames = [coder decodeObjectForKey:@"inputNames"];
        self.inputValues = [coder decodeObjectForKey:@"inputValues"];
        self.plotNames = [coder decodeObjectForKey:@"plotNames"];

    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject: self.identifier forKey:@"identifier"];
    [coder encodeObject: self.name forKey:@"name"];
    
    [coder encodeObject: self.plotColor forKey:@"plotColor"];
    [coder encodeObject: self.plotType forKey:@"plotType"];
    [coder encodeObject: self.inputNames forKey:@"inputNames"];
    [coder encodeObject: self.inputValues forKey:@"inputValues"];
    [coder encodeObject: self.plotNames forKey:@"plotNames"];
}

- (void) dealloc
{
    //NSLog(@"Dealloc indicator %x", self);
    self.plotColor = nil;
    self.plotData = nil;
    self.plotType = nil;
    self.name = nil;
    [super dealloc];
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"name=%@|id=%@", self.name, self.identifier];
}
@end
