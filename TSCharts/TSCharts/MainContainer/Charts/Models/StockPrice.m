//
//  StockPrice.m
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StockPrice.h"
#import "UIDateExtensions.h"
#import "JSONKit.h"

@implementation StockPrice
@synthesize prices = _prices;
@synthesize symbol = _symbol;
@synthesize name = _name;
//
//+ (StockPrice*) stockPriceWithMP: (NSData*) data
//{
//    NSDictionary *root = [data messagePackParse];
//    NSArray *prices = [root objectForKey:@"prices"];
//
//    StockPrice *s = [[[StockPrice alloc] init] autorelease];
//    NSMutableArray *pList = [NSMutableArray arrayWithCapacity:1];
//    s.symbol = [root objectForKey:@"symbol"];
//    s.name = [root objectForKey:@"name"];
// 
//    for(NSArray *key in prices.reverseObjectEnumerator)
//    {        
//        if(key.count < 6)
//            continue;
//        
//        Price *p = [[[Price alloc] init] autorelease];
//
//        NSString *d = [key objectAtIndex:0];
//        NSDate *nsDate = [NSDate dateFromJSON: d];
//        //NSLog(@"date =  %@ %@", d, [nsDate description]);
//        p.date = nsDate;
//        p.open = [[key objectAtIndex:1] floatValue];
//        p.high = [[key objectAtIndex:2] floatValue];
//        p.low = [[key objectAtIndex:3] floatValue];
//        p.close = [[key objectAtIndex:4] floatValue];
//        p.volume = [[key objectAtIndex:5] floatValue];
//        
//        [pList addObject: p];
//    }
//    s.prices = pList;
//    //NSLog(@"a %@", [pList description]);
//    return s;
//}
//

+ (StockPrice*) stockPriceWithJSON: (NSData*) data;
{
    NSDictionary *root = [data objectFromJSONData];
    NSArray *prices = [root objectForKey:@"prices"];
    
    StockPrice *s = [[[StockPrice alloc] init] autorelease];
    NSMutableArray *pList = [NSMutableArray arrayWithCapacity:1];
    s.symbol = [root objectForKey:@"symbol"];
    s.name = [root objectForKey:@"name"];
    
    for(NSArray *key in prices.reverseObjectEnumerator)
    {        
        if(key.count < 6)
            continue;
        
        Price *p = [[[Price alloc] init] autorelease];
        
        NSString *d = [key objectAtIndex:0];
        NSDate *nsDate = [NSDate dateFromJSON: d];
        //NSLog(@"date =  %@ %@", d, [nsDate description]);
        p.date = nsDate;
        p.open = [[key objectAtIndex:1] floatValue];
        p.high = [[key objectAtIndex:2] floatValue];
        p.low = [[key objectAtIndex:3] floatValue];
        p.close = [[key objectAtIndex:4] floatValue];
        p.volume = [[key objectAtIndex:5] floatValue];
        
        [pList addObject: p];
    }
    s.prices = pList;
    //NSLog(@"a %@", [pList description]);
    return s;
}

- (void) dealloc
{
    self.name =nil;
    self.prices = nil;
    self.symbol = nil;
    
    [super dealloc];
}
@end
