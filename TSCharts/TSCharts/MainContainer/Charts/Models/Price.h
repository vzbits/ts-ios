//
//  Price.h
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Price : NSObject
@property (assign, nonatomic) float open;
@property (assign, nonatomic) float close;
@property (assign, nonatomic) float low;
@property (assign, nonatomic) float high;
@property (assign, nonatomic) float volume;
@property (strong, nonatomic) NSDate *date;

@end
