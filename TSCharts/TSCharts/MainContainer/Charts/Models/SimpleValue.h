//
//  SimpleValue.h
//  TSCharts
//
//  Created by Quoc Le on 7/29/12.
//
//

#import <Foundation/Foundation.h>

@interface SimpleValue : NSObject
@property(strong) NSDate *date;
@property(assign) float value;
@end
