//
//  SimpleValue.m
//  TSCharts
//
//  Created by Quoc Le on 7/29/12.
//
//

#import "SimpleValue.h"

@implementation SimpleValue
@synthesize value = _value;
@synthesize date = _date;

- (void) dealloc
{
    self.date = nil;
    [super dealloc];
}

@end
