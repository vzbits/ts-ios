//
//  StockPrice.h
//  TSCharts
//
//  Created by Quoc Le on 7/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Price.h"
//#import "MessagePack.h"

@interface StockPrice : NSObject
{

}
@property (strong, nonatomic) NSString *symbol;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSArray *prices;

+ (StockPrice*) stockPriceWithMP: (NSData*) data;
+ (StockPrice*) stockPriceWithJSON: (NSData*) data;
@end
