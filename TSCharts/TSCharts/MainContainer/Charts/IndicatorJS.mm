//
//  IndicatorJS.m
//  TSCharts
//
//  Created by Quoc Le on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "IndicatorJS.h"
#include <sys/types.h>
#include <sys/stat.h>
#import "ASIHTTPRequest.h"
#import "ServiceManager.h"
#import "jsdate.h"
#import "Settings.h"

#define ENGINE_JS "vsi.indicator.js"

#define NSLog //

void
printError(JSContext *cx, const char *message, JSErrorReport *report)
{
    int where;
    const char* cstr;
    
    fprintf(stderr, "JSERROR: %s:%d:\n    %s\n",
            (report->filename ? report->filename : "NULL"),
            report->lineno,
            message);
    if (report->linebuf) {
        if (!(cstr=(report->linebuf))) {
            fprintf(stderr, "out of memory\n");return;
        }
        fprintf(stderr, "    \"%s\"\n",cstr);
        //free(cstr);
        if (report->tokenptr) {
            where=report->tokenptr - report->linebuf;
            /* Todo: 80 */
            if ((where>=0)&&(where<80)) {
                where+=6;
                while (--where>0) fputc(' ',stderr);
                fprintf(stderr, "^\n");
            }
        }
    }
    fprintf(stderr, "    Flags:");
    if (JSREPORT_IS_WARNING(report->flags)) fprintf(stderr, " WARNING");
    if (JSREPORT_IS_EXCEPTION(report->flags)) fprintf(stderr, " EXCEPTION");
    if (JSREPORT_IS_STRICT(report->flags)) fprintf(stderr, " STRICT");
    fprintf(stderr, " (Error number: %d)\n", report->errorNumber);
    /* print exception object */
    //printJSException(cx);
}

using namespace std;

JSClass js_global_object_class = {
    "System",
    0,
    JS_PropertyStub,
    JS_PropertyStub,
    JS_PropertyStub,
    JS_PropertyStub,
    JS_EnumerateStub,
    JS_ResolveStub,
    JS_ConvertStub,
    JS_FinalizeStub,
    JSCLASS_NO_OPTIONAL_MEMBERS
};


IndicatorJS::IndicatorJS()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDir = string([documentsDirectory UTF8String]);
    //NSLog(@"Docs %@", documentsDirectory);
    indicators = [[NSMutableArray alloc] init];
    
    buildjs_runtime();
    
	// TODO: disable this
//    if( !file_exists(ENGINE_JS) )
//    {
//        download_script(ENGINE_JS, true);
//    }
    
    if(execute_file(context, get_absolute(ENGINE_JS).c_str()) == JS_TRUE)
    {
        NSLog(@"Successfully loaded Engine JS");
        
        jsval obj;
        if (JS_GetProperty(context, jsObj, "vsi_indicator", &obj))
        {
            vsiObj = JSVAL_TO_OBJECT(obj);
            //get_indicator();
            //add_indicator(17);
            
            load_settings();
        }
    }
}

void IndicatorJS::load_settings()
{
    remove_all();
    
    NSArray *indicatorsLoaded = [[Settings instance] loadIndicators];
    releaseAndNil(indicators);
    indicators = [[NSMutableArray alloc] initWithArray: indicatorsLoaded];
    
    for(Indicator *ind in indicators)
    {
        add_indicator([ind.identifier UTF8String]);
    }
    NSLog(@"Loaded indicators %d", indicators.count);
}

bool IndicatorJS::buildjs_runtime()
{
    runtime = JS_NewRuntime(1024L*1024L);
    if (!runtime){
        printf("Failed to initialize JS Runtime.\n");
        return false;
    }
    
    context = JS_NewContext(runtime, 8192);
    if (!context){
        printf("Failed to initialize JS Context.\n");
        JS_DestroyRuntime(runtime);
        return false;
    }
    
    jsObj = JS_NewObject(context, NULL, NULL, NULL);
    if (!jsObj){
        printf("Failed to create global object.\n");
        JS_DestroyContext(context);
        JS_DestroyRuntime(runtime);
        return false;
    }
    
    JS_InitStandardClasses(context, jsObj);
        
    return true;
}

string IndicatorJS::build_remote_url(std::string script, bool engine)
{
    if(engine)
    {
        NSString *str = [NSString stringWithFormat: @"%@%@", [ServiceManager getServerURL], [ServiceManager getScriptPath]];
        return string([str UTF8String]);
    }
    else 
    {
        NSString *str = [NSString stringWithFormat: @"%@/api/indicator_codes/%s.txt", [ServiceManager getServerURL], script.c_str()];
        return string([str UTF8String]);
    }
}

string IndicatorJS::get_absolute(std::string script)
{
    return documentsDir + "/" + script;
}
bool IndicatorJS::file_exists(std::string script)
{
    struct stat statinfo;
    string scriptAbsolute = get_absolute(script);
    return (stat(scriptAbsolute.c_str(), &statinfo) != -1);
}

void IndicatorJS::download_script(std::string script, bool engine)
{
    std::string remote_url = build_remote_url(script, engine);
    NSLog(@"Remote URL %s to %s", remote_url.c_str(), get_absolute(script).c_str());
    NSString *url = [NSString stringWithUTF8String: remote_url.c_str()];
    NSURL *urlType = [NSURL URLWithString:url];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL: urlType];    
    [request setDownloadDestinationPath: [NSString stringWithUTF8String: get_absolute(script).c_str()]];    
    [request startSynchronous];
}

JSBool IndicatorJS::execute_file(JSContext *ctx, const char *file)
{
    NSLog(@"Loading script %s", file);
        
    JSScript *script;
    jsval returnValue;
    JSObject *global = JS_GetGlobalObject(ctx);
    struct stat statinfo;
    
    if (file == NULL){
        return JS_FALSE;
    }
    
    if (stat(file, &statinfo) == -1){
        return JS_FALSE;
    }
    
    if (!S_ISREG(statinfo.st_mode)){
        return JS_FALSE;
    }
    
    script = JS_CompileFile(ctx, global, file);
    
    if (script == NULL){
        return JS_FALSE;
    }
    
    return JS_ExecuteScript(ctx, global, script, &returnValue);
}

void IndicatorJS::add_indicator(std::string identifier)
{
    NSLog(@"Adding indicator %s", identifier.c_str());
    
    //http://localhost:3000/indicator_codes/21.json
    // the last line
    //NSString *str = [NSString stringWithFormat:@"%d",identifier];
    download_script(identifier);

    NSString *filename = [NSString stringWithFormat:@"%s", get_absolute(identifier).c_str()];
    NSString* content = [NSString stringWithContentsOfFile: filename
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    
    NSLog(@"Content %@ size=%d", content, [content length]);

    jsval retVal;
    jsval input[1];
    //input[0] = INT_TO_JSVAL(lastIndicatorIndex++);
    input[0] = STRING_TO_JSVAL(JS_NewStringCopyZ(context, [content UTF8String]));
    
    if( JS_CallFunctionName(context, vsiObj, "add_indicator", 1, input, &retVal))
    {
        int indicatorCount = JSVAL_TO_INT(retVal);
        NSLog(@"Added.. %d %s", indicatorCount, identifier.c_str());
        lastIdentifier = identifier;
        //get_indicator();
    }    
}

void IndicatorJS::get_indicator()
{
    jsval retVal;
    if( JS_CallFunctionName(context, vsiObj, "get_indicators", 0, NULL, &retVal))
    {
        jsuint len;
        JSObject *object = JSVAL_TO_OBJECT(retVal);
        JS_GetArrayLength(context, object, &len);
        NSLog(@"call get_indicator size=%d", len);
        jsval current;
        BOOL existing;
        //[indicators removeAllObjects];
        for(int i=0; i < len ; i++)
        {
            JS_GetElement(context, object, i, &current);
            JSObject *currenObj = JSVAL_TO_OBJECT(current);
            
            Indicator *ind;
            if(i < indicators.count)
            {
                ind = [indicators objectAtIndex: i];
                existing = YES;
            }
            else
            {
                existing = NO;
                ind = [[[Indicator alloc] init] autorelease];
                ind.identifier = [NSString stringWithUTF8String: lastIdentifier.c_str()];
                [indicators addObject: ind];
            }
            // get name
            jsval obj;
            string code ("code.meta.name");
            if(JS_EvaluateScript(context, currenObj, code.c_str(), code.length(), NULL, NULL, &obj))
            {
                JSString *str = JSVAL_TO_STRING(obj);
                //printf("Some val %s", (char*)JS_GetStringBytes(str));
                ind.name = [NSString stringWithUTF8String: JS_GetStringBytes(str)];
            }
            
            // get type
            code = "code.meta.type";
            if(JS_EvaluateScript(context, currenObj, code.c_str(), code.length(), NULL, NULL, &obj))
            {
                JSString *str = JSVAL_TO_STRING(obj);
                char *cstr = JS_GetStringBytes(str);
                //printf("Some val %s", cstr);
                
                if(strcmp(cstr, "overlay") == 0)
                    ind.type  = INDICATOR_OVERLAY;
                else
                    ind.type  = INDICATOR_PLOT;
            }
            
            if(JS_GetProperty(context, currenObj, "plotData", &obj))
            {
                JSObject *plotData = JSVAL_TO_OBJECT(obj);
                jsuint plotCount;
                JS_GetArrayLength(context, plotData, &plotCount);
                
                [ind.plotData removeAllObjects];
                
                for(int j=0; j< plotCount; j++)
                {
                    NSMutableArray *nplot = [NSMutableArray array];
                    [ind.plotData addObject: nplot];
                    
                    jsval plotVal;
                    if(JS_GetElement(context, plotData, j, &plotVal)) // inside array
                    {
                        JSObject *plotObject = JSVAL_TO_OBJECT(plotVal);
                        jsuint len;
                        JS_GetArrayLength(context, plotObject, &len);
                        jsval plotPoint;
                        
                        for(int x = 0; x < len; x++)
                        {
                            if(JS_GetElement(context, plotObject, x, &plotPoint))
                            {
                                JSObject *plotPointObj = JSVAL_TO_OBJECT(plotPoint);

                                SimpleValue *simpleValue = [[[SimpleValue alloc] init] autorelease];
                                jsval dateVal, valueVal;
                                
                                if(JS_GetProperty(context, plotPointObj, "Date", &dateVal))
                                {
                                    JSObject *dateOBJ = JSVAL_TO_OBJECT(dateVal);
                                    double time = js_DateGetMsecSinceEpoch(context, dateOBJ);
                                    simpleValue.date = [NSDate dateWithTimeIntervalSince1970: time];
                                }
                                if(JS_GetProperty(context, plotPointObj, "Value", &valueVal))
                                {
                                    if(JSVAL_IS_DOUBLE(valueVal))
                                        simpleValue.value = *JSVAL_TO_DOUBLE(valueVal);
                                    else if(JSVAL_IS_INT(valueVal))
                                        simpleValue.value = JSVAL_TO_INT(valueVal);
                                }
                                //NSLog(@"date = %@ value=%f",  [simpleValue.date description] , simpleValue.value);
                                [nplot addObject: simpleValue];
                            }
                        }
                        
                        NSLog(@"Plot %d counts=%d", j, [nplot count]);
                    }
                }
            }
            
            if(!existing)
            {
                if(JS_GetProperty(context, currenObj, "plotColor", &obj))
                {
                    JSObject *plotColor = JSVAL_TO_OBJECT(obj);
                    jsuint plotCount;
                    JS_GetArrayLength(context, plotColor, &plotCount);

                    [ind.plotColor removeAllObjects];
                    
                    for(int j=0; j < plotCount; j++)
                    {
                        jsval plotVal;
                        if(JS_GetElement(context, plotColor, j, &plotVal)) // inside array
                        {
                            JSString *str = JSVAL_TO_STRING(plotVal);
                            [ind.plotColor addObject: [NSString stringWithUTF8String: JS_GetStringBytes(str)]];
                        }
                    }
                    NSLog(@"PlotColor count %d",plotCount);
                    
                }
            }

            if(JS_GetProperty(context, currenObj, "plotType", &obj))
            {
                JSObject *plotColor = JSVAL_TO_OBJECT(obj);
                jsuint plotCount;
                JS_GetArrayLength(context, plotColor, &plotCount);
                
                [ind.plotType removeAllObjects];
                
                for(int j=0; j < plotCount; j++)
                {
                    jsval plotVal;
                    if(JS_GetElement(context, plotColor, j, &plotVal)) // inside array
                    {
                        JSString *str = JSVAL_TO_STRING(plotVal);
                        [ind.plotType addObject: [NSString stringWithUTF8String: JS_GetStringBytes(str)]];
                    }
                }
            }

            [ind.plotNames removeAllObjects];

            if(JS_GetProperty(context, currenObj, "plotNames", &obj) && JSVAL_IS_OBJECT(obj))
            {
                JSObject *plotColor = JSVAL_TO_OBJECT(obj);
                jsuint plotCount;
                JS_GetArrayLength(context, plotColor, &plotCount);
                
                
                for(int j=0; j < plotCount; j++)
                {
                    jsval plotVal;
                    if(JS_GetElement(context, plotColor, j, &plotVal)) // inside array
                    {
                        JSString *str = JSVAL_TO_STRING(plotVal);
                        NSString *nsStr = [NSString stringWithUTF8String: JS_GetStringBytes(str)];
                        NSLog(@"Plotname = %@", nsStr);
                        [ind.plotNames addObject: nsStr];
                    }
                }
                NSLog(@"plotNames count %d",plotCount);
            }
            
            [ind.inputTypes removeAllObjects];
            if(JS_GetProperty(context, currenObj, "inputTypes", &obj)&& JSVAL_IS_OBJECT(obj))
            {                
                JSObject *plotColor = JSVAL_TO_OBJECT(obj);
                jsuint plotCount;
                JS_GetArrayLength(context, plotColor, &plotCount);
                                
                for(int j=0; j < plotCount; j++)
                {
                    jsval plotVal;
                    if(JS_GetElement(context, plotColor, j, &plotVal)) // inside array
                    {
                        JSString *str = JSVAL_TO_STRING(plotVal);
                        [ind.inputTypes addObject: [NSString stringWithUTF8String: JS_GetStringBytes(str)]];
                    }
                }
                NSLog(@"InputTypes count %d",plotCount);
            }
            
            [ind.inputNames removeAllObjects];
            if(JS_GetProperty(context, currenObj, "inputNames", &obj)&& JSVAL_IS_OBJECT(obj))
            {
                JSObject *plotColor = JSVAL_TO_OBJECT(obj);
                jsuint plotCount;
                JS_GetArrayLength(context, plotColor, &plotCount);
                
                for(int j=0; j < plotCount; j++)
                {
                    jsval plotVal;
                    if(JS_GetElement(context, plotColor, j, &plotVal)) // inside array
                    {
                        JSString *str = JSVAL_TO_STRING(plotVal);
                        [ind.inputNames addObject: [NSString stringWithUTF8String: JS_GetStringBytes(str)]];
                    }
                }
                NSLog(@"InputNames count %d",plotCount);
            }
            
            if(!existing)
            {
                [ind.inputValues removeAllObjects];
                if(JS_GetProperty(context, currenObj, "inputs", &obj)&& JSVAL_IS_OBJECT(obj))
                {
                    JSObject *plotColor = JSVAL_TO_OBJECT(obj);
                    jsuint plotCount;
                    JS_GetArrayLength(context, plotColor, &plotCount);
                    
                    for(int j=0; j < plotCount; j++)
                    {
                        jsval plotVal;
                        if(JS_GetElement(context, plotColor, j, &plotVal)) // inside array
                        {
                            if(JSVAL_IS_STRING(plotVal))
                            {
                                JSString *str = JSVAL_TO_STRING(plotVal);
                                [ind.inputValues addObject: [NSString stringWithUTF8String: JS_GetStringBytes(str)]];
                            }
                            else if(JSVAL_IS_INT(plotVal))
                            {
                                jsint val = JSVAL_TO_INT(plotVal);
                                [ind.inputValues addObject: [NSNumber numberWithInt: val]];
                            }
                        }
                    }
                    NSLog(@"InputNames count %d",plotCount);
                }
            }
        }
    }
    
    [[Settings instance] saveIndicators: indicators];
}

JSObject *IndicatorJS::stockPriceToJSArray(StockPriceData *data)
{
    JSObject *jsObject = JS_NewArrayObject(context, 0, NULL);
    
    for(int i=0; i < data.prices.count; i ++)
    {
        PriceData *p = [data.prices objectAtIndex:i];
        JSObject *priceObj = JS_NewObject(context, NULL, NULL, NULL);

        jsval openVal;
        JS_NewNumberValue(context, p.open, &openVal);
        JS_SetProperty(context, priceObj, "Open", &openVal);

        jsval closeVal;
        JS_NewNumberValue(context, p.close, &closeVal);
        JS_SetProperty(context, priceObj, "Close", &closeVal);

        jsval lowVal;
        JS_NewNumberValue(context, p.low, &lowVal);
        JS_SetProperty(context, priceObj, "Low", &lowVal);
        
        jsval highVal;
        JS_NewNumberValue(context, p.high, &highVal);
        JS_SetProperty(context, priceObj, "High", &highVal);

        jsval volumeVal;
        JS_NewNumberValue(context, p.volume, &volumeVal);
        JS_SetProperty(context, priceObj, "Volume", &volumeVal);

        //NSLog(@"Date %d", p.date());
        JSObject *dateObj = js_NewDateObjectMsec(context, p.date/1000);
        jsval dateVal = OBJECT_TO_JSVAL(dateObj);
        JS_SetProperty(context, priceObj, "Date", &dateVal);
        
        JS_DefineElement(context, jsObject, i, OBJECT_TO_JSVAL(priceObj), NULL, NULL, JSPROP_ENUMERATE);
        
        //NSLog(@"define element %d", i);
    }
    
    return jsObject;
}

void IndicatorJS::set_inputs(int index)
{
    Indicator *ind = [indicators objectAtIndex: index];
    NSArray *inputValues = ind.inputValues;
    
    JSObject *jsObject = JS_NewArrayObject(context, 0, NULL);
    for(int i=0; i< inputValues.count; i++)
    {
        jsval val;
        id obj = [inputValues objectAtIndex:i];
        if([obj isKindOfClass: [NSNumber class]])
        {
            val = INT_TO_JSVAL([obj intValue]);
        }
        else if([obj isKindOfClass: [NSString class]])
        {
            const char *str = [obj UTF8String];
            val = STRING_TO_JSVAL(JS_NewStringCopyN(context, str , strlen(str)));
        }
        
        JS_DefineElement(context, jsObject, i, val, NULL, NULL, JSPROP_ENUMERATE);
    }
    
    jsval input[2];
    jsval retVal;
    input[0] = INT_TO_JSVAL(index);
    input[1] = OBJECT_TO_JSVAL(jsObject);
        
    if( JS_CallFunctionName(context, vsiObj, "set_indicator_input", 2, input, &retVal))
    {
        JS_GC(context);
        NSLog(@"set_indicator_input success!");
    }
}
void IndicatorJS::set_inputs()
{
    for(int i=0; i < indicators.count; i++)
    {
        set_inputs(i);
    }
}

void IndicatorJS::run_indicator(StockPriceData *data)
{
    NSLog(@"Running indicators...");

    set_inputs();

    NSDate *startTime = [NSDate date];
    
    JS_SetErrorReporter(context, printError);

    JSObject *dataObj = stockPriceToJSArray(data);
    jsval input[1];
    jsval retVal;
    input[0] = OBJECT_TO_JSVAL(dataObj);
        
    if( JS_CallFunctionName(context, vsiObj, "run_indicator", 1, input, &retVal))
    {
        NSTimeInterval time = [startTime timeIntervalSinceNow];
        startTime = [NSDate date];
        NSLog(@"Success.. Time=%.3f s", fabs(time));
        JS_GC(context);
        get_indicator();
        
        time = fabs([startTime timeIntervalSinceNow]);
        NSLog(@"Get_indicator.. Time=%.3f s", time);

    }
}

void IndicatorJS::remove_indicator(int index)
{
    NSLog(@"remove_indicator %d", index);
    
    jsval retVal;
    jsval input[1];
    input[0] = INT_TO_JSVAL(index);
    
    if( JS_CallFunctionName(context, vsiObj, "remove_indicator", 1, input, &retVal))
    {
        NSLog(@"Remove indicator at %d", index);
        //get_indicator();
    }
}

void IndicatorJS::remove_all()
{
    jsval retVal;
    
    if( JS_CallFunctionName(context, vsiObj, "remove_all", 0, NULL, &retVal))
    {
        NSLog(@"Remove ALL");
    }
}
