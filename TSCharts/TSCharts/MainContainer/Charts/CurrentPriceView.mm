//
//  CurrentPriceView.m
//  TSCharts
//
//  Created by Quoc Le on 8/2/12.
//
//

#import "CurrentPriceView.h"
#import "ChartUtils.h"

#define ARROW_WIDTH 5

@implementation CurrentPriceView

@synthesize price = _price;

- (id)init
{
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void) setPrice: (float) price_
{
    _price = price_;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    UIColor *strokeColor = [UIColor blueColor];
    UIColor *fillColor = [Theme currentPriceBackgroundColor];
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 1.0);
    CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
    CGContextSetFillColorWithColor(context, fillColor.CGColor);
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, self.width, 0);
    CGContextAddLineToPoint(context, ARROW_WIDTH, 0);
    CGContextAddLineToPoint(context, 0, self.height/2);
    CGContextAddLineToPoint(context, ARROW_WIDTH, self.height);
    CGContextAddLineToPoint(context, self.width, self.height);
    CGContextAddLineToPoint(context, self.width, 0);
    //CGContextStrokePath(context);
    CGContextFillPath(context);
    
    [[UIColor blackColor] set];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size: 11];
    NSString *indicatorName = [ChartUtils formatCurrentPrice: self.price];
    CGRect textRect = CGRectMake(ARROW_WIDTH, 2, self.width - ARROW_WIDTH, self.height - 4);
    [indicatorName drawInRect:textRect withFont: font lineBreakMode: UILineBreakModeClip alignment: UITextAlignmentCenter];
}

@end
