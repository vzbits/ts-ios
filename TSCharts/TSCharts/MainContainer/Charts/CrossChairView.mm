//
//  CrossChairView.m
//  TSCharts
//
//  Created by Quoc Le on 8/2/12.
//
//

#import "CrossChairView.h"

@implementation CrossChairView

@synthesize myChart   = _myChart;
@synthesize delegate = _delegate;

- (id)initWithChart: (Chart*) chart
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.myChart = chart;
        self.multipleTouchEnabled = YES;
        self.myTouches = [NSMutableSet set];
//        UIPanGestureRecognizer *panGesture = [[[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(panOne:)] autorelease];
//        panGesture.cancelsTouchesInView = NO;
//        [self addGestureRecognizer: panGesture];
    }
    return self;
}

- (void) panOne: (UIPanGestureRecognizer*) gesture
{
    
    for(int i=0; i< MIN(2,gesture.numberOfTouches); i++)
    {
        touchPoints[i] = [gesture locationOfTouch: i inView: self];
    }

    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        numberOfTouches = gesture.numberOfTouches;
    }
    else if(gesture.state == UIGestureRecognizerStateChanged)
    {
        numberOfTouches = gesture.numberOfTouches;
    }
    else if(gesture.state == UIGestureRecognizerStateCancelled || gesture.state == UIGestureRecognizerStateEnded)
    {
        numberOfTouches = 0;
    }
    [self setNeedsDisplay];
}


- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    /*
     int index = 0;
     for(UITouch *touch in touches)
     {
     if(index >= 2) return;
     touchPoints[index++] = [touch locationInView: self];
     }
     numberOfTouches = index;
     */
    
	notified = NO;
	self.panStartDate = [NSDate date];
	[self.myTouches unionSet: touches];
	
	[NSTimer scheduledTimerWithTimeInterval:0.15f target: self selector:@selector(timerFired:) userInfo:nil repeats: NO];
}

- (void) timerFired: (NSTimer*) timer
{
	[self setNeedsDisplay];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	if(!notified)
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:@"START_SCROLL" object: nil];
		notified = YES;
	}
	
	if(fabsf([self.panStartDate timeIntervalSinceNow]) > 0.15f)
	{
		[self.myTouches unionSet: touches];
		[self setNeedsDisplay];
	}
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.myTouches minusSet: touches];
    [self setNeedsDisplay];
 
	[[NSNotificationCenter defaultCenter] postNotificationName:@"END_SCROLL" object: nil];

    //NSLog(@"Cross chair ended");
}
- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self touchesEnded: touches withEvent: event];
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor *darkGray = [UIColor colorWithRGBHex: 0];
    int y_offset = -30.0;
    id firstObject = nil;
    CGPoint firstPoint = CGPointMake(-1, -1);
    int count = 0;
    int axis_width = 45;
    
    numberOfTouches = self.myTouches.count;
    
    for(UITouch *touch in self.myTouches)
    {
        count++;

        if(count > 2 )
            return;
        
        //CGPoint p = touchPoints[i];
        CGPoint p = [touch locationInView: self];
        
        id object = [self.myChart.scalerX toObjectByPixel: p.x];
        if(object == nil)
            return;
        
        p.x = [self.myChart.scalerX toPixelByObject: object];
        
        if(firstObject == nil)
        {
            firstObject = object;
            firstPoint  = p;
        }
            
        CGContextSetLineWidth(context, 1.0);
        CGContextSetStrokeColorWithColor(context, darkGray.CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, p.x+0.5, 0);
        CGContextAddLineToPoint(context, p.x+0.5, self.height);
        CGContextStrokePath(context);
        
        // draw the overlay
        if(count == 2)
        {
            UIColor *fillColor = [UIColor colorWithRed:0xcc/255.0 green:0xcc/255.0 blue: 0xcc/255.0 alpha: 0.6];
            CGRect rect = CGRectMake(firstPoint.x, 0, p.x - firstPoint.x, self.height);
            CGContextSetFillColorWithColor(context, fillColor.CGColor);
            CGContextFillRect(context, rect);
        }
    }
    
    firstPoint.y += y_offset;
    
    if(numberOfTouches == 1)
    {
        CGContextSetLineWidth(context, 1.0);
        CGContextSetStrokeColorWithColor(context, darkGray.CGColor);
        CGContextBeginPath(context);
        CGContextMoveToPoint(context, 0.0, floorf(firstPoint.y) + 0.5);
        CGContextAddLineToPoint(context, self.width - axis_width, floorf(firstPoint.y) + 0.5);
        CGContextStrokePath(context);
        
        CGRect dateLabelRect = CGRectMake(firstPoint.x - (50/2), self.height-21, 50, 20);
        
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect: dateLabelRect cornerRadius: 6];
        [[Theme currentPriceBackgroundColor] set];
        [path fill];
        
        [[UIColor blackColor] set];
        UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size: 12];
        NSString *indicatorName = [NSString stringWithFormat:@"%@", [firstObject stringWithFormat:@"M/dd/yy"]];
        CGRect rect = CGRectInset(dateLabelRect, 2, 2);
        [indicatorName drawInRect:rect withFont: font lineBreakMode: UILineBreakModeClip alignment: UITextAlignmentCenter];
    }
    
    if(numberOfTouches == 0)
    {
        [self.delegate crossChairStateChanged: UIGestureRecognizerStateEnded object: firstObject point: firstPoint];
    }
    else
    {
        [self.delegate crossChairStateChanged: UIGestureRecognizerStateChanged object: firstObject point: firstPoint];
    }
}

@end
