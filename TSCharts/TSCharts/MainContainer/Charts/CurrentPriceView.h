//
//  CurrentPriceView.h
//  TSCharts
//
//  Created by Quoc Le on 8/2/12.
//
//

#import <UIKit/UIKit.h>

@interface CurrentPriceView : UIView
@property (assign, nonatomic) float price;
@end
