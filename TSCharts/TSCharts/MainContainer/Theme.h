//
//  Theme.h
//  TSCharts
//
//  Created by Quoc Le on 8/2/12.
//
//

#import <UIKit/UIKit.h>

@interface Theme : NSObject
+ (UIColor*) currentPriceBackgroundColor;
+ (void) stylizeToolbar: (UIToolbar*) toolbar;
+ (void) stylizeNewsToolbar: (UIToolbar*) toolbar;
+ (void) stylizeNavbar: (UINavigationBar*) toolbar;
@end
