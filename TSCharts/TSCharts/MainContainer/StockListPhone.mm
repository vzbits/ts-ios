//
//  StockListPhone.m
//  TSCharts
//
//  Created by Quoc Le on 6/29/13.
//
//

#import "StockListPhone.h"
#import "JTTransformableTableViewCell.h"
#import "JTTableViewGestureRecognizer.h"
#import "UIColor+JTGestureBasedTableViewHelper.h"
#import "TableData+Array.h"
#import "AppDelegate.h"
#import "SymbolSearchPhone.h"
#import "ViewController.h"
#import "StockListItemCell.h"
#import "StockListEditView.h"
#import "StockListEditCell.h"

#define ADDING_CELL @{@"Symbol" : @"Continue..." }
#define DONE_CELL @{@"Symbol" : @"Done" }
#define DUMMY_CELL @{@"Symbol" : @"Dummy" }
#define ADDED_CELL @{@"Symbol" : @"Added!" }

@interface StockListPhone () <JTTableViewGestureEditingRowDelegate, JTTableViewGestureAddingRowDelegate, JTTableViewGestureMoveRowDelegate, SymbolSearchPhoneDelegate, StockListItemCellDelegate>
@property (nonatomic, strong) JTTableViewGestureRecognizer *tableViewRecognizer;
@property (nonatomic, strong) id grabbedObject;
@property (nonatomic, strong) NSIndexPath *grabbedIndex;
@property (nonatomic, strong) NSIndexPath *currentInsertIndex;
@property (nonatomic, strong) SymbolSearchPhone *symbolSearch;
@property (nonatomic, strong) StockListEditView *editView;
@property (nonatomic, strong) UIView *filterView;
@property (nonatomic, assign) BOOL grabbedSelected;
@property (nonatomic, strong) NSIndexPath *lastSelected;

- (void)moveRowToBottomForIndexPath:(NSIndexPath *)indexPath;

@end

@implementation StockListPhone

- (id)init
{
    self = [super init];
    if (self) {
		
		self.tableView = [[UITableView alloc] initWithFrame: CGRectZero style: UITableViewStylePlain];
		[self addSubview: self.tableView];
		
		self.tableView.delegate = self;
		self.tableView.dataSource = self;

		NSArray *columns = @[@"Name", @"Last", @"Change%", @"ChangePrice"];
		NSArray *row = @[@"", @"", @"", @"", @""];
		NSArray *rowList = @[row, row, row, row, row, row];
		NSArray *symbols = @[@"AAPL", @"BAC", @"YELP", @"C", @"ZNGA", @"ARCC"];
		
		TableData *tableData = [[TableData alloc] initWithColumnNames:columns rowList:rowList symbols:symbols];
		self.rows = tableData;
		
		// Setup your tableView.delegate and tableView.datasource,
		// then enable gesture recognition in one line.
		self.tableViewRecognizer = [self.tableView enableGestureTableViewWithDelegate:self];
		
		self.tableView.backgroundColor = TABLE_BGCOLOR;
		self.backgroundColor = TABLE_BGCOLOR;
		
		self.tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
		self.tableView.rowHeight       = NORMAL_CELL_FINISHING_HEIGHT;
		self.tableView.separatorColor = DIVIDER_COLOR;
		//self.tableView.panGestureRecognizer.maximumNumberOfTouches = 1;
		
		[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(alertsChanged:) name: NOTIFICATION_ALERT_CHANGED object: nil];

    }
    return self;
}

- (void) layoutSubviews
{
	[super layoutSubviews];
	self.tableView.frame = self.bounds;
}

- (void) alertsChanged: (NSNotification*) notif
{
	[self reloadWithSelection];
}

- (void) setData: (UserList*) userList
{
	self.userList = userList;
	NSMutableArray *symbols = [NSMutableArray array];
	NSMutableArray *rowList = [NSMutableArray array];
	NSArray *row = @[@"", @"", @"", @"", @""];

	[userList.listItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		[symbols addObject: [obj symbol]];
		[rowList addObject: row];
	}];
	
	self.viewType = userList.listTypeEnum;
	self.rows.symbols = symbols;
	self.rows.rowList = rowList;
	
	NSLog(@"Updated list symbols = %d", symbols.count);
	
	NSNumber *indexSelected = self.userList.indexSelected;
	if(indexSelected == nil)
	{
		NSLog(@"Automatically select first on the list");
		indexSelected = [NSNumber numberWithInt: 0];
	}
	
	[self loadData:^(BOOL finished) {
		[self setSelection: [NSIndexPath indexPathForRow: indexSelected.integerValue inSection:0]];
	}];
}

- (void) loadData: (void (^)(BOOL finished))completion
{
	NSIndexPath *selected = [self.tableView indexPathForSelectedRow];
	if(self.rows.symbols.count == 0)
	{
		[self.tableView reloadData];
		[self.tableView selectRowAtIndexPath: selected animated: NO scrollPosition: UITableViewScrollPositionNone];
		return;
	}
	
	[[ServiceManager instance] getPricesFromYahoo: self.rows.symbols withBlock:^(id data) {
		if([data isKindOfClass: [TableData class]])
		{
			self.rows = data;
			[self.delegate stockListUpdateList: self.rows];
			[self.tableView reloadData];			
			[self.tableView selectRowAtIndexPath: selected animated: NO scrollPosition: UITableViewScrollPositionNone];
		}
		if(completion)
		{
			completion(YES);
		}
	} withErrorBlock:^(NSString *error) {
		completion(NO);
	}];
}

- (NSDictionary*) dataForSymbol: (NSString*) symbol
{
	for(int i=0; i < self.rows.count; i++)
	{
		NSDictionary *dict = [self.rows objectAtIndex: i];
		if([dict[@"Symbol"] isEqualToString: symbol])
		{
			return dict;
		}
	}
	
	return nil;
}

- (void) setSelection: (NSIndexPath*) indexPath
{
	if(indexPath.row < 0 || indexPath.row >= self.rows.count)
	{
		NSLog(@"!! setSelection failed. !!");
		return;
	}
	UserListItem *data = [self.userList.listItems objectAtIndex: indexPath.row];
	[self.tableView selectRowAtIndexPath:indexPath animated: YES scrollPosition:UITableViewScrollPositionNone];
	[self.delegate stockListSymbolClicked: data.symbol]; // load the chart
}

#pragma mark Private Method

- (void)moveRowToBottomForIndexPath:(NSIndexPath *)indexPath {
    [self.tableView beginUpdates];
    
    id object = [self.rows objectAtIndex:indexPath.row];
    [self.rows removeObjectAtIndex:indexPath.row];
    [self.rows addObject:object];
	
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:[self.rows count] - 1 inSection:0];
    [self.tableView moveRowAtIndexPath:indexPath toIndexPath:lastIndexPath];
	
    [self.tableView endUpdates];
	
    [self.tableView performSelector:@selector(reloadVisibleRowsExceptIndexPath:) withObject:lastIndexPath afterDelay:JTTableViewRowAnimationDuration];
}

- (void) editForIndexPath: (NSIndexPath*) indexPath {
	UserListItem *data = [self.userList.listItems objectAtIndex: indexPath.row];
	UIViewController *viewController = [[[UIViewController alloc] init] autorelease];
	StockListEditView *editView = [[[StockListEditView alloc] init] autorelease];
	editView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	[viewController.view addSubview: editView];
	editView.frame = viewController.view.bounds;
	[editView setData: data];
	
	if(data.historyList.count == 1 && [[[data.historyList firstObject] quantity] isEqualToNumber:@0]){
		StockListEditCell *cell = [editView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
		[cell.quantityField becomeFirstResponder];
	}
	self.editView = editView;
	
	UINavigationController *navController = [[[UINavigationController alloc] initWithRootViewController: viewController] autorelease];
	AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	UIBarButtonItem *doneButton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone target: self action: @selector(editDoneClicked:)] autorelease];
	navController.navigationBar.barStyle = UIBarStyleBlack;
	navController.navigationBar.backgroundColor = [UIColor colorWithRGBHex:0x494f5d];
	navController.navigationBar.tintColor = [UIColor colorWithRGBHex: 0x087695];
	viewController.navigationItem.rightBarButtonItem = doneButton;
	viewController.title = [NSString stringWithFormat:@"Edit %@", data.symbol];
	[delegate.viewController presentModalViewController: navController animated: YES];
}

- (void) stocklistItemCellEditClicked: (UITableViewCell*) sender
{
	NSIndexPath *indexPath = [self.tableView indexPathForCell: sender];
	if(indexPath)
	{
		[self editForIndexPath: indexPath];
	}
}

- (void) editDoneClicked: (id) sender
{
	[self reloadWithSelection];
	[self.editView save];
	self.editView = nil;
	
	AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	[delegate.viewController dismissModalViewControllerAnimated: YES];
}

- (void) stocklistItemCellTrashClicked:(id)sender
{
	NSIndexPath *indexPath = [self.tableView indexPathForCell: sender];
	if(indexPath)
	{
		[self.rows removeObjectAtIndex:indexPath.row];
		UserListItem *data = [self.userList.listItems objectAtIndex: indexPath.row];
		[data delete];
		
		[[CoreDataManager instance] saveContext];

		//[self.userList.listItems]
		[self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
		
		// select next one, if no next one, select previous one.
		NSIndexPath *next = nil;
		if(indexPath.row < self.userList.listItems.count)
		{
			next = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
		}
		else if(indexPath.row-1 >= 0 && indexPath.row-1 < self.userList.listItems.count)
		{
			next = [NSIndexPath indexPathForRow:indexPath.row-1 inSection:0];
		}
		
		if(next != nil)
		{
			[self setSelection: next];
		}
	}

	[self.delegate stockListUpdateList: self.rows];
}

#pragma mark UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.rows count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    NSDictionary *object = [self.rows objectAtIndex:indexPath.row];
    UIColor *backgroundColor = CELL_COLOR;
	
	//[[UIColor colorWithRGBHex:0x4D6173] colorWithHueOffset:0.12 * indexPath.row / [self tableView:tableView numberOfRowsInSection:indexPath.section]];
	
	//    if ([object isEqual:ADDING_CELL]) {
	if([object[@"Symbol"] isEqualToString: @"Continue..."]){
        NSString *cellIdentifier = nil;
        JTTransformableTableViewCell *cell = nil;
		
        // IndexPath.row == 0 is the case we wanted to pick the pullDown style
        if (indexPath.row == 0) {
            cellIdentifier = @"PullDownTableViewCell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if (cell == nil) {
                cell = [JTTransformableTableViewCell transformableTableViewCellWithStyle:JTTransformableTableViewCellStylePullDown
																		 reuseIdentifier:cellIdentifier];
                cell.textLabel.adjustsFontSizeToFitWidth = YES;
                cell.textLabel.textColor = [UIColor whiteColor];
            }
            
            
            cell.finishedHeight = COMMITING_CREATE_CELL_HEIGHT;
            if (cell.frame.size.height > COMMITING_CREATE_CELL_HEIGHT * 2) {
                cell.imageView.image = [UIImage imageNamed:@"reload.png"];
                cell.tintColor = [UIColor blackColor];
                cell.textLabel.text = @"Return to list...";
            } else if (cell.frame.size.height > COMMITING_CREATE_CELL_HEIGHT) {
                cell.imageView.image = nil;
                // Setup tint color
                cell.tintColor = backgroundColor;
                cell.textLabel.text = @"Release to create cell...";
            } else {
                cell.imageView.image = nil;
                // Setup tint color
                cell.tintColor = backgroundColor;
                cell.textLabel.text = @"Continue Pulling...";
            }
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.textLabel.shadowOffset = CGSizeMake(0, 1);
            cell.textLabel.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
            return cell;
			
        } else
		{
            // Otherwise is the case we wanted to pick the pullDown style
            cellIdentifier = @"UnfoldingTableViewCell";
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
			
            if (cell == nil) {
                cell = [JTTransformableTableViewCell transformableTableViewCellWithStyle:JTTransformableTableViewCellStyleUnfolding
																		 reuseIdentifier:cellIdentifier];
                cell.textLabel.adjustsFontSizeToFitWidth = YES;
                cell.textLabel.textColor = [UIColor whiteColor];
            }
            
            // Setup tint color
            cell.tintColor = backgroundColor;
            
            cell.finishedHeight = COMMITING_CREATE_CELL_HEIGHT;
            if (cell.frame.size.height > COMMITING_CREATE_CELL_HEIGHT) {
                cell.textLabel.text = @"Release to create cell...";
            } else {
                cell.textLabel.text = @"Continue Pinching...";
            }
            cell.contentView.backgroundColor = [UIColor clearColor];
//            cell.textLabel.shadowOffset = CGSizeMake(0, 1);
//            cell.textLabel.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
            return cell;
        }
		
    }
	else {
		
        static NSString *cellIdentifier = @"MyCell";
        StockListItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[StockListItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		}
		
		NSArray *alerts = [[CoreDataStore instance] alertsBySymbol: object[@"Symbol"]];
		cell.bellImage.hidden =  (alerts == nil || alerts.count == 0);
		//cell.separatorTop.hidden = indexPath.row != 0;
		[cell setData:object toggleType: (ListPriceToggleType)_toggleType viewType: _viewType];
		cell.delegate = self;
		return cell;
    }
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return NORMAL_CELL_FINISHING_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"tableView:didSelectRowAtIndexPath: %@", indexPath);

	self.userList.indexSelected = [NSNumber numberWithInteger: indexPath.row];
	[[CoreDataManager instance] saveContext];

	[self loadData:^(BOOL finished) {
	}];

	NSDictionary *dict = [self.rows objectAtIndex: indexPath.row];
	NSString *symbol = dict[@"Symbol"];
	[self.delegate stockListSymbolClicked: symbol]; // load the chart

	//[self.delegate stockListSymbolClicked: dict[@"Symbol"]];
}

- (void) displaySymbolSearch
{
	if(self.symbolSearch == nil)
	{
		AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
		UIView *view = delegate.window;
		
		self.filterView = [[[UIView alloc] init] autorelease];
		self.filterView.backgroundColor = [UIColor blackColor];
		self.filterView.alpha = 0.3f;
		[view addSubview: self.filterView];
		self.filterView.frame = view.bounds;
		
		self.symbolSearch = [[[SymbolSearchPhone alloc] init] autorelease];
		self.symbolSearch.delegate = self;
		float sideMargin = 14;
		self.symbolSearch.frame = CGRectSetY(self.bounds, self.height);
//		self.symbolSearch.frame = CGRectMake(sideMargin, self.height,
//											self.width - (sideMargin*2), self.height - 64);
		[view addSubview: self.symbolSearch];
		[self.symbolSearch.inputBox becomeFirstResponder];
		
		[UIView animateWithDuration:0.25 animations:^{
			self.symbolSearch.frame = view.bounds; //CGRectSetY(self.symbolSearch.frame, 64);
			self.filterView.alpha = 0.75f;
		}
		 completion:^(BOOL finished) {
		 }];
	}
}

- (void) removeSymbolSearch
{
	self.currentInsertIndex = nil;
	AppDelegate *delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	UIView *view = delegate.viewController.view;
	[self.symbolSearch.inputBox resignFirstResponder];

	[UIView animateWithDuration:0.25 animations:^{
		self.symbolSearch.frame =  CGRectSetY(view.bounds, view.height);
		self.filterView.alpha = 0.0f;
	}
					 completion:^(BOOL finished) {
						 [self.symbolSearch removeFromSuperview];
						 self.symbolSearch = nil;
						 [self.filterView removeFromSuperview];
						 self.filterView = nil;
					 }];
}

- (void) symbolSearchCancelClicked: (id) sender
{
	if(self.currentInsertIndex)
	{
		[self.rows removeObjectAtIndex: self.currentInsertIndex.row];
		[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:self.currentInsertIndex] withRowAnimation:UITableViewRowAnimationMiddle];
	}

	[self removeSymbolSearch];
}
- (void) symbolSearchSymbolClicked: (NSString*) symbol
{
	NSLog(@"symbol selected: %@", symbol);
	NSDictionary *dict = @{@"Symbol" : symbol };
	NSIndexPath *selectIndex;
	
	if(self.currentInsertIndex)
	{
		selectIndex = self.currentInsertIndex;
		[self.rows replaceObjectAtIndex: self.currentInsertIndex.row withObject:dict];
		[self.tableView reloadRowsAtIndexPaths: @[self.currentInsertIndex] withRowAnimation:UITableViewRowAnimationMiddle];
	}
	else
	{
		//NSMutableSet *currentSet = [self.userList.listItems mutableCopy];
		UserListItem *item = [UserListItem create: @{@"symbol" : symbol }];
		NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.userList.listItems];
		[tempSet addObject:item];
		self.userList.listItems = tempSet;
		[[CoreDataManager instance] saveContext];

		[self.rows addObject: dict];
		NSIndexPath *lastRow = [NSIndexPath indexPathForRow: self.rows.count -1 inSection: 0];
		[self.tableView insertRowsAtIndexPaths:@[ lastRow] withRowAnimation:UITableViewRowAnimationFade];
		[self.tableView scrollToRowAtIndexPath:lastRow atScrollPosition: UITableViewScrollPositionBottom animated:YES];
		selectIndex = lastRow;
	}
	[self.tableView selectRowAtIndexPath:selectIndex animated: YES scrollPosition:UITableViewScrollPositionBottom];
	[self loadData:^(BOOL finished) {
		[self.delegate stockListSymbolClicked: symbol]; // load the chart
		
		if([self.userList.listType isEqualToString: @"portfolio"])
		{
			[self editForIndexPath: selectIndex];
		}
	}];

	//	[self tableView: self.tableView didSelectRowAtIndexPath: selectIndex];
	[self removeSymbolSearch];
}

- (void) reloadWithSelection
{
	NSIndexPath *selected = [self.tableView indexPathForSelectedRow];
	[self.tableView reloadData];
	[self.tableView selectRowAtIndexPath: selected animated: NO scrollPosition: UITableViewScrollPositionNone];
}
- (void) togglePriceBox
{
	//if(_viewType == ListViewType_List)
	{
		_toggleType = (_toggleType + 1) % 3;
	}
	
	[self reloadWithSelection];
}

#pragma mark -
#pragma mark JTTableViewGestureAddingRowDelegate

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsAddRowAtIndexPath:(NSIndexPath *)indexPath {
	self.currentInsertIndex = indexPath;
    [self.rows insertObject:ADDING_CELL atIndex:indexPath.row];
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsCommitRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.rows replaceObjectAtIndex:indexPath.row withObject: ADDED_CELL];
    JTTransformableTableViewCell *cell = (id)[gestureRecognizer.tableView cellForRowAtIndexPath:indexPath];
	
    BOOL isFirstCell = indexPath.section == 0 && indexPath.row == 0;
    if (isFirstCell && cell.frame.size.height > COMMITING_CREATE_CELL_HEIGHT * 2) {
        [self.rows removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        // Return to list
    }
    else {
        cell.finishedHeight = NORMAL_CELL_FINISHING_HEIGHT;
        cell.imageView.image = nil;
        cell.textLabel.text = @"   "; // placeholder
		
		[self displaySymbolSearch];
    }	
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsDiscardRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.rows removeObjectAtIndex:indexPath.row];
	self.currentInsertIndex = nil;
}

// Uncomment to following code to disable pinch in to create cell gesture
//- (NSIndexPath *)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer willCreateCellAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0 && indexPath.row == 0) {
//        return indexPath;
//    }
//    return nil;
//}

#pragma mark JTTableViewGestureEditingRowDelegate

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer didEnterEditingState:(JTTableViewCellEditingState)state forRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	
    UIColor *backgroundColor = nil;
    switch (state) {
        case JTTableViewCellEditingStateMiddle:
            backgroundColor = [[UIColor colorWithRGBHex:0x4D6173] colorWithHueOffset:0.12 * indexPath.row / [self tableView:self.tableView numberOfRowsInSection:indexPath.section]];
            break;
        case JTTableViewCellEditingStateRight:
            backgroundColor = [UIColor greenColor];
            break;
        default:
            backgroundColor = [UIColor darkGrayColor];
            break;
    }
//    cell.contentView.backgroundColor = backgroundColor;
//    if ([cell isKindOfClass:[JTTransformableTableViewCell class]]) {
//        ((JTTransformableTableViewCell *)cell).tintColor = backgroundColor;
//    }
	
}

// This is needed to be implemented to let our delegate choose whether the panning gesture should work
- (BOOL)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer commitEditingState:(JTTableViewCellEditingState)state forRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableView *tableView = gestureRecognizer.tableView;
    
    
    NSIndexPath *rowToBeMovedToBottom = nil;
	
    [tableView beginUpdates];
    if (state == JTTableViewCellEditingStateLeft) {
        // An example to discard the cell at JTTableViewCellEditingStateLeft
        [self.rows removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    } else if (state == JTTableViewCellEditingStateRight) {
        // An example to retain the cell at commiting at JTTableViewCellEditingStateRight
        [self.rows replaceObjectAtIndex:indexPath.row withObject:DONE_CELL];
        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        rowToBeMovedToBottom = indexPath;
    } else {
        // JTTableViewCellEditingStateMiddle shouldn't really happen in
        // - [JTTableViewGestureDelegate gestureRecognizer:commitEditingState:forRowAtIndexPath:]
    }
    [tableView endUpdates];
	
	
    // Row color needs update after datasource changes, reload it.
    [tableView performSelector:@selector(reloadVisibleRowsExceptIndexPath:) withObject:indexPath afterDelay:JTTableViewRowAnimationDuration];
	
    if (rowToBeMovedToBottom) {
        [self performSelector:@selector(moveRowToBottomForIndexPath:) withObject:rowToBeMovedToBottom afterDelay:JTTableViewRowAnimationDuration * 2];
    }
}

#pragma mark JTTableViewGestureMoveRowDelegate

- (BOOL)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsCreatePlaceholderForRowAtIndexPath:(NSIndexPath *)indexPath {

	NSLog(@"Placeholder at %d selected at =%d", indexPath.row, [self.tableView indexPathForSelectedRow].row);
	self.grabbedSelected = (indexPath.row == [self.tableView indexPathForSelectedRow].row);
    self.grabbedObject = [self.rows objectAtIndex:indexPath.row];
    [self.rows replaceObjectAtIndex:indexPath.row withObject:DUMMY_CELL];
	self.grabbedIndex = indexPath;
	
	NSIndexPath *selectedRow = [self.tableView indexPathForSelectedRow];
	UserListItem *item = [self.userList.listItems objectAtIndex: selectedRow.row];
	self.lastSelectionIndex = item.objectID;
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsMoveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
	
	NSLog(@"Move from %d => %d", sourceIndexPath.row, destinationIndexPath.row);
	
    id object = [self.rows objectAtIndex:sourceIndexPath.row];
    [self.rows removeObjectAtIndex:sourceIndexPath.row];
    [self.rows insertObject:object atIndex:destinationIndexPath.row];
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer needsReplacePlaceholderForRowAtIndexPath:(NSIndexPath *)indexPath {

	NSLog(@"Replace %d selected %d", indexPath.row, [self.tableView indexPathForSelectedRow].row);
	
	self.lastSelected = [self.tableView indexPathForSelectedRow];
	[self.rows replaceObjectAtIndex:indexPath.row withObject:self.grabbedObject];

	NSMutableOrderedSet *newSet = [NSMutableOrderedSet orderedSetWithOrderedSet: self.userList.listItems];
	id data = [newSet objectAtIndex: self.grabbedIndex.row];
	[newSet removeObjectAtIndex:self.grabbedIndex.row];
	[newSet insertObject:data atIndex: indexPath.row];
	self.userList.listItems = newSet;
	[[CoreDataManager instance] saveContext];
	
    self.grabbedObject = nil;
	self.grabbedIndex = nil;
}

- (void)gestureRecognizer:(JTTableViewGestureRecognizer *)gestureRecognizer updateCompleted:(NSIndexPath *)indexPath
{
	if(self.grabbedSelected) {
		[self.tableView selectRowAtIndexPath: indexPath animated: NO scrollPosition:UITableViewScrollPositionNone];
	} else {
		[self.tableView selectRowAtIndexPath: self.lastSelected animated: NO scrollPosition:UITableViewScrollPositionNone];
	}
}

@end
