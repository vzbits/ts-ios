//
//  ChartHeader.m
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChartHeader.h"
#import "UIDateExtensions.h"

@implementation ChartHeader

@synthesize symbolLabel = _symbolLabel;
@synthesize dateLabel = _dateLabel;
@synthesize openLabel = _openLabel;
@synthesize openValue = _openValue;
@synthesize highLabel = _highLabel;
@synthesize highValue = _highValue;
@synthesize lowLabel = _lowLabel;
@synthesize lowValue = _lowValue;
@synthesize closeLabel = _closeLabel;
@synthesize closeValue = _closeValue;
@synthesize volumeLabel = _volumeLabel;
@synthesize volumeValue = _volumeValue;
@synthesize changeLabel = _changeLabel;
@synthesize changeValue = _changeValue;
@synthesize backgroundView = _backgroundView;

- (id)init
{
    self = [super init];
    if (self) 
    {
        self.backgroundView = [[[UIView alloc] init] autorelease];
        self.backgroundView.backgroundColor = [Theme currentPriceBackgroundColor];
        self.backgroundView.hidden = YES;
        self.backgroundView.layer.borderColor = [UIColor grayColor].CGColor;
        self.backgroundView.layer.borderWidth = 1.0;
        [self addSubview: self.backgroundView];
        
        self.symbolLabel = [[[UILabel alloc] init] autorelease];
        self.symbolLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size: 18];
        self.symbolLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.symbolLabel];

        self.dateLabel = [[[UILabel alloc] init] autorelease];
        self.dateLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size: 12];
        self.dateLabel.text = @"";
        self.dateLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.dateLabel];

        self.openLabel = [[[UILabel alloc] init] autorelease];
        self.openLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 12];
        self.openLabel.text = @"Open";
        [self.openLabel sizeToFit];
        self.openLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.openLabel];

        self.openValue = [[[UILabel alloc] init] autorelease];
        self.openValue.font = [UIFont fontWithName:@"HelveticaNeue" size: 12];
        self.openValue.backgroundColor = [UIColor clearColor];
        [self addSubview: self.openValue];

        self.highLabel = [[[UILabel alloc] init] autorelease];
        self.highLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 12];
        self.highLabel.text = @"High";
        [self.highLabel sizeToFit];
        self.highLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.highLabel];

        self.highValue = [[[UILabel alloc] init] autorelease];
        self.highValue.font = [UIFont fontWithName:@"HelveticaNeue" size: 12];
        self.highValue.backgroundColor = [UIColor clearColor];
        [self addSubview: self.highValue];

        self.lowLabel = [[[UILabel alloc] init] autorelease];
        self.lowLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 12];
        self.lowLabel.text = @"Low";
        [self.lowLabel sizeToFit];
        self.lowLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.lowLabel];

        self.lowValue = [[[UILabel alloc] init] autorelease];
        self.lowValue.font = [UIFont fontWithName:@"HelveticaNeue" size: 12];
        self.lowValue.backgroundColor = [UIColor clearColor];
        [self addSubview: self.lowValue];

        self.closeLabel = [[[UILabel alloc] init] autorelease];
        self.closeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 12];
        self.closeLabel.text = @"Close";
        [self.closeLabel sizeToFit];
        self.closeLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.closeLabel];

        self.closeValue = [[[UILabel alloc] init] autorelease];
        self.closeValue.font = [UIFont fontWithName:@"HelveticaNeue" size: 12];
        self.closeValue.backgroundColor = [UIColor clearColor];
        [self addSubview: self.closeValue];

        self.volumeLabel = [[[UILabel alloc] init] autorelease];
        self.volumeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 12];
        self.volumeLabel.text = @"Volume";
        [self.volumeLabel sizeToFit];
        self.volumeLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.volumeLabel];

        self.volumeValue = [[[UILabel alloc] init] autorelease];
        self.volumeValue.font = [UIFont fontWithName:@"HelveticaNeue" size: 12];
        self.volumeValue.backgroundColor = [UIColor clearColor];
        [self addSubview: self.volumeValue];

        self.changeLabel = [[[UILabel alloc] init] autorelease];
        self.changeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 12];
        self.changeLabel.text = @"Chg";
        self.changeLabel.backgroundColor = [UIColor clearColor];
        [self addSubview: self.changeLabel];
        
        self.changeValue = [[[UILabel alloc] init] autorelease];
        self.changeValue.font = [UIFont fontWithName:@"HelveticaNeue" size: 12];
        self.changeValue.backgroundColor = [UIColor clearColor];
        [self addSubview: self.changeValue];
        
        
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    int y_pos = 27;
    self.backgroundView.frame = CGRectMake(-1,25,self.width, self.height - 25);
    
    self.symbolLabel.frame = CGRectMake(10,5,500, 21);
    self.dateLabel.frame = CGRectMake(10,30,100,13);
    
    self.openLabel.frame = CGRectMake(240,y_pos, self.openLabel.width,self.openLabel.height);
    self.openValue.frame = CGRectMake(self.openLabel.right+5,y_pos, 57,self.openLabel.height);

    self.highLabel.frame = CGRectMake(self.openValue.right+5,y_pos, self.highLabel.width,self.highLabel.height);
    self.highValue.frame = CGRectMake(self.highLabel.right+5,y_pos, 57,self.openLabel.height);

    self.lowLabel.frame = CGRectMake(self.highValue.right+5,y_pos, self.lowLabel.width,self.lowLabel.height);
    self.lowValue.frame = CGRectMake(self.lowLabel.right+5,y_pos, 57,self.openLabel.height);

    self.closeLabel.frame = CGRectMake(self.lowValue.right+5,y_pos, self.closeLabel.width,self.closeLabel.height);
    self.closeValue.frame = CGRectMake(self.closeLabel.right+5,y_pos, 57,self.openLabel.height);

    self.volumeLabel.frame = CGRectMake(self.closeValue.right+5,y_pos, self.volumeLabel.width,self.volumeLabel.height);
    self.volumeValue.frame = CGRectMake(self.volumeLabel.right+5,y_pos, 57,self.openLabel.height);

    self.changeLabel.frame = CGRectMake(self.volumeValue.right+5,y_pos, self.closeLabel.width,self.closeLabel.height);
    self.changeValue.frame = CGRectMake(self.changeLabel.right+5,y_pos, 100,self.openLabel.height);
}

+ (NSString*) formatNumber: (float) n
{
    return [NSString stringWithFormat:@"%.2f", n];
}
- (void) setPriceData: (PriceData*) p
{
	if(p== nil)
	{
		self.dateLabel.text = @"";
		self.openValue.text = @"n/a";
		self.closeValue.text = @"n/a";
		self.highValue.text = @"n/a";
		self.lowValue.text = @"n/a";
		self.volumeValue.text = @"n/a";
		self.changeValue.text = @"n/a";		
	}
	else
	{
		NSDate *myDate = [NSDate dateFromPB: p.date];
		self.dateLabel.text = [myDate stringWithFormat:@"dd-MMM-yyyy"];
		self.openValue.text = [ChartHeader formatNumber: p.open];
		self.highValue.text = [ChartHeader formatNumber: p.high];
		self.lowValue.text = [ChartHeader formatNumber: p.low];
		self.closeValue.text = [ChartHeader formatNumber: p.close];
		self.volumeValue.text = [ChartUtils formatYAxis: p.volume twoDecimal: NO];
		
		float dollarChange = p.close - p.open;
		float percentChange = dollarChange/p.open;
		
		NSString *change = [NSString stringWithFormat:@"%+.2f (%+1.2f%%)",
							dollarChange,
							percentChange];
		self.changeValue.text = change;
	}
}

- (void) setData: (StockPriceData*) sp
{
    self.backgroundView.hidden = YES;
    self.symbolLabel.text = [NSString stringWithFormat:@"%@ (%@)", sp.name, sp.symbol];
	if(sp.prices.count > 0)
	{
		PriceData *p = [sp.prices objectAtIndex:sp.prices.count-1];
		[self setPriceData: p];
	}
	else
	{
		[self setPriceData: nil];
	}
}


@end
