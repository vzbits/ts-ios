//
//  AnnotationGraphicsView.m
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import "AnnotationGraphicsView.h"
#import "NSTimer+Blocks.h"
#import "AppDelegate.h"

#define DOUBLETAP_TIME 0.200f  // 100 ms ?


@implementation AnnotationGraphicsView

- (id)init
{
    self = [super init];
    if (self)
    {
        self.graphics = [NSMutableArray array];
        self.selectionIndexes = [NSMutableIndexSet indexSet];
        
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        self.multipleTouchEnabled = YES;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    //CGContextRef context = UIGraphicsGetCurrentContext();
    
    for(NSInteger index = 0; index < self.graphics.count; index++)
    {
        AnnotationGraphics *g = [self.graphics objectAtIndex: index];
        BOOL drawSelectionHandles = [self.selectionIndexes containsIndex: index];

        // draw
        [g drawContentsInView: self isBeingCreateOrEdited: NO];
        if(drawSelectionHandles)
        {
            [g drawHandlesInView: self];
        }
    }
}

- (void)createGraphicOfClass:(Class)graphicClass withEvent:(UIEvent *)event graphicType: (AnnotationGraphicType) type
{
    self.creatingGraphics = [[[graphicClass alloc] init] autorelease];
    [self.graphics addObject: self.creatingGraphics];
    self.creatingGraphics.bounds = CGRectMake(self.width/2, self.height/2, 100, 100);
    [self setNeedsDisplay];
    
    if(type == ANNOTATION_TEXT)
    {
        self.creatingGraphics.isDrawingStroke = NO;
    }
    if(type == ANNOTATION_TEXT_BORDERED)
    {
        self.creatingGraphics.isDrawingFill = YES;
        self.creatingGraphics.isDrawingStroke = YES;
        self.creatingGraphics.bounds = CGRectMake(self.width/2, self.height/2, 200, 50);
    }
    if(type == ANNOTATION_TEXT_CORNERED)
    {
        self.creatingGraphics.isDrawingFill = YES;
        self.creatingGraphics.isDrawingStroke = YES;
        self.creatingGraphics.isDrawingRoundCorner = YES;
        self.creatingGraphics.bounds = CGRectMake(self.width/2, self.height/2, 200, 50);
    }
    if(type == ANNOTATION_RECT_ROUNDED)
    {
        self.creatingGraphics.isDrawingRoundCorner = YES;
    }
    if(type == ANNOTATION_ARROW_UP || type == ANNOTATION_ARROW_DOWN)
    {
        AnnotationArrow *arrow = (AnnotationArrow*)self.creatingGraphics;
        arrow.direction = type;
        arrow.bounds = CGRectMake(self.width/2, self.height/2, arrow.preferredSize.width, arrow.preferredSize.height);
    }
    
    
    [self.selectionIndexes removeAllIndexes];
    [self.selectionIndexes addIndex: self.graphics.count -1];
    
    [self.delegate annotationGraphicViewHandleSelected: self.creatingGraphics];
}

- (void) addGraphicType: (AnnotationGraphicType) type
{
    Class graphicClass = nil;
    switch (type)
    {
        case ANNOTATION_RECT:
        case ANNOTATION_RECT_ROUNDED:
            graphicClass = [AnnotationRect class];
            break;
        case ANNOTATION_LINE:
            graphicClass = [AnnotationLine class];
            break;
        case ANNOTATION_CIRCLE:
            graphicClass = [AnnotationCircle class];
            break;
        case ANNOTATION_TEXT_CORNERED:
        case ANNOTATION_TEXT:
            graphicClass = [AnnotationText class];
            break;
        case ANNOTATION_TEXT_BORDERED:
            graphicClass = [AnnotationText class];
        case ANNOTATION_ARROW_UP:
        case ANNOTATION_ARROW_DOWN:
            graphicClass = [AnnotationArrow class];
        default:
            break;
    };
    
    if (graphicClass) {
        [self createGraphicOfClass:graphicClass withEvent:nil graphicType: type];
    }

}

- (void) setStrokeColorForHandle: (NSInteger) handle color: (UIColor*) color
{
    AnnotationGraphics  *graphic = [self.graphics objectAtIndex: handle];
    graphic.strokeColor = color;
    [self setNeedsDisplay];
}

#pragma mark - UI Events

- (AnnotationGraphics *)graphicUnderPoint:(CGPoint)point index:(NSUInteger *)outIndex isSelected:(BOOL *)outIsSelected handle:(NSInteger *)outHandle {
    
    // We don't touch *outIndex, *outIsSelected, or *outHandle if we return nil. Those values are undefined if we don't return a match.
    
    // Search through all of the graphics, front to back, looking for one that claims that the point is on a selection handle (if it's selected) or in the contents of the graphic itself.
    AnnotationGraphics *graphicToReturn = nil;
    NSArray *graphics = [self graphics];
    NSIndexSet *selectionIndexes = [self selectionIndexes];
    NSUInteger graphicCount = [graphics count];
    for (NSUInteger index = 0; index<graphicCount; index++) {
        AnnotationGraphics *graphic = [graphics objectAtIndex:index];
        
        // Do a quick check to weed out graphics that aren't even in the neighborhood.
        if (CGRectContainsPoint([graphic drawingBounds], point)) {
            
            // Check the graphic's selection handles first, because they take precedence when they overlap the graphic's contents.
            BOOL graphicIsSelected = [selectionIndexes containsIndex:index];
            if (graphicIsSelected) {
                NSInteger handle = [graphic handleUnderPoint:point];
                if (handle!=SKTGraphicNoHandle) {
                    
                    // The user clicked on a handle of a selected graphic.
                    graphicToReturn = graphic;
                    if (outHandle) {
                        *outHandle = handle;
                    }
                    
                }
            }
            if (!graphicToReturn) {
                BOOL clickedOnGraphicContents = [graphic isContentsUnderPoint:point];
                if (clickedOnGraphicContents) {
                    
                    // The user clicked on the contents of a graphic.
                    graphicToReturn = graphic;
                    if (outHandle) {
                        *outHandle = SKTGraphicNoHandle;
                    }
                    
                }
            }
            if (graphicToReturn) {
                
                // Return values and stop looking.
                if (outIndex) {
                    *outIndex = index;
                }
                if (outIsSelected) {
                    *outIsSelected = graphicIsSelected;
                }
                break;
                
            }
            
        }
        
    }
    return graphicToReturn;
    
}

- (NSArray *)selectedGraphics
{
    return [[self graphics] objectsAtIndexes:[self selectionIndexes]];
}


- (void)moveSelectedGraphicsWithEvent:(UIEvent *)event  touches: (NSSet *)touches
{
    //UITouch
    CGPoint currentPos = [[touches anyObject] locationInView: self];
    CGPoint lastPos = [[touches anyObject] previousLocationInView: self];
    CGPoint diffPos = CGPointMake(currentPos.x - lastPos.x, currentPos.y - lastPos.y);
    
    //NSRect selBounds = [[SKTGraphic self] boundsOfGraphics:selGraphics];
    for(AnnotationGraphics *g in [self selectedGraphics])
    {
        //g.bounds = CGRectSetPosition(g.bounds,  mouseLocation);
        g.bounds = CGRectOffset(g.bounds, diffPos.x, diffPos.y);
    }
}


- (void)resizeGraphic:(AnnotationGraphics *)graphic usingHandle:(NSInteger)handle withEvent:(UIEvent *)event  touches: (NSSet *)touches{
    
    CGPoint handleLocation = [[touches anyObject] locationInView: self];
    handle = [graphic resizeByMovingHandle:handle toPoint:handleLocation];
    
    //[[self undoManager] setActionName:NSLocalizedStringFromTable(@"Resize", @"UndoStrings", @"Action name for resizes.")];
    
}

- (void) displayTextBox: (AnnotationText*) graphic handle: (NSInteger) handle
{
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if ([menuController isMenuVisible]) {
        [menuController setMenuVisible:NO animated: NO];
    }
    
    UIViewController *controller = ROOT_VIEWCONTROLLER();

    self.popupTextView = [[YIPopupTextView alloc] initWithPlaceHolder:@"text" maxCount:300];
    self.popupTextView.delegate = self;
    self.popupTextView.tag = handle;
    self.popupTextView.showCloseButton = YES;
    self.popupTextView.caretShiftGestureEnabled = YES;   // default = NO
    self.popupTextView.text = graphic.text;
    [self.popupTextView showInView:controller.view];
}
- (BOOL)canBecomeFirstResponder {
	return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    BOOL retValue = NO;
	//ColorTile *theTile = [self colorTileForOrigin:currentSelection];
	
	if (action == @selector(delete:)) {
        retValue = YES;
    }
    return retValue;
}

- (void)delete:(id)sender
{
    [self.graphics removeObject: self.clickedGraphic];
    [self.selectionIndexes removeAllIndexes];
    self.lastTap = nil;
    
    [self setNeedsDisplay];
    
    [self.delegate annotationGraphicViewHandleSelected: nil];

}

- (BOOL)selectAndTrackMouseWithEvent:(UIEvent *)event touches: (NSSet *)touches touchType: (UIGestureRecognizerState) state
{
    BOOL modifyingExistingSelection = NO;
    // Has the user clicked on a graphic?
    CGPoint mouseLocation = [[touches anyObject] locationInView: self];
    
    static NSUInteger clickedGraphicIndex;
    static BOOL clickedGraphicIsSelected;
    static NSInteger clickedGraphicHandle;
    
    if(state == UIGestureRecognizerStateBegan)
    {
        self.clickedGraphic = [self graphicUnderPoint:mouseLocation index:&clickedGraphicIndex isSelected:&clickedGraphicIsSelected handle:&clickedGraphicHandle];
        [self.delegate annotationGraphicViewHandleSelected: self.clickedGraphic];
        
        if(self.lastTap != nil)
        {
            //if( fabs([self.lastTap timeIntervalSinceNow]) <= DOUBLETAP_TIME)
            if(self.lastTap && [[touches anyObject] tapCount] == 2)
            {
                NSLog(@"time = %f", [self.lastTap timeIntervalSinceNow]);
                
                if([self.clickedGraphic isKindOfClass: [AnnotationText class]])
                {
                    [self displayTextBox: (AnnotationText*)self.clickedGraphic handle: clickedGraphicIndex];
                }
                
                // double tap happenned
                NSLog(@"double tap");
            }
        }
        else // single tap
        {

        }
        
        self.lastTap = [NSDate date];

    }
    else if(state == UIGestureRecognizerStateChanged)
    {

    }
    else if(state == UIGestureRecognizerStateEnded)
    {
        if([[touches anyObject] tapCount] == 1)
        {
            if(self.clickedGraphic && [self becomeFirstResponder])
            {
                UIMenuController *theMenu = [UIMenuController sharedMenuController];
                BOOL visible = ![theMenu isMenuVisible];
                
                [theMenu setTargetRect: [self.clickedGraphic bounds] inView:self];
                [theMenu setMenuVisible:visible animated:YES];
            }
        }
    }
    
    if (self.clickedGraphic) {
        // Clicking on a graphic knob takes precedence.
        if (clickedGraphicHandle!=SKTGraphicNoHandle) {
            
            // The user clicked on a graphic's handle. Let the user drag it around.
            [self resizeGraphic:self.clickedGraphic usingHandle:clickedGraphicHandle withEvent:event touches: touches];
            
            UIMenuController *menuController = [UIMenuController sharedMenuController];
            
            if ([menuController isMenuVisible]) {
                [menuController setMenuVisible:NO animated:YES];
            }
            
        } else {
            
            // The user clicked on a graphic's contents. Update the selection.
            if (modifyingExistingSelection) {
                if (clickedGraphicIsSelected) {
                    
                    // Remove the graphic from the selection.
                    NSMutableIndexSet *newSelectionIndexes = [[self selectionIndexes] mutableCopy];
                    [newSelectionIndexes removeIndex:clickedGraphicIndex];
                    self.selectionIndexes = newSelectionIndexes;
                    [newSelectionIndexes release];
                    clickedGraphicIsSelected = NO;
                    
                } else {
                    // Add the graphic to the selection.
                    NSMutableIndexSet *newSelectionIndexes = [[self selectionIndexes] mutableCopy];
                    [newSelectionIndexes addIndex:clickedGraphicIndex];
                    self.selectionIndexes = newSelectionIndexes;
                    [newSelectionIndexes release];
                    clickedGraphicIsSelected = YES;
                    
                }
            } else {
                
                // If the graphic wasn't selected before then it is now, and none of the rest are.
                if (!clickedGraphicIsSelected) {
                    self.selectionIndexes = [NSMutableIndexSet indexSetWithIndex:clickedGraphicIndex];
                    clickedGraphicIsSelected = YES;
                }
                
            }
            
            // Is the graphic that the user has clicked on now selected?
            if (clickedGraphicIsSelected) {
                
                // Yes. Let the user move all of the selected objects.
                [self moveSelectedGraphicsWithEvent:event touches: touches];
                
                if(state == UIGestureRecognizerStateChanged)
                {
                    UIMenuController *menuController = [UIMenuController sharedMenuController];
                    
                    if ([menuController isMenuVisible]) {
                        [menuController setMenuVisible:NO animated: NO];
                    }
                }
                
            } else {
                
//                // No. Just swallow mouse events until the user lets go of the mouse button. We don't even bother autoscrolling here.
//                while ([event type]!=NSLeftMouseUp) {
//                    event = [[self window] nextEventMatchingMask:(NSLeftMouseDraggedMask | NSLeftMouseUpMask)];
//                }
                
            }
            
        }
        
        [self setNeedsDisplay];
        
    } else {
	    
        // The user clicked somewhere other than on a graphic. Clear the selection, unless the user is holding down the shift key.
        if (!modifyingExistingSelection) {
            [self deselect];
        }
        
        // The user clicked on a point where there is no graphic. Select and deselect graphics until the user lets go of the mouse button.
        //[self marqueeSelectWithEvent:event];
        
    }
    
    return NO;
}

- (void) deselect
{
    [self.selectionIndexes removeAllIndexes];
    //NSLog(@"Deselect");
    self.lastTap = nil;
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if ([menuController isMenuVisible]) {
            [menuController setMenuVisible:NO animated: NO];
    }
    [self setNeedsDisplay];
}



- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"annotation touch began");
    [self selectAndTrackMouseWithEvent: event touches: touches touchType: UIGestureRecognizerStateBegan];
    
//    [NSTimer scheduledTimerWithTimeInterval: 0.20 block:^
//    {
        if(self.clickedGraphic == nil)
            [self.superview touchesBegan: touches withEvent: event];
        // UITouch
//    } repeats:NO];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //NSLog(@"annotation touch moved");
    [self selectAndTrackMouseWithEvent: event touches: touches touchType: UIGestureRecognizerStateChanged];

//    int tapCount = [[touches anyObject] tapCount];
//    NSLog(@"Tap0 count = %d", tapCount);
    
    if(self.clickedGraphic == nil)
        [self.superview touchesMoved: touches withEvent: event];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.superview touchesCancelled: touches withEvent: event];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self selectAndTrackMouseWithEvent: event touches: touches touchType: UIGestureRecognizerStateEnded];
    [self.superview touchesEnded: touches withEvent: event];
    
//    int tapCount = [[touches anyObject] tapCount];
//    NSLog(@"Tap count = %d", tapCount);
}

//-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
//    return YES;
//}

#pragma mark YIPopupTextViewDelegate

- (void)popupTextView:(YIPopupTextView *)textView willDismissWithText:(NSString *)text
{
    AnnotationText *graphic = [self.graphics objectAtIndex: textView.tag];
    graphic.text = text;
    [self setNeedsDisplay];
    NSLog(@"will dismiss");
}

- (void)popupTextView:(YIPopupTextView *)textView didDismissWithText:(NSString *)text
{
    NSLog(@"did dismiss");
}
@end
