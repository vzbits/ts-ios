//
//  AnnotationArrow.m
//  TSCharts
//
//  Created by Quoc Le on 8/11/12.
//
//

#import "AnnotationArrow.h"

@implementation AnnotationArrow
- (id) init
{
    if(self = [super init])
    {
        self.graphicType = ANNOTATION_ARROW;
        self.isDrawingFill = YES;
        self.strokeWidth = 1.0;
        self.fillColor = [UIColor greenColor];
        self.handleColor = [UIColor lightGrayColor];
    }
    return self;
}
- (UIBezierPath *)bezierPathForDrawing
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    float minX = CGRectGetMinX(self.bounds);
    float minY = CGRectGetMinY(self.bounds);
    float midX = CGRectGetMidX(self.bounds);
    float midY = CGRectGetMidY(self.bounds);
    float thirdX = self.bounds.size.width * 0.33f;
    float maxY = CGRectGetMaxY(self.bounds);
    float maxX = CGRectGetMaxX(self.bounds);

    [path moveToPoint: CGPointMake(minX+thirdX, maxY)];
    [path addLineToPoint: CGPointMake(minX+thirdX, midY)];
    [path addLineToPoint: CGPointMake(minX, midY)];
    [path addLineToPoint: CGPointMake(midX, minY)];
    [path addLineToPoint: CGPointMake(maxX, midY)];
    [path addLineToPoint: CGPointMake(minX+thirdX*2, midY)];
    [path addLineToPoint: CGPointMake(minX+thirdX*2, maxY)];
    [path addLineToPoint: CGPointMake(minX+thirdX, maxY)];
    [path setLineWidth:[self strokeWidth]];
    
    if(self.direction == ANNOTATION_ARROW_DOWN)
    {
        CGAffineTransform t = CGAffineTransformMakeRotationAt(M_PI, CGPointMake(midX, midY));
        [path applyTransform: t];
    }
    
    return path;
}


- (void)drawHandlesInView:(UIView *)view
{
    // Draw handles at the corners and on the sides.
    CGRect bounds = CGRectInset([self bounds], -10, -10);
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMidX(bounds), CGRectGetMinY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMinY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMidY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMidY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMaxY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMidX(bounds), CGRectGetMaxY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds))];
    
}


- (BOOL)isContentsUnderPoint:(CGPoint)point {
    
    CGRect bounds = CGRectInset([self bounds], -10, -10);
    // Just check against the graphic's bounds.
    return CGRectContainsPoint(bounds,point);
    
}

- (NSInteger)handleUnderPoint:(CGPoint)point {
    
    // Check handles at the corners and on the sides.
    NSInteger handle = SKTGraphicNoHandle;
    return handle;
}
- (CGSize) preferredSize
{
    return CGSizeMake(20,20);
}
@end
