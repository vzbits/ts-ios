//
//  AnnotationLine.m
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import "AnnotationLine.h"

@implementation AnnotationLine

- (id) init
{
    if(self = [super init])
    {
        _pointsRight = YES;
        self.graphicType = ANNOTATION_LINE;
    }
    return self;
}
- (CGPoint)beginPoint {
    
    // Convert from our odd storage format to something natural.
    CGPoint beginPoint;
    CGRect bounds = [self bounds];
    beginPoint.x = _pointsRight ? CGRectGetMinX(bounds) : CGRectGetMaxX(bounds);
    beginPoint.y = _pointsDown ? CGRectGetMinY(bounds) : CGRectGetMaxY(bounds);
    return beginPoint;
    
}

- (CGPoint)endPoint {
    
    // Convert from our odd storage format to something natural.
    CGPoint endPoint;
    CGRect bounds = [self bounds];
    endPoint.x = _pointsRight ? CGRectGetMaxX(bounds) : CGRectGetMinX(bounds);
    endPoint.y = _pointsDown ? CGRectGetMaxY(bounds) : CGRectGetMinY(bounds);
    return endPoint;
    
}


+ (CGRect)boundsWithBeginPoint:(CGPoint)beginPoint endPoint:(CGPoint)endPoint pointsRight:(BOOL *)outPointsRight down:(BOOL *)outPointsDown {
    
    // Convert the begin and end points of the line to its bounds and flags specifying the direction in which it points.
    BOOL pointsRight = beginPoint.x<endPoint.x;
    BOOL pointsDown = beginPoint.y<endPoint.y;
    CGFloat xPosition = pointsRight ? beginPoint.x : endPoint.x;
    CGFloat yPosition = pointsDown ? beginPoint.y : endPoint.y;
    CGFloat width = fabs(endPoint.x - beginPoint.x);
    CGFloat height = fabs(endPoint.y - beginPoint.y);
    if (outPointsRight) {
        *outPointsRight = pointsRight;
    }
    if (outPointsDown) {
        *outPointsDown = pointsDown;
    }
    return CGRectMake(xPosition, yPosition, width, height);
    
}

- (void)setBeginPoint:(CGPoint)beginPoint {
    // It's easiest to compute the results of setting these points together.
    [self setBounds:[[self class] boundsWithBeginPoint:beginPoint endPoint:[self endPoint] pointsRight:&_pointsRight down:&_pointsDown]];
    
}


- (void)setEndPoint:(CGPoint)endPoint {
    // It's easiest to compute the results of setting these points together.
    [self setBounds:[[self class] boundsWithBeginPoint:[self beginPoint] endPoint:endPoint pointsRight:&_pointsRight down:&_pointsDown]];
	
}

- (NSInteger)resizeByMovingHandle:(NSInteger)handle toPoint:(CGPoint)point {
    
    // A line just has handles at its ends.
    if (handle==SKTLineBeginHandle) {
        [self setBeginPoint:point];
    } else if (handle==SKTLineEndHandle) {
        [self setEndPoint:point];
    } // else a cataclysm occurred.
    
    // We don't have to do the kind of handle flipping that SKTGraphic does.
    return handle;
    
}
- (BOOL)isDrawingFill {
    
    // You can't fill a line.
    return NO;
    
}

- (BOOL)canSetDrawingFill
{
    return NO;
}


- (BOOL)isDrawingStroke {
    
    // You can't not stroke a line.
    return YES;
    
}

- (UIBezierPath *)bezierPathForDrawing {
    
    // Simple.
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:[self beginPoint]];
    [path addLineToPoint:[self endPoint]];
    [path setLineWidth:[self strokeWidth]];
    return path;
    
}


- (void)drawHandlesInView:(UIView *)view {
    
    // A line only has two handles.
    [self drawHandleInView:view atPoint:[self beginPoint]];
    [self drawHandleInView:view atPoint:[self endPoint]];
    
}



- (BOOL)isContentsUnderPoint:(CGPoint)point {
    
    // Do a gross check against the bounds.
    BOOL isContentsUnderPoint = NO;
    if (CGRectContainsPoint([self bounds],point)) {
        
        // Let the user click within the stroke width plus some slop.
        CGFloat acceptableDistance = ([self strokeWidth] / 2.0f) + 7.0f;
        
        // Before doing anything avoid a divide by zero error.
        CGPoint beginPoint = [self beginPoint];
        CGPoint endPoint = [self endPoint];
        CGFloat xDelta = endPoint.x - beginPoint.x;
        if (xDelta==0.0f && fabs(point.x - beginPoint.x)<=acceptableDistance) {
            isContentsUnderPoint = YES;
        } else {
            
            // Do a weak approximation of distance to the line segment.
            CGFloat slope = (endPoint.y - beginPoint.y) / xDelta;
            if (fabs(((point.x - beginPoint.x) * slope) - (point.y - beginPoint.y))<=acceptableDistance) {
                isContentsUnderPoint = YES;
            }
            
        }
        
    }
    return isContentsUnderPoint;
    
}


- (NSInteger)handleUnderPoint:(CGPoint)point {
    
    // A line just has handles at its ends.
    NSInteger handle = SKTGraphicNoHandle;
    if ([self isHandleAtPoint:[self beginPoint] underPoint:point]) {
        handle = SKTLineBeginHandle;
    } else if ([self isHandleAtPoint:[self endPoint] underPoint:point]) {
        handle = SKTLineEndHandle;
    }
    return handle;
    
}


@end
