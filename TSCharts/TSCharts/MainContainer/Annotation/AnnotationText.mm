//
//  AnnotationText.m
//  TSCharts
//
//  Created by Quoc Le on 8/6/12.
//
//

#import "AnnotationText.h"

@implementation AnnotationText

- (id) init
{
    if(self = [super init])
    {
        self.graphicType = ANNOTATION_TEXT;
        self.font = [UIFont fontWithName:@"Helvetica" size:14.0];
        self.lineBreak = UILineBreakModeTailTruncation;
        self.textAlignment = UITextAlignmentCenter;
        self.fillColor = [UIColor whiteColor];
        self.text = @"Text";
        self.strokeWidth = 3.0;
        self.textColor = [UIColor blackColor];
    }
    return self;
}

- (BOOL)canSetDrawingRoundCorner
{
    return YES;
}

- (UIBezierPath *)bezierPathForDrawing {
    
    //NSLog(@"bounds %f %f %f %f", self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height);
    // Simple.
    if([self isDrawingRoundCorner])
    {
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:[self bounds] cornerRadius: 6.0];
        [path setLineWidth:[self strokeWidth]];
        return path;
    }
    else
    {
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:[self bounds]];
        [path setLineWidth:[self strokeWidth]];
        return path;
    }
    
}

- (void)drawContentsInView:(UIView *)view isBeingCreateOrEdited:(BOOL)isBeingCreatedOrEditing
{    
    UIBezierPath *path = [self bezierPathForDrawing];
    if(path)
    {
        if ([self isDrawingStroke]) {
            
            if(self.strokeStyle == Stroke_Style_Dots)
            {
                const float p[2] = {2, 2};
                [path setLineDash:p count:2 phase:0];
            }
            if(self.strokeStyle == Stroke_Style_SmallDash)
            {
                const float p[2] = {4, 4};
                [path setLineDash:p count:2 phase:0.3];
            }
            if(self.strokeStyle == Stroke_Style_LongDash)
            {
                const float p[2] = {10, 10};
                [path setLineDash:p count:2 phase:0.3];
            }
            
            [[self strokeColor] set];
            path.lineWidth = self.strokeWidth;
            [path stroke];
        }
        if ([self isDrawingFill]) {
            [[self fillColor] set];
            [path fillWithBlendMode: kCGBlendModeNormal alpha: self.fillOpacity];
        }
        
        [self.textColor set];
        CGRect constraintRect = CGRectInset([self bounds],5,5) ;
        CGSize newSize = [self.text sizeWithFont:self.font
                                     constrainedToSize:CGSizeMake(constraintRect.size.width, constraintRect.size.height)
                                         lineBreakMode: UILineBreakModeWordWrap];

        [self.text drawInRect: CGRectMake(constraintRect.origin.x,
                                          constraintRect.origin.y + ((constraintRect.size.height - newSize.height)/2),
                                          constraintRect.size.width,
                                          newSize.height)
                     withFont: self.font
                lineBreakMode: self.lineBreak
                    alignment: self.textAlignment];

    }
}

@end
