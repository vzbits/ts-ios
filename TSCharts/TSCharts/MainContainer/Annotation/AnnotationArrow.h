//
//  AnnotationArrow.h
//  TSCharts
//
//  Created by Quoc Le on 8/11/12.
//
//

#import "AnnotationGraphics.h"

@interface AnnotationArrow : AnnotationGraphics
@property(assign) AnnotationGraphicType direction;

- (CGSize) preferredSize;
@end
