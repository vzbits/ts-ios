//
//  AnnotationLine.h
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import <Foundation/Foundation.h>
#import "AnnotationGraphics.h"

enum {
    SKTLineBeginHandle = 1,
    SKTLineEndHandle = 2
};

@interface AnnotationLine : AnnotationGraphics
{
    BOOL _pointsRight;
    BOOL _pointsDown;
}
@end
