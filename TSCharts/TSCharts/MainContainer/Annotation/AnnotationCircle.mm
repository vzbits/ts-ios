//
//  AnnotationCircle.m
//  TSCharts
//
//  Created by Quoc Le on 8/6/12.
//
//

#import "AnnotationCircle.h"

@implementation AnnotationCircle

- (id) init
{
    if(self = [super init])
    {
        self.graphicType = ANNOTATION_CIRCLE;
    }
    return self;
}

- (UIBezierPath *)bezierPathForDrawing {
    
    // Simple.
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:[self bounds]];
    [path setLineWidth:[self strokeWidth]];
    return path;
    
}


- (BOOL)isContentsUnderPoint:(CGPoint)point {
    
    // Just check to see if the point is in the path.
    return [[self bezierPathForDrawing] containsPoint:point];
    
}

@end
