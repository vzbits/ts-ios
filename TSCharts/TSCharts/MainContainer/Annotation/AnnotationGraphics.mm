//
//  AnnotationGraphics.m
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import "AnnotationGraphics.h"

CGFloat SKTGraphicHandleWidth = 10.0f;
CGFloat SKTGraphicHandleHalfWidth = 10.0f / 2.0f;

@implementation AnnotationGraphics

- (id) init
{
    if(self = [super init])
    {
        self.isDrawingFill = NO;
        self.isDrawingStroke = YES;
        self.strokeColor = [UIColor blackColor];
        self.strokeWidth = 2.0;
        self.fillOpacity = 1.0;
        self.fillColor = [UIColor blueColor];
        self.handleColor = [UIColor blueColor];
    }
    return self;
}
+ (void)translateGraphics:(NSArray *)graphics byX:(CGFloat)deltaX y:(CGFloat)deltaY
{
}
+ (CGRect)boundsOfGraphics:(NSArray *)graphics
{
    return CGRectZero;
}
+ (CGRect)drawingBoundsOfGraphics:(NSArray *)graphics
{
    return CGRectZero;
}

- (CGRect)drawingBounds {
    
    // Assume that -[SKTGraphic drawContentsInView:] and -[SKTGraphic drawHandlesInView:] will be doing the drawing. Start with the plain bounds of the graphic, then take drawing of handles at the corners of the bounds into account, then optional stroke drawing.
    CGFloat outset = SKTGraphicHandleHalfWidth;
    if ([self isDrawingStroke]) {
        CGFloat strokeOutset = [self strokeWidth] / 2.0f;
        if (strokeOutset>outset) {
            outset = strokeOutset;
        }
    }
    CGFloat inset = 0.0f - outset;
    CGRect drawingBounds = CGRectInset([self bounds], inset, inset);
    
    // -drawHandleInView:atPoint: draws a one-unit drop shadow too.
    drawingBounds.size.width += 1.0f;
    drawingBounds.size.height += 1.0f;
    return drawingBounds;
    
}




- (UIBezierPath *)bezierPathForDrawing
{
    return nil;
}

- (void)drawContentsInView:(UIView *)view isBeingCreateOrEdited:(BOOL)isBeingCreatedOrEditing
{
    UIBezierPath *path = [self bezierPathForDrawing];
    if (path) {
        if ([self isDrawingFill]) {
            [[self fillColor] set];
            [path fillWithBlendMode: kCGBlendModeNormal alpha: self.fillOpacity];
        }
        if ([self isDrawingStroke]) {
            
            if(self.strokeStyle == Stroke_Style_Dots)
            {
                const float p[2] = {2, 2};
                [path setLineDash:p count:2 phase:0];
            }
            if(self.strokeStyle == Stroke_Style_SmallDash)
            {
                const float p[2] = {4, 4};
                [path setLineDash:p count:2 phase:0.3];
            }
            if(self.strokeStyle == Stroke_Style_LongDash)
            {
                const float p[2] = {10, 10};
                [path setLineDash:p count:2 phase:0.3];
            }
            
            [[self strokeColor] set];
            path.lineWidth = self.strokeWidth;
            [path stroke];
        }
    }
}


- (void)drawHandleInView:(UIView *)view atPoint:(CGPoint)point {
    
    // Figure out a rectangle that's centered on the point but lined up with device pixels.
    CGRect handleBounds;
    handleBounds.origin.x = point.x - SKTGraphicHandleHalfWidth;
    handleBounds.origin.y = point.y - SKTGraphicHandleHalfWidth;
    handleBounds.size.width = SKTGraphicHandleWidth;
    handleBounds.size.height = SKTGraphicHandleWidth;
    //handleBounds = [view centerScanRect:handleBounds];
    
    // Draw the shadow of the handle.
    CGRect handleShadowBounds = CGRectOffset(handleBounds, 1.0f, 1.0f);
    [[UIColor darkGrayColor] set];
    UIRectFill(handleShadowBounds);
    
    // Draw the handle itself.
    [self.handleColor set];
    UIRectFill(handleBounds);
    
}


- (void)drawHandlesInView:(UIView *)view
{
    // Draw handles at the corners and on the sides.
    CGRect bounds = [self bounds];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMidX(bounds), CGRectGetMinY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMinY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMidY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMidY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMaxY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMidX(bounds), CGRectGetMaxY(bounds))];
    [self drawHandleInView:view atPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds))];
    
}

- (BOOL)isContentsUnderPoint:(CGPoint)point {
    
    // Just check against the graphic's bounds.
    return CGRectContainsPoint([self bounds],point);
    
}

- (BOOL)isHandleAtPoint:(CGPoint)handlePoint underPoint:(CGPoint)point
{
    // Check a handle-sized rectangle that's centered on the handle point.
    CGRect handleBounds;
    handleBounds.origin.x = handlePoint.x - SKTGraphicHandleWidth;
    handleBounds.origin.y = handlePoint.y - SKTGraphicHandleWidth; //SKTGraphicHandleHalfWidth
    handleBounds.size.width = SKTGraphicHandleWidth*2;
    handleBounds.size.height = SKTGraphicHandleWidth*2;
    return CGRectContainsPoint(handleBounds,point);
}

- (NSInteger)handleUnderPoint:(CGPoint)point {
    
    // Check handles at the corners and on the sides.
    NSInteger handle = SKTGraphicNoHandle;
    CGRect bounds = [self bounds];
    if ([self isHandleAtPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMinY(bounds)) underPoint:point]) {
        handle = SKTGraphicUpperLeftHandle;
    } else if ([self isHandleAtPoint:CGPointMake(CGRectGetMidX(bounds), CGRectGetMinY(bounds)) underPoint:point]) {
        handle = SKTGraphicUpperMiddleHandle;
    } else if ([self isHandleAtPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMinY(bounds)) underPoint:point]) {
        handle = SKTGraphicUpperRightHandle;
    } else if ([self isHandleAtPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMidY(bounds)) underPoint:point]) {
        handle = SKTGraphicMiddleLeftHandle;
    } else if ([self isHandleAtPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMidY(bounds)) underPoint:point]) {
        handle = SKTGraphicMiddleRightHandle;
    } else if ([self isHandleAtPoint:CGPointMake(CGRectGetMinX(bounds), CGRectGetMaxY(bounds)) underPoint:point]) {
        handle = SKTGraphicLowerLeftHandle;
    } else if ([self isHandleAtPoint:CGPointMake(CGRectGetMidX(bounds), CGRectGetMaxY(bounds)) underPoint:point]) {
        handle = SKTGraphicLowerMiddleHandle;
    } else if ([self isHandleAtPoint:CGPointMake(CGRectGetMaxX(bounds), CGRectGetMaxY(bounds)) underPoint:point]) {
        handle = SKTGraphicLowerRightHandle;
    }
    return handle;
    
}
- (BOOL)canSetDrawingFill
{
    return YES;
}

- (BOOL)canSetDrawingStroke
{
    return YES;
}

- (BOOL)canSetDrawingRoundCorner
{
    return NO;
}


- (void)flipHorizontally {
    
    // Live to be overridden.
    
}


- (void)flipVertically {
    
    // Live to be overridden.
    
}

- (NSInteger)resizeByMovingHandle:(NSInteger)handle toPoint:(CGPoint)point {
    
    // Start with the original bounds.
    CGRect bounds = [self bounds];
    
    // Is the user changing the width of the graphic?
    if (handle==SKTGraphicUpperLeftHandle || handle==SKTGraphicMiddleLeftHandle || handle==SKTGraphicLowerLeftHandle) {
        
        // Change the left edge of the graphic.
        bounds.size.width = CGRectGetMaxX(bounds) - point.x;
        bounds.origin.x = point.x;
        
    } else if (handle==SKTGraphicUpperRightHandle || handle==SKTGraphicMiddleRightHandle || handle==SKTGraphicLowerRightHandle) {
        
        // Change the right edge of the graphic.
        bounds.size.width = point.x - bounds.origin.x;
        
    }
    
    // Did the user actually flip the graphic over?
    if (bounds.size.width<0.0f) {
        
        // The handle is now playing a different role relative to the graphic.
        static NSInteger flippings[9];
        static BOOL flippingsInitialized = NO;
        if (!flippingsInitialized) {
            flippings[SKTGraphicUpperLeftHandle] = SKTGraphicUpperRightHandle;
            flippings[SKTGraphicUpperMiddleHandle] = SKTGraphicUpperMiddleHandle;
            flippings[SKTGraphicUpperRightHandle] = SKTGraphicUpperLeftHandle;
            flippings[SKTGraphicMiddleLeftHandle] = SKTGraphicMiddleRightHandle;
            flippings[SKTGraphicMiddleRightHandle] = SKTGraphicMiddleLeftHandle;
            flippings[SKTGraphicLowerLeftHandle] = SKTGraphicLowerRightHandle;
            flippings[SKTGraphicLowerMiddleHandle] = SKTGraphicLowerMiddleHandle;
            flippings[SKTGraphicLowerRightHandle] = SKTGraphicLowerLeftHandle;
            flippingsInitialized = YES;
        }
        handle = flippings[handle];
        
        // Make the graphic's width positive again.
        bounds.size.width = 0.0f - bounds.size.width;
        bounds.origin.x -= bounds.size.width;
        
        // Tell interested subclass code what just happened.
        [self flipHorizontally];
        
    }
    
    // Is the user changing the height of the graphic?
    if (handle==SKTGraphicUpperLeftHandle || handle==SKTGraphicUpperMiddleHandle || handle==SKTGraphicUpperRightHandle) {
        
        // Change the top edge of the graphic.
        bounds.size.height = CGRectGetMaxY(bounds) - point.y;
        bounds.origin.y = point.y;
        
    } else if (handle==SKTGraphicLowerLeftHandle || handle==SKTGraphicLowerMiddleHandle || handle==SKTGraphicLowerRightHandle) {
        
        // Change the bottom edge of the graphic.
        bounds.size.height = point.y - bounds.origin.y;
        
    }
    
    // Did the user actually flip the graphic upside down?
    if (bounds.size.height<0.0f) {
        
        // The handle is now playing a different role relative to the graphic.
        static NSInteger flippings[9];
        static BOOL flippingsInitialized = NO;
        if (!flippingsInitialized) {
            flippings[SKTGraphicUpperLeftHandle] = SKTGraphicLowerLeftHandle;
            flippings[SKTGraphicUpperMiddleHandle] = SKTGraphicLowerMiddleHandle;
            flippings[SKTGraphicUpperRightHandle] = SKTGraphicLowerRightHandle;
            flippings[SKTGraphicMiddleLeftHandle] = SKTGraphicMiddleLeftHandle;
            flippings[SKTGraphicMiddleRightHandle] = SKTGraphicMiddleRightHandle;
            flippings[SKTGraphicLowerLeftHandle] = SKTGraphicUpperLeftHandle;
            flippings[SKTGraphicLowerMiddleHandle] = SKTGraphicUpperMiddleHandle;
            flippings[SKTGraphicLowerRightHandle] = SKTGraphicUpperRightHandle;
            flippingsInitialized = YES;
        }
        handle = flippings[handle];
        
        // Make the graphic's height positive again.
        bounds.size.height = 0.0f - bounds.size.height;
        bounds.origin.y -= bounds.size.height;
        
        // Tell interested subclass code what just happened.
        [self flipVertically];
        
    }
    
    // Done.
    [self setBounds:bounds];
    return handle;
    
}

@end
