//
//  AnnotationGraphics.h
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import <Foundation/Foundation.h>


enum {
    SKTGraphicNoHandle = 0,
    SKTGraphicUpperLeftHandle = 1,
    SKTGraphicUpperMiddleHandle = 2,
    SKTGraphicUpperRightHandle = 3,
    SKTGraphicMiddleLeftHandle = 4,
    SKTGraphicMiddleRightHandle = 5,
    SKTGraphicLowerLeftHandle = 6,
    SKTGraphicLowerMiddleHandle = 7,
    SKTGraphicLowerRightHandle = 8,
};

typedef enum {
    Stroke_Style_Line = 0,
    Stroke_Style_Dots = 1,
    Stroke_Style_SmallDash = 2,
    Stroke_Style_LongDash = 3,
} StrokeStyle;

typedef enum
{
    ANNOTATION_LINE,
    ANNOTATION_RECT,
    ANNOTATION_RECT_ROUNDED,
    ANNOTATION_CIRCLE,
    ANNOTATION_TEXT,
    ANNOTATION_TEXT_BORDERED,
    ANNOTATION_TEXT_CORNERED,
    ANNOTATION_ARROW,
    ANNOTATION_ARROW_UP,
    ANNOTATION_ARROW_DOWN,
}
AnnotationGraphicType;

//const NSInteger SKTGraphicNoHandle = 0;

@interface AnnotationGraphics : NSObject

@property(assign) CGRect bounds;
@property(assign) BOOL isDrawingFill;
@property(assign) BOOL isDrawingStroke;
@property(assign) BOOL isDrawingRoundCorner;
@property(strong) UIColor *fillColor;
@property(strong) UIColor *strokeColor;
@property(assign) float strokeWidth;
@property(assign) StrokeStyle strokeStyle;
@property(assign) float fillOpacity;
@property(assign) AnnotationGraphicType graphicType;
@property(strong) UIColor *handleColor;

+ (void)translateGraphics:(NSArray *)graphics byX:(CGFloat)deltaX y:(CGFloat)deltaY;
+ (CGRect)boundsOfGraphics:(NSArray *)graphics;
+ (CGRect)drawingBoundsOfGraphics:(NSArray *)graphics;

- (void)drawContentsInView:(UIView *)view isBeingCreateOrEdited:(BOOL)isBeingCreatedOrEditing;
- (void)drawHandleInView:(UIView *)view atPoint:(CGPoint)point;
- (void)drawHandlesInView:(UIView *)view;
- (CGRect)drawingBounds;
- (BOOL)isContentsUnderPoint:(CGPoint)point;
- (NSInteger)handleUnderPoint:(CGPoint)point;
- (NSInteger)resizeByMovingHandle:(NSInteger)handle toPoint:(CGPoint)point;
- (BOOL)isHandleAtPoint:(CGPoint)handlePoint underPoint:(CGPoint)point;

- (BOOL)canSetDrawingFill;
- (BOOL)canSetDrawingRoundCorner;
- (BOOL)canSetDrawingStroke;
- (UIBezierPath *)bezierPathForDrawing;

@end
