//
//  AnnotationText.h
//  TSCharts
//
//  Created by Quoc Le on 8/6/12.
//
//

#import "AnnotationGraphics.h"

@interface AnnotationText : AnnotationGraphics
@property (strong) NSString *text;
@property (strong) UIFont   *font;
@property (assign) UILineBreakMode lineBreak;
@property (assign) UITextAlignment textAlignment;
@property (strong) UIColor *textColor;
@end
