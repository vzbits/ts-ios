//
//  AnnotationRect.m
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import "AnnotationRect.h"

@implementation AnnotationRect

- (id) init
{
    if(self = [super init])
    {
        self.graphicType = ANNOTATION_RECT;
    }
    return self;
}
- (UIBezierPath *)bezierPathForDrawing {
    
    //NSLog(@"bounds %f %f %f %f", self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height);
    // Simple.
    if([self isDrawingRoundCorner])
    {
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:[self bounds] cornerRadius: 6.0];
        [path setLineWidth:[self strokeWidth]];
        return path;
    }
    else
    {
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:[self bounds]];
        [path setLineWidth:[self strokeWidth]];
        return path;        
    }
}

- (BOOL)canSetDrawingRoundCorner
{
    return YES;
}


@end
