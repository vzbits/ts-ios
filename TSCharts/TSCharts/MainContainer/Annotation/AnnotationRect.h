//
//  AnnotationRect.h
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import <Foundation/Foundation.h>
#import "AnnotationGraphics.h"

@interface AnnotationRect : AnnotationGraphics

- (UIBezierPath *)bezierPathForDrawing;

@end
