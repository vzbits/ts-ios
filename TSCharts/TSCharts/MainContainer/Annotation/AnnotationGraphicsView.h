//
//  AnnotationGraphicsView.h
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import <UIKit/UIKit.h>
#import "AnnotationGraphics.h"
#import "YIPopupTextView.h"
#import "AnnotationLine.h"
#import "AnnotationRect.h"
#import "AnnotationCircle.h"
#import "AnnotationText.h"
#import "AnnotationArrow.h"


@protocol AnnotationGraphicViewDelegate <NSObject>
- (void) annotationGraphicViewHandleSelected: (AnnotationGraphics*) selected;
@end

@interface AnnotationGraphicsView : UIView <YIPopupTextViewDelegate>

@property (strong) NSMutableArray *graphics;
@property (strong) AnnotationGraphics *creatingGraphics;
@property (strong) NSMutableIndexSet *selectionIndexes;
@property (assign) id<AnnotationGraphicViewDelegate> delegate;
@property(strong) NSDate *lastTap;
@property (assign) YIPopupTextView* popupTextView;
@property (assign) AnnotationGraphics *clickedGraphic;

- (void) addGraphicType: (AnnotationGraphicType) type;
- (void) setStrokeColorForHandle: (NSInteger) handle color: (UIColor*) color;
- (void) deselect;
@end
