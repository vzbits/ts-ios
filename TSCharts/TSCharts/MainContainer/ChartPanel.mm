//
//  ChartPanel.m
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChartPanel.h"
#import "ColorScheme.h"
#import "UIImageExtensions.h"
#import "CoreDataManager.h"
#import "ButtonRight.h"

#define ALERT_ENABLED NO

@implementation ChartPanel

@synthesize chart = _chart;
@synthesize chartHeader = _chartHeader;
@synthesize crosschair = _crosschair;

implementSingleton(ChartPanel);

- (id)init
{
    self = [super init];
    if (self) 
    {
//        self.layer.cornerRadius = 8.0;
//        self.layer.borderWidth = 2.0;
//        self.layer.borderColor = [UIColor colorWithRGBHex: 0xc7c7c7].CGColor;
        self.backgroundColor = TABLE_BGCOLOR;
//        self.layer.masksToBounds = YES;
        
        indicatorJS = IndicatorJS::instance();

        self.chart = [[[Chart alloc] init] autorelease];
        [self addSubview: self.chart];
        
        self.chartHeader = [[[UIView alloc] init] autorelease];
        [self addSubview: self.chartHeader];

        self.chartHeader.backgroundColor = HEADER_COLOR;
//		self.chartHeader.symbolLabel.textColor = HEADER_TEXTCOLOR;
//		self.chartHeader.openLabel.textColor = self.chart.style.fontColor;
//		self.chartHeader.openValue.textColor = self.chart.style.fontColor;
//		self.chartHeader.dateLabel.textColor = self.chart.style.fontColor;
		
//        self.crosschair = [[[CrossChairView alloc] initWithChart: self.chart] autorelease];
//        self.crosschair.delegate = self;
//        [self addSubview: self.crosschair];

//        self.alertEditView = [[[AlertVisualEditView alloc] initWithChart: self.chart] autorelease];
//        //self.alertEditView.delegate = self;
//        [self addSubview: self.alertEditView];
		
//        
//        self.annotation = [[[AnnotationGraphicsView alloc] init] autorelease];
//        self.annotation.delegate = self;
//        //[self addSubview: self.annotation];
//        [self.crosschair addSubview: self.annotation];
		
		self.bellBtn = [UIButton buttonWithType: UIButtonTypeCustom];
		self.bellBtn.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
		[self.bellBtn setImage: [[UIImage imageNamed:@"bell.png"] getImageWithTint: CHART_PERIODICITY_COLOR_OFF withIntensity: 1.0]   forState: UIControlStateNormal];
		[self.bellBtn addTarget: self action: @selector(alertClicked:) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview: self.bellBtn];

		self.dailyBtn = [UIButton buttonWithType: UIButtonTypeCustom];
		[self.dailyBtn setTitle:@"Daily" forState: UIControlStateNormal];
		self.dailyBtn.titleLabel.font = CHART_PERIODICITY_FONT;
		[self.dailyBtn setTitleColor: CHART_PERIODICITY_COLOR_OFF  forState: UIControlStateNormal];
		[self.dailyBtn setTitleColor: CHART_PERIODICITY_COLOR_ON  forState: UIControlStateSelected];
		[self.dailyBtn addTarget: self action: @selector(periodicyPressed:) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview: self.dailyBtn];
		
		self.weeklyBtn = [UIButton buttonWithType: UIButtonTypeCustom];
		[self.weeklyBtn setTitle:@"Weekly" forState: UIControlStateNormal];
		self.weeklyBtn.titleLabel.font = CHART_PERIODICITY_FONT;
		[self.weeklyBtn setTitleColor: CHART_PERIODICITY_COLOR_OFF  forState: UIControlStateNormal];
		[self.weeklyBtn setTitleColor: CHART_PERIODICITY_COLOR_ON  forState: UIControlStateSelected];
		[self.weeklyBtn addTarget: self action: @selector(periodicyPressed:) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview: self.weeklyBtn];

		self.monthlyBtn = [UIButton buttonWithType: UIButtonTypeCustom];
		[self.monthlyBtn setTitle:@"Monthly" forState: UIControlStateNormal];
		self.monthlyBtn.titleLabel.font = CHART_PERIODICITY_FONT;
		[self.monthlyBtn setTitleColor: CHART_PERIODICITY_COLOR_OFF  forState: UIControlStateNormal];
		[self.monthlyBtn setTitleColor: CHART_PERIODICITY_COLOR_ON  forState: UIControlStateSelected];
		[self.monthlyBtn addTarget: self action: @selector(periodicyPressed:) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview: self.monthlyBtn];

//		self.periodicityToggle = [[UISegmentedControl alloc] initWithItems: @[@"Daily", @"Weekly", @"Monthly"]];
//		[self addSubview: self.periodicityToggle];
//		self.periodicityToggle.segmentedControlStyle = UISegmentedControlStylePlain;
		
		self.newsToggleBtn = [[ButtonRight alloc] init];
		[self.newsToggleBtn setTitle:@"News" forState: UIControlStateNormal];
		[self.newsToggleBtn setTitle:@"List" forState: UIControlStateSelected];
		[self.newsToggleBtn setImage: [UIImage imageNamed:@"up_icon"] forState: UIControlStateNormal];
		[self.newsToggleBtn setImage: [UIImage imageNamed:@"down_icon"] forState: UIControlStateSelected];

		self.newsToggleBtn.titleLabel.font = CHART_PERIODICITY_FONT;
		[self.newsToggleBtn setTitleColor: NEWS_TOGGLE_OFF_TEXT  forState: UIControlStateNormal];
		[self.newsToggleBtn setTitleColor: NEWS_TOGGLE_ON_TEXT  forState: UIControlStateSelected];
		[self addSubview: self.newsToggleBtn];
		self.newsToggleBtn.layer.cornerRadius = 10;
		self.newsToggleBtn.layer.borderWidth = 1;
		self.newsToggleBtn.layer.borderColor = NEWS_TOGGLE_BG_COLOR.CGColor;
		self.newsToggleBtn.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 7);
		
		self.symbolLabel = [[[UILabel alloc] init] autorelease];
        self.symbolLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 15];
        self.symbolLabel.backgroundColor = [UIColor clearColor];
		self.symbolLabel.textColor = HEADER_TEXTCOLOR;
        [self.chartHeader addSubview: self.symbolLabel];
		
		self.closeValue = [[[UILabel alloc] init] autorelease];
        self.closeValue.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 15];
        self.closeValue.backgroundColor = [UIColor clearColor];
		self.closeValue.textAlignment = UITextAlignmentRight;
		self.closeValue.textColor = HEADER_TEXTCOLOR;
        [self.chartHeader addSubview: self.closeValue];

		self.timestampLabel = [[[UILabel alloc] init] autorelease];
        self.timestampLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size: 10];
        self.timestampLabel.backgroundColor = [UIColor clearColor];
		self.timestampLabel.textColor = TIMESTAMP_COLOR;
		self.timestampLabel.textAlignment = UITextAlignmentRight;
        //[self.chartHeader addSubview: self.timestampLabel];
		
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.chartHeader.frame = CGRectMake(0,0,self.width,30);
    self.chart.frame = CGRectMake(18,45+30,self.width-18,self.height-45-39);
    self.crosschair.frame = self.chart.frame;
    self.alertEditView.frame = self.chart.frame;
	self.timestampLabel.frame = CGRectMake(self.width - 180, self.chart.bottom - 20, 140,40);
	
    self.annotation.frame = self.crosschair.bounds;
	self.symbolLabel.frame = CGRectMake(15,(self.chartHeader.height-21)/2,500, 21);
	self.closeValue.frame = CGRectMake(self.width - 150, (self.chartHeader.height-21)/2, 150-15, 21);
	
	float marginTop = 10;
	if(ALERT_ENABLED)
	{
		self.bellBtn.frame = CGRectMake(5,self.chartHeader.bottom+marginTop, 25, 25);
	}
	else
	{
		self.bellBtn.hidden = YES;
	}
	self.dailyBtn.frame = CGRectMake(self.bellBtn.right + 10,self.chartHeader.bottom+marginTop, 50, 25);
	self.weeklyBtn.frame = CGRectMake(self.dailyBtn.right + 10,self.chartHeader.bottom+marginTop, 50, 25);
	self.monthlyBtn.frame = CGRectMake(self.weeklyBtn.right + 10,self.chartHeader.bottom+marginTop, 50, 25);
	//self.periodicityToggle.frame = CGRectMake(30, self.chartHeader.bottom+marginTop, self.width - 30 - 70, 25);
	
	UIImage *image = [self.newsToggleBtn imageForState: UIControlStateNormal];
	self.newsToggleBtn.frame = CGRectMake(self.width - 70,self.chartHeader.bottom+marginTop, 60, 20);
//	self.newsToggleBtn.imageEdgeInsets = UIEdgeInsetsMake(0., self.newsToggleBtn.width - (image.size.width + 15.), 0., 0.);
//	self.newsToggleBtn.titleEdgeInsets = UIEdgeInsetsMake(0., 0., 0., image.size.width);
	
//	NSLog(@"%s %x width=%f", __FUNCTION__, self, self.width);
}

- (void) setEmptyChart: (NSString*) name symbol: (NSString*) symbol
{
	StockPriceData *stockPrice = [[[StockPriceData alloc] init] autorelease];
	stockPrice.name = name;
	stockPrice.symbol = symbol;
	NSMutableArray *prices = [NSMutableArray array];
	stockPrice.prices = prices;
	[self setData: stockPrice];
}


- (void) alertClicked: (id) sender
{
	PriceData *p = [self.chart.priceData lastObject];
	[self.alertEditView addNewAlertForPrice: p.close * 1.05f  lastPrice: p.close];
}

- (void) periodicyPressed: (UIButton*) sender
{
	PriceType pt;
	if(sender == self.dailyBtn)
	{
		pt = PriceType_PT_DAILY;
	}
	if(sender == self.weeklyBtn)
	{
		pt = PriceType_PT_WEEKLY;
	}
	if(sender == self.monthlyBtn)
	{
		pt = PriceType_PT_MONTHLY;
	}
	
	[self setPeriodicity: pt];
	[self periodictyChange: pt];
}

- (void) setPeriodicity: (PriceType) priceType
{
	self.dailyBtn.selected = priceType == PriceType_PT_DAILY ? YES : NO;
	self.weeklyBtn.selected = priceType == PriceType_PT_WEEKLY ? YES : NO;;
	self.monthlyBtn.selected = priceType == PriceType_PT_MONTHLY ? YES : NO;
	
	self.dailyBtn.titleLabel.font = self.dailyBtn.selected ? CHART_PERIODICITY_FONT_ON : CHART_PERIODICITY_FONT;
	self.weeklyBtn.titleLabel.font = self.weeklyBtn.selected ? CHART_PERIODICITY_FONT_ON : CHART_PERIODICITY_FONT;
	self.monthlyBtn.titleLabel.font = self.monthlyBtn.selected ? CHART_PERIODICITY_FONT_ON : CHART_PERIODICITY_FONT;
}

- (void) periodictyChange: (PriceType) priceType
{
	[self.delegate chartPanelPeriodictyChanged: priceType symbol: self.lastData.symbol];
}

- (void) setData: (StockPriceData*) sp
{
    self.lastData = sp;
    //indicatorJS->run_indicator(sp);

	self.symbolLabel.text = sp.name;
	if(sp.prices.count > 0)
	{
		PriceData *p = [sp.prices objectAtIndex:sp.prices.count-1];
		self.closeValue.text = [ChartHeader formatNumber: p.close];
	}
    //[self.chartHeader setData: sp];
    [self.chart loadChart: sp Indicators: indicatorJS->get_data()];
	[self.alertEditView loadData: sp.symbol];
	[self setPeriodicity: (PriceType)sp.priceType];
	
	PriceData *price = [self.lastData.prices lastObject];
	NSDate *lastDate = [NSDate dateFromPB: price.date];
	
	NSString *timeStr = [lastDate stringWithFormat:@"HH:mm a"];
	NSString *marketStr = @"";
	self.timestampLabel.text = [NSString stringWithFormat:@"%@ %@", marketStr, timeStr];
}

- (void) setLastTick: (PriceData*) p
{
	[self.chart setLastTick: p];
}

- (void) reloadChart
{
	if(self.lastData != nil)
		[self setData: self.lastData];
}

- (void) reloadChart: (BOOL) runIndicator
{
    if(runIndicator)
        [self reloadChart];
    else
    {
       //[self.chartHeader setData: self.lastData];
        [self.chart loadChart: self.lastData Indicators: indicatorJS->get_data()];
    }
}

- (void) addIndicator: (IndicatorCodeData *)item
{
    indicatorJS->add_indicator([item.id UTF8String]);
}

- (void) reloadIndicators
{
    indicatorJS->load_settings();
}

- (void) crossChairStateChanged:(UIGestureRecognizerState)state object:(NSDate*)obj point: (CGPoint) point
{
    if(state == UIGestureRecognizerStateChanged)
    {
        NSTimeInterval time = [obj timeIntervalSince1970];
        for(int i =0; i< self.lastData.prices.count; i++)
        {
            PriceData *p = [self.lastData.prices objectAtIndex: i];
            if(p.date/1000 == time)
            {
                //self.chartHeader.backgroundView.hidden = NO;
                //[self.chartHeader setPriceData: p];
                [self.chart drawCurrentPriceAtY: point.y];
            }
        }
    }
    else if(state == UIGestureRecognizerStateEnded)
    {
        if(self.lastData.prices.count > 0)
        {
            //self.chartHeader.backgroundView.hidden = YES;
            //[self.chartHeader setData: self.lastData];
            [self.chart drawCurrentPrice];
        }
    }
}

- (void) annotationGraphicViewHandleSelected: (AnnotationGraphics*) selected //SKTGraphicNoHandle for no handle
{
    [self.delegate chartPanelAnnotationSelected: selected];
}

- (UIImage*) generateImage
{
    UIImage *image;
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end
