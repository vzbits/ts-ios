//
//  MainView.m
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChartContainer.h"
#import "ASIHTTPRequest.h"
#import "StockPrice.h"
#import "Settings.h"
#import "AppDelegate.h"
#import "Theme.h"

#define CHART_MARGIN 11
#define SEARCH_TIMER NO

@implementation ChartContainer

@synthesize chartPanel = _chartPanel;
@synthesize navBar = _navBar;
@synthesize searchBar = _searchBar;
@synthesize currentPopover = _currentPopover;
@synthesize lastSymbolQueried = _lastSymbolQueried;
@synthesize lastSymbolInSearch = _lastSymbolInSearch;
@synthesize popoverContent = _popoverContent;

- (id)init
{
    self = [super init];
    if (self) 
    {        
        self.chartPanel = [ChartPanel instance]; //[[[ChartPanel alloc] init] autorelease];
        self.chartPanel.delegate = self;
        [self addSubview: self.chartPanel];
        
        self.backgroundColor = [UIColor colorWithRGBHex:0xeeeeee];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];

    NSLog(@"Rect %f %f %f %f", self.x, self.y, self.width, self.height);    
    
    self.chartPanel.frame = CGRectMake(CHART_MARGIN,CHART_MARGIN, self.width-(CHART_MARGIN*2), self.height-(CHART_MARGIN*2));
}

- (void) getChartData:(NSString *)symbol 
{
    [[ServiceManager instance] getChartData: symbol
                       withBlock: ^(StockPriceData *data)
    {
         [self.chartPanel setData: data];
        [self.delegate chartContainerChartChanged: symbol];
     }
        withErrorBlock: ^(NSString *error)
     {
         
     }];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{    
//    NSArray *rightItems = self.navBar.items;
//    self.currentPopover =[SymbolSearchViewController createPopover];
//    self.currentPopover.delegate = self;
//    ((SymbolSearchViewController*)self.currentPopover.contentViewController).delegate = self;
//    [self.currentPopover presentPopoverFromBarButtonItem: [rightItems lastObject] 
//                    permittedArrowDirections: UIPopoverArrowDirectionUp 
//                                    animated: YES];
    
    return YES;
}

- (void) getSearchDataWithTimer: (NSTimer*) timer
{
    NSString *text = [[timer userInfo] objectForKey:@"text"];
    
    [[ServiceManager instance] getSymbolSearchData: text
                              withBlock:^(SymbolSearchResultData *data) {
                                  [(SymbolSearchViewController*)self.popoverContent setData: data];
                              }
                         withErrorBlock:^(NSString *error) {
                             
                         }];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(self.lastSymbolQueried != nil)
    {
        [self.lastSymbolQueried invalidate];
        self.lastSymbolQueried = nil;
    }
    
    if(searchText == nil || [searchText isEqualToString:@""])
    {
        [(SymbolSearchViewController*)self.popoverContent showRecentResults];
        return;
    }
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:searchText forKey: @"text"];
    
    if(SEARCH_TIMER)
    {
        self.lastSymbolQueried = [NSTimer scheduledTimerWithTimeInterval: 0.5
                                                                  target: self 
                                                                selector:@selector(getSearchDataWithTimer:) 
                                                                userInfo: userInfo 
                                                                 repeats: NO];
    }
    else
    {
        [[ServiceManager instance] getSymbolSearchData: searchText
                                             withBlock:^(SymbolSearchResultData *data) {
                                                 [(SymbolSearchViewController*)self.popoverContent setData: data];
                                             }
                                        withErrorBlock:^(NSString *error) {
                                            
                                        }];
    }

//    NSLog(@"Interval %f",[self.lastSymbolQueried timeIntervalSinceNow]);
//    if(self.lastSymbolQueried == nil || [self.lastSymbolQueried timeIntervalSinceNow] <= -0.30)
//    {
//        [self getSearchData: searchText];
//        self.lastSymbolQueried = [NSDate date];
//    }
}

- (BOOL) searchBarShouldEndEditing:(UISearchBar *)searchBar
{
//    [self.currentPopover dismissPopoverAnimated: YES];
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    SymbolSearchResultData *data = ((SymbolSearchViewController*)self.currentPopover.contentViewController).searchData;
    if(data.results.count == 1)
    {
        [self symbolSearchSelected: [data.results objectAtIndex:0]];
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.popoverContent = nil;
    [self.searchBar resignFirstResponder];
    self.searchBar.text = @"";
}

- (void) symbolSearchSelected:(SymbolData*) searchData
{
    [[Settings instance] addRecentSearch: searchData];
    self.searchBar.text = @"";
    [self.searchBar resignFirstResponder];
    [self.currentPopover dismissPopoverAnimated: YES];
    self.popoverContent = nil;
    self.currentPopover = nil;
    [self getChartData: searchData.symbol];
}
- (void) dismissPopover
{    
    [self.currentPopover dismissPopoverAnimated: NO];
    self.currentPopover = nil;
    self.popoverContent = nil;
}

#pragma mark - UIBarButton delegate

- (void) searchButtonClicked: (id) sender
{
    if(self.popoverContent == nil || ![self.popoverContent isKindOfClass: [SymbolSearchViewController class]])
    {
        [self dismissPopover];
    
        SymbolSearchViewController *rootController = [[[SymbolSearchViewController alloc] initWithStyle: UITableViewStylePlain] autorelease];
        rootController.delegate = self;
        UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController: rootController];
        UIPopoverController *popover = [[[UIPopoverController alloc] initWithContentViewController: controller] autorelease];
        self.currentPopover = popover;
        self.currentPopover.delegate = self;
        self.popoverContent = rootController;
        
        UISearchBar *searchBar = [[[UISearchBar alloc] initWithFrame: CGRectMake(0,0,300,44)] autorelease];
        //UIBarButtonItem *searchBarItem = [[[UIBarButtonItem alloc] initWithCustomView: self.searchBar] autorelease];
        
        for(int i =0; i<[searchBar.subviews count]; i++) {
            if([[searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
                [(UITextField*)[searchBar.subviews objectAtIndex:i] setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
        }
        
        searchBar.delegate = self;
        rootController.navigationItem.titleView = searchBar;
        [searchBar becomeFirstResponder];

        [self.currentPopover presentPopoverFromBarButtonItem: sender
                                    permittedArrowDirections: UIPopoverArrowDirectionUp
                                                    animated: YES];
    }
    else
    {
        [self.currentPopover dismissPopoverAnimated: YES];
        self.popoverContent = nil;
    }
}
- (void) toolsClicked: (id) sender
{
    if(self.popoverContent == nil || ![self.popoverContent isKindOfClass: [ToolsMenuViewController class]])
    {
        [self dismissPopover];
        
        ToolsMenuViewController *rootController = [[[ToolsMenuViewController alloc] initWithStyle: UITableViewStylePlain] autorelease];
        UINavigationController *navController =[[[UINavigationController alloc] initWithRootViewController: rootController] autorelease];
        UIPopoverController *popover = [[[UIPopoverController alloc] initWithContentViewController: navController] autorelease];
        self.currentPopover = popover;
        rootController.delegate = self;
        
        self.popoverContent = rootController;
        self.currentPopover.delegate = self;
        self.currentPopover.popoverContentSize = CGSizeMake(300,330);
        [self.currentPopover presentPopoverFromBarButtonItem: sender
                                    permittedArrowDirections: UIPopoverArrowDirectionUp
                                                    animated: YES];
    }
    else
    {
        [self.currentPopover dismissPopoverAnimated: YES];
        self.popoverContent = nil;
    }
}

- (void) addClicked: (id) sender
{
    if(self.popoverContent == nil || ![self.popoverContent isKindOfClass: [AddViewController class]])
    {
        [self dismissPopover];

        AddViewController *rootController = [[[AddViewController alloc] init] autorelease];
        rootController.delegate = self;
        UINavigationController *navController =[[[UINavigationController alloc] initWithRootViewController: rootController] autorelease];
        UIPopoverController *popover = [[[UIPopoverController alloc] initWithContentViewController: navController] autorelease];
        self.currentPopover = popover;
        
        self.popoverContent = rootController;
        self.currentPopover.delegate = self;
        self.currentPopover.popoverContentSize = CGSizeMake(320,440);
        rootController.contentSizeForViewInPopover = CGSizeMake(320,400);
        [self.currentPopover presentPopoverFromBarButtonItem: sender
                                    permittedArrowDirections: UIPopoverArrowDirectionAny
                                                    animated: YES];
        
    }
    else
    {
        [self.currentPopover dismissPopoverAnimated: YES];
        self.popoverContent = nil;
    }
}

- (void) colorClicked: (id) sender
{
    if(self.popoverContent == nil || ![self.popoverContent isKindOfClass: [AddViewController class]])
    {
        [self dismissPopover];

        ColorSelectionTab *tab = [[[ColorSelectionTab alloc] init] autorelease];
        tab.fillColor = (self.lastSelectedAnnotation.isDrawingFill) ? self.lastSelectedAnnotation.fillColor : [UIColor clearColor];
        tab.strokeColor = self.lastSelectedAnnotation.strokeColor;
        tab.strokeWidth = self.lastSelectedAnnotation.strokeWidth;
        tab.strokeStyle = self.lastSelectedAnnotation.strokeStyle;
        tab.fillOpacity =  self.lastSelectedAnnotation.fillOpacity;
        tab.canSetDrawingFill = self.lastSelectedAnnotation.canSetDrawingFill;
        tab.graphicType = self.lastSelectedAnnotation.graphicType;
        if([self.lastSelectedAnnotation isKindOfClass:[AnnotationText class]])
        {
            AnnotationText *at = (AnnotationText*) self.lastSelectedAnnotation;
            tab.textColor  = at.textColor;
        }
        
        tab.delegate = self;
        UINavigationController *navController =[[[UINavigationController alloc] initWithRootViewController: tab] autorelease];        
        UIPopoverController *popover = [[[UIPopoverController alloc] initWithContentViewController: navController] autorelease];
        self.currentPopover = popover;

        self.popoverContent = tab;
        self.currentPopover.delegate = self;
        self.currentPopover.popoverContentSize = CGSizeMake(320,440);
        tab.contentSizeForViewInPopover =  CGSizeMake(320,400);
        [self.currentPopover presentPopoverFromBarButtonItem: sender
                                    permittedArrowDirections: UIPopoverArrowDirectionAny
                                                    animated: YES];
        
        [tab selectIndex: 0];
    }
    else
    {
        [self.currentPopover dismissPopoverAnimated: YES];
        self.popoverContent = nil;
    }
}

#pragma mark - Add Controller delegate

- (void) addViewControllerIndicatorClicked:(IndicatorCodeData *)item
{
    [self.chartPanel addIndicator: item];
    [self.chartPanel reloadChart];
    [self.currentPopover dismissPopoverAnimated: YES];
    self.currentPopover = nil;
    self.popoverContent = nil;
}
- (void) addViewControllerIndicatorRemoved: (id) sender
{
    [self.chartPanel reloadChart];
}
- (void) addViewControllerAnnotationClicked:(AnnotationGraphicType) tag
{
    [self.chartPanel.annotation addGraphicType: tag];
    [self dismissPopover];
}

#pragma mark - Tools delegate

- (void) toolsMenuIndicatorRemoved:(id)sender
{
    [self.chartPanel reloadChart];
}

- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
	NSString *message;
	NSString *title;
	if (!error) {
		title = @"Save successful";
		message = nil;
	} else {
		title = @"Save failed.";
		message = [error description];
	}
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
													message:message
												   delegate:nil
										  cancelButtonTitle:@"OK"
										  otherButtonTitles:nil];
	[alert show];
	[alert release];
}

- (void) toolsMenuShareClicked:(id) sender menuItem:(NSString *)menuItem
{
    [self dismissPopover];
    [self.chartPanel.annotation deselect];
    
    UIImage *image = [self.chartPanel generateImage];
    
    if([menuItem isEqualToString:@"Save to Photo Album"])
    {
        UIImageWriteToSavedPhotosAlbum(image, self, @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), nil);
    }
    if([menuItem isEqualToString:@"Twitter"])
    {
        //Check if device can send tweet
        if ([TWTweetComposeViewController canSendTweet])
        {
            //Create tweet sheet and set initial text
            NSString *initialText = [NSString stringWithFormat: @"#%@ provided by TopStock.us", self.chartPanel.lastData.symbol];
            TWTweetComposeViewController *tweetSheet = [[[TWTweetComposeViewController alloc] init] autorelease];
            [tweetSheet setInitialText: initialText];
            [tweetSheet addURL:[NSURL URLWithString:@"http://www.iosdeveloperguide.com"]];
            
            //Show tweet sheet
            UIViewController *controller = ROOT_VIEWCONTROLLER();
            [controller presentModalViewController:tweetSheet animated:YES];
        }
        else
        {
            //Device cannot send Tweet.  Show error notification
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Unable to Tweet"
                                      message:@"Please ensure you have at least one Twitter account setup and have internet connectivity."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
            
        }
    }
    if([menuItem isEqualToString:@"Email Chart"])
    {
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        [picker setSubject: [NSString stringWithFormat: @"%@ Chart by TopStock.us", self.chartPanel.lastData.symbol]];
        [picker addAttachmentData: UIImagePNGRepresentation(image) mimeType: @"image/png" fileName:@"chart.png"];
        
        UIViewController *controller = ROOT_VIEWCONTROLLER();
        [controller presentModalViewController:picker animated:YES];
        [picker release];
    }
    
    if([menuItem isEqualToString:@"Print Chart"])
    {
        if (![UIPrintInteractionController isPrintingAvailable])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"No printer available"
                                                               delegate: nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil,
                                      nil];
            [alertView show];
            [alertView release];
        }
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            if (!completed && error)
                NSLog(@"FAILED! due to error in domain %@ with error code %u",
                      error.domain, error.code);
        };
        
        UIPrintInteractionController *controller = [UIPrintInteractionController sharedPrintController];
        // other code here...
        UIPrintInfo *printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputPhoto;
        printInfo.jobName = @"Chart from TopStock.us";
        printInfo.duplex = UIPrintInfoDuplexNone;
        controller.printingItem = image;
        
        if (!controller.printingItem && image.size.width > image.size.height)
            printInfo.orientation = UIPrintInfoOrientationLandscape;
        
        NSArray *rightItems = self.navBar.items;

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [controller presentFromBarButtonItem: [rightItems objectAtIndex:6] animated:YES completionHandler:completionHandler];
            //UINavigationController *navController = (UINavigationController*)self.currentPopover.contentViewController;
            //[navController pushViewController: controller animated: YES];
        } else {
            [controller presentAnimated:YES completionHandler:completionHandler];
        }
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    UIViewController *rootController = ROOT_VIEWCONTROLLER();
    [rootController dismissModalViewControllerAnimated:YES];
}

- (void) toolsChartSettingsValueChangedForKey: (NSString*) str
{
    [self.chartPanel reloadChart];
}
- (void) toolsChartSettingsDidActionForKey: (NSString*) str
{
    [self.chartPanel reloadIndicators];
    [self.chartPanel reloadChart];
}
#pragma mark - ChartPanel delegate

- (void) chartPanelAnnotationSelected: (AnnotationGraphics*) selected
{
    self.lastSelectedAnnotation = selected;
    
    NSArray *rightItems = self.navBar.items;
    UIBarButtonItem *item = [rightItems objectAtIndex: 1];
    
    if(selected == nil )
        item.enabled =  NO;
    else
        item.enabled = YES;
}

#pragma mark - ColorSelection delegate
- (void) colorSelectionStrokeColorChanged: (UIColor*) color
{
    self.lastSelectedAnnotation.strokeColor = color;
    [self.chartPanel.annotation setNeedsDisplay];
}
- (void) colorSelectionFillColorChanged: (UIColor*) color
{
    self.lastSelectedAnnotation.fillColor = color;
 
    if(color.alpha == 0.0)
        self.lastSelectedAnnotation.isDrawingFill = NO;
    else
        self.lastSelectedAnnotation.isDrawingFill = YES;
    
    [self.chartPanel.annotation setNeedsDisplay];
}
- (void) colorSelectionStrokeSizeChanged: (int) size
{
    self.lastSelectedAnnotation.strokeWidth = size;
    [self.chartPanel.annotation setNeedsDisplay];
}
- (void) colorSelectionStrokeStyleChanged: (StrokeStyle) strokeStyle
{
    self.lastSelectedAnnotation.strokeStyle = strokeStyle;
    [self.chartPanel.annotation setNeedsDisplay];
}
- (void) colorSelectionFillOpacity: (float) opacity
{
    self.lastSelectedAnnotation.fillOpacity = opacity;
    [self.chartPanel.annotation setNeedsDisplay];
}

- (void) colorSelectionTextAlignmentChanged:(UITextAlignment)alignment
{
    if([self.lastSelectedAnnotation isKindOfClass: [AnnotationText class]])
    {
        AnnotationText *graphic = (AnnotationText*) self.lastSelectedAnnotation;
        graphic.textAlignment  = alignment;
        [self.chartPanel.annotation setNeedsDisplay];
    }
}

- (void) colorSelectionTextColorChanged: (UIColor*) color;
{
    if([self.lastSelectedAnnotation isKindOfClass: [AnnotationText class]])
    {
        AnnotationText *graphic = (AnnotationText*) self.lastSelectedAnnotation;
        graphic.textColor = color;
        [self.chartPanel.annotation setNeedsDisplay];
    }
}
@end
