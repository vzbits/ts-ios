//
//  ColorSelectionTab.m
//  TSCharts
//
//  Created by Quoc Le on 8/5/12.
//
//

#import "ColorSelectionTab.h"

@implementation ColorSelectionTab
- (id) init
{
    if(self = [super init])
    {
        self.hidesBottomBarWhenPushed = YES;
 
    }
    return self;
}

- (void) loadView
{
    [super loadView];
    
    NSMutableArray *viewControllers = [NSMutableArray arrayWithCapacity:2];
    
    MNColorSelectionViewController *rainbowViewController = [MNColorSelectionViewController rainbowSelectionViewController];
    rainbowViewController.title = @"Color Selection";
    if (rainbowViewController) [viewControllers addObject:rainbowViewController];
    rainbowViewController.delegate = self;
    
    MNColorSelectionViewController *monochromeViewController = [MNColorSelectionViewController monoChromeSelectionViewController];
    monochromeViewController.title = @"Color Selection";
    if (monochromeViewController) [viewControllers addObject:monochromeViewController];
    monochromeViewController.delegate = self;
    
    // Navigation logic may go here. Create and push another view controller.
    MNPageViewController *pageViewController = [[[MNPageViewController alloc] initWithViewControllers:viewControllers] autorelease];

    OthersSelectionViewController *others = [[[OthersSelectionViewController alloc] init] autorelease];
    others.fillOpacity = self.fillOpacity;
    others.strokeStyle = self.strokeStyle;
    others.strokeWidth = self.strokeWidth;
    others.canSetDrawingFill = self.canSetDrawingFill;
    others.graphicType = self.graphicType;
    others.textColor = self.textColor;
    others.delegate = self;

    NSArray *controllers;
    NSArray *segmentArray;
    
    if(self.canSetDrawingFill)
    {
        controllers = [NSArray arrayWithObjects: pageViewController, pageViewController, others, nil];
        segmentArray = [NSArray arrayWithObjects: @"Border Color", @"Fill Color",@"Styles", nil];
    }
    else
    {
        controllers = [NSArray arrayWithObjects: pageViewController, others, nil];
        segmentArray = [NSArray arrayWithObjects: @"Stroke Color",@"Styles", nil];
    }
    
    if(self.graphicType == ANNOTATION_TEXT)
    {
        segmentArray = [NSArray arrayWithObjects: @"Border Color", @"Fill Color",@"Styles", nil];
    }
    
    self.viewControllers = controllers;
    
    UISegmentedControl *sc = [[[UISegmentedControl alloc] initWithItems: segmentArray] autorelease];
    sc.segmentedControlStyle = UISegmentedControlStyleBar;
    sc.selectedSegmentIndex = 0;
    [sc addTarget: self action: @selector(segmentControlSelected:) forControlEvents: UIControlEventValueChanged];
    UIBarButtonItem *scItem = [[[UIBarButtonItem alloc] initWithCustomView:sc] autorelease];
    self.navigationItem.titleView = scItem.customView;
}

- (void) selectIndex: (int) index
{
    [self.currentViewController.view removeFromSuperview];
    UIViewController *newController = [self.viewControllers objectAtIndex: index];
    newController.view.frame = self.view.bounds;
    [self.view addSubview: newController.view];
    self.currentViewController = newController;
    self.selectionIndex = index;
    
    if([newController isKindOfClass:[OthersSelectionViewController class]])
    {
        OthersSelectionViewController *others = (OthersSelectionViewController*) self.currentViewController;
        others.contentSizeForViewInPopover = self.contentSizeForViewInPopover;
        others.myNavController = self.navigationController;
    }
    
    // scroll to the right page with check mark
    if(index == 0 || (self.canSetDrawingFill && index == 1))
    {
        MNPageViewController *pageViewController  = (MNPageViewController*) newController;
        MNColorSelectionViewController *page1 = [pageViewController.viewControllers objectAtIndex:0];
        MNColorSelectionViewController *page2 = [pageViewController.viewControllers objectAtIndex:1];
        UIColor *currentColor;

        //[pageViewController viewWillAppear: YES];
        
        if(index == 0)
            currentColor = self.strokeColor;
        else
            currentColor = self.fillColor;
            
        if([page1 viewHasColor: currentColor])
        {
            [pageViewController changePageToIndex: 0];
            return;
        }
        if([page2 viewHasColor: currentColor])
        {
            [pageViewController changePageToIndex: 1];
            return;
        }
    }
}

- (void) segmentControlSelected: (UISegmentedControl*) sender
{
    [self selectIndex: sender.selectedSegmentIndex];
}

#pragma mark - MNColorSelectionViewControllerDelegate

- (UIColor *)colorSelectionControllerSelectedColor:(MNColorSelectionViewController *)controller
{
    if(self.selectionIndex == 0)
    {
        return self.strokeColor;
    }
    else
    {
        return self.fillColor;
    }
}
- (void)colorSelectionController:(MNColorSelectionViewController *)controller didSelectedColor:(UIColor *)color
{
   // self.lastSelectedAnnotation.strokeColor = color;
    //[self.chartPanel.annotation setNeedsDisplay];
    
    if(self.selectionIndex == 0)
        [self.delegate colorSelectionStrokeColorChanged: color];
    if(self.selectionIndex == 1 && self.canSetDrawingFill)
        [self.delegate colorSelectionFillColorChanged: color];

}
#pragma mark - OthersSelection Delegate
- (void) othersSelectionStrokeSizeChanged: (int) size
{
    [self.delegate colorSelectionStrokeSizeChanged: size];
}
- (void) othersSelectionStrokeStyleChanged: (StrokeStyle) strokeStyle
{
    [self.delegate colorSelectionStrokeStyleChanged: strokeStyle];
}
- (void) othersSelectionFillOpacity: (float) opacity
{
    [self.delegate colorSelectionFillOpacity: opacity];
}

- (void) othersSelectionTextAlignmentChanged:(UITextAlignment)alignment
{
    [self.delegate colorSelectionTextAlignmentChanged: alignment];
}

- (void) othersSelectionTextColorChanged: (UIColor*) color;
{
    [self.delegate colorSelectionTextColorChanged: color];
}
@end
