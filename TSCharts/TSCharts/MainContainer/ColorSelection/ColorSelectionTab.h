//
//  ColorSelectionTab.h
//  TSCharts
//
//  Created by Quoc Le on 8/5/12.
//
//

#import <UIKit/UIKit.h>
#import "MNColorSelectionViewController.h"
#import "MNPageViewController.h"
#import "OthersSelectionViewController.h"
#import "AnnotationGraphicsView.h"

@protocol ColorSelectionTabDelegate <NSObject>

- (void) colorSelectionStrokeColorChanged: (UIColor*) color;
- (void) colorSelectionFillColorChanged: (UIColor*) color;
- (void) colorSelectionStrokeSizeChanged: (int) size;
- (void) colorSelectionStrokeStyleChanged: (StrokeStyle) strokeStyle;
- (void) colorSelectionFillOpacity: (float) opacity;
- (void) colorSelectionTextAlignmentChanged: (UITextAlignment) alignment;
- (void) colorSelectionTextColorChanged: (UIColor*) color;
@end

@interface ColorSelectionTab : UIViewController <MNColorSelectionViewControllerDelegate, OthersSelectionDelegate>

@property(strong) UIColor *textColor;
@property(strong) UIColor *fillColor;
@property(strong) UIColor *strokeColor;
@property(assign) float strokeWidth;
@property(assign) StrokeStyle strokeStyle;
@property(assign) float fillOpacity;
@property(assign) BOOL canSetDrawingFill;
@property(assign) AnnotationGraphicType graphicType;
@property (assign) UITextAlignment textAlignment;

@property (strong) UIViewController *currentViewController;
@property (strong) NSArray *viewControllers;
@property (assign) NSInteger selectionIndex;
@property (assign) id<ColorSelectionTabDelegate> delegate;

- (void) selectIndex: (int) index;

@end
