//
//  OthersSelectionViewController.h
//  TSCharts
//
//  Created by Quoc Le on 8/5/12.
//
//

#import <UIKit/UIKit.h>
#import "AnnotationGraphicsView.h"
#import "MNColorSelectionViewController.h"

@protocol OthersSelectionDelegate <NSObject>
- (void) othersSelectionStrokeSizeChanged: (int) size;
- (void) othersSelectionStrokeStyleChanged: (StrokeStyle) strokeStyle;
- (void) othersSelectionFillOpacity: (float) opacity;
- (void) othersSelectionTextAlignmentChanged: (UITextAlignment) alignment;
- (void) othersSelectionTextColorChanged: (UIColor*) color;
@end

@interface OthersSelectionViewController : UITableViewController <MNColorSelectionViewControllerDelegate>

@property(assign) UINavigationController *myNavController;
@property(strong) NSMutableArray *sections;
@property(strong) UIColor *textColor;
@property(assign) float strokeWidth;
@property(assign) StrokeStyle strokeStyle;
@property(assign) float fillOpacity;
@property(assign) BOOL canSetDrawingFill;
@property(assign) AnnotationGraphicType graphicType;
@property (assign) UITextAlignment textAlignment;
//@property (assign) BOOL canDrawingRoundedCorner;

@property(assign) id<OthersSelectionDelegate> delegate;

@end
