//
//  OthersSelectionViewController.m
//  TSCharts
//
//  Created by Quoc Le on 8/5/12.
//
//

#import "OthersSelectionViewController.h"
#import "MNPageViewController.h"
#import "MNColorTableViewCell.h"
#import "MNColorView.h"
#import "ChartUtils.h"

@interface OthersSelectionViewController ()

@end

@implementation OthersSelectionViewController

- (id)init
{
    self = [super initWithStyle: UITableViewStyleGrouped];
    if (self)
    {
        self.sections = [NSMutableArray array];
    }
    return self;
}

- (void) setup
{
    if(self.graphicType == ANNOTATION_TEXT)
        [self.sections addObject: @"Text Alignment"];
    
    if(self.graphicType == ANNOTATION_TEXT )
    {
        [self.sections addObject: @"Color"];
    }

    [self.sections addObject: @"Border Size"];
    [self.sections addObject: @"Border Style"];
        
    if(self.canSetDrawingFill)
        [self.sections addObject: @"Fill Opacity"];
    
//    if(self.canDrawingRoundedCorner)
//        [self.sections addObject: @"Rounded Corner"];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setup];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (UITableViewCell *)colorCellForRowIndex:(NSUInteger)rowIndex
{
    MNColorTableViewCell *cell = [MNColorTableViewCell cellForTableView:self.tableView];
    cell.textLabel.text = NSLocalizedStringFromTable(@"Color", @"inspector", @"color cell text field");
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.colorView.color = self.textColor;
    cell.textLabel.text = @"Text Color";
    
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[self.sections objectAtIndex: section] isEqualToString:@"Color"] ? @"" : [self.sections objectAtIndex: section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionName = [self.sections objectAtIndex: section];

    if([sectionName isEqualToString:@"Border Style"])
        return 4;

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionName = [self.sections objectAtIndex: indexPath.section];

    if([sectionName isEqualToString:@"Color"])
    {
        return [self colorCellForRowIndex: 0];
    }
    
    NSString *CellIdentifier = [NSString stringWithFormat: @"SECTION_%d", indexPath.section];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellIdentifier] autorelease];
                
        if([sectionName isEqualToString:@"Border Size"])
        {
            NSArray *items = [NSArray arrayWithObjects:@"Thin", @"Medium", @"Thick", nil];
            UISegmentedControl *control = [[[UISegmentedControl alloc] initWithItems:items] autorelease];
            [control addTarget: self action: @selector(strokeSizeChanged:) forControlEvents: UIControlEventValueChanged];
            control.frame = CGRectMake(0,0,300,44);
            [cell.contentView addSubview: control];
            
            if(self.strokeWidth == 1)
                control.selectedSegmentIndex = 0;
            if(self.strokeWidth == 2)
                control.selectedSegmentIndex = 1;
            if(self.strokeWidth == 4)
                control.selectedSegmentIndex = 2;        
        }
        
        if([sectionName isEqualToString:@"Fill Opacity"])
        {
            UISlider *slider = [[[UISlider alloc]  init] autorelease];
            [slider addTarget: self action: @selector(sliderValueChanged:) forControlEvents: UIControlEventValueChanged];
            slider.frame = CGRectMake(10,0,240,44);
            slider.value = self.fillOpacity;
            
            UILabel *label = [[[UILabel alloc] initWithFrame: CGRectMake(250,0,50,44)] autorelease];
            label.backgroundColor = [UIColor clearColor];
            label.tag = 100;
            label.text = [NSString stringWithFormat: @"%d%%", int(slider.value*100)];
            [cell.contentView addSubview: slider];
            [cell.contentView addSubview: label];
        }
        
        if([sectionName isEqualToString:@"Text Alignment"])
        {
            NSArray *items = [NSArray arrayWithObjects:@"Left", @"Center", @"Right", nil];
            UISegmentedControl *control = [[[UISegmentedControl alloc] initWithItems:items] autorelease];
            [control addTarget: self action: @selector(textAlignmentChanged:) forControlEvents: UIControlEventValueChanged];
            control.frame = CGRectMake(0,0,300,44);
            [cell.contentView addSubview: control];
            
            if(self.textAlignment == UITextAlignmentLeft)
                control.selectedSegmentIndex = 0;
            if(self.textAlignment == UITextAlignmentCenter)
                control.selectedSegmentIndex = 1;
            if(self.textAlignment == UITextAlignmentRight)
                control.selectedSegmentIndex = 2;                
        }
        
        if([sectionName isEqualToString:@"Border Style"])
        {
            if(indexPath.row == (int)self.strokeStyle)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            
            NSArray *images = [NSArray arrayWithObjects:@"strokestyle_line.png", @"strokestyle_dots.png", @"strokestyle_shortdash.png", @"strokestyle_longdash.png", nil];
            UIImageView *img = [[[UIImageView alloc] initWithImage: [UIImage imageNamed: [images objectAtIndex: indexPath.row]]] autorelease];
            [cell.contentView addSubview: img];
        }
    }
    //cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionName = [self.sections objectAtIndex: indexPath.section];

    if([sectionName isEqualToString:@"Border Style"])
    {
        for(int i=0; i<3; i++)
        {
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow: i inSection:1]];
            if(i==indexPath.row)
            {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        [self.delegate othersSelectionStrokeStyleChanged: (StrokeStyle) indexPath.row];
    }
    
    else if([sectionName isEqualToString:@"Color"])
    {
        //NSString *viewControllerTitle = @"Color";
        //NSMutableArray *viewControllers = [NSMutableArray arrayWithCapacity:2];
        MNColorSelectionViewController *rainbowViewController = [MNColorSelectionViewController rainbowSelectionViewController];
        //rainbowViewController.title = viewControllerTitle;
        //if (rainbowViewController) [viewControllers addObject:rainbowViewController];
        rainbowViewController.contentSizeForViewInPopover = self.contentSizeForViewInPopover;
        rainbowViewController.delegate = self;
        rainbowViewController.title = @"Text Color";
        [rainbowViewController loadView];
        rainbowViewController.view.frame = self.view.bounds;
        rainbowViewController.view.backgroundColor = [UIColor colorWithRGBHex:0xd1d1d1];
        [self.myNavController pushViewController: rainbowViewController animated: YES];
        
    }
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

#pragma mark - MNColorSelectionViewController Delegate

- (UIColor *)colorSelectionControllerSelectedColor:(MNColorSelectionViewController *)controller
{
    int section = [self.sections indexOfObject:@"Color"];
    MNColorTableViewCell *cell = (MNColorTableViewCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section]];
    return cell.colorView.color;
}
- (void)colorSelectionController:(MNColorSelectionViewController *)controller didSelectedColor:(UIColor *)color
{
    int section = [self.sections indexOfObject:@"Color"];

    MNColorTableViewCell *cell = (MNColorTableViewCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow: 0 inSection:section]];
    cell.colorView.color = color;
    
    [self.delegate othersSelectionTextColorChanged: color];
}


- (void) strokeSizeChanged: (UISegmentedControl*) sender
{
    int strokeSize = 0;
    if(sender.selectedSegmentIndex == 0)
        strokeSize = 1;
    if(sender.selectedSegmentIndex == 1)
        strokeSize = 2;
    if(sender.selectedSegmentIndex == 2)
        strokeSize = 4;
    
    [self.delegate othersSelectionStrokeSizeChanged: strokeSize];
}

- (void) sliderValueChanged: (UISlider*) sender
{
    float val = [sender value];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow: 0 inSection:2]];
    UILabel *label = (UILabel*)[cell.contentView viewWithTag: 100];
    label.text = [NSString stringWithFormat: @"%d%%", int(val*100)];
    
    [self.delegate othersSelectionFillOpacity: val];
}

- (void) textAlignmentChanged: (UISegmentedControl*) sender
{
    UITextAlignment alignment;
    if(sender.selectedSegmentIndex == 0)
        alignment = UITextAlignmentLeft;
    if(sender.selectedSegmentIndex == 1)
        alignment = UITextAlignmentCenter;
    if(sender.selectedSegmentIndex == 2)
        alignment = UITextAlignmentRight;

    [self.delegate othersSelectionTextAlignmentChanged: alignment];
}
@end
