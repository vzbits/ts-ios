//
//  StockListPhone.h
//  TSCharts
//
//  Created by Quoc Le on 6/29/13.
//
//

#import <UIKit/UIKit.h>
#import "ServiceManager.h"
#import "SymbolSearchViewController.h"
#import "UserList.h"
#import "UserListItem.h"

@protocol StockListPhoneDelegate <NSObject>
- (void) stockListUpdateList: (TableData*) symbols;
- (void) stockListSymbolClicked: (NSString*) symbol;
@end

@interface StockListPhone : UIView <UITableViewDataSource, UITableViewDelegate, SymbolSearchViewDelegate>
@property(strong) UITableView *tableView;
@property(assign) ListViewType viewType;
@property(assign) int toggleType;
@property(assign) id<StockListPhoneDelegate> delegate;
@property (nonatomic, strong) TableData *rows;
@property (strong, nonatomic) UserList *userList;
@property (strong) NSManagedObjectID *lastSelectionIndex;

- (void) setData: (UserList*) userList;
- (void) setSelection: (NSIndexPath*) indexPath;

- (void) displaySymbolSearch;
- (void) loadData;
- (NSDictionary*) dataForSymbol: (NSString*) symbol;
@end
