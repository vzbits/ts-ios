//
//  ApplicationSettings.m
//  TSCharts
//
//  Created by Quoc Le on 8/1/12.
//
//

#import "Settings.h"

#define KEY_RECENTSEARCH @"RECENT_SEARCHES"
#define KEY_LAST_LIST @"LAST_LIST_SELECTED"
#define KEY_DEVICE_TOKEN @"DEVICE_TOKEN"
#define KEY_TOKEN_STATUS @"TOKEN_STATUS"

@implementation Settings
implementSingleton(Settings);

- (NSArray*) loadIndicators
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *savedChannels = [userDefaults objectForKey: @"myIndicators"];
    NSArray *indicators;
    
    if( savedChannels == nil )
        indicators = [NSArray array];
    else
        indicators =  [NSKeyedUnarchiver unarchiveObjectWithData:savedChannels];

    
    //NSLog(@"Loading %@", [indicators description] );
        
    return indicators;
}

- (void) saveIndicators: (NSArray*) indicators
{
    //NSLog(@"Saving %@", [indicators description] );
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject: [NSKeyedArchiver archivedDataWithRootObject: indicators] forKey:  @"myIndicators"];
    [userDefaults synchronize];
}

- (NSInteger) addSegmentIndex
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults integerForKey:@"ADD_SEGMENT_INDEX"];
}
- (void) setSegmentIndex: (NSInteger) index
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setInteger: index forKey:@"ADD_SEGMENT_INDEX"];
}

- (void) resetSettings
{
    [self saveIndicators: [NSArray array]];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject: nil forKey:@"PriceScale"];
    [userDefaults setObject: nil forKey:@"VolumeScale"];
    [userDefaults setObject: nil forKey:KEY_RECENTSEARCH];
    [userDefaults synchronize];
}

- (SymbolSearchResultData*) recentSearches
{
    NSData *recentSearches = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_RECENTSEARCH];
    if(recentSearches)
    {
        SymbolSearchResultData *stored = (SymbolSearchResultData*)[NSKeyedUnarchiver unarchiveObjectWithData:recentSearches];
        return stored;
    }
    else
    {
        NSMutableArray *symbols = [NSMutableArray array];
        SymbolSearchResultData *results = [[[SymbolSearchResultData alloc] initWithResults:symbols searchString:@""] autorelease];
        return results;
    }
}

- (void) addRecentSearch: (SymbolData*) symbol
{
    NSData *recentSearches = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_RECENTSEARCH];
    SymbolSearchResultData *builder;
    NSMutableArray *symbols = [NSMutableArray array];

    if(recentSearches)
    {
        builder = (SymbolSearchResultData*)[NSKeyedUnarchiver unarchiveObjectWithData:recentSearches];
    }
    else
    {
        builder = [[[SymbolSearchResultData alloc] initWithResults:symbols] autorelease];
    }
    
    [symbols addObject: symbol];

    for(int i=0; i< builder.results.count; i++)
    {
        SymbolData *d = [builder.results objectAtIndex: i];
        if(![d.symbol isEqualToString: symbol.symbol])
            [symbols addObject: [builder.results objectAtIndex:i]];
    }
    [builder setResults: symbols];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:builder];
    
    [[NSUserDefaults standardUserDefaults] setObject: data forKey: KEY_RECENTSEARCH];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void) setListSelected: (NSIndexPath*) indexPath
{
	NSData *data = [NSKeyedArchiver archivedDataWithRootObject: indexPath];
	[[NSUserDefaults standardUserDefaults] setObject: data forKey: KEY_LAST_LIST];
    [[NSUserDefaults standardUserDefaults] synchronize];	
}

- (NSIndexPath*) listSelected
{
	id data = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_LAST_LIST];
	if(data == nil)
		return nil;
	
	return [NSKeyedUnarchiver unarchiveObjectWithData: data];
}

- (NSString*) deviceToken
{
	return [[NSUserDefaults standardUserDefaults] stringForKey: KEY_DEVICE_TOKEN];
}
- (void) setDeviceToken: (NSString*) deviceToken
{
	[[NSUserDefaults standardUserDefaults] setObject: deviceToken forKey: KEY_DEVICE_TOKEN];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSInteger) tokenStatus
{
	return [[NSUserDefaults standardUserDefaults] integerForKey: KEY_TOKEN_STATUS];
}
- (void) setTokenStatus: (NSInteger) tokenStatus
{
	[[NSUserDefaults standardUserDefaults] setInteger: tokenStatus forKey: KEY_TOKEN_STATUS];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

@end
