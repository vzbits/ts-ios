//
//  NewsViewController.h
//  TSCharts
//
//  Created by Quoc Le on 8/12/12.
//
//

#import <UIKit/UIKit.h>
#import "NewsCell.h"
#import "ServiceManager.h"

@protocol NewsViewDelegate <NSObject>
- (void) newsViewDidSelect: (NewsData*) news;
@end

@interface NewsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong) NSArray *data;
@property (strong) UIActivityIndicatorView *spinner;
@property (assign) id<NewsViewDelegate> delegate;
@property (strong) UITableView *tableView;

- (void) loadData: (NSString*) symbol;

@end
