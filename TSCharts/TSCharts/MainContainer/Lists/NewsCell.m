//
//  NewsCell.m
//  TSCharts
//
//  Created by Quoc Le on 8/12/12.
//
//

#import "NewsCell.h"
#import "ColorScheme.h"

#define MARGIN (IS_IPHONE ? 20 : 5)
#define HIDE_DETAIL YES

@implementation NewsCell

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle: UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.backgroundColor =  [UIColor clearColor]; //[UIColor colorWithRGBHex:0xf8f8f8];
        self.selectionStyle = UITableViewCellSelectionStyleGray;
        //UILabel;
        self.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size: 12];
        //self.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        self.textLabel.adjustsFontSizeToFitWidth = YES;
        self.textLabel.minimumFontSize = 11;
        self.textLabel.numberOfLines = 1;
        self.detailTextLabel.font = [UIFont fontWithName:@"TimesNewRomanPSMT" size: 12];
        self.detailTextLabel.textColor = [UIColor blackColor];
        self.detailTextLabel.backgroundColor = [UIColor clearColor];
        self.detailTextLabel.numberOfLines = 1;
        
		if(HIDE_DETAIL)
		{
			self.detailTextLabel.hidden = YES;
		}
		
        self.timeAgo = [[[UILabel alloc] init] autorelease];
        self.timeAgo.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size: 10];
        self.timeAgo.textColor = [UIColor darkGrayColor];
        [self addSubview: self.timeAgo];

		self.source = [[[UILabel alloc] init] autorelease];
        self.source.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size: 10];
        self.source.textColor = [UIColor darkGrayColor];
        [self addSubview: self.source];
		
		if(IS_IPHONE)
		{
			self.textLabel.textColor = CELL_NEWS_TEXTCOLOR;
			self.detailTextLabel.textColor = CELL_NEWS_SUBTEXTCOLOR;
			self.timeAgo.textColor = CELL_NEWS_SUBTEXTCOLOR;
			self.source.textColor = CELL_NEWS_SUBTEXTCOLOR;
		}
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    //CGSize titleSize = [self.textLabel.text sizeWithFont: self.textLabel.font constrainedToSize: CGSizeMake(self.width - (2*MARGIN) , 30)];
    CGSize detailSize = [self.detailTextLabel.text sizeWithFont: self.detailTextLabel.font constrainedToSize: CGSizeMake(self.width - (2*MARGIN) , 30)];
    
	CGSize titleSize = CGSizeMake(0,20);
	
    self.textLabel.frame = CGRectMake(MARGIN, 0, self.width - (2*MARGIN) , titleSize.height);
    //[self.textLabel sizeThatFits: CGSizeMake(self.width - (2*MARGIN) , 30) ];
    
    if([self.detailTextLabel.text isEqualToString:@""])
        detailSize.height = 0;
    self.detailTextLabel.frame = CGRectMake(MARGIN, self.textLabel.bottom + 3, detailSize.width, detailSize.height);

	if(HIDE_DETAIL)
	{
		self.source.frame = CGRectMake(self.detailTextLabel.x, self.textLabel.bottom+0, 100,12);
		self.timeAgo.frame = CGRectMake(self.detailTextLabel.x + 125, self.textLabel.bottom+0, 100,12);
	}
	else
	{
		self.source.frame = CGRectMake(self.detailTextLabel.x, self.detailTextLabel.bottom+MARGIN, 100,12);
		self.timeAgo.frame = CGRectMake(self.detailTextLabel.x + 125, self.detailTextLabel.bottom+MARGIN, 100,12);		
	}
}

+ (float) getHeightFor:(NSString*) title detailText: (NSString*) detail
{
    static NewsCell *cell = nil;
    if(cell == nil)
    {
        cell = [[NewsCell alloc] initWithReuseIdentifier: @"TEST"];
    }
    float totalHeight = 0;
    float WIDTH = 180;
//    CGSize titleSize = [title sizeWithFont: cell.textLabel.font constrainedToSize: CGSizeMake(WIDTH - (2*MARGIN) , 30)];
    CGSize detailSize = [detail sizeWithFont: cell.detailTextLabel.font constrainedToSize: CGSizeMake(WIDTH - (2*MARGIN) , 30)];
    
    totalHeight += 5;
    totalHeight += 20;
    
    if([detail isEqualToString:@""])
        detailSize.height = 0;

	if(!HIDE_DETAIL)
	{
		totalHeight += 3;
		totalHeight += detailSize.height;
	}
	
    totalHeight += 15;
    
    return totalHeight;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) drawRect:(CGRect)rect
{
    
}
@end
