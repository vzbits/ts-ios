//
//  ListMiniView.m
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListMiniView.h"
#import "Theme.h"

@implementation ListMiniView

@synthesize navBar = _navBar;

- (id)init
{
    self = [super init];
    if (self) {
        self.navBar = [[[UIToolbar alloc] init] autorelease];
        self.titleLabel = [[[UILabel alloc] init] autorelease];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.text = @"News";
        self.titleLabel.autoresizingMask = UIViewAutoresizingAll;
        self.titleLabel.textAlignment = UITextAlignmentCenter;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.shadowOffset = CGSizeMake(0,-1);
        self.titleLabel.shadowColor = [UIColor blackColor];
        
        UIBarButtonItem *flex = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace
                                                                               target: nil action: nil] autorelease];
        UIBarButtonItem *titleButton = [[[UIBarButtonItem alloc] initWithCustomView: self.titleLabel] autorelease];

        NSArray *rightItems = [NSArray arrayWithObjects: flex, titleButton, flex, nil];
        [self.navBar setItems: rightItems];

        self.backgroundColor = [UIColor lightGrayColor];
        
        self.tabBarController = [[[UITabBarController alloc] init] autorelease];
        self.newsController = [[[NewsViewController alloc] init] autorelease];
        UIViewController *test2 = [[[UIViewController alloc] init] autorelease];
        UIViewController *test3 = [[[UIViewController alloc] init] autorelease];
        test2.view.backgroundColor = [UIColor lightGrayColor];
        self.newsController.tabBarItem.image = [UIImage imageNamed:@"305"];
        test2.tabBarItem.image = [UIImage imageNamed:@"32"];
//        test3.tabBarItem.image = [UIImage imageNamed:@""];
        test2.title = @"News";

        self.tabBarController.viewControllers = [NSArray arrayWithObjects: self.newsController, test2, nil];
        self.tabBarController.delegate = self;
        self.tabBarController.wantsFullScreenLayout = NO;
        [self addSubview: self.tabBarController.view];
        [self addSubview: self.navBar];
        [Theme stylizeToolbar: self.navBar];
        
        self.tabBarController.tabBar.tintColor = [UIColor colorWithRGBHex:0x304B6A];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.navBar.frame = CGRectMake(0,0,self.width,44);
    self.tabBarController.view.frame = CGRectMake(0,44,self.width, self.height-44);

//    [self.tabBarController.view setNeedsLayout];
//    [self.navBar setNeedsLayout];
//    [self.navBar setNeedsDisplay];
//    [self.tabBarController viewDidAppear: YES];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    NSLog(@"Title = %@", viewController.title);
    self.titleLabel.text = viewController.title;
}

- (void) updateSymbol: (NSString*) symbol
{
    [self.tabBarController.view setNeedsLayout];
    [self.tabBarController.view setNeedsDisplay];
    self.newsController.view.frame = CGRectMake(0,0, self.newsController.view.width, self.newsController.view.height);
    [self.newsController loadData: symbol];
}
@end
