//
//  ListMiniView.h
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsViewController.h"

@interface ListMiniView : UIView <UITabBarControllerDelegate>

@property (strong, nonatomic) UIToolbar *navBar;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UITabBarController *tabBarController;
@property (strong, nonatomic) NewsViewController *newsController;
- (void) updateSymbol: (NSString*) symbol;
@end
