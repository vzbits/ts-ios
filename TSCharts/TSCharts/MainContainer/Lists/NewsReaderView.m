//
//  NewsReaderView.m
//  TSCharts
//
//  Created by Quoc Le on 8/12/12.
//
//

#import "NewsReaderView.h"
#import "ServiceManager.h"
#import "NSString+Extras.h"

@implementation NewsReaderView

- (id)init
{
    self = [super init];
    if (self) {
        self.navBar = [[[UINavigationBar alloc] init] autorelease];
		[self.navBar stylize];
        [self addSubview: self.navBar];
        
        UIBarButtonItem *flex = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace
                                                                               target: nil action: nil] autorelease];

		UIBarButtonItem *fixed = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFixedSpace
                                                                               target: nil action: nil] autorelease];
		fixed.width = 20;
		UIButton *btn = [UIButton buttonWithType: UIButtonTypeCustom];
		[btn setImage:[UIImage imageNamed:@"x.png"] forState: UIControlStateNormal];
		[btn addTarget: self action: @selector(closeClicked:) forControlEvents: UIControlEventTouchUpInside];

		UIButton *btn1 = [UIButton buttonWithType: UIButtonTypeCustom];
		[btn1 setImage:[UIImage imageNamed:@"arrow-3.png"] forState: UIControlStateNormal];
		[btn1 addTarget: self action: @selector(backClicked:) forControlEvents: UIControlEventTouchUpInside];

		UIButton *btn2 = [UIButton buttonWithType: UIButtonTypeCustom];
		[btn2 setImage:[UIImage imageNamed:@"arrow-1.png"] forState: UIControlStateNormal];
		[btn2 addTarget: self action: @selector(forwardClicked:) forControlEvents: UIControlEventTouchUpInside];
		btn.frame = CGRectMake(0,0,44,44);
		btn1.frame = CGRectMake(0,0,44,44);
		btn2.frame = CGRectMake(0,0,44,44);
		
		self.backBtn = btn1;
		self.forwardBtn = btn2;
		self.closeBtn = btn;
		
		[self.navBar addSubview: self.backBtn];
		[self.navBar addSubview: self.forwardBtn];
		[self.navBar addSubview: self.closeBtn];
		
//        UIBarButtonItem *close = [[[UIBarButtonItem alloc]  initWithCustomView: btn] autorelease];
//        UIBarButtonItem *back = [[[UIBarButtonItem alloc] initWithCustomView: btn1] autorelease];
//        UIBarButtonItem *forward = [[[UIBarButtonItem alloc] initWithCustomView: btn2] autorelease];
//
//        NSArray *rightItems = [NSArray arrayWithObjects: back,forward, flex, close, nil];
//        [self.navBar setItems: rightItems];
		
//        back.enabled = NO;
//        forward.enabled = NO;
		
        //[Theme stylizeNewsToolbar: self.navBar];
//		UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"white-1px.png"]];
//		[self.navBar addSubview: imgView];
		
		//[self.navBar setBackgroundImage:[UIImage imageNamed:@"white-1px.png"] forToolbarPosition: UIBarPositionAny barMetrics: UIBarMetricsDefault];
		[self.navBar setTintColor: [UIColor whiteColor]];
		
        self.webView = [[[UIWebView alloc] init] autorelease];
        self.webView.scalesPageToFit = YES;
        self.webView.delegate = self;
        [self addSubview: self.webView];

//		self.borderView = [[UIView alloc] init];
//		self.borderView.backgroundColor = [UIColor blackColor];
//		[self addSubview: self.borderView];
    }
    return self;
}

- (void) dealloc
{
    self.webView = nil;
    self.navBar = nil;
    
    [super dealloc];
}

- (void) backClicked: (id) sender
{
    [self.webView goBack];
}

- (void) forwardClicked: (id) sender
{
    [self.webView goForward];
}

- (void) closeClicked: (id) sender
{
    [self.delegate newsReaderViewCloseClicked: self];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    if(self.webView.canGoBack)
        [[self.navBar.items objectAtIndex: 0] setEnabled: YES];
    else
        [[self.navBar.items objectAtIndex: 0] setEnabled: NO];
    if(self.webView.canGoForward)
        [[self.navBar.items objectAtIndex: 1] setEnabled: YES];
    else
        [[self.navBar.items objectAtIndex: 1] setEnabled: NO];

//	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"runme" ofType:@"js"];
//	NSString *js = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error: nil];
//	NSString *result = [self.webView stringByEvaluatingJavaScriptFromString: js];
//	NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.zoom = 1.0;"];
//	[self.webView stringByEvaluatingJavaScriptFromString:jsCommand];
//	
//	NSLog(@"Result = %@", result);
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.navBar.frame = CGRectMake(0,0,self.width,44);
    [self.navBar setNeedsLayout];
	self.borderView.frame = CGRectMake(10,44, self.width - 20, 1);
    self.webView.frame = CGRectMake(0,44,self.width, self.height-44);
	
	self.backBtn.frame = CGRectMake(0,0,44,44);
	self.forwardBtn.frame = CGRectMake(self.backBtn.right + 25,0,44,44);
	self.closeBtn.frame = CGRectMake(self.width - 44,0,44,44);
}

- (void) loadURL: (NSString*) url
{
	BOOL readabilityEnabled = NO;
	
    NSLog(@"NewsReader load uRL = %@", url);
    NSURL *newURL;
	NSString *urlEnc = [url urlEncodeUsingEncoding:NSUTF8StringEncoding];
	if(readabilityEnabled)
	{
		NSString *read_host = @"http://www.topstock.us/readability/query.php";
		newURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@?url=%@",read_host, urlEnc]];
		NSLog(@"calling url %@", [newURL absoluteString]);
	}
	else
	{
		newURL = [NSURL URLWithString: url];
	}
//	//self.webView.suppressesIncrementalRendering = YES;
    [self.webView loadRequest: [NSURLRequest requestWithURL: newURL]];
	
//	GGReadability * read = [[GGReadability alloc] initWithURL: [NSURL URLWithString:@"http://uk.finance.yahoo.com/news/china-nestle-danone-play-book-073245592.html"] completionHandler:^(NSString * string){
//	NSLog(@"GGReadability = %@", string);
//	}
//	errorHandler:^(NSError * error){
//		NSLog(@"GGReadability error");
//	}];
//	
//	[read render];
	
//	NSArray *strs = [url componentsSeparatedByString:@"*"];
	
//	[[ServiceManager instance] getNewsArticle: url withBlock:^(id data) {
//		NSLog(@"Result back");
//		if([data isKindOfClass: [NSDictionary class]])
//		{
//			NSLog(@"%@", data[@"text"]);
//			NSLog(@"%@", data[@"imageUrl"]);
//			
//			NSString *html = [NSString stringWithFormat:@"<div style=\"width:320px\">%@</div>", data[@"text"]];
//			[self.webView loadHTMLString:html baseURL: newURL];
//		}
//	} withErrorBlock:^(NSString *error) {
//	}];
}
@end
