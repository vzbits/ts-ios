//
//  NewsReaderView.h
//  TSCharts
//
//  Created by Quoc Le on 8/12/12.
//
//

#import <UIKit/UIKit.h>

@protocol NewsReaderViewDelegate <NSObject>
- (void) newsReaderViewCloseClicked: (id) sender;
@end

@interface NewsReaderView : UIView <UIWebViewDelegate>
@property (strong, nonatomic) UINavigationBar *navBar;
@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UIView *borderView;
@property (strong, nonatomic) UIButton *backBtn;
@property (strong, nonatomic) UIButton *forwardBtn;
@property (strong, nonatomic) UIButton *closeBtn;
@property (assign, nonatomic) id<NewsReaderViewDelegate> delegate;

- (void) loadURL: (NSString*) url;
@end
