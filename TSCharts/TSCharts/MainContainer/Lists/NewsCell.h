//
//  NewsCell.h
//  TSCharts
//
//  Created by Quoc Le on 8/12/12.
//
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell

@property(strong) UILabel *timeAgo;
@property(strong) UILabel *source;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;
+ (float) getHeightFor:(NSString*) title detailText: (NSString*) detail;

@end
