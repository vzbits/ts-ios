//
//  NewsViewController.m
//  TSCharts
//
//  Created by Quoc Le on 8/12/12.
//
//

#import "NewsViewController.h"
#import "UIDateExtensions.h"

@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)init
{
    self = [super init];
    if (self) {
        self.title = @"News";
		
    }
    return self;
}

- (void) viewDidLoad
{
	self.tableView = [[UITableView alloc] initWithFrame: self.view.bounds style: UITableViewStylePlain];
	self.tableView.autoresizingMask = UIViewAutoresizingAll;
	self.tableView.delegate = self;
	self.tableView.dataSource = self;
	[self.view addSubview: self.tableView];

	if(!IS_IPHONE)
		self.tableView.separatorColor = [UIColor lightGrayColor];
	else
		self.tableView.separatorColor = [UIColor clearColor];

	self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhite];
	self.spinner.hidesWhenStopped = YES;
	[self.view addSubview: self.spinner];
}

- (void) viewWillLayoutSubviews
{
	[super viewWillLayoutSubviews];
	[self.view centerSubview: self.spinner];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) loadData: (NSString*) symbol
{
	self.tableView.alpha = 0.0;
	[self.spinner startAnimating];
    [[ServiceManager instance] getNewsData: symbol
                                         withBlock:^(NSArray *data)
    {
		[self.spinner stopAnimating];
		
        self.data = data;
        NSLog(@"Load news %@ results=%d", symbol, self.data.count);
        [self.tableView reloadData];
		
		[UIView animateWithDuration:0.25 animations:^{
			self.tableView.alpha = 1.0;
		}];
    }
                                    withErrorBlock:^(NSString *error) {
                                        NSLog(@"NEWS ERROR!");
                                    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NewsData *news = [self.data objectAtIndex: indexPath.row];
    return [NewsCell getHeightFor: news.title detailText: news.summary];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell_NEWS";
    NewsCell *cell = (NewsCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[NewsCell alloc] initWithReuseIdentifier: CellIdentifier] autorelease];
    }
    
    NewsData *news = [self.data objectAtIndex: indexPath.row];
    cell.textLabel.text = news.title;
	cell.source.text = news.source;
    cell.detailTextLabel.text = [news.summary stringByReplacingOccurrencesOfString:@"[at " withString:@"["];
    cell.timeAgo.text = [[NSDate dateWithTimeIntervalSince1970: news.publish_date/1000] dateFromNowString];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    [self.delegate newsViewDidSelect: [self.data objectAtIndex: indexPath.row]];
}

@end
