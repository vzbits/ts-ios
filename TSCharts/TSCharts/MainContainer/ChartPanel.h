//
//  ChartPanel.h
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chart.h"
#import "ChartHeader.h"
#import "StockPrice.h"
#import "ServiceManager.h"
#import "IndicatorJS.h"
#import "CrossChairView.h"
#import "AnnotationGraphicsView.h"
#import "AlertVisualEditView.h"

@protocol ChartPanelDelegate <NSObject>
- (void) chartPanelAnnotationSelected: (AnnotationGraphics*) selected;
- (void) chartPanelPeriodictyChanged: (PriceType) periodicity symbol: (NSString*) symbol;
@end

@interface ChartPanel : UIView <CrossChairDelegate, AnnotationGraphicViewDelegate>
{
    IndicatorJS *indicatorJS;
}

@property (strong, nonatomic) Chart *chart;
@property (strong, nonatomic) UIView *chartHeader;
@property (strong, nonatomic) UILabel *symbolLabel;
@property (strong, nonatomic) UILabel *closeValue;
@property (strong, nonatomic) CrossChairView *crosschair;
@property (strong, nonatomic) AlertVisualEditView *alertEditView;
@property (strong, nonatomic) AnnotationGraphicsView *annotation;
@property (assign) id<ChartPanelDelegate> delegate;
@property (strong) StockPriceData *lastData;
@property (strong, nonatomic) UIButton *dailyBtn;
@property (strong, nonatomic) UIButton *weeklyBtn;
@property (strong, nonatomic) UIButton *monthlyBtn;
@property (strong, nonatomic) UISegmentedControl *periodicityToggle;
@property (strong, nonatomic) UIButton *newsToggleBtn;
@property (strong, nonatomic) UIButton *bellBtn;
@property (strong, nonatomic) UILabel *timestampLabel;

declareSingleton(ChartPanel);

- (void) setEmptyChart: (NSString*) name symbol: (NSString*) symbol;
- (void) setData: (StockPriceData *) sp;
- (void) setLastTick: (PriceData*) p;

- (void) reloadChart;
- (void) reloadChart: (BOOL) runIndicator;
- (void) reloadIndicators;
- (void) addIndicator: (IndicatorCodeData*)item;
- (UIImage*) generateImage;
@end
