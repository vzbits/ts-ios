//
//  ToolsMenuViewController.h
//  TSCharts
//
//  Created by Quoc Le on 7/31/12.
//
//

#import <UIKit/UIKit.h>
#import "ManageIndicatorsTableController.h"
#import "GenericMenuSelectionViewController.h"
#import "BAMSettings.h"
#import "GenerateURLViewController.h"

@protocol ToolsMenuDelegate <NSObject>
- (void) toolsMenuIndicatorRemoved: (id) sender;
- (void) toolsMenuShareClicked:(id) sender menuItem: (NSString*) menuItem;
- (void) toolsChartSettingsValueChangedForKey: (NSString*) str;
- (void) toolsChartSettingsDidActionForKey: (NSString*) str;
@end

@interface ToolsMenuViewController : UITableViewController <ManageIndicatorsDelegate, GenericMenuSelectionDelegate, BAMSettingsDelegate>
@property(strong) NSMutableArray *data;
@property(strong) NSMutableArray *icons;
@property(assign) id<ToolsMenuDelegate> delegate;

@end
