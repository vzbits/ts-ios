//
//  GenerateURLViewController.h
//  TSCharts
//
//  Created by Quoc Le on 8/8/12.
//
//

#import <UIKit/UIKit.h>
#import "GenerateURLView.h"

@interface GenerateURLViewController : UIViewController
@property (strong) GenerateURLView *generateView;
@end
