//
//  GenerateURLViewController.m
//  TSCharts
//
//  Created by Quoc Le on 8/8/12.
//
//

#import "GenerateURLViewController.h"

@interface GenerateURLViewController ()

@end

@implementation GenerateURLViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        self.title = @"Copy URL";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.generateView = [[[GenerateURLView alloc] init] autorelease];
    self.generateView.frame = self.view.bounds;
    self.generateView.autoresizingMask = UIViewAutoresizingAll;
    [self.view addSubview: self.generateView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.generateView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
