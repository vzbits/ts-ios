//
//  ManageIndicatorsTableController.m
//  TSCharts
//
//  Created by Quoc Le on 8/1/12.
//
//

#import "ManageIndicatorsTableController.h"
#import "IndicatorJS.h"

@interface ManageIndicatorsTableController ()

@end

@implementation ManageIndicatorsTableController

@synthesize indicators = _indicators;
@synthesize deletingItems = _deletingItems;
@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"Manage Indicators";
        self.navigationItem.rightBarButtonItem = self.editButtonItem;
        self.deletingItems = [NSMutableArray array];
        [self loadData];
    }
    return self;
}

- (void) dealloc
{
    self.indicators = nil;
    [super dealloc];
}

#pragma mark - Table view data source

- (void) loadData
{
    IndicatorJS *indicatorJS = IndicatorJS::instance();
    self.indicators = indicatorJS->get_data();
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.indicators.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellIdentifier] autorelease];
    }

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = [[self.indicators objectAtIndex: indexPath.row] name];
    
    return cell;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if(editing == YES)
    {
    } else
    {
        NSLog(@"Reload chart..");
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        IndicatorJS *indicatorJS = IndicatorJS::instance();
        indicatorJS->remove_indicator(indexPath.row);
        
        [self.indicators removeObjectAtIndex: indexPath.row];
        [self.tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath] withRowAnimation: UITableViewRowAnimationAutomatic];
        
        [self.delegate manageIndicatorRemovedItem: self];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


@end
