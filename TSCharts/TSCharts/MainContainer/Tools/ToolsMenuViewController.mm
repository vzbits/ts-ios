//
//  ToolsMenuViewController.m
//  TSCharts
//
//  Created by Quoc Le on 7/31/12.
//
//

#import "ToolsMenuViewController.h"
#import "Settings.h"
#import "ServiceManager.h"
#import "ChartPanel.h"

@interface ToolsMenuViewController ()

@end

@implementation ToolsMenuViewController
@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        self.title = @"Tools";
        self.data = [NSMutableArray arrayWithObjects:@"Chart Settings", @"General Settings", @"Share & Print", @"Help", nil];
        self.icons = [NSMutableArray arrayWithObjects: [UIImage imageNamed:@"114.png"],
                                                    [UIImage imageNamed:@"193.png"],
                                                      [UIImage imageNamed:@"226.png"],
                                                      [UIImage imageNamed:@"191.png"],
                                                      nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) viewDidAppear:(BOOL)animated
{
    self.contentSizeForViewInPopover = CGSizeMake(300,330);
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 0;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellIdentifier] autorelease];
    }
    cell.textLabel.text = [self.data objectAtIndex:indexPath.row];
    cell.imageView.image = [self.icons objectAtIndex: indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

//    if(indexPath.row == 0)
//    {
//        ManageIndicatorsTableController *detailViewController = [[ManageIndicatorsTableController alloc] initWithStyle: UITableViewStylePlain];
//        detailViewController.contentSizeForViewInPopover = CGSizeMake(300,330);
//        detailViewController.delegate = self;
//        [self.navigationController pushViewController:detailViewController animated:YES];
//        [detailViewController release];
//    }
    if(indexPath.row == 0)
    {
        BAMSettings *settings = [[BAMSettings alloc] initWithTitle:@"Chart Settings" propertyListNamed:@"ChartSettings"];
        settings.delegate = self;
        settings.contentSizeForViewInPopover = CGSizeMake(300,330);
        [self.navigationController pushViewController:settings animated:YES];
        [settings release];
    }
    if(indexPath.row == 1)
    {
        BAMSettings *settings = [[BAMSettings alloc] initWithTitle:@"General Settings" propertyListNamed:@"GeneralSettings"];
        settings.delegate = self;
        settings.contentSizeForViewInPopover = CGSizeMake(300,330);
        [self.navigationController pushViewController:settings animated:YES];
        [settings release];
    }
    if(indexPath.row == 2)
    {
        NSArray *menuItems = [NSArray arrayWithObjects: @"Twitter", @"Copy URL", @"Save to Photo Album",@"Email Chart", @"Print Chart", nil];
        NSArray *images    = [NSArray arrayWithObjects: [UIImage imageNamed:@"233.png"],[UIImage imageNamed:@"115.png"],
                                                            [UIImage imageNamed:@"215.png"],
                                                       [UIImage imageNamed:@"41_mail.png"],
                              [UIImage imageNamed:@"100.png"], nil];
                
        NSArray *accessory = [NSArray arrayWithObjects: [NSNumber numberWithInt: UITableViewCellAccessoryNone],
                                                    [NSNumber numberWithInt: UITableViewCellAccessoryDisclosureIndicator],
                                                    [NSNumber numberWithInt: UITableViewCellAccessoryNone],
                                                    [NSNumber numberWithInt: UITableViewCellAccessoryNone],
                                                    [NSNumber numberWithInt: UITableViewCellAccessoryNone], nil];
        
        GenericMenuSelectionViewController *menuController = [[[GenericMenuSelectionViewController alloc] initWithMenuItems: menuItems
                                                                                                                     images: images
                                                                                                                  accessory:accessory] autorelease];
        menuController.title = @"Share & Print";
        menuController.contentSizeForViewInPopover = CGSizeMake(300,330);
        menuController.delegate = self;
        [self.navigationController pushViewController:menuController animated:YES];
    }
}

- (void) manageIndicatorRemovedItem: (id) sender
{
    [self.delegate toolsMenuIndicatorRemoved: sender];
}

- (void) genericMenuSelectionDidClicked: (NSString*) menuItem
{
    if([menuItem isEqualToString:@"Copy URL"])
    {
        GenerateURLViewController *controller = [[[GenerateURLViewController alloc] init] autorelease];
        controller.contentSizeForViewInPopover = CGSizeMake(300,330);
        [self.navigationController pushViewController: controller animated: YES];
     
        UIImage *image = [[ChartPanel instance] generateImage];
        [ServiceManager getImageURLForImage: image withBlock:^(NSString *url)
         {
             NSLog(@"%@", url);
             controller.generateView.textView.text = url;
         }
                             withErrorBlock: ^(NSString *error)
         {
             
         }];
    }
    else
    {
        // do something
        [self.delegate toolsMenuShareClicked: self menuItem: menuItem];
    }
}

#pragma mark - BAMSettingsDelegate Methods
- (void)settingsDidActionForKey:(NSString *)key
{
    if( [key isEqualToString: @"reset_settings"] )
    {
        [[Settings instance] resetSettings];
    }
    [self.delegate toolsChartSettingsDidActionForKey: key];
}
- (void)settingsDidChangeValueForKey:(NSString *)key {
    NSLog(@"BAMSettingsDelegate settingsViewDidChangeValueForKey:%@ fired", key);
    [self.delegate toolsChartSettingsValueChangedForKey: key];
}

- (void)settingsExitedWithChangedValues:(BOOL)didChange {
    NSLog(@"BAMSettingsDelegate settingsViewExitedWithChangedValues:%@ fired", didChange ? @"Yes" : @"No");
}

- (void)settingsNavigatedAwayFromPane:(NSString *)propertyListName withChangedValues:(BOOL)didChange {
    NSLog(@"BAMSettingsDelegate settingsViewNavigatedAwayFromPane:%@ withChangedValues:%@ fired", propertyListName, didChange ? @"Yes" : @"No");
}

@end
