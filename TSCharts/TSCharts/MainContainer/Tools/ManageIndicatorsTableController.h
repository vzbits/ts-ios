//
//  ManageIndicatorsTableController.h
//  TSCharts
//
//  Created by Quoc Le on 8/1/12.
//
//

#import <UIKit/UIKit.h>

@protocol ManageIndicatorsDelegate <NSObject>
- (void) manageIndicatorRemovedItem: (id) sender;
@end

@interface ManageIndicatorsTableController : UITableViewController
@property(strong) NSMutableArray *indicators;
@property(strong) NSMutableArray *deletingItems;
@property(assign) id<ManageIndicatorsDelegate> delegate;
@end
