//
//  GenerateURLView.m
//  TSCharts
//
//  Created by Quoc Le on 8/8/12.
//
//

#import "GenerateURLView.h"
#import <MobileCoreServices/UTCoreTypes.h>

@implementation GenerateURLView


- (id)init
{
    self = [super init];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithRGBHex:0xD5D7DD];
        self.textView = [[[UITextField alloc] init] autorelease];
        self.textView.text =@"url";
        self.textView.borderStyle = UITextBorderStyleRoundedRect;
        //self.textView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview: self.textView];
        
        self.copyClipboardBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        [self.copyClipboardBtn setTitle: @"Copy to Clipboard" forState: UIControlStateNormal];
        [self.copyClipboardBtn addTarget: self action: @selector(copyClipboardClicked:) forControlEvents: UIControlEventTouchUpInside];
        [self addSubview: self.copyClipboardBtn];

    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.textView.frame = CGRectMake(10,20, self.width-20, 44);
    self.copyClipboardBtn.frame = CGRectMake(10, 20+44+20, self.width-20, 44);
    
}

- (void) copyClipboardClicked:(id) sender
{
    UIPasteboard* pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setValue: self.textView.text forPasteboardType:(NSString*)kUTTypeUTF8PlainText];
    
}

@end
