//
//  GenerateURLView.h
//  TSCharts
//
//  Created by Quoc Le on 8/8/12.
//
//

#import <UIKit/UIKit.h>

@interface GenerateURLView : UIView
@property (strong, nonatomic) UITextField *textView;
@property (strong, nonatomic) UIButton *copyClipboardBtn;
@end
