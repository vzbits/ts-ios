//
//  MainView.h
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListMiniView.h"
#import "ChartContainer.h"
#import "NewsReaderView.h"
#import "FullListView.h"

@interface MainView : UIView <ChartContainerDelegate, NewsViewDelegate, NewsReaderViewDelegate>
@property (strong, nonatomic) ChartContainer *chartContainer;
@property (strong, nonatomic) ListMiniView *listMiniView;
@property (strong, nonatomic) NewsReaderView *newsReader;
@property (strong, nonatomic) FullListView *fullListView;
@property (strong, nonatomic) UIToolbar *navBar;
@property (strong, nonatomic) UISegmentedControl *segmentControl;
@property (strong, nonatomic) NSArray *chartToolItems;
@property (strong, nonatomic) NSArray *listToolItems;

@end
