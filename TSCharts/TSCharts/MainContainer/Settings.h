//
//  ApplicationSettings.h
//  TSCharts
//
//  Created by Quoc Le on 8/1/12.
//
//

#import <Foundation/Foundation.h>
#import "Indicator.h"
#import "service.h"

@interface Settings : NSObject
{
}

declareSingleton(Settings);

- (NSArray*) loadIndicators;
- (void) saveIndicators: (NSArray*) indicators;
- (NSInteger) addSegmentIndex;
- (void) setSegmentIndex: (NSInteger) index;
- (void) addRecentSearch: (SymbolData*) symbol;
- (SymbolSearchResultData*) recentSearches;
- (void) resetSettings;
- (void) setListSelected: (NSIndexPath*) indexPath;
- (NSIndexPath*) listSelected;

- (NSString*) deviceToken;
- (void) setDeviceToken: (NSString*) deviceToken;

- (NSInteger) tokenStatus;
- (void) setTokenStatus: (NSInteger) tokenStatus;

@end
