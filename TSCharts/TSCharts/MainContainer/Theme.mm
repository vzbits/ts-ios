//
//  Theme.m
//  TSCharts
//
//  Created by Quoc Le on 8/2/12.
//
//

#import "Theme.h"

@implementation Theme
+ (UIColor*) currentPriceBackgroundColor
{
    return [UIColor colorWithRGBHex: 0xF4AE64];
}

+ (void) stylizeToolbar:(UIToolbar*)toolbar
{
    UIImage *gradientImage44 = [[UIImage imageNamed:@"header"]
                                resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
    
    [toolbar setBackgroundImage:gradientImage44
                 forToolbarPosition: UIToolbarPositionTop
                         barMetrics:UIBarMetricsDefault];
    
    [toolbar setTintColor: [UIColor colorWithRGBHex:0x5C6D89] ];

}

+ (void) stylizeNavbar: (UINavigationBar*) navBar
{
    UIImage *gradientImage44 = [[UIImage imageNamed:@"header"]
                                resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
    
    [navBar setBackgroundImage:gradientImage44
                     forBarMetrics:UIBarMetricsDefault];
    
    [navBar setTintColor: [UIColor colorWithRGBHex:0x5C6D89] ];
}

+ (void) stylizeNewsToolbar: (UIToolbar*) toolbar;
{
//    UIImage *gradientImage44 = [[UIImage imageNamed:@"news_header"]
//                                resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
//    
//    [toolbar setBackgroundImage:gradientImage44
//             forToolbarPosition: UIToolbarPositionTop
//                     barMetrics:UIBarMetricsDefault];
    
    [toolbar setTintColor: [UIColor colorWithRGBHex: 0x444444] ];
}
@end
