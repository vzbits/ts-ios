//
//  FullListView.m
//  TSCharts
//
//  Created by Quoc Le on 8/13/12.
//
//

#import "FullListView.h"

#define MARGIN 5

@implementation FullListView

- (id)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor colorWithRGBHex:0xeeeeee];
        
        self.complexTableView = [[[ComplexTableView alloc] init] autorelease];
        [self addSubview: self.complexTableView];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.complexTableView.frame = CGRectInset(self.bounds, 5,5);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
