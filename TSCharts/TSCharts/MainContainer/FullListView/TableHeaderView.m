//
//  TableHeaderView.m
//  TSCharts
//
//  Created by Quoc Le on 8/13/12.
//
//

#import "TableHeaderView.h"

@implementation TableHeaderView

- (id)init
{
    self = [super init];
    if (self)
    {
        self.headerView = [[[HeaderView alloc] init] autorelease];
        self.headerView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview: self.headerView];
        
        self.headerHeight = 44;
        self.dataView = [[[UITableView alloc] init] autorelease];
        self.dataView.delegate = self;
        self.dataView.dataSource = self;
        [self addSubview: self.dataView];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.headerView.frame = CGRectMake(0, 0, self.width, self.headerHeight);
    self.dataView.frame = CGRectMake(0, self.headerHeight, self.width, self.height - self.headerHeight);
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
        
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.delegate tableHeaderViewScrollOffset: scrollView.contentOffset sender: self];
}

@end
