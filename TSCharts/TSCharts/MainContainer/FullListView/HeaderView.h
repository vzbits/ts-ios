//
//  HeaderView.h
//  TSCharts
//
//  Created by Quoc Le on 8/13/12.
//
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView

@property (strong) NSArray *columns;

@end
