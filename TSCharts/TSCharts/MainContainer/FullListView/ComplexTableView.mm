//
//  ComplexTableView.m
//  TSCharts
//
//  Created by Quoc Le on 8/13/12.
//
//

#import "ComplexTableView.h"

#define LEFT_WIDTH 250

@implementation ComplexTableView

- (id)init
{
    self = [super init];
    if (self) {
        
        self.layer.cornerRadius = 8.0;
        self.layer.borderWidth = 2.0;
        self.layer.borderColor = [UIColor colorWithRGBHex: 0xc7c7c7].CGColor;
        self.backgroundColor = [UIColor whiteColor];
        self.layer.masksToBounds = YES;
                
        self.leftTableView = [[[TableHeaderView alloc] init] autorelease];
        self.leftTableView.delegate = self;
        self.leftTableView.dataView.showsVerticalScrollIndicator = NO;
        [self addSubview: self.leftTableView];
        
        self.rightTableView = [[[TableHeaderView alloc] init] autorelease];
        self.rightTableView.delegate = self;
        [self addSubview: self.rightTableView];
        
//        UIPanGestureRecognizer *panGesture = [[[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(panGesture:)] autorelease];
//        panGesture.cancelsTouchesInView = NO;
//        [self.leftTableView.dataView addGestureRecognizer: panGesture];
//        [self.rightTableView.dataView addGestureRecognizer: panGesture];

        
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.leftTableView.frame = CGRectMake(0,0, LEFT_WIDTH, self.height);
    self.rightTableView.frame = CGRectMake(LEFT_WIDTH+1, 0, self.width - LEFT_WIDTH, self.height);
}

- (void)drawRect:(CGRect)rect
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint: CGPointMake(LEFT_WIDTH, 0)];
    [path addLineToPoint: CGPointMake(LEFT_WIDTH, self.height)];
    [[UIColor lightGrayColor] setStroke];
    [path stroke];
}

- (void) tableHeaderViewScrollOffset:(CGPoint)p sender:(id)sender
{
    if(sender == self.leftTableView)
    {
        [self.rightTableView.dataView setContentOffset:  CGPointMake(0, p.y)];
    }
    else
    {
        [self.leftTableView.dataView setContentOffset:  CGPointMake(0, p.y)];
    }
}

//- (void) panGesture: (UIPanGestureRecognizer*) gesture
//{
//    //CGPoint p = [gesture translationInView:  gesture.view];
//    CGPoint p = [gesture locationInView: gesture.view];
//    
//    if(gesture.view == self.leftTableView)
//        [self.rightTableView.dataView setContentOffset:  CGPointMake(0, p.y)];
//    if(gesture.view == self.rightTableView)
//        [self.leftTableView.dataView setContentOffset:  CGPointMake(0, p.y)];
//}

@end
