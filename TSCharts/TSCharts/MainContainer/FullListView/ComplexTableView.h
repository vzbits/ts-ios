//
//  ComplexTableView.h
//  TSCharts
//
//  Created by Quoc Le on 8/13/12.
//
//

#import <UIKit/UIKit.h>
#import "TableHeaderView.h"

@interface ComplexTableView : UIView <TableHeaderViewDelegate>

@property(strong) TableHeaderView *leftTableView;
@property(strong) TableHeaderView *rightTableView;
@property(strong) NSArray *data;

@end
