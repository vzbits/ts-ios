//
//  FullListView.h
//  TSCharts
//
//  Created by Quoc Le on 8/13/12.
//
//

#import <UIKit/UIKit.h>
#import "ComplexTableView.h"

@interface FullListView : UIView
@property (strong) ComplexTableView *complexTableView;
@end
