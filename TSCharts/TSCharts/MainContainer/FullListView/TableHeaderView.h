//
//  TableHeaderView.h
//  TSCharts
//
//  Created by Quoc Le on 8/13/12.
//
//

#import <UIKit/UIKit.h>
#import "HeaderView.h"

@protocol TableHeaderViewDelegate <NSObject>
- (void) tableHeaderViewScrollOffset: (CGPoint) p sender: (id) sender;
@end

@interface TableHeaderView : UIView <UITableViewDataSource, UITableViewDelegate>

@property (assign) float headerHeight;
@property (strong) UITableView *dataView;
@property (strong) HeaderView *headerView;

@property (assign) id<TableHeaderViewDelegate> delegate;
@end
