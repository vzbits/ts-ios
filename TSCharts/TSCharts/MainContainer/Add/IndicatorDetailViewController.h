//
//  IndicatorDetailViewController.h
//  TSCharts
//
//  Created by Quoc Le on 8/10/12.
//
//

#import <UIKit/UIKit.h>
#import "Indicator.h"
#import "MNColorSelectionViewController.h"

@protocol IndicatorDetailViewDelegate <NSObject>
- (void) indicatorDetailColorChanged: (Indicator*) ind;
@end

@interface IndicatorDetailViewController : UITableViewController <MNColorSelectionViewControllerDelegate>

@property(strong) Indicator *indicator;
@property(assign) id<IndicatorDetailViewDelegate> delegate;

- (id) initWithIndicator: (Indicator*) indicator_;
@end
