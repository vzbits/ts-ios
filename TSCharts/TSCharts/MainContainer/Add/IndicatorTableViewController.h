//
//  IndicatorTableViewController.h
//  TSCharts
//
//  Created by Quoc Le on 7/31/12.
//
//

#import <UIKit/UIKit.h>
#import "ServiceManager.h"

@protocol IndicatorTableViewDelegate <NSObject>
-(void) indicatorTableViewItemClicked: (IndicatorCodeData*) item;
-(void) indicatorTableViewRemovedItem: (id) sender;
@end

@interface IndicatorTableViewController : UITableViewController
{
}
@property(assign) UINavigationController *myNavController;
@property(strong) NSMutableArray *indicators;
@property(strong) NSMutableArray *deletingItems;
@property(assign) id<IndicatorTableViewDelegate> delegate;
@property(strong) IndicatorCodeListData *data;
- (void) loadData;

@end
