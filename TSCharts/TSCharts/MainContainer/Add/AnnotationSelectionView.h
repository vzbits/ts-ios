//
//  AnnotationSelectionView.h
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import <UIKit/UIKit.h>
#import "AnnotationGraphicsView.h"

@protocol AnnotationSelectionViewDelegate <NSObject>
- (void) annotationSelectioViewDidClicked: (id) sender tag: (AnnotationGraphicType) tag;
@end

@interface AnnotationSelectionView : UIView
@property(strong) NSMutableArray *buttons;
@property(strong) NSArray *images;
@property(strong) NSArray *highlighted;
@property(strong) NSArray *tags;

@property(assign) int rows;
@property(assign) int columns;
@property(assign) id<AnnotationSelectionViewDelegate> delegate;

+ (AnnotationSelectionView*) makePage1;
@end
