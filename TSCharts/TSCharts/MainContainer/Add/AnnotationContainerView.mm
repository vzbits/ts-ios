//
//  AnnotationContainerView.m
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import "AnnotationContainerView.h"

@implementation AnnotationContainerView

- (id)init
{
    self = [super init];
    if (self) {
        self.selView = [AnnotationSelectionView makePage1];
        self.selView.delegate = self;
        [self addSubview: self.selView];
        
//        self.toolbar = [[[UIToolbar alloc] init] autorelease];
//        
//        UIBarButtonItem *flex  = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace
//                                                                                target:nil action: nil] autorelease];
//        
//        UIBarButtonItem *color = [[[UIBarButtonItem alloc] initWithImage: [UIImage imageNamed:@"33.png"]
//                                                                  style: UIBarButtonItemStyleBordered
//                                                                 target: self
//                                                                  action: @selector(colorClicked:)] autorelease];
//
//        UIBarButtonItem *stroke = [[[UIBarButtonItem alloc] initWithImage: [UIImage imageNamed:@"49.png"]
//                                                                   style: UIBarButtonItemStyleBordered
//                                                                  target: self
//                                                                  action: @selector(colorClicked:)] autorelease];
//        
//        self.toolbar.items = [NSArray arrayWithObjects: flex, color, stroke, flex, nil];
//        [self addSubview: self.toolbar];
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    self.selView.frame = CGRectMake(0, 0, self.width, self.height);
    //self.toolbar.frame = CGRectMake(0, self.height - 44, self.width, 44);
}

- (void) dealloc
{
    self.selView = nil;
    [super dealloc];
}

- (void) annotationSelectioViewDidClicked: (id) sender tag: (AnnotationGraphicType) tag
{
    [self.delegate annotationContainerViewDidClicked: self tag: tag];
}

- (void) colorClicked: (id) sender
{
    
}

@end
