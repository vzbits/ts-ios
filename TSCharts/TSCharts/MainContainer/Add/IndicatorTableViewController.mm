//
//  IndicatorTableViewController.m
//  TSCharts
//
//  Created by Quoc Le on 7/31/12.
//
//

#import "IndicatorTableViewController.h"
#import "IndicatorJS.h"
#import "IndicatorDetailViewController.h"

@interface IndicatorTableViewController ()

@end

@implementation IndicatorTableViewController

@synthesize delegate = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Public functions

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return section == 0 ? @"Current Indicators" : @"Select to Add";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return section == 0 ? MAX(1,self.indicators.count) : self.data.indicators.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellIdentifier] autorelease];
    }
    
    cell.textLabel.textColor = [UIColor blackColor];
    
    if(indexPath.section == 0)
    {
        if(self.indicators.count > 0)
        {
            Indicator *indicator = [self.indicators objectAtIndex: indexPath.row];
            cell.textLabel.text = [indicator name];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        else
        {
            cell.textLabel.textColor = [UIColor lightGrayColor];
            cell.textLabel.text = @"(None)";
        }
    }
    else if(indexPath.section == 1)
    {
        IndicatorCodeData *item = [self.data.indicators objectAtIndex: indexPath.row];
        cell.textLabel.text = item.name;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if(editing == YES)
    {
    } else
    {
        NSLog(@"Reload chart..");
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        IndicatorJS *indicatorJS = IndicatorJS::instance();
        indicatorJS->remove_indicator(indexPath.row);
        
        [self.indicators removeObjectAtIndex: indexPath.row];

        NSLog(@"delete ind count = %d", self.indicators.count);
        if(self.indicators.count >= 1)
        {
            [self.tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath] withRowAnimation: UITableViewRowAnimationAutomatic];
        }
        else
        {
            [self.tableView reloadRowsAtIndexPaths: [NSArray arrayWithObject: indexPath] withRowAnimation: UITableViewRowAnimationAutomatic];
        }
        [self.delegate indicatorTableViewRemovedItem: self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath.section == 0 ? YES : NO;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 1)
    {
        IndicatorCodeData *item = [self.data.indicators objectAtIndex: indexPath.row];
        [self.delegate indicatorTableViewItemClicked: item];
    }
    else
    {
        Indicator *indicator = [self.indicators objectAtIndex: indexPath.row];
        IndicatorDetailViewController *detailController = [[[IndicatorDetailViewController alloc] initWithIndicator: indicator] autorelease];
        detailController.title = indicator.name;
        detailController.contentSizeForViewInPopover = self.contentSizeForViewInPopover;
        [self.myNavController pushViewController: detailController animated: YES];
    }
}

- (void) loadData
{
    IndicatorJS *indicatorJS = IndicatorJS::instance();
    self.indicators = indicatorJS->get_data();
    
    [[ServiceManager instance] getInstrumentListDataWithBlock: ^(IndicatorCodeListData *data_)
     {
         NSLog(@"Got indicators %d", data_.indicators.count);
         self.data = data_;
         [self.tableView reloadData];
     }
                                    withErrorBlock: ^(NSString *error)
     {
         
     }];
}
@end
