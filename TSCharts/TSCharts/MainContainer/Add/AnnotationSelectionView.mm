//
//  AnnotationSelectionView.m
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import "AnnotationSelectionView.h"

@implementation AnnotationSelectionView

- (id)initForImage: (NSArray*) images_ Highlight: (NSArray*) highlighted_ Tags: (NSArray*) tags_
{
    self = [super init];
    if (self)
    {
        self.buttons = [NSMutableArray array];
        self.images = images_;
        self.tags   = tags_;
        self.highlighted = highlighted_;
        
        self.columns = 3;
        self.rows = ceilf(self.images.count / self.columns);
        self.backgroundColor = [UIColor whiteColor];
        
        [self buildGrid];
    }
    return self;
}

- (void) dealloc
{
    self.buttons = nil;
    self.images = nil;
    self.tags = nil;
    self.highlighted = nil;
    
    [super dealloc];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    NSLog(@"Relayout %d %f",self.buttons.count, self.width);
    
    for(int i=0; i < self.buttons.count; i++)
    {
        int r = floorf(i/self.columns);
        int c = i%self.columns;
        int w = self.width /self.columns;
        
        UIButton *b = [self.buttons objectAtIndex: i];
        b.frame = CGRectMake(w*c,w*r, w, w);
    }
}

- (int) getIndexForRow: (int) r Column: (int) c
{
    return (r*self.rows) + c;
}

- (void) buildGrid
{
    for(int i=0; i< self.images.count; i++)
    {
        UIImage *image = [self.images objectAtIndex: i];
        UIImage *image_h = [self.highlighted objectAtIndex: i];
        
        UIButton *b = [UIButton buttonWithType: UIButtonTypeCustom];
        b.tag = [[self.tags objectAtIndex: i] intValue];
        b.adjustsImageWhenHighlighted = YES;
        b.reversesTitleShadowWhenHighlighted = YES;
        b.showsTouchWhenHighlighted = YES;
        [b setImage: image forState: UIControlStateNormal];
        [b setImage: image_h forState: UIControlStateHighlighted];
        [b addTarget: self action: @selector(buttonClicked:) forControlEvents: UIControlEventTouchUpInside];
        [self addSubview: b];
        [self.buttons addObject: b];
    }
}

- (void) buttonClicked: (UIButton*)sender
{
    [self.delegate annotationSelectioViewDidClicked: self tag: (AnnotationGraphicType)sender.tag];
}

+ (AnnotationSelectionView*) makePage1
{
    NSArray *images = [NSArray arrayWithObjects: [UIImage imageNamed:@"annotation_line.png"],
                                                    [UIImage imageNamed:@"annotation_rect.png"],
                                                    [UIImage imageNamed:@"annotation_rect_round.png"],
                                                    [UIImage imageNamed:@"annotation_circle.png"],
                                                    [UIImage imageNamed:@"annotation_text.png"],
                                                    [UIImage imageNamed:@"annotation_text_bordered.png"],
                                                    [UIImage imageNamed:@"annotation_text_corner.png"],
                                                    [UIImage imageNamed:@"annotation_up.png"],
                                                    [UIImage imageNamed:@"annotation_down.png"],
                                                    nil];
    NSArray *highlighted = [NSArray arrayWithObjects: [UIImage imageNamed:@"annotation_line_h.png"],
                                                    [UIImage imageNamed:@"annotation_rect_round_h.png"],
                                                    [UIImage imageNamed:@"annotation_rect.png"],                            
                                                    [UIImage imageNamed:@"annotation_circle_h.png"],
                                                    [UIImage imageNamed:@"annotation_text_h.png"],
                                                    [UIImage imageNamed:@"annotation_text_bordered_h.png"],
                                                    [UIImage imageNamed:@"annotation_text_corner_h.png"],
                                                    [UIImage imageNamed:@"annotation_up_h.png"],
                                                    [UIImage imageNamed:@"annotation_down_h.png"],
                                                    nil];
    
    NSArray *tags = [NSArray arrayWithObjects: [NSNumber numberWithInt: ANNOTATION_LINE],
                                                [NSNumber numberWithInt: ANNOTATION_RECT],
                                                [NSNumber numberWithInt: ANNOTATION_RECT_ROUNDED],
                                                [NSNumber numberWithInt: ANNOTATION_CIRCLE],
                                                [NSNumber numberWithInt: ANNOTATION_TEXT],
                                                [NSNumber numberWithInt: ANNOTATION_TEXT_BORDERED],
                                                [NSNumber numberWithInt: ANNOTATION_TEXT_CORNERED],
                                                 [NSNumber numberWithInt: ANNOTATION_ARROW_UP],
                                                 [NSNumber numberWithInt: ANNOTATION_ARROW_DOWN],
                                            nil];

    AnnotationSelectionView *obj = [[[AnnotationSelectionView alloc] initForImage: images Highlight: highlighted Tags: tags] autorelease];
    return obj;
}
@end
