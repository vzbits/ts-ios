//
//  ToolsMenuViewController.m
//  TSCharts
//
//  Created by Quoc Le on 7/31/12.
//
//

#import "AddViewController.h"

@interface AddViewController ()

@end

@implementation AddViewController

@synthesize indicatorTableController = _indicatorTableController;
@synthesize delegate = _delegate;

- (id)init
{
    self = [super init];
    if (self)
    {    
        UISegmentedControl *sc = [[[UISegmentedControl alloc] initWithItems: [NSArray arrayWithObjects: @"Indicators", @"Annotations", nil]] autorelease];
        sc.segmentedControlStyle = UISegmentedControlStyleBar;
        sc.selectedSegmentIndex = 0;
        [sc addTarget: self action: @selector(segmentControlSelected:) forControlEvents: UIControlEventValueChanged];
        UIBarButtonItem *scItem = [[[UIBarButtonItem alloc] initWithCustomView:sc] autorelease];
        self.navigationItem.titleView = scItem.customView;
        
        self.indicatorTableController = [[[IndicatorTableViewController alloc] initWithStyle:UITableViewStylePlain] autorelease];
        self.indicatorTableController.delegate = self;
        
        self.annotationView = [[[AnnotationContainerView alloc] init] autorelease];
        self.annotationView.delegate = self;
    }
    return self;
}

- (void) dealloc
{
    self.annotationView = nil;
    self.indicatorTableController = nil;
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self addChildViewController: self.indicatorTableController];
}

- (void) viewDidAppear:(BOOL)animated
{
    [self setSegmentViewByIndex: [[Settings instance] addSegmentIndex]];
    ((UISegmentedControl*)self.navigationItem.titleView).selectedSegmentIndex = [[Settings instance] addSegmentIndex];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void) indicatorTableViewItemClicked:(IndicatorCodeData *)item
{
    [self.delegate addViewControllerIndicatorClicked: item];
}

- (void) indicatorTableViewRemovedItem: (id) sender
{
    [self.delegate addViewControllerIndicatorRemoved: self];
}

- (void) annotationContainerViewDidClicked: (id) sender tag: (AnnotationGraphicType) tag
{
    [self.delegate addViewControllerAnnotationClicked: tag];
}

- (void) setSegmentViewByIndex: (NSInteger) index
{
    [self.indicatorTableController.view removeFromSuperview];
    [self.annotationView removeFromSuperview];
    
    //NSLog(@"W = %f", self.view.bounds.size.width);
    
    if(index == 0)
    {
        [self.view addSubview: self.indicatorTableController.view];
        self.indicatorTableController.contentSizeForViewInPopover = self.contentSizeForViewInPopover;
        self.indicatorTableController.myNavController = self.navigationController;
        self.indicatorTableController.view.frame = self.view.bounds;
        [self.indicatorTableController loadData];
        self.navigationItem.rightBarButtonItem = self.indicatorTableController.editButtonItem;
    }
    else
    {
        [self.view addSubview: self.annotationView];
        self.annotationView.frame = self.view.bounds;
        [self.annotationView setNeedsLayout];
        self.navigationItem.rightBarButtonItem = nil;
    }
}
- (void) segmentControlSelected: (UISegmentedControl*)sender
{
    [self setSegmentViewByIndex: sender.selectedSegmentIndex];
    [[Settings instance] setSegmentIndex: sender.selectedSegmentIndex];
}


@end
