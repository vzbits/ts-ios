//
//  IndicatorDetailViewController.m
//  TSCharts
//
//  Created by Quoc Le on 8/10/12.
//
//

#import "IndicatorDetailViewController.h"
#import "MNPageViewController.h"
#import "MNColorTableViewCell.h"
#import "MNColorView.h"
#import "ChartPanel.h"
#import "TextStepperField.h"

enum {
    InputSection = 0,
    ColorSection,
    OtherSection,
};

@interface IndicatorDetailViewController ()

@end

@implementation IndicatorDetailViewController

- (id) initWithIndicator: (Indicator*) indicator_;
{
    self = [super initWithStyle: UITableViewStyleGrouped];
    if (self)
    {
        self.indicator = indicator_;
        self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Apply"
                                                                                  style:UIBarButtonItemStyleDone
                                                                                  target: self action:@selector(applyClicked:)] autorelease];
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) applyClicked: (id) sender
{
    [[ChartPanel instance] reloadChart];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void) numberChanged: (TextStepperField*)sender
{
    self.navigationItem.rightBarButtonItem.enabled = YES;
    
    NSNumber *n = [NSNumber numberWithInt: sender.Current];
    int index = sender.tag;
    
    if(index < self.indicator.inputValues.count)
        [self.indicator.inputValues replaceObjectAtIndex: index withObject: n];
    else
        NSAssertFalse(@"Expecting input value");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.indicator.type == INDICATOR_OVERLAY ? 2 : 2;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==InputSection)
        return @"Input Parameters";
    if(section==ColorSection)
        return @"Plot Color";
    if(section==OtherSection)
        return @"Others";
    
    return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == InputSection)
    {
        return self.indicator.inputValues.count;
    }
    if(section == ColorSection)
    {
        return self.indicator.plotColor.count;
    }
    if(section == OtherSection)
        return 1;
    
    return 0;
}


- (UITableViewCell *)colorCellForRowIndex:(NSUInteger)rowIndex
{
    MNColorTableViewCell *cell = [MNColorTableViewCell cellForTableView:self.tableView];
    cell.textLabel.text = NSLocalizedStringFromTable(@"Color", @"inspector", @"color cell text field");
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.colorView.color = [ChartUtils convertColor: [self.indicator.plotColor objectAtIndex: rowIndex]];

    if(rowIndex < self.indicator.plotNames.count)
    {
        cell.textLabel.text = [self.indicator.plotNames objectAtIndex: rowIndex];
    }

    return cell;
}

- (UITableViewCell*) numberCellForRowIndex: (NSInteger) rowIndex
{
    NSString *CellIdentifier = @"NUMBER_CELL";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        TextStepperField *textStepper = [[[TextStepperField alloc] initWithFrame:CGRectMake(0,1,150,42)] autorelease];
        cell.accessoryView = textStepper;
        [textStepper addTarget: self action: @selector(numberChanged:) forControlEvents:UIControlEventValueChanged];
    }
    
    NSString *inputName = rowIndex < self.indicator.inputNames.count ? [self.indicator.inputNames objectAtIndex: rowIndex] : @"";
    cell.textLabel.text = inputName;
    
    id obj = rowIndex < self.indicator.inputValues.count ? [self.indicator.inputValues objectAtIndex: rowIndex] : @"";
    TextStepperField *textStepper = (TextStepperField*) cell.accessoryView;
    textStepper.tag = rowIndex;

    if([obj isKindOfClass:[NSNumber class]])
    {
        NSNumber *n = (NSNumber*) obj;
        textStepper.Current = [n intValue];
    }
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier;
    
    if(indexPath.section == InputSection)
    {
        return [self numberCellForRowIndex: indexPath.row];        
    }
    else if(indexPath.section == ColorSection)
    {
        return [self colorCellForRowIndex: indexPath.row];
    }
    else
    {
        CellIdentifier = @"ABC";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier: CellIdentifier] autorelease];
            
            if([CellIdentifier isEqualToString: @"COLOR_CELL"])
            {
            }
        }
        
        return cell;
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == ColorSection) {
        NSString *viewControllerTitle = @"Color";
        NSMutableArray *viewControllers = [NSMutableArray arrayWithCapacity:2];
        MNColorSelectionViewController *rainbowViewController = [MNColorSelectionViewController rainbowSelectionViewController];
        rainbowViewController.title = viewControllerTitle;
        if (rainbowViewController) [viewControllers addObject:rainbowViewController];
        rainbowViewController.delegate = self;
        
        MNColorSelectionViewController *monochromeViewController = [MNColorSelectionViewController monoChromeSelectionViewController];
        monochromeViewController.title = viewControllerTitle;
        if (monochromeViewController) [viewControllers addObject:monochromeViewController];
        monochromeViewController.delegate = self;
        
        // Navigation logic may go here. Create and push another view controller.
        MNPageViewController *pageViewController = [[MNPageViewController alloc] initWithViewControllers:viewControllers];
        pageViewController.contentSizeForViewInPopover = self.contentSizeForViewInPopover;
        [self.navigationController pushViewController:pageViewController animated:YES];
        
        rainbowViewController.view.tag = indexPath.row;
        monochromeViewController.view.tag = indexPath.row;
        [pageViewController release];
    }
}


#pragma mark - MNColorSelectionViewController Delegate

- (UIColor *)colorSelectionControllerSelectedColor:(MNColorSelectionViewController *)controller
{
    MNColorTableViewCell *cell = (MNColorTableViewCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:controller.view.tag inSection:ColorSection]];
    return cell.colorView.color;
}
- (void)colorSelectionController:(MNColorSelectionViewController *)controller didSelectedColor:(UIColor *)color
{
    int row = controller.view.tag;
    NSString *colorHex = [NSString stringWithFormat:@"0x%@", [color hexStringFromColor]];
    NSLog(@"Setting color to %@", colorHex);
    
    [self.indicator.plotColor replaceObjectsInRange: NSMakeRange(row, 1) withObjectsFromArray: [NSArray arrayWithObject: colorHex]];
    
    [[ChartPanel instance] reloadChart: NO];
    
    [self.delegate indicatorDetailColorChanged: self.indicator];
    
    MNColorTableViewCell *cell = (MNColorTableViewCell *) [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow: controller.view.tag inSection:ColorSection]];
    cell.colorView.color = color;
}

@end
