//
//  AnnotationContainerView.h
//  TSCharts
//
//  Created by Quoc Le on 8/3/12.
//
//

#import <UIKit/UIKit.h>
#import "AnnotationSelectionView.h"

@protocol AnnotationContainerViewDelegate <NSObject>
- (void) annotationContainerViewDidClicked: (id) sender tag: (AnnotationGraphicType) tag;
@end

@interface AnnotationContainerView : UIView <AnnotationSelectionViewDelegate>
@property(strong) AnnotationSelectionView *selView;
//@property(strong) UIToolbar *toolbar;
@property(assign) id<AnnotationContainerViewDelegate> delegate;
@end
