//
//  ToolsMenuViewController.h
//  TSCharts
//
//  Created by Quoc Le on 7/31/12.
//
//

#import <UIKit/UIKit.h>
#import "IndicatorTableViewController.h"
#import "AnnotationContainerView.h"
#import "Settings.h"

@protocol AddViewControllerDelegate <NSObject>
- (void) addViewControllerIndicatorClicked:(IndicatorCodeData *)item;
- (void) addViewControllerIndicatorRemoved: (id) sender;
- (void) addViewControllerAnnotationClicked:(AnnotationGraphicType) tag;
@end

@interface AddViewController : UIViewController <IndicatorTableViewDelegate, AnnotationContainerViewDelegate>
@property(strong) NSMutableArray *data;
@property(strong) NSMutableArray *icons;
@property(strong) IndicatorTableViewController *indicatorTableController;
@property(strong) AnnotationContainerView *annotationView;

@property(assign) id<AddViewControllerDelegate> delegate;
@end
