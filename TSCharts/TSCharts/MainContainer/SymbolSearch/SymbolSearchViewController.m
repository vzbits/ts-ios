//
//  SymbolSearchViewController.m
//  TSCharts
//
//  Created by Quoc Le on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SymbolSearchViewController.h"
#import "Settings.h"
#import "ColorScheme.h"

@interface SymbolSearchViewController ()

@end

@implementation SymbolSearchViewController

@synthesize searchData = _searchData;
@synthesize delegate   = _delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self showRecentResults];
    }
    return self;
}

- (void)viewDidLoad
{
	self.tableView.backgroundColor = CELL_SEARCH_BGCOLOR;
	self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchData.results.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
   // return self.displayRecent ? @"Recent Searches" : @"Search Results";
	return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleSubtitle reuseIdentifier: CellIdentifier] autorelease];
		UIView *separator = [[[UIView alloc] initWithFrame:CGRectMake(0,43,self.view.width,1)] autorelease];
		separator.backgroundColor = CELL_SEARCH_SEPCOLOR;
		[cell addSubview: separator];
    }
    
    SymbolData *symbol = [self.searchData.results objectAtIndex: indexPath.row];
	cell.textLabel.font = CELL_SEARCH_FONT;
    cell.textLabel.backgroundColor = [UIColor clearColor];
	cell.textLabel.textColor = CELL_SEARCH_TEXTCOLOR;
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
	cell.detailTextLabel.textColor = CELL_SEARCH_TEXTCOLOR;
	
	NSUInteger count = 0, length = [symbol.name length];
	NSRange range = NSMakeRange(0, length);

	NSLog(@"compute %@", symbol);
	NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString: symbol.name];
	while(range.location != NSNotFound)
	{
		range = [symbol.name rangeOfString: self.searchData.searchString options:NSCaseInsensitiveSearch range:range];
		if(range.location != NSNotFound)
		{
			NSLog(@"Ranges found %@/%@ %d %d", symbol.symbol, self.searchData.searchString, range.location, range.length);
			[string addAttribute:NSForegroundColorAttributeName value:CELL_SEARCH_TEXTHIGHLIGHTCOLOR range: range];
			range = NSMakeRange(range.location + range.length, length - (range.location + range.length));
			count++;
		}
	}
	cell.detailTextLabel.attributedText = string;

	range = NSMakeRange(0, symbol.symbol.length);
	NSString *combinedStr = symbol.exchange != nil ?
				[NSString stringWithFormat:@"%@  %@", symbol.symbol, symbol.exchange]
				:symbol.symbol;
	
	NSMutableAttributedString * string2 = [[NSMutableAttributedString alloc] initWithString: combinedStr];
	range = [symbol.symbol rangeOfString: self.searchData.searchString options:NSCaseInsensitiveSearch range:range];
	if(range.location != NSNotFound)
	{
		[string2 addAttribute:NSForegroundColorAttributeName value:CELL_SEARCH_TEXTHIGHLIGHTCOLOR range: range];
	}
	
	if(symbol.exchange != nil)
	{
		[string2 addAttribute:NSForegroundColorAttributeName value: [UIColor darkGrayColor]
					range: NSMakeRange(symbol.symbol.length, combinedStr.length - symbol.symbol.length)];
	}
	
	cell.textLabel.attributedText = string2;
	
	if(indexPath.row % 2 == 0)
		cell.contentView.backgroundColor = CELL_SEARCH_BGCOLOR;
	else
		cell.contentView.backgroundColor = CELL_SEARCH_ALTBGCOLOR;

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SymbolData *symbol = [self.searchData.results objectAtIndex: indexPath.row];
    [self.delegate symbolSearchSelected: symbol];
}

- (void) showRecentResults
{
    self.searchData = [[Settings instance] recentSearches];
    self.displayRecent = NO;
    [self.tableView reloadData];
}

- (void) setData: (SymbolSearchResultData*) data
{
    self.displayRecent = NO;
    NSLog(@"Got records %d", data.results.count);
    self.searchData = data;
    [self.tableView reloadData];
}

+ (UIPopoverController*) createPopover
{
    SymbolSearchViewController *controller = [[[SymbolSearchViewController alloc] initWithStyle: UITableViewStylePlain] autorelease];
    UIPopoverController *popover = [[[UIPopoverController alloc] initWithContentViewController: controller] autorelease];
    return popover;
}

@end
