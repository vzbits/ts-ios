//
//  SymbolSearchViewController.h
//  TSCharts
//
//  Created by Quoc Le on 7/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "service.h"

@protocol SymbolSearchViewDelegate <NSObject>

- (void) symbolSearchSelected: (SymbolData*) symbol;

@end

@interface SymbolSearchViewController : UITableViewController
@property (strong, nonatomic) SymbolSearchResultData *searchData;
@property (assign, nonatomic) id<SymbolSearchViewDelegate> delegate;
@property (assign, nonatomic) BOOL displayRecent;

+ (UIPopoverController*) createPopover;
- (void) setData: (SymbolSearchResultData*) data;
- (void) showRecentResults;
@end
