//
//  MainView.m
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainView.h"
#define LEFT_WIDTH 180

@implementation MainView
@synthesize chartContainer = _chartContainer;
@synthesize listMiniView = _listMiniView;

- (id)init
{
    self = [super init];
    if (self) 
    {
        self.fullListView = [[[FullListView alloc] init] autorelease];
        self.fullListView.hidden = YES;
        [self addSubview: self.fullListView];

        self.chartContainer = [[[ChartContainer alloc] init] autorelease];
        self.chartContainer.delegate = self;
        [self addSubview: self.chartContainer];
        
        self.listMiniView = [[[ListMiniView alloc] init] autorelease];
        self.listMiniView.newsController.delegate = self;
        [self addSubview: self.listMiniView];
        
        [self buildNavBar];
        self.chartContainer.navBar = self.navBar;
    }
    return self;
}

- (void) buildNavBar
{
    self.navBar = [[[UIToolbar alloc] init] autorelease];
    [self addSubview: self.navBar];
    
    self.segmentControl = [[[UISegmentedControl alloc] initWithItems: [NSArray arrayWithObjects:@"List", @"Charts", @"Screener", nil]] autorelease];
    self.segmentControl.selectedSegmentIndex = 1;
    self.segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
    [self.segmentControl addTarget: self action: @selector(segmentControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *screenSelect = [[[UIBarButtonItem alloc] initWithCustomView: self.segmentControl] autorelease];
    
    
    UIBarButtonItem *searchBarItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemSearch
                                                                                    target: self.chartContainer action: @selector(searchButtonClicked:)] autorelease];
    searchBarItem.style = UIBarButtonItemStyleBordered;
    
    UIBarButtonItem *flex = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace
                                                                           target: self.chartContainer action: nil] autorelease];
    UIBarButtonItem *periodicity = [[[UIBarButtonItem alloc] initWithTitle:@"Daily" style: UIBarButtonItemStyleBordered
                                                                    target:nil action:nil] autorelease];
    UIBarButtonItem *setting = [[[UIBarButtonItem alloc] initWithImage: [UIImage imageNamed:@"332.png"]
                                                                 style: UIBarButtonItemStyleBordered
                                                                target: self.chartContainer action: @selector(addClicked:)] autorelease];
    UIBarButtonItem *tools = [[[UIBarButtonItem alloc] initWithImage: [UIImage imageNamed:@"53.png"] style: UIBarButtonItemStyleBordered
                                                              target: self.chartContainer action: @selector(toolsClicked:)] autorelease];
    UIBarButtonItem *add = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemAdd
                                                                          target:nil action:nil] autorelease];
    UIBarButtonItem *color = [[[UIBarButtonItem alloc] initWithImage: [UIImage imageNamed:@"174.png"]
                                                               style: UIBarButtonItemStyleBordered
                                                              target:self.chartContainer action: @selector(colorClicked:)] autorelease];
    color.enabled = NO;
    add.style = UIBarButtonItemStyleBordered;
    setting.style = UIBarButtonItemStyleBordered;
    
    self.chartToolItems = [NSArray arrayWithObjects: add, color, flex, screenSelect, flex, setting, tools, periodicity, searchBarItem, nil];
    [self.navBar setItems: self.chartToolItems];
    
    self.listToolItems = [NSArray arrayWithObjects: add, flex, screenSelect, flex, tools, nil];
    
    [Theme stylizeToolbar: self.navBar];
}

- (void) layoutSubviews
{
    NSLog(@"Mainview layoutsubview");
    [super layoutSubviews];
    
    if(self.segmentControl.selectedSegmentIndex == 0)
    {
        self.navBar.frame =  CGRectMake(0,0,self.width,44);
        self.listMiniView.frame = CGRectMake(-LEFT_WIDTH,0, LEFT_WIDTH, self.height);
        self.chartContainer.alpha = 0.0;
    } else
    {
        self.navBar.frame = CGRectMake(LEFT_WIDTH+2,0,self.width - LEFT_WIDTH,44);
        self.listMiniView.frame = CGRectMake(0,0, LEFT_WIDTH, self.height);
        self.chartContainer.alpha = 1.0;
    }
    
    //[self.navBar setNeedsLayout];
    self.chartContainer.frame = CGRectMake(LEFT_WIDTH+2, self.navBar.bottom, self.width - LEFT_WIDTH, self.height - 44.0f);
    self.fullListView.frame = CGRectMake(0, self.navBar.bottom, self.width, self.height - 44.0f);
}

- (void) chartContainerChartChanged: (NSString*) symbol
{
    [self.listMiniView updateSymbol: symbol];
}

- (void) newsViewDidSelect:(NewsData *)news
{
    if(self.newsReader == nil)
    {
        self.newsReader = [[[NewsReaderView alloc] init] autorelease];
        self.newsReader.delegate = self;
        
        CGRect endRect = CGRectMake(LEFT_WIDTH+2,0, self.width -LEFT_WIDTH - 2.0, self.height);
        CGRect startRect = CGRectSetY(endRect, self.height);
        self.newsReader.frame = startRect;
        [self addSubview: self.newsReader];
        
        [UIView beginAnimations:nil context:@"MINE"];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        [UIView setAnimationDuration: .25];
        self.newsReader.frame = endRect;
        [UIView commitAnimations];        
    }
    
    [self.newsReader loadURL: news.url];
}

- (void) newsReaderViewCloseClicked:(id)sender
{
    CGRect endRect = CGRectMake(LEFT_WIDTH+2,0, self.width- LEFT_WIDTH - 2.0, self.height);
    CGRect startRect = CGRectSetY(endRect, self.height);

    [UIView animateWithDuration: 0.25
                     animations:^{
                         self.newsReader.frame = startRect;
                     }
                     completion:^(BOOL finished){
                         [self.newsReader removeFromSuperview];
                         self.newsReader = nil;
                     }];
}

- (void) selectMasterSegment: (int) index animated: (BOOL) animated
{
    self.segmentControl.selectedSegmentIndex = index;
    
    UIView *frontView;
    CGRect navFrame;
    BOOL hideListMiniView;
    CGRect miniFrame;
    
    if(index==0)
    {
        hideListMiniView = YES;
        frontView = self.fullListView;
        navFrame = CGRectMake(0,0,self.width,44);
        miniFrame = CGRectSetX(self.listMiniView.frame, -self.listMiniView.width);
        [self.navBar setItems: self.listToolItems animated: animated];
    }
    else if(index==1)
    {
        hideListMiniView = NO;
        frontView = self.chartContainer;
        navFrame = CGRectMake(LEFT_WIDTH+2,0,self.width - self.listMiniView.width,44);
        miniFrame = CGRectSetX(self.listMiniView.frame, 0);
        [self.navBar setItems: self.chartToolItems animated: animated];
    }
    else
    {
        return;
    }
    
    frontView.hidden = NO;
    frontView.alpha = 1.0;
    
    if(animated)
    {
        [UIView animateWithDuration: 0.35f delay: 0
                            options:(UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionLayoutSubviews | UIViewAnimationOptionAllowAnimatedContent)
                         animations:^{

                             if(self.segmentControl.selectedSegmentIndex == 0)
                             {
                                 self.navBar.frame =  CGRectMake(0,0,self.width,44);
                             } else
                             {
                                 self.navBar.frame = CGRectMake(self.listMiniView.right+2,0,self.width - self.listMiniView.width,44);
                             }
                             
                             self.listMiniView.frame = miniFrame;
                             
                             if(index==1)
                             {
                                 [self bringSubviewToFront: self.listMiniView];
                                 self.fullListView.alpha = 0.0;
                             }
                         }
                         completion:^(BOOL finished) {
                             [self bringSubviewToFront: frontView];
                         }];
    }
    else
    {
        [self setNeedsLayout];
    }
}

- (void) segmentControlValueChanged: (UISegmentedControl*) sender
{
    [self selectMasterSegment: sender.selectedSegmentIndex animated: YES];
}
@end
