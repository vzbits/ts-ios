//
//  MainView.h
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Twitter/Twitter.h>
#import "ChartPanel.h"
#import "SymbolSearchViewController.h"
#import "ServiceManager.h"
#import "AddViewController.h"
#import "ToolsMenuViewController.h"
#import "ColorSelectionTab.h"

@protocol ChartContainerDelegate <NSObject>
- (void) chartContainerChartChanged: (NSString*) symbol;
@end

@interface ChartContainer: UIView <UISearchBarDelegate,
                            UIPopoverControllerDelegate,
                            SymbolSearchViewDelegate,
                            AddViewControllerDelegate,
                            ToolsMenuDelegate,
                            ChartPanelDelegate,
                            ColorSelectionTabDelegate,
                            MFMailComposeViewControllerDelegate>
{
}

@property (strong, nonatomic) UIToolbar *navBar;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) ChartPanel *chartPanel;
@property (strong, nonatomic) UIPopoverController *currentPopover;
@property (strong, nonatomic) UIViewController *popoverContent;

@property (strong, nonatomic) NSTimer *lastSymbolQueried;
@property (strong, nonatomic) NSString *lastSymbolInSearch;
@property (strong, nonatomic) AnnotationGraphics *lastSelectedAnnotation;

@property (assign, nonatomic) id<ChartContainerDelegate> delegate;

- (void)getChartData: (NSString*) symbol;

@end
