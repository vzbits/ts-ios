//
//  ChartHeader.h
//  TSCharts
//
//  Created by Quoc Le on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StockPrice.h"
#import "ChartUtils.h"
#import "ServiceManager.h"

@interface ChartHeader : UIView
@property (strong, nonatomic) UILabel *symbolLabel;
@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *openLabel;
@property (strong, nonatomic) UILabel *openValue;
@property (strong, nonatomic) UILabel *highLabel;
@property (strong, nonatomic) UILabel *highValue;
@property (strong, nonatomic) UILabel *lowLabel;
@property (strong, nonatomic) UILabel *lowValue;
@property (strong, nonatomic) UILabel *closeLabel;
@property (strong, nonatomic) UILabel *closeValue;
@property (strong, nonatomic) UILabel *volumeLabel;
@property (strong, nonatomic) UILabel *volumeValue;
@property (strong, nonatomic) UILabel *changeLabel;
@property (strong, nonatomic) UILabel *changeValue;
@property (strong, nonatomic) UIView *backgroundView;

- (void) setData: (StockPriceData*) sp;
- (void) setPriceData: (PriceData*) p;
+ (NSString*) formatNumber: (float) n;
@end
